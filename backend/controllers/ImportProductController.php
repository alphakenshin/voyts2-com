<?php

namespace backend\controllers;


use Yii;
use app\models\ImportProduct;
use app\models\ImportProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XMLReader;
use DOMDocument;

/**
 * ImportProductController implements the CRUD actions for ImportProduct model.
 */
class ImportProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ImportProduct models.
     * @return mixed
     */
    public function actionIndex()
    {



//    $reader = new XMLReader();
//
//    $doc = new DOMDocument();
//
//    $reader->open('http://voyts-admin.loc/2.xml'); // указываем ридеру что будем парсить этот файл
//    // циклическое чтение документа
//    while($reader->read()) {
//        $data = array();
//        if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'offer') {
//
//// если находим элемент <card>
//            $node = simplexml_import_dom($doc->importNode($reader->expand(), true));
//
////            if ($reader->localName == 'offer') {
//
//
//
//                // считываем аттрибут number
////            $data['id'] = $reader->getAttribute('id');
////            $data['available'] = $reader->getAttribute('available');
//
//
//            // v2
////            $data['id'] = $node->id;
////            $data['available'] = $node->available;
//if(isset($node->url)) $data['url'] = $node->url;
//            $data['price'] = $node->price;
//            $data['currencyId'] = $node->currencyId;
//            $data['categoryId'] = $node->categoryId;
//            $data['picture'] = $node->picture;
//            $data['delivery'] = $node->delivery;
//            $data['name'] = $node->name;
//            $data['vendor'] = $node->vendor;
//            $data['vendorCode'] = $node->vendorCode;
//            $data['country_of_origin'] = $node->country_of_origin;
//            $data['barcode'] = $node->barcode;
//            $data['sales_notes'] = $node->sales_notes;
//            $data['param_1'] = $node->param[0];
//            $data['param_2'] = $node->param[1];
//            $data['param_3'] = $node->param[2];
//            $data['param_4'] = $node->param[3];
//            $data['param_5'] = $node->param[4];
//            $data['param_6'] = $node->param[5];
//            $data['param_7'] = $node->param[6];
//            $data['param_8'] = $node->param[7];
//            $data['param_9'] = $node->param[8];
//            $data['param_10'] = $node->param[9];
//            $data['param_11'] = $node->param[10];
//            $data['param_12'] = $node->param[11];
//            if(isset($node->param[12])) $data['param_13'] = $node->param[12];
//            if(isset($node->param[13])) $data['param_14'] = $node->param[13];
//            if(isset($node->param[14])) $data['param_15'] = $node->param[14];
//            if(isset($node->param[15])) $data['param_16'] = $node->param[15];
//            if(isset($node->param[16])) $data['param_17'] = $node->param[16];
//            if(isset($node->param[17])) $data['param_18'] = $node->param[17];
//
////            $arr = json_decode( json_encode($data['param']) , 1);
////            $arr['picture'] = json_decode( json_encode($data['picture']) , 1);
//            echo '<pre>' . print_r($data, true) . '</pre>';
////
////            $dbInsertion = serialize($arr);
//
//
//            foreach($data as $key => $simpleXml) {
//                $result['id'] = $reader->getAttribute('id');
//                $result['available'] = $reader->getAttribute('available');
////                $result['picture'] = json_decode( json_encode($data['picture']) , 1);
//                $result[$key] = $simpleXml->asXML();
////                $result['param'] = json_decode( json_encode($data['param']) , 1);
//            }
//
//
//
//            echo '<pre>' . print_r($result, true) . '</pre>';
//
//
////            $data['param_name'] = $node->param->value;
////            $data['param'] = $node->param2;
////            $data['name2'] = $reader->getAttribute('name');
////            $data['param name'] = $reader->value;
//
////            function xml2array ( $data, $out = array () )
////            {
////                foreach ( (array) $data as $index => $node )
////                    $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
////                echo '<pre>' . print_r($out, true) . '</pre>';
////                return $out;
////            }
//
////
//////                 читаем дальше для получения текстового элемента
//////                $reader->read();
//////                if($reader->nodeType == XMLReader::TEXT) {
////////                    $data['available'] = $reader->getAttribute('available');
//////                    $data['available'] = $reader->value;
//////                }
////                $reader->read();
////                if ($reader->localName == 'url') {
//////                    $reader->read();
//////                    if ($reader->nodeType == XMLReader::TEXT) {
////                    $data['url'] = $reader->value;
//////                    }
////                }
////            $reader->read();
////                if ($reader->localName == 'price') {
//////                    $reader->read();
//////                    if ($reader->nodeType == XMLReader::TEXT) {
////                    $data['price'] = $reader->value;
//////                    }
////                }
//
//
////        foreach($data as $key => $simpleXml) {
////            $result[$key] = $simpleXml->asXML();
////        }
//                // ну и запихиваем в бд, используя методы нашего адаптера к субд
//                //            SomeDataBaseAdapter::insertContact($data);
//            }
//        $result = array();
//
////        foreach($data as $key => $simpleXml) {
////            $result[$key] = $simpleXml->asXML();
////        }
//
////        echo '<pre>' . print_r($result, true) . '</pre>';
//    }
//        $reader->close();


        $searchModel = new ImportProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'
//            , 'data'
        ));
    }

    /**
     * Displays a single ImportProduct model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ImportProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ImportProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ImportProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ImportProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ImportProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ImportProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ImportProduct::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
