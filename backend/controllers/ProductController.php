<?php

namespace backend\controllers;

use backend\models\Order;
use Yii;
use common\models\Product;
use backend\models\OffersAllImg;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Intervention\Image\ImageManager;
use yii\data\Pagination;



/**
 * OffersAllController implements the CRUD actions for OffersAll model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OffersAll models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
            'pagination' => [
                'pageSize' => 100
            ],
            'sort' => [

            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionAll()
    {


//
        /*** Работает ***/

        $posts_olla = Yii::$app->db->createCommand('SELECT
`sku`, `param_id`, `available`, `url`, `price`, `old_price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `brand_name`, `brand_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`, `sizes_6`, `sizes_6_id`
FROM `offers_olla_group`')
            ->queryAll();

        $posts_karree = Yii::$app->db->createCommand('SELECT
`sku`, `param_id`, `available`, `url`, `price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type_name`, `product_type_id`, `silhouette_name`, `silhouette_id`, `cloth_name`, `cloth_id`, `color_name`, `color_id`, `sort_color_name`, `sort_color_id`,  `print_name`, `print_id`, `style_name`, `style_id`, `season_name`, `season_id`, `for_full`, `product_length_name`, `product_length_id`, `sleeve_name`, `sleeve_id`, `cutout_collar_name`, `cutout_collar_id`, `additional_details_name`, `additional_details_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`, `sizes_6`, `sizes_6_id`
FROM `offers_karree_group`')
            ->queryAll();
//
        $posts_issaplus = Yii::$app->db->createCommand('SELECT
`sku`,  `param_id`, `available`,`url`, `price`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `collection_name`, `collection_id`, `color_name`, `color_id`, `sort_color_name`, `sort_color_id`,  `style_name`, `style_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`
FROM `offers_issaplus`')
            ->queryAll();
//
        $posts_glem = Yii::$app->db->createCommand('SELECT
`sku`, `param_id`, `available`, `url`, `price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `store`, `product_type_name`, `product_type_id`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `barcode`, `sales_notes`, `color_name`, `color_id`, `sort_color_name`, `sort_color_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`, `sizes_6`, `sizes_6_id`, `sizes_7`, `sizes_7_id`, `sizes_8`, `sizes_8_id`
FROM `offers_glem`')
            ->queryAll();
////
        $posts_garda = Yii::$app->db->createCommand('SELECT
`sku`,  `param_id`, `available`, `url`, `price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type_name`, `product_type_id`, `sales_notes`, `composition_name`, `composition_id`, `silhouette_name`, `silhouette_id`, `cloth_name`, `cloth_id`, `color_name`, `color_id`, `sort_color_name`, `sort_color_id`,  `print_name`, `print_id`, `style_name`, `style_id`, `season_name`, `season_id`, `brand_name`, `brand_id`, `product_length_name`, `product_length_id`, `sleeve_name`, `sleeve_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`, `sizes_6`, `sizes_6_id`
FROM `offers_garda_group`')
            ->queryAll();

//  '<pre>' . print_r($posts_garda) . '<pre>';

        /*** ----------- ***/


        /*** Работает ***/

        Yii::$app->db->createCommand()->batchInsert('product', [
            'sku', 'param_id', 'available', 'url', 'price', 'old_price', 'currency_id', 'category_name', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'delivery', 'model', 'vendor_code', 'description', 'country_of_origin', 'barcode', 'sales_notes', 'brand_name', 'brand_id', 'sizes_1', 'sizes_1_id', 'sizes_2', 'sizes_2_id', 'sizes_3', 'sizes_3_id', 'sizes_4', 'sizes_4_id', 'sizes_5', 'sizes_5_id', 'sizes_6', 'sizes_6_id'],
            $posts_olla)->execute();


        Yii::$app->db->createCommand()->batchInsert('product', [
            'sku', 'param_id', 'available', 'url', 'price', 'currency_id', 'category_name', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'delivery', 'model', 'vendor', 'vendor_code', 'description', 'country_of_origin', 'barcode', 'sales_notes', 'product_type_name', 'product_type_id', 'silhouette_name', 'silhouette_id', 'cloth_name', 'cloth_id', 'color_name', 'color_id', 'sort_color_name', 'sort_color_id', 'print_name', 'print_id', 'style_name', 'style_id', 'season_name', 'season_id', 'for_full', 'product_length_name', 'product_length_id', 'sleeve_name', 'sleeve_id', 'cutout_collar_name', 'cutout_collar_id', 'additional_details_name', 'additional_details_id', 'sizes_1', 'sizes_1_id', 'sizes_2', 'sizes_2_id', 'sizes_3', 'sizes_3_id', 'sizes_4', 'sizes_4_id', 'sizes_5', 'sizes_5_id', 'sizes_6', 'sizes_6_id'
        ],
            $posts_karree)->execute();
//
        Yii::$app->db->createCommand()->batchInsert('product', [
            'sku', 'param_id', 'available', 'url', 'price', 'category_name', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'collection_name', 'collection_id', 'color_name', 'color_id', 'sort_color_name', 'sort_color_id', 'style_name', 'style_id', 'sizes_1', 'sizes_1_id', 'sizes_2', 'sizes_2_id', 'sizes_3', 'sizes_3_id', 'sizes_4', 'sizes_4_id', 'sizes_5', 'sizes_5_id'],
            $posts_issaplus)->execute();
//

        Yii::$app->db->createCommand()->batchInsert('product', [
            'sku', 'param_id', 'available', 'url', 'price', 'currency_id', 'category_name', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'store', 'product_type_name', 'product_type_id', 'model', 'vendor', 'vendor_code', 'description', 'manufacturer_warranty', 'country_of_origin', 'barcode', 'sales_notes', 'color_name', 'color_id', 'sort_color_name', 'sort_color_id', 'sizes_1', 'sizes_1_id', 'sizes_2', 'sizes_2_id', 'sizes_3', 'sizes_3_id', 'sizes_4', 'sizes_4_id', 'sizes_5', 'sizes_5_id', 'sizes_6', 'sizes_6_id', 'sizes_7', 'sizes_7_id', 'sizes_8', 'sizes_8_id'],
            $posts_glem)->execute();
////
        Yii::$app->db->createCommand()->batchInsert('product', [
            'sku', 'param_id', 'available', 'url', 'price', 'currency_id', 'category_name', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'vendor_code', 'description', 'country_of_origin', 'product_type_name', 'product_type_id', 'sales_notes', 'composition_name', 'composition_id', 'silhouette_name', 'silhouette_id', 'cloth_name', 'cloth_id', 'color_name', 'color_id', 'sort_color_name', 'sort_color_id', 'print_name', 'print_id', 'style_name', 'style_id', 'season_name', 'season_id', 'brand_name', 'brand_id', 'product_length_name', 'product_length_id', 'sleeve_name', 'sleeve_id', 'sizes_1', 'sizes_1_id', 'sizes_2', 'sizes_2_id', 'sizes_3', 'sizes_3_id', 'sizes_4', 'sizes_4_id', 'sizes_5', 'sizes_5_id', 'sizes_6', 'sizes_6_id'],
            $posts_garda)->execute();

        /*** ----------- ***/


        // Дальше повторяем цикл (XMLReader ищет cследующий <offer>


        // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.

        return $this->render('all');

    }


    /**
     * Displays a single OffersAll model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OffersAll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Товар {$model->model} добавлен");

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OffersAll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed1
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Товар {$model->model} обновлен");


            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OffersAll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionImage()
    {
        $command = Yii::$app->db->createCommand('SELECT MAX(`id`) FROM `product`')->queryOne();
        $maxIndex = $command['MAX(`id`)'];
        $i = 1;
        while ($i <= $maxIndex) {


            $command2 = Yii::$app->db->createCommand('SELECT  `product`.`id`, `product`.`sku`, `offers_all_img`.`main_img`, `offers_all_img`.`gallery_1`, `offers_all_img`.`gallery_2`, `offers_all_img`.`gallery_3`, `offers_all_img`.`gallery_4`, `offers_all_img`.`gallery_5`
FROM `product`, `offers_all_img` WHERE `product`.`sku` = `offers_all_img`.`sku` && `product`.`id` =:id')
                ->bindParam(':id', $i);

            $post = $command2->queryOne();

            $values[$i] = $post;
            $id = $values[$i]['id'];
            $mainImg = $values[$i]['main_img'];
            $gallery1 = $values[$i]['gallery_1'];
            $gallery2 = $values[$i]['gallery_2'];
            $gallery3 = $values[$i]['gallery_3'];
            $gallery4 = $values[$i]['gallery_4'];
            $gallery5 = $values[$i]['gallery_5'];
//
////
//            echo $gallery1 . '<br>';
//            echo '<pre>' . print_r($gallery1) . '</pre>';
//////
           if ($id > 0) {

            $command2 = Yii::$app->db->createCommand('UPDATE `product` SET `main_img` =:main_img, `gallery_1` =:gallery_1, `gallery_2` =:gallery_2, `gallery_3` =:gallery_3, `gallery_4` =:gallery_4, `gallery_5` =:gallery_5 WHERE id=:id'
            )
                ->bindParam(':main_img', $mainImg)
                ->bindParam(':gallery_1', $gallery1)
                ->bindParam(':gallery_2', $gallery2)
                ->bindParam(':gallery_3', $gallery3)
                ->bindParam(':gallery_4', $gallery4)
                ->bindParam(':gallery_5', $gallery5)
                ->bindParam(':id', $id);
            $command2->execute();
        } else {    }
        $i++;

            unset($mainImg);
            unset($gallery1);
            unset($gallery2);
            unset($gallery3);
            unset($gallery4);
            unset($gallery5);
            unset($post);
            unset($values[$i]);


        }
}
    public function actionUpload()
    {

        $i = 8015; $gg = 1;
        $command = Yii::$app->db->createCommand('SELECT MAX(`id`) FROM `product`')->queryOne();
        $maxIndex =$command['MAX(`id`)'];
        $sku = '';
        $filePathArr = [];
        $imgPathArr = [];
        while ($i<=$maxIndex) {
            $command = Yii::$app->db->createCommand('SELECT `id`, `sku`, `gallery_1`,`gallery_2`,`gallery_3`,`gallery_4`,`gallery_5` FROM `product` WHERE `id`=:id')
                ->bindParam(':id', $i);

            $post = $command->queryOne();
            if($post['sku'] != NULL) $sku = $post['sku'];

//            echo '<pre>' . print_r($post, true) . '</pre>';

            if (!file_exists(__DIR__ . '/images/original/OffersAlls-' . $sku . '/')) mkdir(__DIR__ . '/images/original/OffersAlls-' . $sku . '/', 0777, true);
            if (!file_exists(__DIR__ . '/images/900x600/OffersAlls-' . $sku . '/')) mkdir(__DIR__ . '/images/900x600/OffersAlls-' . $sku . '/', 0777, true);
            if (!file_exists(__DIR__ . '/images/450x300/OffersAlls-' . $sku . '/')) mkdir(__DIR__ . '/images/450x300/OffersAlls-' . $sku . '/', 0777, true);
            if (!file_exists(__DIR__ . '/images/165x110/OffersAlls-' . $sku . '/')) mkdir(__DIR__ . '/images/165x110/OffersAlls-' . $sku . '/', 0777, true);


            $y = 1; $z=0;


                if($post['gallery_1'] != NULL) $z=1;
                if($post['gallery_2'] != NULL) $z=2;
                if($post['gallery_3'] != NULL) $z=3;
                if($post['gallery_4'] != NULL) $z=4;
                if($post['gallery_5'] != NULL) $z=5;

            $filePathArr = [];
                while ($y<$z+1) {
//            $url = preg_replace("/^https:/i", "http:", $url);
                    $url = $post['gallery_' . $y];
                // Каким-то образом получим ссылку

//                echo  '/images/165x110/OffersAlls-' . $sku . ':' . $gg . ' ' . $url . '<br>';


                // Проверим HTTP в адресе ссылки
                if (!preg_match("/^https?:/i", $url) && filter_var($url, FILTER_VALIDATE_URL)) {
                    die('Укажите корректную ссылку на удалённый файл.');
                }

                // Запустим cURL с нашей ссылкой
                $ch = curl_init($url);

                // Укажем настройки для cURL
                curl_setopt_array($ch, [

                    // Укажем максимальное время работы cURL
                    CURLOPT_TIMEOUT => 216000,

                    // Разрешим следовать перенаправлениям
                    CURLOPT_FOLLOWLOCATION => 1,

                    // Разрешим результат писать в переменную
                    CURLOPT_RETURNTRANSFER => 1,

                    // Включим индикатор загрузки данных
                    CURLOPT_NOPROGRESS => 0,

                    // Укажем размер буфера 1 Кбайт
                    CURLOPT_BUFFERSIZE => 1073741824,

                    // Напишем функцию для подсчёта скачанных данных
                    // Подробнее: http://stackoverflow.com/a/17642638
                    CURLOPT_PROGRESSFUNCTION => function ($ch, $dwnldSize, $dwnld, $upldSize, $upld) {

                        // Когда будет скачано больше 8 Гбайт, cURL прервёт работу
                        if ($dwnld > 1024 * 1024 * 1024 * 16) {
                            return -1;
                        }
                    },

                    // Включим проверку сертификата (по умолчанию)
//                CURLOPT_SSL_VERIFYPEER => 1,
//
//                // Проверим имя сертификата и его совпадение с указанным хостом (по умолчанию)
//                CURLOPT_SSL_VERIFYHOST => 2,
//
//                // Укажем сертификат проверки
//                // Скачать: https://curl.haxx.se/docs/caextract.html
//                CURLOPT_CAINFO => __DIR__ . '/cacert.pem',
                ]);

                $raw = curl_exec($ch);    // Скачаем данные в переменную
                $info = curl_getinfo($ch); // Получим информацию об операции
                $error = curl_errno($ch);   // Запишем код последней ошибки
                curl_close($ch);

                // Проверим ошибки cURL и доступность файла
                if ($error === CURLE_OPERATION_TIMEDOUT) die('Превышен лимит ожидания.');
                if ($error === CURLE_ABORTED_BY_CALLBACK) die('Размер не должен превышать 5 Мбайт.');
                if ($info['http_code'] === 200)  {

                // Создадим ресурс FileInfo
                $fi = finfo_open(FILEINFO_MIME_TYPE);

                // Получим MIME-тип используя содержимое $raw
                $mime = (string)finfo_buffer($fi, $raw);

                // Закроем ресурс FileInfo
                finfo_close($fi);

                // Проверим ключевое слово image (image/jpeg, image/png и т. д.)
                if (strpos($mime, 'image') === false) die('Можно загружать только изображения.');

                // Возьмём данные изображения из его содержимого
                $image = getimagesizefromstring($raw);

                // Зададим ограничения для картинок
                $limitWidth = 8000;
                $limitHeight = 8000;

                // Проверим нужные параметры
                if ($image[1] > $limitHeight) die('Высота изображения не должна превышать 8000 точек.');
                if ($image[0] > $limitWidth) die('Ширина изображения не должна превышать 8000 точек.');

                // Сгенерируем новое имя из MD5-хеша изображения
                $name = md5($raw);

                // Сгенерируем расширение файла на основе типа картинки
                $extension = image_type_to_extension($image[2]);

                // Сократим .jpeg до .jpg
                $format = str_replace('jpeg', 'jpg', $extension);

                // Сохраним картинку с новым именем и расширением в папку /pics



                if ((!file_put_contents(__DIR__ . '/images/original/OffersAlls-' . $sku . '/' . $name . $format, $raw)) || (!file_put_contents(__DIR__ . '/images/450x300/OffersAlls-' . $sku . '/' . $name . $format, $raw)) || (!file_put_contents(__DIR__ . '/images/900x600/OffersAlls-' . $sku . '/' . $name . $format, $raw)) || (!file_put_contents(__DIR__ . '/images/165x110/OffersAlls-' . $sku . '/' . $name . $format, $raw))) {
                    die('При сохранении изображения на диск произошла ошибка.');
                }   if ($info['http_code'] !== 200) {   }

//                    echo  'OffersAlls-' . $sku . ':' . $gg . ' ' . $url . '<br>';

                    $filePathArr[$y] =__DIR__ . '/images/original/OffersAlls-' . $sku . '/' . $name . $format;
                    $imgPathArr[$y] =  '/OffersAlls-' . $sku . '/' . $name . $format;
//                    echo '/OffersAlls-' . $sku . '/' . $name . $format . '<br>';

                    $manager = new ImageManager(array('driver' => 'imagick'));
                    $filePath = $filePathArr[$y];
                    $img1 = $manager->make($filePath)->resize(600,900, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();

                    })->encode('jpg', 35)->save(__DIR__ . '/images/900x600/OffersAlls-' . $sku . '/' . $name . $format);

                    $img2 = $manager->make($filePath)->resize(300,450, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->encode('jpg', 35)->save(__DIR__ . '/images/450x300/OffersAlls-' . $sku . '/' . $name . $format);

                    $img3 = $manager->make($filePath)->resize(110,165, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->encode('jpg', 35)->save(__DIR__ . '/images/165x110/OffersAlls-' . $sku . '/' . $name . $format);
//                    $img = \Intervention\Image\Image::make('filePath')->resize(300, 200);
//                    $img->save('bar.jpg', 60);
///
                    $values[$i] = $post;
                    $u = $values[$i]['id'];

                                            echo '<pre>' . print_r($post) . '</pre>';

                    if(isset($imgPathArr[1]))  $mainImg = $imgPathArr[1];
                    if(isset($imgPathArr[1])) $gallery1 = $imgPathArr[1];
                    if(isset($imgPathArr[2])) $gallery2 = $imgPathArr[2];
                    if(isset($imgPathArr[3])) $gallery3 = $imgPathArr[3];
                    if(isset($imgPathArr[4])) $gallery4 = $imgPathArr[4];
                    if(isset($imgPathArr[5])) $gallery5 = $imgPathArr[5];

                    if ($u > 0) {
                        $command2 = Yii::$app->db->createCommand('UPDATE `product` SET `main_img` =:main_img, `gallery_1` =:gallery_1, `gallery_2` =:gallery_2, `gallery_3` =:gallery_3, `gallery_4` =:gallery_4, `gallery_5` =:gallery_5  WHERE id=:id'
                        )
                            ->bindParam(':main_img', $mainImg)
                            ->bindParam(':gallery_1', $gallery1)
                            ->bindParam(':gallery_2', $gallery2)
                            ->bindParam(':gallery_3', $gallery3)
                            ->bindParam(':gallery_4', $gallery4)
                            ->bindParam(':gallery_5', $gallery5)
                            ->bindParam(':id', $id);
                        $command2->execute();
                    } else {                    }
                }
                      unset($mainImg);
                      unset($gallery1);
                      unset($gallery2);
                      unset($gallery3);
                      unset($gallery4);
                      unset($gallery5);
//                      unset($values[$i]);

                    $y++; $gg++;

        }


            $offers_all_img = new OffersAllImg();

            if(isset($imgPathArr[1])) {
//                $offers_all_img->id = $i;
                $offers_all_img->sku = $sku;
                $offers_all_img->main_img = $imgPathArr[1];
                $offers_all_img->gallery_1 = $imgPathArr[1];
            }

            if(isset($imgPathArr[2])) $offers_all_img->gallery_2 =  $imgPathArr[2];
            if(isset($imgPathArr[3])) $offers_all_img->gallery_3 =  $imgPathArr[3];
            if(isset($imgPathArr[4])) $offers_all_img->gallery_4 =  $imgPathArr[4];
            if(isset($imgPathArr[5])) $offers_all_img->gallery_5 =  $imgPathArr[5];

            $offers_all_img->save();

            unset($sku);
            unset($imgPathArr[1]);
            unset($imgPathArr[2]);
            unset($imgPathArr[3]);
            unset($imgPathArr[4]);
            unset($imgPathArr[5]);
//            unset($values[$i]);


            $i++;
    }
            return $this->render('upload');
}
    /**
     * Finds the OffersAll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersAll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionSphinx()
    {
        return $this->render('sphinx');
    }
}
