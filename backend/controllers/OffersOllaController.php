<?php

namespace backend\controllers;

use backend\models\Order;
use Yii;
use backend\models\CategoriesOlla;
use backend\models\OffersOlla;
use backend\models\OffersOllaGroup;
use backend\models\ImportCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XMLReader;

/**
 * OffersollaController implements the CRUD actions for Offersolla model.
 */
class OffersOllaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Offersolla models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OffersOlla::find(),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => [

            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Offersolla model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Offersolla model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OffersOlla();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Offersolla model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Offersolla model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Offersolla model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersOlla the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionImport()
    {
        $reader = new XMLReader();

        $y= 1;

        $reader->open('http://voyts-admin.loc/xml/3.xml'); // указываем ридеру что будем парсить этот файл
        // циклическое чтение документа
        while($reader->read()) {
            $xml = array();

            // если ридер находит элемент <offer> запускаются события
            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'offer') {

                //  simplexml_import_dom Получает объект класса SimpleXMLElement из узла DOM
                $xml = simplexml_load_string($reader->readOuterXml());
                //            if ($reader->localName == 'offer') {


                // считываем аттрибут number
                // Дальше зная примерную структуру документа внутри узла DOM обращаемя к элементам, сохраняя ключи и значения в массив.


                // v2


            if(isset($xml->id)) $xml['id'] = $xml->id;
            if(isset($xml->available))$xml['available'] = $xml->available;
            if(isset($xml->group_id))$xml['group_id'] = $xml->group_id;
                if (isset($xml->url)) $xml['url'] = $xml->url;
                if (isset($xml->price)) $xml['price'] = $xml->price;
                if (isset($xml->oldPrice)) $xml['oldPrice'] = $xml->oldPrice;
                if (isset($xml->currencyId)) $xml['currencyId'] = $xml->currencyId;
                if (isset($xml->categoryId)) $xml['categoryId'] = $xml->categoryId;


                if (isset($xml->picture)) {
                     $xml['main_img'] = $xml->picture[0];
                    $i=1;
                    foreach ($xml->picture as $value) {

                       $xml['gallery_' . $i] =  $value;
                       $i++;
                }
            }

                if(isset($xml->delivery))            $xml['delivery'] = $xml->delivery;
                if(isset($xml->name))            $xml['model'] = $xml->name;
                if(isset($xml->vendor))            $xml['vendor'] = $xml->vendor;
                if(isset($xml->vendorCode))            $xml['vendorCode'] = $xml->vendorCode;
                if(isset($xml->country_of_origin))            $xml['country_of_origin'] = $xml->country_of_origin;
                if(isset($xml->description))            $xml['description'] = $xml->description;
                if(isset($xml->barcode))            $xml['barcode'] = $xml->barcode;
                if(isset($xml->sales_notes))            $xml['sales_notes'] = $xml->sales_notes;



                if(isset($xml->param)) {

                    $u = 0;
                    foreach ($xml->param as $value) {
                        $name = $xml->param[$u]['name'];
                        $unit = $xml->param[$u]['unit'];
                        $xml[(string)$name] = $xml->param[$u];
                        $u++;
                    }
                }

//                echo '<pre>' . print_r($xml, true) . '</pre>';

                // В результате получаем массив объектов SimpleXMLElement с теми ключами, которые МЫ ему присвоили

                // Дальше массив нужно перебрать чтобы получился масиив заполненный строками а не объектами

                $xml = $xml->attributes();
//                echo '<pre>' . print_r($xml, true) . '</pre>';

                $json = json_encode( $xml );
                $xml_array = json_decode( $json, true );

                $result  =  $xml_array['@attributes'];

                // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
                // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

                $offers_olla = new OffersOlla();
//                                            $offers_olla->sort_id = $y;
                if(isset($result['id'])) $offers_olla->id = $result['id'];
                if(isset($result['available'])) $offers_olla->available = $result['available'];
                if(isset($result['group_id'])) $offers_olla->group_id = $result['group_id'];
                if(isset($result['url'])) $offers_olla->url = $result['url'];
                if(isset($result['price'])) $offers_olla->price = $result['price'];
                if(isset($result['oldPrice'])) $offers_olla->old_price = $result['oldPrice'];
                if(isset($result['currencyId'])) $offers_olla->currency_id = $result['currencyId'];
                if(isset($result['categoryId'])) $offers_olla->category_id = $result['categoryId'];
                if(isset($result['main_img'])) $offers_olla->main_img = $result['main_img'];
                if(isset($result['gallery_1']))   $offers_olla->gallery_1 = $result['gallery_1'];
                if(isset($result['gallery_2']))   $offers_olla->gallery_2 = $result['gallery_2'];
                if(isset($result['gallery_3']))   $offers_olla->gallery_3 = $result['gallery_3'];
                if(isset($result['gallery_4']))   $offers_olla->gallery_4 = $result['gallery_4'];
                if(isset($result['gallery_5']))   $offers_olla->gallery_5 = $result['gallery_5'];
                if(isset($result['delivery'])) $offers_olla->delivery = $result['delivery'];
                if(isset($result['model'])) $offers_olla->model = $result['model'];
                if(isset($result['vendor'])) $offers_olla->brand_name = $result['vendor'];
                if(isset($result['vendorCode'])) $offers_olla->vendor_code = $result['vendorCode'];
                if(isset($result['description'])) $offers_olla->description = $result['description'];
                if(isset($result['barcode'])) $offers_olla->barcode = $result['barcode'];
                if(isset($result['sales_notes'])) $offers_olla->sales_notes = $result['sales_notes'];
                if(isset($result['Вид товара'])) $offers_olla->product_type = $result['Вид товара'];
//                if(isset($result['Силуэт'])) $offers_olla->silhouette = $result['Силуэт'];
                if(isset($result['Размер'])) $offers_olla->sizes = $result['Размер'];
                $offers_olla->source = 'olla';

//                $offers_olla->save();

            echo '<pre>' . print_r($result, true) . '</pre>';

            }
            $y++;
            // Дальше повторяем цикл (XMLReader ищет cследующий <offer>

            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'category') {


                if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');
                if(null !== ($reader->getAttribute('parentId')))  $result['parentId'] = $reader->getAttribute('parentId');
                if(null !== ($reader->getAttribute('seo_h1')))  $result['seo_h1'] = $reader->getAttribute('seo_h1');
                if(null !== ($reader->getAttribute('url')))  $result['url'] = $reader->getAttribute('url');
                if(null !== ($reader->readString('id')))  $result['name'] = $reader->readString('id');

                $result['source'] = 'olla';

//                echo '<pre>' . print_r($result, true) . '</pre>';
//


                // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
                // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

                $categoriesOlla = new CategoriesOlla();
                if(isset($result['id'])) $categoriesOlla->id = $result['id'];
                if(isset($result['parentId'])) $categoriesOlla->parent_id = $result['parentId'];
                if(isset($result['name'])) $categoriesOlla->name = $result['name'];
                if(isset($result['source'])) $categoriesOlla->source = $result['source'];

//                $categoriesOlla->save();



                $importCategory = new ImportCategory();
                if(isset($result['id'])) $importCategory->import_id = $result['id'];
                if(isset($result['parentId'])) $importCategory->parent_id = $result['parentId'];
                if(isset($result['name'])) $importCategory->name = $result['name'];
                if(isset($result['source'])) $importCategory->source = $result['source'];

//                $importCategory->save();


            }

            $y++;



        }
        // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.
        $reader->close();
        return $this->render('import');
    }

    public function actionSphinx()
    {
        return $this->render('sphinx');
    }

    public function actionMagic()
    {


        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_olla`')->queryOne();
        $maxIndex = $command['MAX(`sort_id`)'];

//        echo $maxIndex . '<br>';
        $i = 1;
        $y = 0;
        $u = 0;
        $o = 0;
        while ($i <= $maxIndex) {


            $command7 = Yii::$app->db->createCommand('SELECT `offers_olla`.`sort_id`, `offers_olla`.`id`, `offers_olla`.`available`, `offers_olla`.`group_id`, `offers_olla`.`price`, `offers_olla`.`old_price`, `offers_olla`.`currency_id`, `offers_olla`.`category_name`, `offers_olla`.`category_id`, `offers_olla`.`source`, `offers_olla`.`delivery`, `offers_olla`.`model`, `offers_olla`.`brand_name`, `offers_olla`.`brand_id`, `offers_olla`.`vendor_code`, `offers_olla`.`description`, `offers_olla`.`country_of_origin`, `offers_olla`.`barcode`, `offers_olla`.`sales_notes`, `offers_olla`.`product_type`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`
FROM `offers_olla`, `attr_sizes` WHERE `offers_olla`.`sizes` = `attr_sizes`.`input_name` && `offers_olla`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post4 = $command7->queryOne();

            $values[$i] = $post4;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command8 = Yii::$app->db->createCommand('UPDATE `offers_olla` SET `sizes` =:sizes, `sizes_id` =:sizes_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes', $name_displayed)
                    ->bindParam(':sizes_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command8->execute();
            } else {
            }

            $command9 = Yii::$app->db->createCommand('SELECT `offers_olla`.`sort_id`, `offers_olla`.`id`, `match_table_olla`.`param_id`, `offers_olla`.`available`, `offers_olla`.`group_id`, `offers_olla`.`price`, `offers_olla`.`old_price`, `offers_olla`.`currency_id`, `match_table_olla`.`name_cat`, `match_table_olla`.`catalog_id`, `offers_olla`.`source`, `offers_olla`.`delivery`, `offers_olla`.`model`, `offers_olla`.`brand_name`, `offers_olla`.`brand_id`, `offers_olla`.`vendor_code`, `offers_olla`.`description`, `offers_olla`.`country_of_origin`, `offers_olla`.`barcode`, `offers_olla`.`sales_notes`, `offers_olla`.`product_type`, `offers_olla`.`sizes`, `offers_olla`.`sizes_id`
FROM `offers_olla`, `match_table_olla` WHERE `offers_olla`.`category_id` = `match_table_olla`.`olla_id` && `offers_olla`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post5 = $command9->queryOne();

            $values[$i] = $post5;
            $p = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_cat'];
            $catalog_id = $values[$i]['catalog_id'];
            $param_id = $values[$i]['param_id'];

            if ($p > 0) {
                $command10 = Yii::$app->db->createCommand('UPDATE `offers_olla` SET `param_id` =:param_id, `category_name` =:category_name, `category_id` =:category_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':category_name', $name_displayed)
                    ->bindParam(':category_id', $catalog_id)
                    ->bindParam(':param_id', $param_id)
                    ->bindParam(':sort_id', $p);
                $command10->execute();
            } else {
            }

            $command11 = Yii::$app->db->createCommand('SELECT `offers_olla`.`sort_id`, `offers_olla`.`id`, `offers_olla`.`available`, `offers_olla`.`group_id`, `offers_olla`.`price`, `offers_olla`.`old_price`, `offers_olla`.`currency_id`, `offers_olla`.`category_name`, `offers_olla`.`category_id`, `offers_olla`.`source`, `offers_olla`.`delivery`, `offers_olla`.`model`, `attr_brand`.`name_displayed`, `attr_brand`.`brand_id`, `offers_olla`.`vendor_code`, `offers_olla`.`description`, `offers_olla`.`country_of_origin`, `offers_olla`.`barcode`, `offers_olla`.`sales_notes`, `offers_olla`.`product_type`, `offers_olla`.`sizes`, `offers_olla`.`sizes_id`
FROM `offers_olla`, `attr_brand` WHERE `offers_olla`.`brand_name` = `attr_brand`.`input_name` && `offers_olla`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post6 = $command11->queryOne();

            $values[$i] = $post6;
            $po = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $brand_id = $values[$i]['brand_id'];
//                                echo $name_displayed . '<br>';


            if ($po > 0) {
                $command12 = Yii::$app->db->createCommand('UPDATE `offers_olla` SET `brand_name` =:brand_name, `brand_id` =:brand_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':brand_name', $name_displayed)
                    ->bindParam(':brand_id', $brand_id)
                    ->bindParam(':sort_id', $po);
                $command12->execute();
            } else {
            }
            $i++;
        }
        return $this->render('magic');

    }


    protected function findModel($id)
    {
        if (($model = OffersOlla::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGroup()
    {
        $i = 1; $y = 1;
        $qtr = -1; $count = 0;
        $values = [];
        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_olla`')->queryOne();
        $maxIndex =$command['MAX(`sort_id`)'];
        while ($i<=$maxIndex) {
            $command = Yii::$app->db->createCommand('SELECT 
`sort_id`, `id`, `param_id`, `available`, `group_id`, `url`, `price`, `old_price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `brand_name`, `brand_id`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes`, `sizes_id`FROM `offers_olla` WHERE `sort_id`=:sort_id')
                ->bindParam(':sort_id', $i);

            $post = $command->queryOne();
            $values[$i] = $post;
            $values[0] = 0;

            if($values[$i]['group_id'] === $values[$i-1]['group_id']) {
                $count++;
                $values[$i]['count'] = $count;

            }
            if($values[$i]['group_id'] !== $values[$i-1]['group_id']) {
                $count = 0;
                $y++;
                $values[$i]['count'] = $count;
                $values[$i]['y'] = $y;
            }
            $i++;
        }

        for ($i=1; $i <= $maxIndex;) {

            if ($values[$i]['count'] == 0) {
                $offers_olla_group = new OffersOllaGroup();
//            if(isset($values[$y]['sort_id'])) $offers_olla_group->sort_id = $values[$y]['id'];
                if(isset($values[$y]['param_id'])) $offers_olla_group->param_id = $values[$y]['param_id'];
                if(isset($values[$i]['group_id']))$offers_olla_group->sku = $values[$i]['group_id'] . '-olla-sku';
//                if(isset($values[$i]['group_id']))$offers_olla_group->group_id = $values[$i]['group_id'];
                if(isset($values[$i]['available']))$offers_olla_group->available = $values[$i]['available'];
                if(isset($values[$i]['url']))$offers_olla_group->url = $values[$i]['url'];
                if(isset($values[$i]['price']))$offers_olla_group->price = $values[$i]['price'];
                if(isset($values[$i]['currency_id']))$offers_olla_group->currency_id = $values[$i]['currency_id'];
                if(isset($values[$i]['category_name'])) $offers_olla_group->category_name = $values[$i]['category_name'];
                if(isset($values[$i]['category_id'])) $offers_olla_group->category_id = $values[$i]['category_id'];
                if(isset($values[$i]['main_img']))$offers_olla_group->main_img = $values[$i]['main_img'];
                if(isset($values[$i]['gallery_1']))  $offers_olla_group->gallery_1 = $values[$i]['gallery_1'];
                if(isset($values[$i]['gallery_2']))  $offers_olla_group->gallery_2 = $values[$i]['gallery_2'];
                if(isset($values[$i]['gallery_3']))  $offers_olla_group->gallery_3 = $values[$i]['gallery_3'];
                if(isset($values[$i]['gallery_4']))  $offers_olla_group->gallery_4 = $values[$i]['gallery_4'];
                if(isset($values[$i]['gallery_5']))  $offers_olla_group->gallery_5 = $values[$i]['gallery_5'];
                if(isset($values[$i]['model'])) $offers_olla_group->model = $values[$i]['model'];
                if(isset($values[$i]['vendor_code']))$offers_olla_group->vendor_code = $values[$i]['vendor_code'];
                if(isset($values[$i]['description']))$offers_olla_group->description = $values[$i]['description'];
                if(isset($values[$i]['country_of_origin']))$offers_olla_group->country_of_origin = $values[$i]['country_of_origin'];
                if(isset($values[$i]['sales_notes']))$offers_olla_group->sales_notes = $values[$i]['sales_notes'];
                if(isset($values[$i]['brand_name']))$offers_olla_group->brand_name = $values[$i]['brand_name'];
                if(isset($values[$i]['brand_id']))$offers_olla_group->brand_id = $values[$i]['brand_id'];
                if (isset($values[$i]['sizes']) && $values[$i]['count'] === 0) $offers_olla_group->sizes_1 = $values[$i]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i]['count'] === 0) $offers_olla_group->sizes_1_id = $values[$i]['sizes_id'];
                if (isset($values[$i]['sizes']) && $values[$i + 1]['count'] === 1) $offers_olla_group->sizes_2 = $values[$i+1]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i + 1]['count'] === 1) $offers_olla_group->sizes_2_id = $values[$i+1]['sizes_id'];
                if (isset($values[$i]['sizes']) && $values[$i + 2]['count'] === 2) $offers_olla_group->sizes_3 = $values[$i+2]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i + 2]['count'] === 2) $offers_olla_group->sizes_3_id = $values[$i+2]['sizes_id'];
                if (isset($values[$i]['sizes']) && $values[$i + 3]['count'] === 3) $offers_olla_group->sizes_4 = $values[$i+3]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i + 3]['count'] === 3) $offers_olla_group->sizes_4_id = $values[$i+3]['sizes_id'];
                if (isset($values[$i]['sizes']) && $values[$i + 4]['count'] === 4) $offers_olla_group->sizes_5 = $values[$i+4]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i + 4]['count'] === 4) $offers_olla_group->sizes_5_id = $values[$i+4]['sizes_id'];
                if (isset($values[$i]['sizes']) && $values[$i + 5]['count'] === 5) $offers_olla_group->sizes_6 = $values[$i+5]['sizes'];
                if (isset($values[$i]['sizes_id']) && $values[$i + 5]['count'] === 5) $offers_olla_group->sizes_6_id = $values[$i+5]['sizes_id'];
                $offers_olla_group->source = 'olla';

                $offers_olla_group->save();
            } else {

            }
            $i++;
        }
//


        // Дальше повторяем цикл (XMLReader ищет cследующий <offer>





        // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.

        return $this->render('group');
    }


}
