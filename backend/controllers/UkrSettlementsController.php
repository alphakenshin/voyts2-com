<?php

namespace backend\controllers;

use Yii;
use backend\models\UkrSettlements;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UkrSettlementsController implements the CRUD actions for UkrSettlements model.
 */
class UkrSettlementsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionMagic()
    {

        $command = Yii::$app->db->createCommand('SELECT MAX(`id`) FROM `ukr_settlements`')->queryOne();
        $maxIndex =$command['MAX(`id`)'];

        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {

//            $command1 = Yii::$app->db->createCommand('SELECT
//
//`ukr_settlements`.`id`, `np_areas`.`description`, `np_areas`.`areas_id`, `ukr_settlements`.`region`, `ukr_settlements`.`city`, `ukr_settlements`.`city_id`, `ukr_settlements`.`longitude`, `ukr_settlements`.`latitude`
//FROM `ukr_settlements`, `np_areas` WHERE `ukr_settlements`.`area` = `np_areas`.`description` && `ukr_settlements`.`id` =:id')
//                ->bindParam(':id', $i);
//
//            $post1 = $command1->queryOne();
//
//            $values1[$i] = $post1;
//            $u = $values1[$i]['id'];
//
//            $areas_id = $values1[$i]['areas_id'];
//
//            if ($u > 0) {
//                $command2 = Yii::$app->db->createCommand('UPDATE `ukr_settlements` SET `area_id` =:area_id  WHERE id=:id'
//                )
//                    ->bindParam(':id', $u)
//                    ->bindParam(':area_id', $areas_id);
//
//
//                $command2->execute();
//            } else {
//            }
//
//            $command3 = Yii::$app->db->createCommand('SELECT
//
//`ukr_settlements`.`id`, `ukr_settlements`.`city`, `ukr_settlements`.`area`, `ukr_settlements`.`area_id`, `ukr_settlements`.`region`, `ukr_settlements`.`region_id`, `ukr_settlements`.`longitude`, `ukr_settlements`.`latitude`
//FROM `ukr_settlements` WHERE `ukr_settlements`.`id` =:id')
//                ->bindParam(':id', $i);
//
//            $post1 = $command3->queryOne();
//
//            $values1[$i] = $post1;
//            $u = $values1[$i]['id'];
//            $values1[$i]['city'] = ucfirst((mb_strtolower($values1[$i]['city'])));
//            $values1[$i]['city'] = mb_convert_case($values1[$i]['city'], MB_CASE_TITLE, "UTF-8");
//
//            echo $values1[$i]['city'] . '<br>';
//            if ($u > 0) {
//                $command4 = Yii::$app->db->createCommand('UPDATE `ukr_settlements` SET `city` =:city  WHERE id=:id'
//                )
//                    ->bindParam(':city', $values1[$i]['city'])
//                    ->bindParam(':id', $u);
//
//                $command4->execute();
//            } else {
//            }

            $command5 = Yii::$app->db->createCommand('SELECT

`ukr_settlements`.`id`, `ukr_settlements`.`city`, `ukr_settlements`.`area`, `ukr_settlements`.`area_id`, `ukr_settlements`.`region`, `ukr_regions`.`region_id`, `ukr_settlements`.`longitude`, `ukr_settlements`.`latitude`
FROM `ukr_settlements`, `ukr_regions` WHERE `ukr_settlements`.`region` = `ukr_regions`.`region` && `ukr_settlements`.`id` =:id')
                ->bindParam(':id', $i);

            $post1 = $command5->queryOne();

            $values1[$i] = $post1;
            $u = $values1[$i]['id'];

            $region_id = $values1[$i]['region_id'];

            if ($u > 0) {
                $command6 = Yii::$app->db->createCommand('UPDATE `ukr_settlements` SET `region_id` =:region_id  WHERE id=:id'
                )
                    ->bindParam(':id', $u)
                    ->bindParam(':region_id', $region_id);


                $command6->execute();
            } else {
            }

            $i++;
        }
        return $this->render('magic');
    }


    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => UkrSettlements::find(),
        ]);

        $regions = new UkrSettlements();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'regions' => $regions,
        ]);
    }

    /**
     * Displays a single UkrSettlements model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UkrSettlements model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UkrSettlements();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing UkrSettlements model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UkrSettlements model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UkrSettlements model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UkrSettlements the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UkrSettlements::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
