<?php

namespace backend\controllers;

use LisDev\Delivery\NovaPoshtaApi2;
use Yii;
use backend\models\NpWarehouses;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NpWarehousesController implements the CRUD actions for NpWarehouses model.
 */
class NpWarehousesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionImport()
    {
        $np = new NovaPoshtaApi2('9d68a93f6bcaf2bdd6e6d92c61abd22d');

//        $res = $np->documentsTracking('20450197975131');

        $result = $np
            ->model('Address')
            ->method('getWarehouses')
            ->params(array(
                'Имя_параметра_1' => 'Значение_параметра_1',
                'Имя_параметра_2' => 'Значение_параметра_2',
            ))
            ->execute();

        $i=-1;

//        $json = json_encode($result['data']);
//        $res = json_decode( $json, true );

        foreach ($result['data'] as $res) {
            $i++;

            $area = trim(preg_replace("/([^А-я]*)область(.*)/", "", $res['SettlementAreaDescription']));

            $np_warehouses = new NpWarehouses();
//            if(isset($result['id'])) $np_warehouses->id = $result['id'];
            if(isset($res['SiteKey'])) $np_warehouses->site_key = $res['SiteKey'];
            if(isset($res['Description'])) $np_warehouses->description = $res['Description'];
            if(isset($res['DescriptionRu'])) $np_warehouses->description_ru = $res['DescriptionRu'];
            if(isset($res['ShortAddress'])) $np_warehouses->short_address = $res['ShortAddress'];
            if(isset($res['ShortAddressRu'])) $np_warehouses->short_address_ru = $res['ShortAddressRu'];
            if(isset($res['Phone'])) $np_warehouses->phone = $res['Phone'];
            if(isset($res['TypeOfWarehouse'])) $np_warehouses->type_of_warehouse = $res['TypeOfWarehouse'];
            if(isset($res['Ref']))   $np_warehouses->ref = $res['Ref'];
            if(isset($res['Number']))   $np_warehouses->number = $res['Number'];
            if(isset($res['CityRef']))   $np_warehouses->city_ref = $res['CityRef'];
            if(isset($res['CityDescription']))   $np_warehouses->city_description = $res['CityDescription'];
            if(isset($res['CityDescriptionRu']))   $np_warehouses->city_description_ru = $res['CityDescriptionRu'];
            if(isset($res['SettlementRef'])) $np_warehouses->settlement_ref = $res['SettlementRef'];
            if(isset($res['SettlementDescription'])) $np_warehouses->settlement_description = $res['SettlementDescription'];
            if(isset($res['SettlementAreaDescription'])) $np_warehouses->settlement_area_description = $area;
            if(isset($res['SettlementRegionsDescription'])) $np_warehouses->settlement_regions_description = $res['SettlementRegionsDescription'];
            if(isset($res['SettlementTypeDescription'])) $np_warehouses->settlement_type_description = $res['SettlementTypeDescription'];
            if(isset($res['Longitude'])) $np_warehouses->longitude = $res['Longitude'];
            if(isset($res['Latitude'])) $np_warehouses->latitude = $res['Latitude'];
            if(isset($res['PostFinance'])) $np_warehouses->post_finance = $res['PostFinance'];
            if(isset($res['BicycleParking'])) $np_warehouses->bicycle_parking = $res['BicycleParking'];
            if(isset($res['PaymentAccess'])) $np_warehouses->payment_access = $res['PaymentAccess'];
            if(isset($res['POSTerminal'])) $np_warehouses->pos_terminal = $res['POSTerminal'];
            if(isset($res['InternationalShipping'])) $np_warehouses->international_shipping = $res['InternationalShipping'];
            if(isset($res['TotalMaxWeightAllowed'])) $np_warehouses->total_max_weight_allowed = $res['TotalMaxWeightAllowed'];
            if(isset($res['PlaceMaxWeightAllowed'])) $np_warehouses->place_max_weight_allowed = $res['PlaceMaxWeightAllowed'];
//            if(isset($res['Reception'])) $np_warehouses->reception = $res['Reception'];
//            if(isset($res['Delivery'])) $np_warehouses->delivery = $res['Delivery'];
//            if(isset($res['Schedule'])) $np_warehouses->schedule = $res['Schedule'];
            if(isset($res['DistrictCode'])) $np_warehouses->district_code = $res['DistrictCode'];
            if(isset($res['WarehouseStatus'])) $np_warehouses->warehouse_status = $res['WarehouseStatus'];
            if(isset($res['CategoryOfWarehouse'])) $np_warehouses->category_of_warehouse = $res['CategoryOfWarehouse'];



//                echo '<pre>' . print_r($res, true) . '</pre>';
//echo $area;

            $np_warehouses->save();

        }

        return $this->render('import');
    }

    public function actionMagic()
    {

        $command = Yii::$app->db->createCommand('SELECT MAX(`id`) FROM `np_warehouses`')->queryOne();
        $maxIndex =$command['MAX(`id`)'];

        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {

            $command1 = Yii::$app->db->createCommand('SELECT

`np_warehouses`.`id`, `np_warehouses`.`site_key`, `np_warehouses`.`description`, `np_warehouses`.`description_ru`, `np_warehouses`.`short_address`, `np_warehouses`.`short_address_ru`, `np_warehouses`.`phone`, `np_warehouses`.`type_of_warehouse`, `np_warehouses`.`ref`, `np_warehouses`.`number`, `np_warehouses`.`city_ref`, `np_warehouses`.`cities_id`, `np_warehouses`.`city_description`, `np_warehouses`.`city_description_ru`, `np_warehouses`.`settlement_ref`, `np_warehouses`.`settlement_description`, `np_areas`.`areas_center`, `np_warehouses`.`settlement_area_description`, `np_areas`.`areas_id`, `np_warehouses`.`settlement_regions_description`, `np_warehouses`.`regions_id`, `np_warehouses`.`settlement_type_description`, `np_warehouses`.`longitude`, `np_warehouses`.`latitude`, `np_warehouses`.`post_finance`, `np_warehouses`.`bicycle_parking`, `np_warehouses`.`payment_access`, `np_warehouses`.`pos_terminal`, `np_warehouses`.`international_shipping`, `np_warehouses`.`total_max_weight_allowed`, `np_warehouses`.`place_max_weight_allowed`, `np_warehouses`.`reception`, `np_warehouses`.`delivery`, `np_warehouses`.`schedule`, `np_warehouses`.`district_code`, `np_warehouses`.`warehouse_status`, `np_warehouses`.`category_of_warehouse`
FROM `np_warehouses`, `np_areas` WHERE `np_warehouses`.`settlement_area_description` = `np_areas`.`description` && `np_warehouses`.`id` =:id')
                ->bindParam(':id', $i);

            $post1 = $command1->queryOne();

            $values1[$i] = $post1;
            $u = $values1[$i]['id'];
            $areas_center = $values1[$i]['areas_center'];
            $areas_id = $values1[$i]['areas_id'];

            if ($u > 0) {
                $command2 = Yii::$app->db->createCommand('UPDATE `np_warehouses` SET `settlement_area_ref` =:settlement_area_ref, `areas_id` =:areas_id  WHERE id=:id'
                )
                    ->bindParam(':settlement_area_ref', $areas_center)
                    ->bindParam(':id', $u)
                    ->bindParam(':areas_id', $areas_id);


                $command2->execute();
            } else {
            }

            $command3 = Yii::$app->db->createCommand('SELECT 

`np_warehouses`.`id`, `np_warehouses`.`site_key`, `np_warehouses`.`description`, `np_warehouses`.`description_ru`, `np_warehouses`.`short_address`, `np_warehouses`.`short_address_ru`, `np_warehouses`.`phone`, `np_warehouses`.`type_of_warehouse`, `np_warehouses`.`ref`, `np_warehouses`.`number`, `np_warehouses`.`city_ref`, `np_cities`.`cities_id`, `np_warehouses`.`city_description`, `np_warehouses`.`city_description_ru`, `np_warehouses`.`settlement_ref`, `np_warehouses`.`settlement_description`, `np_warehouses`.`settlement_area_ref`, `np_warehouses`.`settlement_area_description`, `np_warehouses`.`areas_id`, `np_warehouses`.`settlement_regions_description`, `np_warehouses`.`regions_id`, `np_warehouses`.`settlement_type_description`, `np_warehouses`.`longitude`, `np_warehouses`.`latitude`, `np_warehouses`.`post_finance`, `np_warehouses`.`bicycle_parking`, `np_warehouses`.`payment_access`, `np_warehouses`.`pos_terminal`, `np_warehouses`.`international_shipping`, `np_warehouses`.`total_max_weight_allowed`, `np_warehouses`.`place_max_weight_allowed`, `np_warehouses`.`reception`, `np_warehouses`.`delivery`, `np_warehouses`.`schedule`, `np_warehouses`.`district_code`, `np_warehouses`.`warehouse_status`, `np_warehouses`.`category_of_warehouse`
FROM `np_warehouses`, `np_cities` WHERE `np_warehouses`.`city_ref` = `np_cities`.`ref` && `np_warehouses`.`id` =:id')
                ->bindParam(':id', $i);

            $post1 = $command3->queryOne();

            $values1[$i] = $post1;
            $u = $values1[$i]['id'];
            $cities_id = $values1[$i]['cities_id'];

            if ($u > 0) {
                $command4 = Yii::$app->db->createCommand('UPDATE `np_warehouses` SET `cities_id` =:cities_id  WHERE id=:id'
                )
                    ->bindParam(':cities_id', $cities_id)
                    ->bindParam(':id', $u);

                $command4->execute();
            } else {
            }

            $i++;
        }
        return $this->render('magic');
    }
    /**
     * Lists all NpWarehouses models.
     * @return mixed
     */
    public function actionSphinx()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 200;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cities = array();
        $where_areas = array();
        $where_regions = array();


        if (isset($_GET['areas_id'])) {
            $w = implode(',', $_GET['areas_id']);
            $where['areas_id'] = ' areas_id in (' . $w . ') ';
        }

        if (isset($_GET['regions_id'])) {
            $w = implode(',', $_GET['regions_id']);
            $where['regions_id'] = ' regions_id in (' . $w . ') ';
        }

        if (isset($_GET['cities_id'])) {
            $w = implode(',', $_GET['cities_id']);
            $where['cities_id'] = ' cities_id in (' . $w . ') ';
        }

        if (count($where) > 0) {
            $where_cities = $where_areas = $where_regions = $where;

            if (isset($where_areas['areas_id'])) {
                unset($where_areas['areas_id']);
            }
            if (count($where_areas) > 0) {
                $where_areas = ' AND ' . implode(' AND ', $where_areas);
            } else {
                $where_areas = '';
            }

            if (isset($where_regions['regions_id'])) {
                unset($where_regions['regions_id']);
            }
            if (count($where_regions) > 0) {
                $where_regions = ' AND ' . implode(' AND ', $where_regions);
            } else {
                $where_regions = '';
            }
            
            if (isset($where_cities['cities_id'])) {
                unset($where_cities['cities_id']);
            }
            if (count($where_cities) > 0) {
                $where_cities = ' AND ' . implode(' AND ', $where_cities);
            } else {
                $where_cities = '';
            }
 

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cities = $where_areas = $where_regions = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_np_warehouses';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_regions  GROUP BY regions_id  ORDER BY regions_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $regions_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_areas  GROUP BY areas_id  ORDER BY areas_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $areas_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cities  GROUP BY cities_id  ORDER BY cities_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cities_id = $stmt->fetchAll();



        $facets = array();
        foreach ($cities_id as $p) {
            $facets['cities_id'][] = array(
                'value' => $p['cities_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($areas_id as $p) {
            $facets['areas_id'][] = array(
                'value' => $p['areas_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($regions_id as $p) {
            $facets['regions_id'][] = array(
                'value' => $p['regions_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }

        return $this->render('sphinx', compact( 'facets', 'cities_id', 'areas_id', 'regions_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));

    }

    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NpWarehouses::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NpWarehouses model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NpWarehouses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NpWarehouses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NpWarehouses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NpWarehouses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NpWarehouses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NpWarehouses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NpWarehouses::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
