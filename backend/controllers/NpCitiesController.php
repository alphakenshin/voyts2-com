<?php

namespace backend\controllers;

use LisDev\Delivery\NovaPoshtaApi2;
use Yii;
use backend\models\NpCities;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NpCitiesController implements the CRUD actions for NpCities model.
 */
class NpCitiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionImport()
    {
        $np = new NovaPoshtaApi2('9d68a93f6bcaf2bdd6e6d92c61abd22d');

//        $res = $np->documentsTracking('20450197975131');

        $result = $np
            ->model('Address')
            ->method('getCities')
            ->params(array(
                'Имя_параметра_1' => 'Значение_параметра_1',
                'Имя_параметра_2' => 'Значение_параметра_2',
            ))
            ->execute();

        $i=-1;

//        $json = json_encode($result['data']);
//        $res = json_decode( $json, true );
        foreach ($result['data'] as $res) {
            $i++;
            $np_cities = new NpCities();
//            if(isset($result['id'])) $np_cities->id = $result['id'];
            if(isset($res['Description'])) $np_cities->description = $res['Description'];
            if(isset($res['DescriptionRu'])) $np_cities->description_ru = $res['DescriptionRu'];
            if(isset($res['Ref']))   $np_cities->ref = $res['Ref'];
            if(isset($res['Delivery1'])) $np_cities->delivery_1  = $res['Delivery1'];
            if(isset($res['Delivery2'])) $np_cities->delivery_2  = $res['Delivery2'];
            if(isset($res['Delivery3'])) $np_cities->delivery_3  = $res['Delivery3'];
            if(isset($res['Delivery4'])) $np_cities->delivery_4  = $res['Delivery4'];
            if(isset($res['Delivery5'])) $np_cities->delivery_5  = $res['Delivery5'];
            if(isset($res['Delivery6'])) $np_cities->delivery_6  = $res['Delivery6'];
            if(isset($res['Delivery7'])) $np_cities->delivery_7  = $res['Delivery7'];
            if(isset($res['Area'])) $np_cities->area = $res['Area'];
            if(isset($res['SettlementType'])) $np_cities->settlement_type = $res['SettlementType'];
            if(isset($res['IsBranch'])) $np_cities->is_branch  = $res['IsBranch'];
            if(isset($res['PreventEntryNewStreetsUser'])) $np_cities->prevent_entry_new_streets_user = $res['PreventEntryNewStreetsUser'];
//            if(isset($res['Conglomerates'])) $np_cities->conglomerates = $res['Conglomerates'];
            if(isset($res['CityID'])) $np_cities->city_id = $res['CityID'];
            if(isset($res['SettlementTypeDescription'])) $np_cities->settlement_type_description = $res['SettlementTypeDescription'];
            if(isset($res['SettlementTypeDescriptionRu'])) $np_cities->settlement_type_description_ru = $res['SettlementTypeDescriptionRu'];
            if(isset($res['SpecialCashCheck'])) $np_cities->special_cash_check  = $res['SpecialCashCheck'];


            echo '<pre>' . print_r($res, true) . '</pre>';


            $np_cities->save();

        }

        return $this->render('import', compact('res'));
    }
    /**
     * Lists all NpCities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NpCities::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NpCities model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NpCities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NpCities();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NpCities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NpCities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NpCities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NpCities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NpCities::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
