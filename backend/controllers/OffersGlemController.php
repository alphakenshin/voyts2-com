<?php

namespace backend\controllers;

use backend\models\Order;
use Yii;
use backend\models\OffersGlem;
use backend\models\CategoriesGlem;
use backend\models\ImportCategory;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XMLReader;
/**
 * OffersGlemController implements the CRUD actions for OffersGlem model.
 */
class OffersGlemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OffersGlem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OffersGlem::find(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [

            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OffersGlem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OffersGlem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OffersGlem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OffersGlem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OffersGlem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionImport()
    {
        $reader = new XMLReader();


        $reader->open('http://voyts-admin.loc/xml/4.xml'); // указываем ридеру что будем парсить этот файл
        // циклическое чтение документа
        while($reader->read()) {
            $xml = array();
            // если ридер находит элемент <offer> запускаются события
            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'offer') {

                //  simplexml_import_dom Получает объект класса SimpleXMLElement из узла DOM
                $xml = simplexml_load_string($reader->readOuterXml());

                //            if ($reader->localName == 'offer') {


                // считываем аттрибут number
                // Дальше зная примерную структуру документа внутри узла DOM обращаемя к элементам, сохраняя ключи и значения в массив.


                // v2


            if(isset($xml->id)) $xml['id'] = $xml->id;
            if(isset($xml->available))$xml['available'] = $xml->available;

                if (isset($xml->url)) $xml['url'] = $xml->url;
                if (isset($xml->price)) $xml['price'] = $xml->price;
                if (isset($xml->currencyId)) $xml['currencyId'] = $xml->currencyId;
                if (isset($xml->sale)) $xml['sale'] = $xml->sale;
                if (isset($xml->categoryId)) $xml['categoryId'] = $xml->categoryId;
                if(isset($xml->avail['quantity'])) $xml['quantity'] = $xml->avail['quantity'];
                if(isset($xml->avail['quantity'])) {
//                $xml['size_1_quantity']= $xml->avail[0]['quantity'];
                    $u = 0;

                    foreach($xml->avail->children() as $size) {
                        $name = $size;
                        $xml[(string)$name] = $size->attributes()['quantity'];
                        $u++;
                    }
                    unset($u);
                }
                $i=1;
                foreach ($xml->picture as $value) {
                    $xml['main_img'] = $xml->picture[0];

                    $xml['gallery_' . $i] = $value;
                    $i++;
                }
                unset($i);

                if(isset($xml->store))            $xml['delivery'] = $xml->store;
                if(isset($xml->delivery))            $xml['delivery'] = $xml->delivery;
                if(isset($xml->pref))            $xml['pref'] = $xml->pref;
                if(isset($xml->vendor))            $xml['vendor'] = $xml->vendor;
                if(isset($xml->vendorCode))            $xml['vendorCode'] = $xml->vendorCode;
                if(isset($xml->model))            $xml['model'] = $xml->model;
                if(isset($xml->description))            $xml['description'] = $xml->description;
                if(isset($xml->manufacturer_warranty))            $xml['manufacturer_warranty'] = $xml->manufacturer_warranty;

                if(isset($xml->country_of_origin))            $xml['country_of_origin'] = $xml->country_of_origin;
                if(isset($xml->param)) {

                    $z = 0;
                    foreach ($xml->param as $value) {
                        $name = $value['name'];
                        $unit = $value['unit'];
                        $xml[(string)$name] = $value;
                        $z++;

//                        if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');

                    }
                    unset($z);
                }

//                echo '<pre>' . print_r($xml, true) . '</pre>';

                // В результате получаем массив объектов SimpleXMLElement с теми ключами, которые МЫ ему присвоили

                // Дальше массив нужно перебрать чтобы получился масиив заполненный строками а не объектами


                $xml = $xml->attributes();

                $json = json_encode( $xml );
                $xml_array = json_decode( $json, true );

                $result  =  $xml_array['@attributes'];


                if(isset($result['XS']) ) {
                    $sizes_1 = 'XS';
                }
                if(isset($result['S']) && !isset($sizes_1)) {
                    $sizes_1 = 'S';
                }
                if(isset($result['S']) && isset($sizes_1) && $sizes_1 != 'S' ) {
                    $sizes_2 = 'S';
                }
                if(isset($result['M']) && !isset($sizes_1) && !isset($sizes_2)) {
                    $sizes_1 = 'M';
                }
                if(isset($result['M']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'M') {
                    $sizes_2 = 'M';
                }
                 if(isset($result['M']) && isset($sizes_1) && isset($sizes_2) && $sizes_2 != 'M') {
                     $sizes_3 = 'M';
                 }
                if(isset($result['L']) && !isset($sizes_1)) {
                    $sizes_1 = 'L';
                }
                if(isset($result['L']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'L') {
                    $sizes_2 = 'L';
                }
                if(isset($result['L']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != 'L') {
                    $sizes_3 = 'L';
                }
                if(isset($result['L']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != 'L') {
                    $sizes_4 = 'L';
                }
                if(isset($result['XL']) && !isset($sizes_1)) {
                    $sizes_1 = 'XL';
                }
                if(isset($result['XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'XL') {
                    $sizes_2 = 'XL';
                }
                if(isset($result['XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != 'XL') {
                    $sizes_3 = 'XL';
                }
                if(isset($result['XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != 'XL') {
                    $sizes_4 = 'XL';
                }
                if(isset($result['XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != 'XL') {
                    $sizes_5 = 'XL';
                }
                if(isset($result['XXL']) && !isset($sizes_1)) {
                    $sizes_1 = 'XXL';
                }
                if(isset($result['XXL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'XXL') {
                    $sizes_2 = 'XXL';
                }
                if(isset($result['XXL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != 'XXL') {
                    $sizes_3 = 'XXL';
                }
                if(isset($result['XXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != 'XXL') {
                    $sizes_4 = 'XXL';
                }
                if(isset($result['XXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != 'XXL') {
                    $sizes_5 = 'XXL';
                }
                if(isset($result['XXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != 'XXL') {
                    $sizes_6 = 'XXL';
                }
                if(isset($result['XXXL']) && !isset($sizes_1)) {
                    $sizes_1 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'XXXL') {
                    $sizes_2 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != 'XXXL') {
                    $sizes_3 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != 'XXXL') {
                    $sizes_4 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != 'XXXL') {
                    $sizes_5 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != 'XXXL') {
                    $sizes_6 = 'XXXL';
                }
                if(isset($result['XXXL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != 'XXXL') {
                    $sizes_7 = 'XXXL';
                }

//
                if(isset($result['2XL']) && !isset($sizes_1)) {
                    $sizes_1 = '2XL';
                }
                if(isset($result['2XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '2XL') {
                    $sizes_2 = '2XL';
                }
                if(isset($result['2XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '2XL') {
                    $sizes_3 = '2XL';
                }
                if(isset($result['2XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '2XL') {
                    $sizes_4 = '2XL';
                }
                if(isset($result['2XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '2XL') {
                    $sizes_5 = '2XL';
                }
                if(isset($result['2XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '2XL') {
                    $sizes_6 = '2XL';
                }

                if(isset($result['3XL']) && !isset($sizes_1)) {
                    $sizes_1 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '3XL') {
                    $sizes_2 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '3XL') {
                    $sizes_3 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '3XL') {
                    $sizes_4 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '3XL') {
                    $sizes_5 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '3XL') {
                    $sizes_6 = '3XL';
                }
                if(isset($result['3XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '3XL') {
                    $sizes_7 = '3XL';
                }
                if(isset($result['4XL']) && !isset($sizes_1)) {
                    $sizes_1 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '4XL') {
                    $sizes_2 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '4XL') {
                    $sizes_3 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '4XL') {
                    $sizes_4 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '4XL') {
                    $sizes_5 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '4XL') {
                    $sizes_6 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '4XL') {
                    $sizes_7 = '4XL';
                }
                if(isset($result['4XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '4XL') {
                    $sizes_8 = '4XL';
                }
                if(isset($result['5XL']) && !isset($sizes_1)) {
                    $sizes_1 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '5XL') {
                    $sizes_2 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '5XL') {
                    $sizes_3 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '5XL') {
                    $sizes_4 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '5XL') {
                    $sizes_5 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '5XL') {
                    $sizes_6 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '5XL') {
                    $sizes_7 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '5XL') {
                    $sizes_8 = '5XL';
                }
                if(isset($result['5XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '5XL') {
                    $sizes_9 = '5XL';
                }
                if(isset($result['6XL']) && !isset($sizes_1)) {
                    $sizes_1 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '6XL') {
                    $sizes_2 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '6XL') {
                    $sizes_3 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '6XL') {
                    $sizes_4 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '6XL') {
                    $sizes_5 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '6XL') {
                    $sizes_6 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '6XL') {
                    $sizes_7 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '6XL') {
                    $sizes_8 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '6XL') {
                    $sizes_9 = '6XL';
                }
                if(isset($result['6XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '6XL') {
                    $sizes_10 = '6XL';
                }
                if(isset($result['7XL']) && !isset($sizes_1)) {
                    $sizes_1 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '7XL') {
                    $sizes_2 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '7XL') {
                    $sizes_3 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '7XL') {
                    $sizes_4 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '7XL') {
                    $sizes_5 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '7XL') {
                    $sizes_6 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '7XL') {
                    $sizes_7 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '7XL') {
                    $sizes_8 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '7XL') {
                    $sizes_9 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '7XL') {
                    $sizes_10 = '7XL';
                }
                if(isset($result['7XL']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && !isset($sizes_11) && $sizes_9 != '7XL') {
                    $sizes_11 = '7XL';
                }




                if(isset($result['36']) ) {
                    $sizes_1 = '36';
                }
                if(isset($result['38']) && !isset($sizes_1)) {
                    $sizes_1 = '38';
                }
                if(isset($result['38']) && isset($sizes_1) && $sizes_1 != '38' ) {
                    $sizes_2 = '38';
                }
                if(isset($result['40']) && !isset($sizes_1) && !isset($sizes_2)) {
                    $sizes_1 = '40';
                }
                if(isset($result['40']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '40') {
                    $sizes_2 = '40';
                }
                if(isset($result['40']) && isset($sizes_1) && isset($sizes_2) && $sizes_2 != '40') {
                    $sizes_3 = '40';
                }
                if(isset($result['42']) && !isset($sizes_1)) {
                    $sizes_1 = '42';
                }
                if(isset($result['42']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '42') {
                    $sizes_2 = '42';
                }
                if(isset($result['42']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '42') {
                    $sizes_3 = '42';
                }
                if(isset($result['42']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '42') {
                    $sizes_4 = '42';
                }
                if(isset($result['44']) && !isset($sizes_1)) {
                    $sizes_1 = '44';
                }
                if(isset($result['44']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '44') {
                    $sizes_2 = '44';
                }
                if(isset($result['44']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '44') {
                    $sizes_3 = '44';
                }
                if(isset($result['44']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '44') {
                    $sizes_4 = '44';
                }
                if(isset($result['44']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '44') {
                    $sizes_5 = '44';
                }
                if(isset($result['46']) && !isset($sizes_1)) {
                    $sizes_1 = '46';
                }
                if(isset($result['46']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '46') {
                    $sizes_2 = '46';
                }
                if(isset($result['46']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '46') {
                    $sizes_3 = '46';
                }
                if(isset($result['46']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '46') {
                    $sizes_4 = '46';
                }
                if(isset($result['46']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '46') {
                    $sizes_5 = '46';
                }
                if(isset($result['46']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '46') {
                    $sizes_6 = '46';
                }
                if(isset($result['48']) && !isset($sizes_1)) {
                    $sizes_1 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '48') {
                    $sizes_2 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '48') {
                    $sizes_3 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '48') {
                    $sizes_4 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '48') {
                    $sizes_5 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '48') {
                    $sizes_6 = '48';
                }
                if(isset($result['48']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '48') {
                    $sizes_7 = '48';
                }
                if(isset($result['50']) && !isset($sizes_1)) {
                    $sizes_1 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '50') {
                    $sizes_2 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '50') {
                    $sizes_3 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '50') {
                    $sizes_4 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '50') {
                    $sizes_5 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '50') {
                    $sizes_6 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '50') {
                    $sizes_7 = '50';
                }
                if(isset($result['50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '50') {
                    $sizes_8 = '50';
                }
                if(isset($result['52']) && !isset($sizes_1)) {
                    $sizes_1 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '52') {
                    $sizes_2 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '52') {
                    $sizes_3 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '52') {
                    $sizes_4 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '52') {
                    $sizes_5 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '52') {
                    $sizes_6 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '52') {
                    $sizes_7 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '52') {
                    $sizes_8 = '52';
                }
                if(isset($result['52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '52') {
                    $sizes_9 = '52';
                }
                if(isset($result['54']) && !isset($sizes_1)) {
                    $sizes_1 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '54') {
                    $sizes_2 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '54') {
                    $sizes_3 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '54') {
                    $sizes_4 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '54') {
                    $sizes_5 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '54') {
                    $sizes_6 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '54') {
                    $sizes_7 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '54') {
                    $sizes_8 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '54') {
                    $sizes_9 = '54';
                }
                if(isset($result['54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '54') {
                    $sizes_10 = '54';
                }
                if(isset($result['56']) && !isset($sizes_1)) {
                    $sizes_1 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '56') {
                    $sizes_2 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '56') {
                    $sizes_3 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '56') {
                    $sizes_4 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '56') {
                    $sizes_5 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '56') {
                    $sizes_6 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '56') {
                    $sizes_7 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '56') {
                    $sizes_8 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '56') {
                    $sizes_9 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '56') {
                    $sizes_10 = '56';
                }
                if(isset($result['56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && !isset($sizes_11) && $sizes_9 != '56') {
                    $sizes_11 = '56';
                }
                if(isset($result['58']) && !isset($sizes_1)) {
                    $sizes_1 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '58') {
                    $sizes_2 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '58') {
                    $sizes_3 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '58') {
                    $sizes_4 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '58') {
                    $sizes_5 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '58') {
                    $sizes_6 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '58') {
                    $sizes_7 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '58') {
                    $sizes_8 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '58') {
                    $sizes_9 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '58') {
                    $sizes_10 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && !isset($sizes_11) && $sizes_9 != '58') {
                    $sizes_11 = '58';
                }
                if(isset($result['58']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && isset($sizes_11) && !isset($sizes_12) && $sizes_10 != '58') {
                    $sizes_12 = '58';
                }
                if(isset($result['60']) && !isset($sizes_1)) {
                    $sizes_1 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '60') {
                    $sizes_2 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '60') {
                    $sizes_3 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '60') {
                    $sizes_4 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '60') {
                    $sizes_5 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '60') {
                    $sizes_6 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '60') {
                    $sizes_7 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '60') {
                    $sizes_8 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && !isset($sizes_9) && $sizes_8 != '60') {
                    $sizes_9 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && !isset($sizes_10) && $sizes_9 != '60') {
                    $sizes_10 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && !isset($sizes_11) && $sizes_9 != '60') {
                    $sizes_11 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && isset($sizes_11) && !isset($sizes_12) && $sizes_10 != '60') {
                    $sizes_12 = '60';
                }
                if(isset($result['60']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && isset($sizes_8) && isset($sizes_9) && isset($sizes_10) && isset($sizes_11) && isset($sizes_12) && !isset($sizes_13) && $sizes_12 != '60') {
                    $sizes_13 = '60';
                }



                if(isset($result['ONE']) ) {
                    $sizes_1 = 'ONE';
                }
                if(isset($result['TWO']) && !isset($sizes_1)) {
                    $sizes_1 = 'TWO';
                }
                if(isset($result['TWO']) && isset($sizes_1) && $sizes_1 != 'TWO' ) {
                    $sizes_2 = 'TWO';
                }



                if(isset($result['S/М']) ) {
                    $sizes_1 = 'S/М';
                }
                if(isset($result['L/XL']) && !isset($sizes_1)) {
                    $sizes_1 = 'L/XL';
                }
                if(isset($result['L/XL']) && isset($sizes_1) && $sizes_1 != 'L/XL' ) {
                    $sizes_2 = 'L/XL';
                }



                if(isset($result['S-42']) ) {
                    $sizes_1 = 'S-42';
                }
                if(isset($result['M-44']) && !isset($sizes_1)) {
                    $sizes_1 = 'M-44';
                }
                if(isset($result['M-44']) && isset($sizes_1) && $sizes_1 != 'M-44' ) {
                    $sizes_2 = 'M-44';
                }
                if(isset($result['L-46']) && !isset($sizes_1) && !isset($sizes_2)) {
                    $sizes_1 = 'L-46';
                }
                if(isset($result['L-46']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'L-46') {
                    $sizes_2 = 'L-46';
                }
                if(isset($result['L-46']) && isset($sizes_1) && isset($sizes_2) && $sizes_2 != 'L-46') {
                    $sizes_3 = 'L-46';
                }
                if(isset($result['XL-48']) && !isset($sizes_1)) {
                    $sizes_1 = 'XL-48';
                }
                if(isset($result['XL-48']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != 'XL-48') {
                    $sizes_2 = 'XL-48';
                }
                if(isset($result['XL-48']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != 'XL-48') {
                    $sizes_3 = 'XL-48';
                }
                if(isset($result['XL-48']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != 'XL-48') {
                    $sizes_4 = 'XL-48';
                }
                if(isset($result['2XL-50']) && !isset($sizes_1)) {
                    $sizes_1 = '2XL-50';
                }
                if(isset($result['2XL-50']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '2XL-50') {
                    $sizes_2 = '2XL-50';
                }
                if(isset($result['2XL-50']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '2XL-50') {
                    $sizes_3 = '2XL-50';
                }
                if(isset($result['2XL-50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '2XL-50') {
                    $sizes_4 = '2XL-50';
                }
                if(isset($result['2XL-50']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '2XL-50') {
                    $sizes_5 = '2XL-50';
                }
                if(isset($result['3XL-52']) && !isset($sizes_1)) {
                    $sizes_1 = '3XL-52';
                }
                if(isset($result['3XL-52']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '3XL-52') {
                    $sizes_2 = '3XL-52';
                }
                if(isset($result['3XL-52']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '3XL-52') {
                    $sizes_3 = '3XL-52';
                }
                if(isset($result['3XL-52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '3XL-52') {
                    $sizes_4 = '3XL-52';
                }
                if(isset($result['3XL-52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '3XL-52') {
                    $sizes_5 = '3XL-52';
                }
                if(isset($result['3XL-52']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '3XL-52') {
                    $sizes_6 = '3XL-52';
                }
                if(isset($result['4XL-54']) && !isset($sizes_1)) {
                    $sizes_1 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '4XL-54') {
                    $sizes_2 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '4XL-54') {
                    $sizes_3 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '4XL-54') {
                    $sizes_4 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '4XL-54') {
                    $sizes_5 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '4XL-54') {
                    $sizes_6 = '4XL-54';
                }
                if(isset($result['4XL-54']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '4XL-54') {
                    $sizes_7 = '4XL-54';
                }
                if(isset($result['5XL-56']) && !isset($sizes_1)) {
                    $sizes_1 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && !isset($sizes_2) && $sizes_1 != '5XL-56') {
                    $sizes_2 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && !isset($sizes_3) && !isset($sizes_4) && $sizes_2 != '5XL-56') {
                    $sizes_3 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && !isset($sizes_4) && $sizes_3 != '5XL-56') {
                    $sizes_4 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && !isset($sizes_5) && $sizes_4 != '5XL-56') {
                    $sizes_5 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_5) && !isset($sizes_6) && $sizes_5 != '5XL-56') {
                    $sizes_6 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && !isset($sizes_7) && $sizes_6 != '5XL-56') {
                    $sizes_7 = '5XL-56';
                }
                if(isset($result['5XL-56']) && isset($sizes_1) && isset($sizes_2) && isset($sizes_3) && isset($sizes_4) && isset($sizes_4) && isset($sizes_5) && isset($sizes_6) && isset($sizes_7) && !isset($sizes_8) && $sizes_7 != '5XL-56') {
                    $sizes_8 = '5XL-56';
                }




                if(isset($sizes_1))  echo $sizes_1 . ' var1<br>';
                if(isset($sizes_2))  echo $sizes_2 . ' var2<br>';
                if(isset($sizes_3))  echo $sizes_3 . ' var3<br>';
                if(isset($sizes_4))  echo $sizes_4 . ' var4<br>';
                if(isset($sizes_5))  echo $sizes_5 . ' var5<br>';
                if(isset($sizes_6))  echo $sizes_6 . ' var6<br>';
                if(isset($sizes_7))  echo $sizes_7 . ' var7<br>';
                if(isset($sizes_8))  echo $sizes_8 . ' var8<br>';
                if(isset($sizes_9))  echo $sizes_9 . ' var9<br>';
                if(isset($sizes_10))  echo $sizes_10 . ' var10<br>';
                if(isset($sizes_11))  echo $sizes_11 . ' var11<br>';
                if(isset($sizes_12))  echo $sizes_12 . ' var12<br>';
                if(isset($sizes_13))  echo $sizes_13 . ' var13<br>';
                if(isset($sizes_14))  echo $sizes_14 . ' var14<br>';


//                echo '<pre>' . print_r($result, true) . '</pre>';

//                 Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
//                 И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

            $offers_glem = new OffersGlem();
            if(isset($result['id'])) $offers_glem->sku = $result['id'] . '-glem-sku';
            if(isset($result['type'])) $offers_glem->types = $result['type'];
            if(isset($result['available'])) $offers_glem->available = $result['available'];
            if(isset($result['url'])) $offers_glem->url = $result['url'];
            if(isset($result['price'])) $offers_glem->price = $result['price'];
            if(isset($result['currencyId'])) $offers_glem->currency_id = $result['currencyId'];
            if(isset($result['sale'])) $offers_glem->sales_notes = $result['sale'];
            if(isset($result['categoryId'])) $offers_glem->category_id = $result['categoryId'];
            if(isset($result['quantity'])) $offers_glem->all_quantity = $result['quantity'];
            if(isset($result['main_img'])) $offers_glem->main_img = $result['main_img'];
            if(isset($result['store'])) $offers_glem->store = $result['store'];
            if(isset($result['delivery'])) $offers_glem->delivery = $result['delivery'];
            if(isset($result['vendor'])) $offers_glem->vendor = $result['vendor'];
            if(isset($result['vendorCode'])) $offers_glem->vendor_code = $result['vendorCode'];
            if(isset($result['pref'])) $offers_glem->product_type_name = $result['pref'];
            if(isset($result['model'])) $offers_glem->model = $result['model'];
            if(isset($result['description'])) $offers_glem->description = $result['description'];
            if(isset($result['manufacturer_warranty'])) $offers_glem->manufacturer_warranty = $result['manufacturer_warranty'];
            if(isset($result['country_of_origin'])) $offers_glem->country_of_origin = $result['country_of_origin'];
            if(isset($result['Цвет'])) $offers_glem->color_name = $result['Цвет'];
                                        $offers_glem->source = 'glem';



            if(isset($result['gallery_1']))   $offers_glem->gallery_1 = $result['gallery_1'];
            if(isset($result['gallery_2']))   $offers_glem->gallery_2 = $result['gallery_2'];
            if(isset($result['gallery_3']))   $offers_glem->gallery_3 = $result['gallery_3'];
            if(isset($result['gallery_4']))   $offers_glem->gallery_4 = $result['gallery_4'];
            if(isset($result['gallery_5']))   $offers_glem->gallery_5 = $result['gallery_5'];


                if(isset($sizes_1))   $offers_glem->sizes_1 = $sizes_1;
                if(isset($sizes_2))   $offers_glem->sizes_2 = $sizes_2;
                if(isset($sizes_3))   $offers_glem->sizes_3 = $sizes_3;
                if(isset($sizes_4))   $offers_glem->sizes_4 = $sizes_4;
                if(isset($sizes_5))   $offers_glem->sizes_5 = $sizes_5;
                if(isset($sizes_6))   $offers_glem->sizes_6 = $sizes_6;
                if(isset($sizes_7))   $offers_glem->sizes_7 = $sizes_7;
                if(isset($sizes_8))   $offers_glem->sizes_8 = $sizes_8;
                if(isset($sizes_9))   $offers_glem->sizes_9 = $sizes_9;
                if(isset($sizes_10))   $offers_glem->sizes_10 = $sizes_10;
                if(isset($sizes_11))   $offers_glem->sizes_11 = $sizes_11;
                if(isset($sizes_12))   $offers_glem->sizes_12 = $sizes_12;
                if(isset($sizes_13))   $offers_glem->sizes_13 = $sizes_13;
                if(isset($sizes_14))   $offers_glem->sizes_14 = $sizes_14;



                $offers_glem->save();

//                if(isset($result))  echo '<pre>' . print_r($result, true) . '</pre>';

                unset($sizes_1);
                unset($sizes_2);
                unset($sizes_3);
                unset($sizes_4);
                unset($sizes_5);
                unset($sizes_6);
                unset($sizes_7);
                unset($sizes_8);
                unset($sizes_9);
                unset($sizes_10);
                unset($sizes_11);
                unset($sizes_12);
                unset($sizes_13);
                unset($sizes_14);
                unset($result);

            }

            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'category') {


                if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');
                if(null !== ($reader->getAttribute('parentId')))  $result['parentId'] = $reader->getAttribute('parentId');
                if(null !== ($reader->readString('id')))  $result['name'] = $reader->readString('id');

                $result['source'] = 'glem';

//                echo '<pre>' . print_r($result, true) . '</pre>';
//


                // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
                // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

                $categoriesGlem = new CategoriesGlem();
                if(isset($result['id'])) $categoriesGlem->id = $result['id'];
                if(isset($result['parentId'])) $categoriesGlem->parent_id = $result['parentId'];
                if(isset($result['name'])) $categoriesGlem->name = $result['name'];
                if(isset($result['source'])) $categoriesGlem->source = $result['source'];



//                $categoriesGlem->save();


                $importCategory = new ImportCategory();
                if(isset($result['id'])) $importCategory->import_id = $result['id'];
                if(isset($result['parentId'])) $importCategory->parent_id = $result['parentId'];
                if(isset($result['name'])) $importCategory->name = $result['name'];
                if(isset($result['source'])) $importCategory->source = $result['source'];



//                $importCategory->save();
            }
            // Дальше повторяем цикл (XMLReader ищет следующий <offer>

//            unset($sizes_1);
//            unset($sizes_2);
//            unset($sizes_3);
//            unset($sizes_4);
//            unset($sizes_5);
//            unset($sizes_6);
//            unset($sizes_7);
//            unset($sizes_8);
//            unset($sizes_9);
//            unset($sizes_10);
//            unset($sizes_11);
//            unset($sizes_12);
//            unset($sizes_13);
//            unset($sizes_14);
//            unset($result);


        }
        // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.
        $reader->close();
//    return $this->render('import');

        return $this->render('import');
    }

    public function actionMagic()
    {

        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_glem`')->queryOne();
        $maxIndex =$command['MAX(`sort_id`)'];

//        echo $maxIndex .'<br>';
        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {

            $command1 = Yii::$app->db->createCommand('SELECT `offers_glem`.`color_name`, `offers_glem`.`sort_id`
FROM `offers_glem` WHERE `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post1 = $command1->queryOne();

            $values[$i] = $post1;
            $u = $values[$i]['sort_id'];
            $name = $values[$i]['color_name'];

            $name_2 = trim(preg_replace('/[0-9]./', '', $name));

            $mm = strlen($name_2);

            if ($mm >1) {

            if ($name_2{0} === '-') {
                $name_2 = substr($name_2, 1, $mm-1);
                $name_displayed = ($name_2);
            } else {
                $name_displayed = ($name_2);
            }
            } else {
                $name_displayed = $name;
            }

//            echo $name_displayed . '<br>';
            if ($u > 0) {
                $command2 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `color_name` =:color_name  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':color_name', $name_displayed)
//                    ->bindParam(':color_id', $color_id)
                    ->bindParam(':sort_id', $u);
                $command2->execute();
            } else {
            }

            $command2 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `attr_color`.`name_displayed`, `attr_color`.`color_id`, `attr_color`.`sort_name_displayed`, `attr_color`.`sort_color_id`, `sizes_1`, `sizes_1_id`, `sizes_2`, `sizes_2_id`, `sizes_3`, `sizes_3_id`, `sizes_4`, `sizes_4_id`, `sizes_5`, `sizes_5_id`, `sizes_6`, `sizes_6_id`, `sizes_7`, `sizes_7_id`, `sizes_8`, `sizes_8_id`, `sizes_9`, `sizes_9_id`, `sizes_10`, `sizes_10_id`, `sizes_11`, `sizes_11_id`, `sizes_12`, `sizes_12_id`, `sizes_13`, `sizes_13_id`, `sizes_14`, `sizes_14_id`
FROM `offers_glem`, `attr_color` WHERE `offers_glem`.`color_name` = `attr_color`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post2 = $command2->queryOne();

            $values[$i] = $post2;
            $u = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $color_id = $values[$i]['color_id'];
            $sort_name_displayed = $values[$i]['sort_name_displayed'];
            $sort_color_id = $values[$i]['sort_color_id'];

            if ($u > 0) {
                $command3 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `color_name` =:color_name, `color_id` =:color_id, `sort_color_name` =:sort_color_name, `sort_color_id` =:sort_color_id   WHERE sort_id=:sort_id'
                )
                    ->bindParam(':color_name', $name_displayed)
                    ->bindParam(':color_id', $color_id)
                    ->bindParam(':sort_color_name', $sort_name_displayed)
                    ->bindParam(':sort_color_id', $sort_color_id)
                    ->bindParam(':sort_id', $u);
                $command3->execute();
            } else {    }

            $command4 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `attr_types`.`name_displayed`, `attr_types`.`types_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_types` WHERE `offers_glem`.`product_type_name` = `attr_types`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post3 = $command4->queryOne();

            $values[$i] = $post3;
            $o = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $types_id = $values[$i]['types_id'];

            if ($o > 0) {
                $command5 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `product_type_name` =:product_type_name, `product_type_id` =:product_type_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':product_type_name', $name_displayed)
                    ->bindParam(':product_type_id', $types_id)
                    ->bindParam(':sort_id', $o);
                $command5->execute();
            } else {
            }

            $command7 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_1` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post4 = $command7->queryOne();

            $values[$i] = $post4;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command8 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_1` =:sizes_1, `sizes_1_id` =:sizes_1_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_1', $name_displayed)
                    ->bindParam(':sizes_1_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command8->execute();
            } else {
            }

            $command9 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `match_table_glem`.`param_id`, `offers_glem`.`types`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `match_table_glem`.`name_cat`, `match_table_glem`.`catalog_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `match_table_glem` WHERE `offers_glem`.`category_id` = `match_table_glem`.`glem_id` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post5 = $command9->queryOne();

            $values[$i] = $post5;

            $name_displayed = $values[$i]['name_cat'];
            $catalog_id = $values[$i]['catalog_id'];
            $param_id = $values[$i]['param_id'];
            $pi = $values[$i]['sort_id'];

            if ($pi > 0) {
                $command10 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `param_id` =:param_id, `category_name` =:category_name, `category_id` =:category_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':category_name', $name_displayed)
                    ->bindParam(':category_id', $catalog_id)
//                    ->bindParam(':catalog_id', $catalog_id)
                    ->bindParam(':param_id', $param_id)
                    ->bindParam(':sort_id', $pi);
                $command10->execute();
            } else {
            }
            $command11 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_2` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post6 = $command11->queryOne();

            $values[$i] = $post6;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command12 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_2` =:sizes_2, `sizes_2_id` =:sizes_2_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_2', $name_displayed)
                    ->bindParam(':sizes_2_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command12->execute();
            } else {
            }
            $command13 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_3` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post7 = $command13->queryOne();

            $values[$i] = $post7;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command14 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_3` =:sizes_3, `sizes_3_id` =:sizes_3_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_3', $name_displayed)
                    ->bindParam(':sizes_3_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command14->execute();
            } else {
            }
            $command15 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_4` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post8 = $command15->queryOne();

            $values[$i] = $post8;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command16 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_4` =:sizes_4, `sizes_4_id` =:sizes_4_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_4', $name_displayed)
                    ->bindParam(':sizes_4_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command16->execute();
            } else {
            }
            $command17 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_5` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post9 = $command17->queryOne();

            $values[$i] = $post9;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command18 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_5` =:sizes_5, `sizes_5_id` =:sizes_5_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_5', $name_displayed)
                    ->bindParam(':sizes_5_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command18->execute();
            } else {
            }
            $command19 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_6` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post10 = $command19->queryOne();

            $values[$i] = $post10;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command20 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_6` =:sizes_6, `sizes_6_id` =:sizes_6_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_6', $name_displayed)
                    ->bindParam(':sizes_6_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command20->execute();
            } else {
            }
            $command21 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_7` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post11 = $command21->queryOne();

            $values[$i] = $post11;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command22 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_7` =:sizes_7, `sizes_7_id` =:sizes_7_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_7', $name_displayed)
                    ->bindParam(':sizes_7_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command22->execute();
            } else {
            }
            $command23 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_8` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post12 = $command23->queryOne();

            $values[$i] = $post12;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command24 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `offers_glem`.`sizes_8` =:sizes_8, `sizes_8_id` =:sizes_8_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_8', $name_displayed)
                    ->bindParam(':sizes_8_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command24->execute();
            } else {
            }
            $command25 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_9` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post13 = $command25->queryOne();

            $values[$i] = $post13;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command26 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_9` =:sizes_9, `sizes_9_id` =:sizes_9_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_9', $name_displayed)
                    ->bindParam(':sizes_9_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command26->execute();
            } else {
            }
            $command27 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_10` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post14 = $command27->queryOne();

            $values[$i] = $post14;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command28 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_10` =:sizes_10, `sizes_10_id` =:sizes_10_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_10', $name_displayed)
                    ->bindParam(':sizes_10_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command28->execute();
            } else {
            }
            $command29 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_11` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post15 = $command29->queryOne();

            $values[$i] = $post15;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command30 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_11` =:sizes_11, `offers_glem`.`sizes_11_id` =:sizes_11_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_11', $name_displayed)
                    ->bindParam(':sizes_11_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command30->execute();
            } else {
            }
            $command31 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_13`, `offers_glem`.`sizes_13_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_12` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post16 = $command31->queryOne();

            $values[$i] = $post16;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command32 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_12` =:sizes_12, `sizes_12_id` =:sizes_12_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_12', $name_displayed)
                    ->bindParam(':sizes_12_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command32->execute();
            } else {
            }
            $command33 = Yii::$app->db->createCommand('SELECT `offers_glem`.`sort_id`, `offers_glem`.`sku`, `offers_glem`.`available`, `offers_glem`.`url`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_name`, `offers_glem`.`category_id`, `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`, `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`product_type_name`, `offers_glem`.`product_type_id`, `offers_glem`.`model`, `offers_glem`.`vendor_code`, `offers_glem`.`description`, `offers_glem`.`country_of_origin`, `offers_glem`.`color_name`, `offers_glem`.`color_id`, `offers_glem`.`sizes_1`, `offers_glem`.`sizes_1_id`, `offers_glem`.`sizes_2`, `offers_glem`.`sizes_2_id`, `offers_glem`.`sizes_3`, `offers_glem`.`sizes_3_id`, `offers_glem`.`sizes_4`, `offers_glem`.`sizes_4_id`, `offers_glem`.`sizes_5`, `offers_glem`.`sizes_5_id`, `offers_glem`.`sizes_6`, `offers_glem`.`sizes_6_id`, `offers_glem`.`sizes_7`, `offers_glem`.`sizes_7_id`, `offers_glem`.`sizes_8`, `offers_glem`.`sizes_8_id`, `offers_glem`.`sizes_9`, `offers_glem`.`sizes_9_id`, `offers_glem`.`sizes_10`, `offers_glem`.`sizes_10_id`, `offers_glem`.`sizes_11`, `offers_glem`.`sizes_11_id`, `offers_glem`.`sizes_12`, `offers_glem`.`sizes_12_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_glem`.`sizes_14`, `offers_glem`.`sizes_14_id`
FROM `offers_glem`, `attr_sizes` WHERE `offers_glem`.`sizes_13` = `attr_sizes`.`input_name` && `offers_glem`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post17 = $command33->queryOne();

            $values[$i] = $post17;
            $o = $values[$i]['sort_id'];

            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];

            if ($o > 0) {
                $command34 = Yii::$app->db->createCommand('UPDATE `offers_glem` SET `sizes_13` =:sizes_13, `sizes_13_id` =:sizes_13_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':sizes_13', $name_displayed)
                    ->bindParam(':sizes_13_id', $sizes_id)
                    ->bindParam(':sort_id', $o);
                $command34->execute();
            } else {
            }

            $i++;
        }
        return $this->render('magic');
    }

    public function actionSphinx()
    {
        return $this->render('sphinx');
    }


    public function actionToCatalog()
    {

//        Yii::$app->db->createCommand()->truncateTable('product_olla_womens_shoes');


        $posts_1 = Yii::$app->db->createCommand('SELECT  `categories_glem`.`name`, `offers_glem`.`available`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_id`,  `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`,
 `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`vendor`, `offers_glem`.`model`, `offers_glem`.`vendor_code`,  `offers_glem`.`delivery`, `offers_glem`.`description`,
`offers_glem`.`country_of_origin`, `offers_glem`.`sales_notes`, `offers_glem`.`size_s_quantity`, `offers_glem`.`size_m_quantity`, `offers_glem`.`size_l_quantity`, `offers_glem`.`size_xl_quantity`, `offers_glem`.`size_xxl_quantity`,
`offers_glem`.`size_xxxl_quantity`, `offers_glem`.`color`, `offers_glem`.`style`, `offers_glem`.`season`, `offers_glem`.`product_length`, `offers_glem`.`for_full`FROM `offers_glem` JOIN `categories_glem`
ON `offers_glem`.`category_id` = `categories_glem`.`id`')->queryAll();

        Yii::$app->db->createCommand()->batchInsert('product_womens_clothes', ['type', 'available', 'price', 'currency_id', 'category_id', 'source','main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5',
            'brand', 'model', 'vendor_code', 'delivery', 'description', 'country_of_origin', 'sales_notes', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL',  'color', 'style', 'season', 'product_length', 'for_full'], $posts_1)->execute();


        return $this->render('to-catalog');
    }

    /**
     * Finds the OffersGlem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersGlem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OffersGlem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
