<?php

namespace backend\controllers;

use backend\models\OffersKarreeGroup;
use backend\models\Order;
use Yii;
use backend\models\CategoriesKarree;
use backend\models\OffersKarree;
use backend\models\ImportCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XMLReader;

/**
 * OffersKarreeController implements the CRUD actions for OffersKarree model.
 */
class OffersKarreeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OffersKarree models.
     * @return mixed
     */

    public function actionImport()
    {

        $reader = new \XMLReader();



        $reader->open('http://voyts-admin.loc/xml/2.xml'); // указываем ридеру что будем парсить этот файл
        // циклическое чтение документа
        while($reader->read()) {
            $xml = array();
            // если ридер находит элемент <offer> запускаются события
            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'offer') {

                //  simplexml_import_dom Получает объект класса SimpleXMLElement из узла DOM
                $xml = simplexml_load_string($reader->readOuterXml());

//            if ($reader->localName == 'offer') {



                // считываем аттрибут number
                // Дальше зная примерную структуру документа внутри узла DOM обращаемя к элементам, сохраняя ключи и значения в массив.

//                echo '<pre>' . print_r($xml, true) . '</pre>';


                // v2
//                echo '<pre>' . print_r($xml->categoryId, true) . '</pre>';
//                echo '<pre>' . print_r($xml->param, true) . '</pre>';

                if(isset($xml->id)) $xml['id'] = $xml->id;
                if(isset($xml->available))$xml['available'] = $xml->available;
                if(isset($xml->url)) $xml['url'] = $xml->url;
                if(isset($xml->price))            $xml['price'] = $xml->price;
                if(isset($xml->currencyId))            $xml['currencyId'] = $xml->currencyId;
                if(isset($xml->categoryId))            $xml['categoryId'] = $xml->categoryId;


                if (isset($xml->picture)) {
                    $xml['main_img'] = $xml->picture[0];
                    $i=1;
                    foreach ($xml->picture as $value) {

                        $xml['gallery_' . $i] =  $value;
                        $i++;
                    }
                    unset($i);
                }
                if(isset($xml->delivery))            $xml['delivery'] = $xml->delivery;
                if(isset($xml->name))            $xml['model'] = $xml->name;
                if(isset($xml->vendor))            $xml['vendor'] = $xml->vendor;
                if(isset($xml->vendorCode))            $xml['vendorCode'] = $xml->vendorCode;
                if(isset($xml->country_of_origin))            $xml['country_of_origin'] = $xml->country_of_origin;
                if(isset($xml->barcode))            $xml['barcode'] = $xml->barcode;
                if(isset($xml->sales_notes))            $xml['sales_notes'] = $xml->sales_notes;

                if(isset($xml->param[0])) { $xml[(string) ($xml->param[0]->attributes())] = $xml->param[0]; }
                if(isset($xml->param[1])) { $xml[(string) ($xml->param[1]->attributes())] = $xml->param[1]; }
                if(isset($xml->param[2])) { $xml[(string) ($xml->param[2]->attributes())] = $xml->param[2]; }
                if(isset($xml->param[3])) { $xml[(string) ($xml->param[3]->attributes())] = $xml->param[3]; }
                if(isset($xml->param[4])) { $xml[(string) ($xml->param[4]->attributes())] = $xml->param[4]; }
                if(isset($xml->param[5])) { $xml[(string) ($xml->param[5]->attributes())] = $xml->param[5]; }
                if(isset($xml->param[6])) { $xml[(string) ($xml->param[6]->attributes())] = $xml->param[6]; }
                if(isset($xml->param[7])) { $xml[(string) ($xml->param[7]->attributes())] = $xml->param[7]; }
                if(isset($xml->param[8])) { $xml[(string) ($xml->param[8]->attributes())] = $xml->param[8]; }
                if(isset($xml->param[9])) { $xml[(string) ($xml->param[9]->attributes())] = $xml->param[9]; }
                if(isset($xml->param[10])) { $xml[(string) ($xml->param[10]->attributes())] = $xml->param[10]; }
                if(isset($xml->param[11])) { $xml[(string) ($xml->param[11]->attributes())] = $xml->param[11]; }
                if(isset($xml->param[12])) { $xml[(string) ($xml->param[12]->attributes())] = $xml->param[12]; }
                if(isset($xml->param[13])) { $xml[(string) ($xml->param[13]->attributes())] = $xml->param[13]; }
                if(isset($xml->param[14])) { $xml[(string) ($xml->param[14]->attributes())] = $xml->param[14]; }
                if(isset($xml->param[15])) { $xml[(string) ($xml->param[15]->attributes())] = $xml->param[15]; }
                if(isset($xml->param[16])) { $xml[(string) ($xml->param[16]->attributes())] = $xml->param[16]; }
                if(isset($xml->param[17])) { $xml[(string) ($xml->param[17]->attributes())] = $xml->param[17]; }
                if(isset($xml->param[18])) { $xml[(string) ($xml->param[18]->attributes())] = $xml->param[18]; }
                if(isset($xml->param[19])) { $xml[(string) ($xml->param[19]->attributes())] = $xml->param[19]; }
                if(isset($xml->param[20])) { $xml[(string) ($xml->param[20]->attributes())] = $xml->param[20]; }
                if(isset($xml->param[21])) { $xml[(string) ($xml->param[21]->attributes())] = $xml->param[21]; }
                if(isset($xml->param[22])) { $xml[(string) ($xml->param[22]->attributes())] = $xml->param[22]; }
                if(isset($xml->param[23])) { $xml[(string) ($xml->param[23]->attributes())] = $xml->param[23]; }
                if(isset($xml->param[24])) { $xml[(string) ($xml->param[24]->attributes())] = $xml->param[24]; }
                if(isset($xml->param[25])) { $xml[(string) ($xml->param[25]->attributes())] = $xml->param[25]; }



//  echo $name1;
//                if(isset($xml->param[1]))  $name2 = $xml->param[1]->attributes(); $xml[$name2] = $xml->param[1];

//                    $e=0;
//
//                  foreach ($xml->param as $value) {
//                            $name = $value[$e]->attributes();
//                        $xml[(string)$name] = $value;
//                      echo '<pre>' . print_r( $xml[(string)$name] , true) . '</pre>';
//
//                      $e++;
//
//                    }
//                    unset($e);


                $data = $xml->attributes();
//                echo '<pre>' . print_r( $xml, true) . '</pre>';

                $json = json_encode( $data );
                $xml_array = json_decode( $json, true );

                $result  =  $xml_array['@attributes'];




                // В результате получаем массив объектов SimpleXMLElement с теми ключами, которые МЫ ему присвоили
//                echo '<pre>' . print_r($result, true) . '</pre>';

                // Дальше массив нужно перебрать чтобы получился масиив заполненный строками а не объектами

//                foreach($xml as $key => $simpleXml) {
//                    if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');
//                    if(null !== ($reader->getAttribute('available'))) $result['available'] = $reader->getAttribute('available');
//
//                    // На выходя после :   $result[$key] = ($simpleXml->asXML()); -- получаем массив заполненный строками с XML тегами,
//                    // которые удаляются придобавлении strip_tags к $simpleXml->asXML()
//                    $result[$key] = strip_tags($simpleXml->asXML());
//
//                    // В результаете получаем массив строк с ключасм которые мы ему присвоили
//
//                }

//                echo '<pre>' . print_r($result, true) . '</pre>';
//
//
//                // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
//                // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result
//
                $offers_karree = new OffersKarree();
                if(isset($result['id']))$offers_karree->id = $result['id'];
                if(isset($result['available']))$offers_karree->available = $result['available'];
                if(isset($result['url']))$offers_karree->url = $result['url'];
                if(isset($result['price']))$offers_karree->price = $result['price'];
                if(isset($result['currencyId']))$offers_karree->currency_id = $result['currencyId'];

                if(isset($result['categoryId'])) $offers_karree->category_id = $result['categoryId'];
                if(isset($result['main_img']))  $offers_karree->main_img = $result['main_img'];
                if(isset($result['gallery_1']))  $offers_karree->gallery_1 = $result['gallery_1'];
                if(isset($result['gallery_2']))  $offers_karree->gallery_2 = $result['gallery_2'];
                if(isset($result['gallery_3']))  $offers_karree->gallery_3 = $result['gallery_3'];
                if(isset($result['gallery_4']))  $offers_karree->gallery_4 = $result['gallery_4'];
                if(isset($result['gallery_5']))  $offers_karree->gallery_5 = $result['gallery_5'];
                if(isset($result['delivery']))$offers_karree->delivery = $result['delivery'];
                if(isset($result['model']))$offers_karree->model = $result['model'];
                if(isset($result['vendor']))$offers_karree->vendor = $result['vendor'];
                if(isset($result['vendorCode']))$offers_karree->vendor_code = $result['vendorCode'];
                if(isset($result['country_of_origin']))$offers_karree->country_of_origin = $result['country_of_origin'];
                if(isset($result['barcode']))$offers_karree->barcode = $result['barcode'];
                if(isset($result['sales_notes']))$offers_karree->sales_notes = $result['sales_notes'];
                if(isset($result['Вид товара']))$offers_karree->product_type_name = $result['Вид товара'];
                if(isset($result['Силуэт']))$offers_karree->silhouette_name = $result['Силуэт'];
                if(isset($result['Ткань']))$offers_karree->cloth_name = $result['Ткань'];
                if(isset($result['Цвет']))$offers_karree->color_name = $result['Цвет'];
                if(isset($result['Принт']))$offers_karree->print_name = $result['Принт'];
                if(isset($result['Стиль']))$offers_karree->style_name = $result['Стиль'];
                if(isset($result['Сезон']))$offers_karree->season_name = $result['Сезон'];
                if(isset($result['Для полных']))$offers_karree->for_full = $result['Для полных'];
                if(isset($result['Длина изделия']))$offers_karree->product_length_name = $result['Длина изделия'];
                if(isset($result['Рукав']))$offers_karree->sleeve_name = $result['Рукав'];
                if(isset($result['Воротник-вырез']))$offers_karree->cutout_collar_name = $result['Воротник-вырез'];
                if(isset($result['Дополнительные детали']))$offers_karree->additional_details_name = $result['Дополнительные детали'];



                $offers_karree->source = 'karree';

                $offers_karree->save();

//            echo '<pre>' . print_r($result, true) . '</pre>';

                unset($result);
            }

            if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'category') {


                if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');

                if(null !== ($reader->readString('id')))  $result['name'] = $reader->readString('id');

                $result['source'] = 'karree';

//                echo '<pre>' . print_r($result, true) . '</pre>';
//


                // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
                // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

                $categoriesKarree = new CategoriesKarree();
                if(isset($result['id'])) $categoriesKarree->id = $result['id'];
                if(isset($result['name'])) $categoriesKarree->name = $result['name'];
                if(isset($result['source'])) $categoriesKarree->source = $result['source'];

//                $categoriesKarree->save();

                $importCategory = new ImportCategory();
                if(isset($result['id'])) $importCategory->import_id = $result['id'];
                if(isset($result['name'])) $importCategory->name = $result['name'];
                if(isset($result['source'])) $importCategory->source = $result['source'];

//                $importCategory->save();

            }
            // Дальше повторяем цикл (XMLReader ищет cследующий <offer>



        }
        // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.
        $reader->close();
        return $this->render('import');
    }

    public function actionSizes() {
        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_karree`')->queryOne();
        $maxIndex =$command['MAX(`sort_id`)'];

//        echo $maxIndex .'<br>';
        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {

            $command27 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`model`,  `offers_karree`.`sizes`, `offers_karree`.`sizes`
        FROM `offers_karree` WHERE `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);
            $sizesZ = '';
            $post13 = $command27->queryOne();
            $values[$i] = $post13;
            $pb = $values[$i]['sort_id'];
            $model = $values[$i]['model'];

            $sizes = trim(preg_replace("/[^ | M | L | S | XL | XXL -]+/","",$model));

            $mm = strlen($sizes);
            $name = trim(preg_replace("/[a-zA-Z][^Karree]\d*/","",$model));

//            echo $name; '<br>';

            if ($mm >1) {
                $sizesZ = trim(substr($sizes, 1, $mm));
                $mmm = '';
                $mmm = strlen($sizesZ);
//            if ($mmm)
                if ($sizesZ{$mmm-1} === '-') {
                    $sizesZ = substr($sizesZ, 0, $mmm-1);

//                    echo $sizesZ; '<br>';

                } else {
//                    echo $sizesZ; '<br>';
                }
            } else {}


            if ($pb > 0) {
                $command28 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `model` =:model, `sizes` =:sizes  WHERE sort_id=:sort_id')
                    ->bindParam(':sizes', $sizesZ )
                    ->bindParam(':model', $name )
                    ->bindParam(':sort_id', $pb);
                $command28->execute();
            } else { }
            $i++;
        }
        return $this->render('sizes');
    }

    public function actionMagic() {



        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_karree`')->queryOne();
        $maxIndex =$command['MAX(`sort_id`)'];

//        echo $maxIndex .'<br>';
        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {
            $command1 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `attr_color`.`name_displayed`, `attr_color`.`color_id`, `attr_color`.`sort_name_displayed`, `attr_color`.`sort_color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_color` WHERE `offers_karree`.`color_name` = `attr_color`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post1 = $command1->queryOne();
            $values[$i] = $post1;
            $y = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $color_id = $values[$i]['color_id'];
            $sort_name_displayed = $values[$i]['sort_name_displayed'];
            $sort_color_id = $values[$i]['sort_color_id'];

//           echo $y . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($y > 0) {
                $command2 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `color_name` =:color_name, `color_id` =:color_id, `sort_color_name` =:sort_color_name, `sort_color_id` =:sort_color_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $y]
                )
                    ->bindParam(':color_name', $name_displayed )
                    ->bindParam(':color_id', $color_id)
                    ->bindParam(':sort_color_name', $sort_name_displayed)
                    ->bindParam(':sort_color_id', $sort_color_id)
                    ->bindParam(':sort_id', $y);
                $command2->execute();
            } else { }

            $command3 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `attr_silhouette`.`name_displayed`, `attr_silhouette`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_silhouette` WHERE `offers_karree`.`silhouette_name` = `attr_silhouette`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post2 = $command3->queryOne();
            $values[$i] = $post2;
            $u = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $silhouette_id = $values[$i]['silhouette_id'];

//           echo $u . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($u > 0) {
                $command4 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `silhouette_name` =:silhouette_name, `silhouette_id` =:silhouette_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $u]
                )
                    ->bindParam(':silhouette_name', $name_displayed )
                    ->bindParam(':silhouette_id', $silhouette_id)
                    ->bindParam(':sort_id', $u);
                $command4->execute();
            } else { }



            $command5 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `attr_types`.`name_displayed`, `attr_types`.`types_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_types` WHERE `offers_karree`.`product_type_name` = `attr_types`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post3 = $command5->queryOne();
            $values[$i] = $post3;
            $o = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $types_id = $values[$i]['types_id'];

//           echo $o . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($o > 0) {
                $command6 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `product_type_name` =:product_type_name, `product_type_id` =:product_type_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $o]
                )
                    ->bindParam(':product_type_name', $name_displayed )
                    ->bindParam(':product_type_id', $types_id)
                    ->bindParam(':sort_id', $o);
                $command6->execute();
            } else { }

            $command7 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `attr_cloth`.`name_displayed`, `attr_cloth`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_cloth` WHERE `offers_karree`.`cloth_name` = `attr_cloth`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post4 = $command7->queryOne();
            $values[$i] = $post4;
            $p = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $cloth_id = $values[$i]['cloth_id'];

//           echo $p . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($p > 0) {
                $command8 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `cloth_name` =:cloth_name, `cloth_id` =:cloth_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':cloth_name', $name_displayed )
                    ->bindParam(':cloth_id', $cloth_id)
                    ->bindParam(':sort_id', $p);
                $command8->execute();
            } else { }


            $command9 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `attr_print`.`name_displayed`, `attr_print`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_print` WHERE `offers_karree`.`print_name` = `attr_print`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post5 = $command9->queryOne();
            $values[$i] = $post5;
            $p = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $print_id = $values[$i]['print_id'];

//           echo $p . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($p > 0) {
                $command10 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `print_name` =:print_name, `print_id` =:print_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':print_name', $name_displayed )
                    ->bindParam(':print_id', $print_id)
                    ->bindParam(':sort_id', $p);
                $command10->execute();
            } else { }


            $command11 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `attr_style`.`name_displayed`, `attr_style`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_style` WHERE `offers_karree`.`style_name` = `attr_style`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post6 = $command11->queryOne();
            $values[$i] = $post6;
            $pl = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $style_id = $values[$i]['style_id'];

//           echo $pl . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($pl > 0) {
                $command12 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `style_name` =:style_name, `style_id` =:style_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':style_name', $name_displayed )
                    ->bindParam(':style_id', $style_id)
                    ->bindParam(':sort_id', $pl);
                $command12->execute();
            } else { }



            $command13 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `attr_season`.`name_displayed`, `attr_season`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_season` WHERE `offers_karree`.`season_name` = `attr_season`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post7 = $command13->queryOne();
            $values[$i] = $post7;
            $pr = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $season_id = $values[$i]['season_id'];

//           echo $pr . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($pr > 0) {
                $command14 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `season_name` =:season_name, `season_id` =:season_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':season_name', $name_displayed )
                    ->bindParam(':season_id', $season_id)
                    ->bindParam(':sort_id', $pr);
                $command14->execute();
            } else { }


            $command15 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `attr_product_length`.`name_displayed`, `attr_product_length`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_product_length` WHERE `offers_karree`.`product_length_name` = `attr_product_length`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post8 = $command15->queryOne();
            $values[$i] = $post8;
            $pt = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $product_length_id = $values[$i]['product_length_id'];

//           echo $pt . '<id>';
//                                echo '<pre>' . print_r($post) . '</pre>';
            if ($pt > 0) {
                $command16 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `product_length_name` =:product_length_name, `product_length_id` =:product_length_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':product_length_name', $name_displayed )
                    ->bindParam(':product_length_id', $product_length_id)
                    ->bindParam(':sort_id', $pt);
                $command16->execute();
            } else { }


            $command17 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `attr_sleeve`.`name_displayed`, `attr_sleeve`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_sleeve` WHERE `offers_karree`.`sleeve_name` = `attr_sleeve`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post9 = $command17->queryOne();
            $values[$i] = $post9;
            $py = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $sleeve_id = $values[$i]['sleeve_id'];


            if ($py > 0) {
                $command18 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `sleeve_name` =:sleeve_name, `sleeve_id` =:sleeve_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':sleeve_name', $name_displayed )
                    ->bindParam(':sleeve_id', $sleeve_id)
                    ->bindParam(':sort_id', $py);
                $command18->execute();
            } else { }

            $command19 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `attr_cutout_collar`.`name_displayed`, `attr_cutout_collar`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_cutout_collar` WHERE `offers_karree`.`cutout_collar_name` = `attr_cutout_collar`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post9 = $command19->queryOne();
            $values[$i] = $post9;
            $pu = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $cutout_collar_id = $values[$i]['cutout_collar_id'];


            if ($pu > 0) {
                $command20 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `cutout_collar_name` =:cutout_collar_name, `cutout_collar_id` =:cutout_collar_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':cutout_collar_name', $name_displayed )
                    ->bindParam(':cutout_collar_id', $cutout_collar_id)
                    ->bindParam(':sort_id', $pu);
                $command20->execute();
            } else { }
//




            $command21 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `attr_additional_details`.`name_displayed`, `attr_additional_details`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `attr_additional_details` WHERE `offers_karree`.`additional_details_name` = `attr_additional_details`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post10 = $command21->queryOne();
            $values[$i] = $post10;
            $po = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $additional_details_id = $values[$i]['additional_details_id'];


            if ($po > 0) {
                $command22 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `additional_details_name` =:additional_details_name, `additional_details_id` =:additional_details_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':additional_details_name', $name_displayed )
                    ->bindParam(':additional_details_id', $additional_details_id)
                    ->bindParam(':sort_id', $po);
                $command22->execute();
            } else { }


            $command25 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `match_table_karree`.`param_id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `match_table_karree`.`name_cat`, `match_table_karree`.`catalog_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `offers_karree`.`sizes`, `offers_karree`.`sizes_id`
        FROM `offers_karree`, `match_table_karree` WHERE `offers_karree`.`category_id` = `match_table_karree`.`karree_id` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post12 = $command25->queryOne();
            $values[$i] = $post12;
            $pa = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_cat'];
            $catalog_id = $values[$i]['catalog_id'];
            $param_id = $values[$i]['param_id'];


            if ($pa > 0) {
                $command26 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `param_id` =:param_id, `category_name` =:category_name, `category_id` =:category_id  WHERE sort_id=:sort_id'
                )
                    ->bindParam(':category_name', $name_displayed)
                    ->bindParam(':category_id', $catalog_id)
                    ->bindParam(':param_id', $param_id)
                    ->bindParam(':sort_id', $pa);
                $command26->execute();
            } else { }

            $command27 = Yii::$app->db->createCommand('
SELECT `offers_karree`.`sort_id`, `offers_karree`.`id`, `offers_karree`.`available`, `offers_karree`.`url`, `offers_karree`.`price`, `offers_karree`.`currency_id`, `offers_karree`.`category_id`, `offers_karree`.`source`, `offers_karree`.`main_img`, `offers_karree`.`gallery_1`, `offers_karree`.`gallery_2`, `offers_karree`.`gallery_3`, `offers_karree`.`gallery_4`, `offers_karree`.`gallery_5`, `offers_karree`.`delivery`, `offers_karree`.`model`, `offers_karree`.`vendor`, `offers_karree`.`vendor_code`, `offers_karree`.`description`, `offers_karree`.`country_of_origin`, `offers_karree`.`barcode`, `offers_karree`.`sales_notes`, `offers_karree`.`product_type_name`, `offers_karree`.`product_type_id`, `offers_karree`.`silhouette_name`, `offers_karree`.`silhouette_id`, `offers_karree`.`cloth_name`, `offers_karree`.`cloth_id`, `offers_karree`.`color_name`, `offers_karree`.`color_id`, `offers_karree`.`print_name`, `offers_karree`.`print_id`, `offers_karree`.`style_name`, `offers_karree`.`style_id`, `offers_karree`.`season_name`, `offers_karree`.`season_id`, `offers_karree`.`for_full`, `offers_karree`.`product_length_name`, `offers_karree`.`product_length_id`, `offers_karree`.`sleeve_name`, `offers_karree`.`sleeve_id`, `offers_karree`.`cutout_collar_name`, `offers_karree`.`cutout_collar_id`, `offers_karree`.`additional_details_name`, `offers_karree`.`additional_details_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`
        FROM `offers_karree`, `attr_sizes` WHERE `offers_karree`.`sizes` = `attr_sizes`.`input_name` && `offers_karree`.`sort_id` =:sort_id')
                ->bindParam(':sort_id', $i);

            $post13 = $command27->queryOne();
            $values[$i] = $post13;
            $pp = $values[$i]['sort_id'];
            $name_displayed = $values[$i]['name_displayed'];
            $sizes_id = $values[$i]['sizes_id'];


            if ($pp > 0) {
                $command24 = Yii::$app->db->createCommand('UPDATE `offers_karree` SET `sizes` =:sizes, `sizes_id` =:sizes_id  WHERE sort_id=:sort_id'
//        'offers_issaplus', ['id', 'sku', 'category_name', 'category_id', 'source', 'model', 'url', 'collection_name', 'collection_id', 'color_name', 'color_id', 'style_name', 'style_id', 'size_1', 'size_1_id', 'size_2', 'size_2_id', 'size_3', 'size_3_id', 'size_4', 'size_4_id', 'size_5', 'size_5_id', 'price', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'id = :id', [':id' => $p]
                )
                    ->bindParam(':sizes', $name_displayed )
                    ->bindParam(':sizes_id', $sizes_id)
                    ->bindParam(':sort_id', $pp);
                $command24->execute();
            } else { }

            $i++;
        }
        return $this->render('magic');

    }

    public function actionIndex()
    {

        $dataProvider = new ActiveDataProvider([
            'query' => OffersKarree::find(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [

            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OffersKarree model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionSphinx()
    {
        return $this->render('sphinx');
    }

    public function actionGroup()
    {
        $i = 1;
        $y = 1;
        $qtr = -1;
        $count = 0;
        $values = [];
        $command = Yii::$app->db->createCommand('SELECT MAX(`sort_id`) FROM `offers_karree`')->queryOne();
        $maxIndex = $command['MAX(`sort_id`)'];


        while ($i <= $maxIndex) {
            $command = Yii::$app->db->createCommand('SELECT 

`id`, `param_id`, `available`, `url`, `price`, `currency_id`, `category_name`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type_name`, `product_type_id`, `silhouette_name`, `silhouette_id`, `cloth_name`, `cloth_id`, `color_name`, `color_id`, `sort_color_name`, `sort_color_id`, `print_name`, `print_id`, `style_name`, `style_id`, `season_name`, `season_id`, `for_full`, `product_length_name`, `product_length_id`, `sleeve_name`, `sleeve_id`, `cutout_collar_name`, `cutout_collar_id`, `additional_details_name`, `additional_details_id`, `sizes`, `sizes_id`FROM `offers_karree` WHERE `sort_id`=:sort_id')
                ->bindParam(':sort_id', $i);

            $post = $command->queryOne();
            $values[$i] = $post;

            $values[0] = 0;
//            $values[0]['group_id'] = 0;

            if (isset($values[$i]['vendor_code'])) {
                if ($values[$i]['vendor_code'] === $values[$i - 1]['vendor_code']) {
                    $count++;
                    $values[$i]['count'] = $count;

                }
                if ($values[$i]['vendor_code'] !== $values[$i - 1]['vendor_code']) {
                    $count = 0;
                    $y++;
                    $values[$i]['count'] = $count;
                    $values[$i]['y'] = $y;
                }

//            echo $count . '<br>';
//            '<pre>' . print_r($values) . '</pre>';

//            echo 'count: ' . $count . ',  y: ' . $y . ', i: ' . $i . '<br>';

                $i++;
            }
        }
        for ($i = 1; $i <= $maxIndex; $i++) {

            if ($values[$i]['count'] == 0) {

//               $ii = trim(preg_replace("/[^ | M | L | S | XL | XXL -]+/","",$values[$i]['model']));
//echo $ii;


                $offers_karree_group = new OffersKarreeGroup();

                if(isset($values[$i]['id']))$offers_karree_group->sku = $values[$i]['id'] . '-karree-sku';
                if(isset($values[$i]['param_id']))$offers_karree_group->param_id = $values[$i]['param_id'];
                if(isset($values[$i]['available']))$offers_karree_group->available = $values[$i]['available'];
                if(isset($values[$i]['url']))$offers_karree_group->url = $values[$i]['url'];
                if(isset($values[$i]['price']))$offers_karree_group->price = $values[$i]['price'];
                if(isset($values[$i]['currency_id']))$offers_karree_group->currency_id = $values[$i]['currency_id'];
                if(isset($values[$i]['category_name'])) $offers_karree_group->category_name = $values[$i]['category_name'];
                if(isset($values[$i]['category_id'])) $offers_karree_group->category_id = $values[$i]['category_id'];
                if(isset($values[$i]['main_img']))$offers_karree_group->main_img = $values[$i]['main_img'];
                if(isset($values[$i]['gallery_1']))  $offers_karree_group->gallery_1 = $values[$i]['gallery_1'];
                if(isset($values[$i]['gallery_2']))  $offers_karree_group->gallery_2 = $values[$i]['gallery_2'];
                if(isset($values[$i]['gallery_3']))  $offers_karree_group->gallery_3 = $values[$i]['gallery_3'];
                if(isset($values[$i]['gallery_4']))  $offers_karree_group->gallery_4 = $values[$i]['gallery_4'];
                if(isset($values[$i]['gallery_5']))  $offers_karree_group->gallery_5 = $values[$i]['gallery_5'];
                if(isset($values[$i]['delivery']))$offers_karree_group->delivery = $values[$i]['delivery'];
//                if(isset($values[$i]['model']))$offers_karree_group->model = $values[$i]['model'];
                if(isset($values[$i]['vendor']))$offers_karree_group->vendor = $values[$i]['vendor'];
                if(isset($values[$i]['vendor_code']))$offers_karree_group->vendor_code = $values[$i]['vendor_code'];
                if(isset($values[$i]['country_of_origin']))$offers_karree_group->country_of_origin = $values[$i]['country_of_origin'];
                if(isset($values[$i]['barcode']))$offers_karree_group->barcode = $values[$i]['barcode'];
                if(isset($values[$i]['sales_notes']))$offers_karree_group->sales_notes = $values[$i]['sales_notes'];
                if(isset($values[$i]['product_type_name']))$offers_karree_group->product_type_name = $values[$i]['product_type_name'];
                if(isset($values[$i]['product_type_id']))$offers_karree_group->product_type_id = $values[$i]['product_type_id'];
                if(isset($values[$i]['silhouette_name']))$offers_karree_group->silhouette_name = $values[$i]['silhouette_name'];

                if(isset($values[$i]['cloth_name']))$offers_karree_group->cloth_name = $values[$i]['cloth_name'];
                if(isset($values[$i]['cloth_id']))$offers_karree_group->cloth_id = $values[$i]['cloth_id'];

                if(isset($values[$i]['color_name']))$offers_karree_group->color_name = $values[$i]['color_name'];
                if(isset($values[$i]['color_id']))$offers_karree_group->color_id = $values[$i]['color_id'];
                if(isset($values[$i]['sort_color_name']))$offers_karree_group->sort_color_name = $values[$i]['sort_color_name'];
                if(isset($values[$i]['sort_color_id']))$offers_karree_group->sort_color_id = $values[$i]['sort_color_id'];


                if(isset($values[$i]['print_name']))$offers_karree_group->print_name = $values[$i]['print_name'];
                if(isset($values[$i]['print_id']))$offers_karree_group->print_id = $values[$i]['print_id'];

                if(isset($values[$i]['style_name']))$offers_karree_group->style_name = $values[$i]['style_name'];
                if(isset($values[$i]['style_id']))$offers_karree_group->style_id = $values[$i]['style_id'];

                if(isset($values[$i]['season_name']))$offers_karree_group->season_name = $values[$i]['season_name'];
                if(isset($values[$i]['season_id']))$offers_karree_group->season_id = $values[$i]['season_id'];

                if(isset($values[$i]['for_full']))$offers_karree_group->for_full = $values[$i]['for_full'];
                if(isset($values[$i]['product_length_name']))$offers_karree_group->product_length_name = $values[$i]['product_length_name'];
                if(isset($values[$i]['product_length_id']))$offers_karree_group->product_length_id = $values[$i]['product_length_id'];

                if(isset($values[$i]['sleeve_name']))$offers_karree_group->sleeve_name = $values[$i]['sleeve_name'];
                if(isset($values[$i]['sleeve_id']))$offers_karree_group->sleeve_id = $values[$i]['sleeve_id'];

                if(isset($values[$i]['cutout_collar_name']))$offers_karree_group->cutout_collar_name = $values[$i]['cutout_collar_name'];
                if(isset($values[$i]['cutout_collar_id']))$offers_karree_group->cutout_collar_id = $values[$i]['cutout_collar_id'];

                if(isset($values[$i]['additional_details_name']))$offers_karree_group->additional_details_name = $values[$i]['additional_details_name'];
                if(isset($values[$i]['additional_details_id']))$offers_karree_group->additional_details_id = $values[$i]['additional_details_id'];

                if (isset($values[$i]['model'])) $offers_karree_group->model = $values[$i]['model'];
                if (isset($values[$i]['sizes']) && $values[$i]['count'] === 0) $offers_karree_group->sizes_1 = $values[$i]['sizes'];
                if (isset($values[$i]['sizes']) && $values[$i]['count'] === 0) $offers_karree_group->sizes_1_id = $values[$i]['sizes_id'];

                if (isset($values[$i + 1]['sizes']) && $values[$i + 1]['count'] === 1) $offers_karree_group->sizes_2 = $values[$i + 1]['sizes'];
                if (isset($values[$i + 1]['sizes']) && $values[$i + 1]['count'] === 1) $offers_karree_group->sizes_2_id = $values[$i + 1]['sizes_id'];
                if (isset($values[$i + 2]['sizes']) && $values[$i + 2]['count'] === 2) $offers_karree_group->sizes_3 = $values[$i + 2]['sizes'];
                if (isset($values[$i + 2]['sizes']) && $values[$i + 2]['count'] === 2) $offers_karree_group->sizes_3_id = $values[$i + 2]['sizes_id'];
                if (isset($values[$i + 3]['sizes']) && $values[$i + 3]['count'] === 3) $offers_karree_group->sizes_4 = $values[$i + 3]['sizes'];
                if (isset($values[$i + 3]['sizes']) && $values[$i + 3]['count'] === 3) $offers_karree_group->sizes_4_id = $values[$i + 3]['sizes_id'];
                if (isset($values[$i + 4]['sizes']) && $values[$i + 4]['count'] === 4) $offers_karree_group->sizes_5 = $values[$i + 4]['sizes'];
                if (isset($values[$i + 4]['sizes']) && $values[$i + 4]['count'] === 4) $offers_karree_group->sizes_5_id = $values[$i + 4]['sizes_id'];
                if (isset($values[$i + 5]['sizes']) && $values[$i + 5]['count'] === 5) $offers_karree_group->sizes_6 = $values[$i + 5]['sizes'];
                if (isset($values[$i + 5]['sizes']) && $values[$i + 5]['count'] === 5) $offers_karree_group->sizes_6_id = $values[$i + 5]['sizes_id'];


                $offers_karree_group->source = 'karree';

                $offers_karree_group->save();
            } else {
//                echo 'Повтор';
            }
//          $i++;
        }

        return $this->render('group');
    }

    public function actionToCatalog()
    {

//        Yii::$app->db->createCommand()->truncateTable('product_olla_womens_shoes');


        $posts_1 = Yii::$app->db->createCommand('SELECT `categories_karree_womens_clothes`.`name`, `offers_karree_group`.`available`, `offers_karree_group`.`price`, `offers_karree_group`.`currency_id`, `offers_karree_group`.`category_id`, `offers_karree_group`.`source`, `offers_karree_group`.`main_img`,
`offers_karree_group`.`gallery_1`, `offers_karree_group`.`gallery_2`, `offers_karree_group`.`gallery_3`, `offers_karree_group`.`gallery_4`, `offers_karree_group`.`gallery_5`, `offers_karree_group`.`model`, `offers_karree_group`.`vendor_code`, `offers_karree_group`.`delivery`, `offers_karree_group`.`description`, `offers_karree_group`.`country_of_origin`, `offers_karree_group`.`sales_notes`, `offers_karree_group`.`sizes_eu_1`, `offers_karree_group`.`sizes_eu_2`, `offers_karree_group`.`sizes_eu_3`, `offers_karree_group`.`sizes_eu_4`, `offers_karree_group`.`sizes_eu_5`, `offers_karree_group`.`sizes_eu_6`, `offers_karree_group`.`sizes_eu_7`, `offers_karree_group`.`sizes_eu_8`, `offers_karree_group`.`sizes_eu_9`, `offers_karree_group`.`sizes_eu_10`, `offers_karree_group`.`cloth`, `offers_karree_group`.`color`, `offers_karree_group`.`style`, `offers_karree_group`.`season`, `offers_karree_group`.`product_length`, `offers_karree_group`.`for_full`, `offers_karree_group`.`silhouette`,
`offers_karree_group`.`print`, `offers_karree_group`.`cutout_collar`, `offers_karree_group`.`additional_details`, `offers_karree_group`.`sleeve` FROM `offers_karree_group` JOIN `categories_karree_womens_clothes` ON `offers_karree_group`.`category_id` = `categories_karree_womens_clothes`.`id`')->queryAll();

        Yii::$app->db->createCommand()->batchInsert('product_womens_clothes', ['type', 'available', 'price', 'currency_id', 'category_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model','vendor_code', 'delivery', 'description', 'country_of_origin', 'sales_notes', 'sizes_eu_1', 'sizes_eu_2', 'sizes_eu_3', 'sizes_eu_4', 'sizes_eu_5', 'sizes_eu_6', 'sizes_eu_7', 'sizes_eu_8', 'sizes_eu_9', 'sizes_eu_10',
            'cloth', 'color', 'style', 'season', 'product_length', 'for_full', 'silhouette', 'print', 'cutout_collar', 'additional_details', 'sleeve'], $posts_1)->execute();


        return $this->render('to-catalog');
    }


    /**
     * Creates a new OffersKarree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OffersKarree();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OffersKarree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OffersKarree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OffersKarree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersKarree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OffersKarree::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
