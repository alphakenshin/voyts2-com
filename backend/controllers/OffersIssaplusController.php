<?php

namespace backend\controllers;

use backend\models\Order;
use Yii;
use backend\models\CategoriesIssaplus;
use backend\models\OffersIssaplus;
use backend\models\OffersIssaplusSearch;
use backend\models\ImportCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use XMLReader;
use PDO;
/**
 * OffersIssaplusController implements the CRUD actions for OffersIssaplus model.
 */
class OffersIssaplusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OffersIssaplus models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OffersIssaplus::find(),
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [

            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OffersIssaplus model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OffersIssaplus model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OffersIssaplus();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OffersIssaplus model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OffersIssaplus model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OffersIssaplus model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersIssaplus the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


    public function actionSphinx()
    {
        return $this->render('sphinx');
    }

    public function actionImport()
    {
    $reader = new XMLReader();


    $reader->open('http://voyts-admin.loc/xml/issa.xml'); // указываем ридеру что будем парсить этот файл
    // циклическое чтение документа
    while($reader->read()) {
    $xml = array();
    $countI = 0;
    // если ридер находит элемент <offer> запускаются события
        if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'product') {

        //  simplexml_import_dom Получает объект класса SimpleXMLElement из узла DOM
            $xml = simplexml_load_string($reader->readOuterXml());
        //            if ($reader->localName == 'offer') {

            $countI++;

        // считываем аттрибут number
        // Дальше зная примерную структуру документа внутри узла DOM обращаемя к элементам, сохраняя ключи и значения в массив.



        // v2


        if(isset($xml->id)) $xml['id'] = $xml->id;
        if(isset($xml->available))$xml['available'] = $xml->available;
        if(isset($xml->group_id))$xml['group_id'] = $xml->group_id;
        if(isset($xml->sku)) $xml['sku'] = $xml->sku;
        if(isset($xml->id_category))            $xml['id_category'] = $xml->id_category;
        if(isset($xml->product_name))            $xml['product_name'] = $xml->product_name;
        if(isset($xml->product_link))            $xml['product_link'] = $xml->product_link;
        if(isset($xml->product_collection))            $xml['product_collection'] = $xml->product_collection;
        if(isset($xml->product_color))            $xml['product_color'] = $xml->product_color;
        if(isset($xml->product_style))            $xml['product_style'] = $xml->product_style;
        if (isset($xml->product_sizes)) {

        $i=0;
        foreach ($xml->product_sizes->size as $value) {

        $xml['product_sizes_' . $i] =  $value;
        $i++;
        }
        unset($i);
        }
        if(isset($xml->product_prices->uah)) $xml['uah'] = $xml->product_prices->uah;
        if(isset($xml->product_prices->rub)) $xml['rub'] = $xml->product_prices->rub;
        if(isset($xml->product_prices->usd)) $xml['usd'] = $xml->product_prices->usd;
        if(isset($xml->product_prices_opt->uah)) $xml['uah_opt'] = $xml->product_prices_opt->uah;
        if(isset($xml->product_prices_opt->rub)) $xml['rub_opt'] = $xml->product_prices_opt->rub;
        if(isset($xml->product_prices_opt->usd)) $xml['usd_opt'] = $xml->product_prices_opt->usd;
        if (isset($xml->product_images->image)) {
        if(isset($xml->product_images->image[0])) $xml['main_img'] = $xml->product_images->image[0];

        $u=1;
        foreach ($xml->product_images->image as $value) {

        $xml['image_' . $u] =  $value;
        $u++;
        }
        unset($u);
        }





        // В результате получаем массив объектов SimpleXMLElement с теми ключами, которые МЫ ему присвоили

        // Дальше массив нужно перебрать чтобы получился масиив заполненный строками а не объектами


            $xml = $xml->attributes();
//                echo '<pre>' . print_r($xml, true) . '</pre>';

            $json = json_encode( $xml );
            $xml_array = json_decode( $json, true );

            $result  =  $xml_array['@attributes'];
        // В результаете получаем массив строк с ключасм которые мы ему присвоили


//        echo '<pre>' . print_r($result, true) . '</pre>';

        // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
        // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

                        $offers_issaplus = new OffersIssaplus();
//                        if(isset($result['id'])) $offers_issaplus->id = $result['id'];
                        if(isset($result['sku'])) $offers_issaplus->sku = $result['sku'];
                        if(isset($result['id_category'])) $offers_issaplus->category_id = $result['id_category'];
                        if(isset($result['product_name'])) $offers_issaplus->model = $result['product_name'];
                        if(isset($result['product_link'])) $offers_issaplus->url = $result['product_link'];
                        if(isset($result['product_collection'])) $offers_issaplus->collection_name = $result['product_collection'];
                        if(isset($result['product_color'])) $offers_issaplus->color_name = $result['product_color'];
                        if(isset($result['product_style'])) $offers_issaplus->style_name = $result['product_style'];
                        if(isset($result['product_sizes_0'])) $offers_issaplus->sizes_1 = $result['product_sizes_0'];
                        if(isset($result['product_sizes_1'])) $offers_issaplus->sizes_2 = $result['product_sizes_1'];
                        if(isset($result['product_sizes_2'])) $offers_issaplus->sizes_3 = $result['product_sizes_2'];
                        if(isset($result['product_sizes_3'])) $offers_issaplus->sizes_4 = $result['product_sizes_3'];
                        if(isset($result['product_sizes_4'])) $offers_issaplus->sizes_5 = $result['product_sizes_4'];
                        if(isset($result['uah']))   $offers_issaplus->	price = $result['uah'];
                        if(isset($result['uah']))   $offers_issaplus->	price_uah = $result['uah'];
                        if(isset($result['rub']))   $offers_issaplus->price_rub = $result['rub'];
                        if(isset($result['usd']))   $offers_issaplus->price_usd = $result['usd'];
                        if(isset($result['uah_opt']))   $offers_issaplus->opt_price_uah = $result['uah_opt'];
                        if(isset($result['rub_opt']))   $offers_issaplus->opt_price_rub = $result['rub_opt'];
                        if(isset($result['usd_opt'])) $offers_issaplus->opt_price_usd = $result['usd_opt'];
                        if(isset($result['main_img'])) $offers_issaplus->main_img = $result['main_img'];
                        if(isset($result['image_1'])) $offers_issaplus->gallery_1 = $result['image_1'];
                        if(isset($result['image_2'])) $offers_issaplus->gallery_2 = $result['image_2'];
                        if(isset($result['image_3'])) $offers_issaplus->gallery_3 = $result['image_3'];
                        if(isset($result['image_4'])) $offers_issaplus->gallery_4 = $result['image_4'];
                        if(isset($result['image_5'])) $offers_issaplus->gallery_5 = $result['image_5'];

                        $offers_issaplus->	source = 'issaplus';


//            $style = Yii::$app->db->createCommand('SELECT * FROM `attr_style`')->queryAll();
//            $color = Yii::$app->db->createCommand('SELECT * FROM `attr_color`')->queryAll();
//            $collections = Yii::$app->db->createCommand('SELECT * FROM `attr_collection`')->queryAll();

//            echo '<pre>' . print_r($result['product_style'], true) . '</pre>';

//                                    echo '<pre>' . print_r($result, true) . '</pre>';

                        $offers_issaplus->save();




            if(isset($result['product_collection'])) {
                $collections = Yii::$app->db->createCommand('SELECT * FROM `attr_collection`')->queryAll();
                $i = 0;
                foreach ($collections as $value) {

                    if ($value['input_name'] === $result['product_collection']) {
                        $offers_issaplus->collection_name = $value['name_displayed'];
                        $offers_issaplus->collection_id = $value['collection_id'];
                        $i++;
//                                    echo '<pre>' . print_r($offers_issaplus->collection_name, true) . '</pre>';

                    }
                    if ($value['input_name'] === '') {
                        $offers_issaplus->collection_name = '';
                        $offers_issaplus->collection_id = -1;
                        $i++;
                    }
                    if ($i < 1 ) {
                        $offers_issaplus->collection_name = $result['product_collection'];
                        $offers_issaplus->collection_id = 0;
                    } else {      }
                }
            }
            unset($result);
        }
        // Дальше повторяем цикл (XMLReader ищет cследующий <offer>
        if($reader->nodeType == XMLReader::ELEMENT && $reader->name == 'category') {


            if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');
            if(null !== ($reader->getAttribute('parentId')))  $result['parentId'] = $reader->getAttribute('parentId');
            if(null !== ($reader->readString('id')))  $result['name'] = $reader->readString('id');

            $result['source'] = 'issaplus';

//            echo '<pre>' . print_r($result, true) . '</pre>';
//


            // Дальше создаем 'OffersKarree' - класс Active Record, который сопоставлен с таблицей offers-karree
            // И используя интерфейс Active Record присваиваем атрибутам OffersKarree значения используя ключи массива $result

            $categoriesIssaplus = new CategoriesIssaplus();
            if(isset($result['id'])) $categoriesIssaplus->id = $result['id'];
            if(isset($result['parentId'])) $categoriesIssaplus->parent_id = $result['parentId'];
            if(isset($result['name'])) $categoriesIssaplus->name = $result['name'];
            if(isset($result['source'])) $categoriesIssaplus->source = $result['source'];


//
//                $categoriesIssaplus->save();

            $importCategory = new ImportCategory();
            if(isset($result['id'])) $importCategory->import_id = $result['id'];
            if(isset($result['parentId'])) $importCategory->parent_id = $result['parentId'];
            if(isset($result['name'])) $importCategory->name = $result['name'];
            if(isset($result['source'])) $importCategory->source = $result['source'];



//            $importCategory->save();

            }

        }
            // Закрывает ввод, который в настоящий момент анализирует объект XMLReader.
            $reader->close();
        return $this->render('import');

    }
    public function actionMagic() {


        $command = Yii::$app->db->createCommand('SELECT MAX(`id`) FROM `offers_issaplus`')->queryOne();
        $maxIndex =$command['MAX(`id`)'];

//        echo $maxIndex .'<br>';
        $i = 1; $y = 0; $u = 0; $o = 0;
        while ($i<=$maxIndex) {
            $command1 = Yii::$app->db->createCommand('SELECT `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `attr_color`.`name_displayed`, `attr_color`.`color_id`, `attr_color`.`sort_name_displayed`, `attr_color`.`sort_color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`,`offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_color` WHERE `offers_issaplus`.`color_name` = `attr_color`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post1 = $command1->queryOne();
            $values[$i] = $post1;
            $y = $values[$i]['id'];
            $name_displayed = $values[$i]['name_displayed'];
            $color_id = $values[$i]['color_id'];
            $sort_name_displayed = $values[$i]['sort_name_displayed'];
            $sort_color_id = $values[$i]['sort_color_id'];

if ($y > 0) {
            $command2 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `color_name` =:color_name, `color_id` =:color_id, `sort_color_name` =:sort_color_name, `sort_color_id` =:sort_color_id  WHERE id=:id'
    )
        ->bindParam(':color_name', $name_displayed )
        ->bindParam(':color_id', $color_id)
        ->bindParam(':sort_color_name', $sort_name_displayed)
        ->bindParam(':sort_color_id', $sort_color_id)
        ->bindParam(':id', $y);
        $command2->execute();
    } else { }
//        $i++;

            $command3 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `attr_style`.`name_displayed`, `attr_style`.`style_id`,`offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
          FROM `offers_issaplus`, `attr_style` WHERE `offers_issaplus`.`style_name` = `attr_style`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post2 = $command3->queryOne();
            $values2[$i] = $post2;
            $u = $values2[$i]['id'];
            $name_displayed = $values2[$i]['name_displayed'];
            $style_id = $values2[$i]['style_id'];

            if ($u > 0) {
                $command4 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `style_name` =:style_name, `style_id` =:style_id  WHERE id=:id'
                )
                    ->bindParam(':style_name', $name_displayed )
                    ->bindParam(':style_id', $style_id)
                    ->bindParam(':id', $u);
                $command4->execute();
            } else { }

            $command5 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_sizes` WHERE `offers_issaplus`.`sizes_1` = `attr_sizes`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post3 = $command5->queryOne();
            $values3[$i] = $post3;
            $o = $values3[$i]['id'];
            $name_displayed = $values3[$i]['name_displayed'];
            $sizes_id = $values3[$i]['sizes_id'];

            if ($o > 0) {
                $command6 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `sizes_1` =:sizes_1, `sizes_1_id` =:sizes_1_id  WHERE id=:id'
                )
                    ->bindParam(':sizes_1', $name_displayed )
                    ->bindParam(':sizes_1_id', $sizes_id)
                    ->bindParam(':id', $o);
                $command6->execute();
            } else { }

            $command7 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`, `offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_sizes` WHERE `offers_issaplus`.`sizes_2` = `attr_sizes`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post4 = $command7->queryOne();
            $values4[$i] = $post4;
            $s2 = $values4[$i]['id'];
            $name_displayed = $values4[$i]['name_displayed'];
            $sizes_id = $values4[$i]['sizes_id'];


            if ($s2 > 0) {
                $command8 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `sizes_2` =:sizes_2, `sizes_2_id` =:sizes_2_id  WHERE id=:id'
                )
                    ->bindParam(':sizes_2', $name_displayed )
                    ->bindParam(':sizes_2_id', $sizes_id)
                    ->bindParam(':id', $s2);
                $command8->execute();
            } else { }

            $command9 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`, `offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_sizes` WHERE `offers_issaplus`.`sizes_3` = `attr_sizes`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post5 = $command9->queryOne();
            $values5[$i] = $post5;
            $s3 = $values5[$i]['id'];
            $name_displayed = $values5[$i]['name_displayed'];
            $sizes_id = $values5[$i]['sizes_id'];

            if ($s3 > 0) {
                $command10 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `sizes_3` =:sizes_3, `sizes_3_id` =:sizes_3_id  WHERE id=:id'
                )
                    ->bindParam(':sizes_3', $name_displayed )
                    ->bindParam(':sizes_3_id', $sizes_id)
                    ->bindParam(':id', $s3);
                $command10->execute();
            } else { }

            $command11 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`, `offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`,`offers_issaplus`.`sizes_3_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_sizes` WHERE `offers_issaplus`.`sizes_4` = `attr_sizes`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post6 = $command11->queryOne();
            $values6[$i] = $post6;
            $s4 = $values6[$i]['id'];
            $name_displayed = $values6[$i]['name_displayed'];
            $sizes_id = $values6[$i]['sizes_id'];

            if ($s4 > 0) {
                $command12 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `sizes_4` =:sizes_4, `sizes_4_id` =:sizes_4_id  WHERE id=:id'
                )
                    ->bindParam(':sizes_4', $name_displayed )
                    ->bindParam(':sizes_4_id', $sizes_id)
                    ->bindParam(':id', $s4);
                $command12->execute();
            } else { }

            $command13 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`, `offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `attr_sizes`.`name_displayed`, `attr_sizes`.`sizes_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `attr_sizes` WHERE `offers_issaplus`.`sizes_5` = `attr_sizes`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post7 = $command13->queryOne();
            $values7[$i] = $post7;
            $s5 = $values7[$i]['id'];
            $name_displayed = $values7[$i]['name_displayed'];
            $sizes_id = $values7[$i]['sizes_id'];

            if ($s5 > 0) {
                $command14 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `sizes_5` =:sizes_5, `sizes_5_id` =:sizes_5_id  WHERE id=:id'
                )
                    ->bindParam(':sizes_5', $name_displayed )
                    ->bindParam(':sizes_5_id', $sizes_id)
                    ->bindParam(':id', $s5);
                $command14->execute();
            } else { }


                      $command17 = Yii::$app->db->createCommand('SELECT  `offers_issaplus`.`id`, `offers_issaplus`.`sku`, `offers_issaplus`.`category_name`, `offers_issaplus`.`category_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `attr_collection`.`name_displayed`, `attr_collection`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`,`offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
          FROM `offers_issaplus`, `attr_collection` WHERE `offers_issaplus`.`collection_name` = `attr_collection`.`input_name` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post9 = $command17->queryOne();
            $values9[$i] = $post9;
            $u = $values9[$i]['id'];
            $name_displayed = $values9[$i]['name_displayed'];
            $collection_id = $values9[$i]['collection_id'];

            if ($u > 0) {
                $command18 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `collection_name` =:collection_name, `collection_id` =:collection_id  WHERE id=:id'
                )
                    ->bindParam(':collection_name', $name_displayed )
                    ->bindParam(':collection_id', $collection_id)
                    ->bindParam(':id', $u);
                $command18->execute();
            } else { }

            $command15 = Yii::$app->db->createCommand('SELECT `offers_issaplus`.`id`, `match_table_issaplus`.`param_id`, `offers_issaplus`.`sku`, `match_table_issaplus`.`name_cat`, `match_table_issaplus`.`catalog_id`, `offers_issaplus`.`source`, `offers_issaplus`.`model`, `offers_issaplus`.`url`, `offers_issaplus`.`collection_name`, `offers_issaplus`.`collection_id`, `offers_issaplus`.`color_name`, `offers_issaplus`.`color_id`, `offers_issaplus`.`style_name`, `offers_issaplus`.`style_id`,`offers_issaplus`.`sizes_1`, `offers_issaplus`.`sizes_1_id`, `offers_issaplus`.`sizes_2`, `offers_issaplus`.`sizes_2_id`, `offers_issaplus`.`sizes_3`, `offers_issaplus`.`sizes_3_id`, `offers_issaplus`.`sizes_4`, `offers_issaplus`.`sizes_4_id`, `offers_issaplus`.`sizes_5`, `offers_issaplus`.`sizes_5_id`, `offers_issaplus`.`price`, `offers_issaplus`.`price_uah`, `offers_issaplus`.`price_rub`, `offers_issaplus`.`price_usd`, `offers_issaplus`.`opt_price_uah`, `offers_issaplus`.`opt_price_rub`, `offers_issaplus`.`opt_price_usd`, `offers_issaplus`.`main_img`, `offers_issaplus`.`gallery_1`, `offers_issaplus`.`gallery_2`, `offers_issaplus`.`gallery_3`, `offers_issaplus`.`gallery_4`, `offers_issaplus`.`gallery_5`
        FROM `offers_issaplus`, `match_table_issaplus` WHERE `offers_issaplus`.`category_id` = `match_table_issaplus`.`issaplus_id` && `offers_issaplus`.`id` =:id')
                ->bindParam(':id', $i);

            $post8 = $command15->queryOne();
            $values8[$i] = $post8;
            $pa = $values8[$i]['id'];
            $name_displayed = $values8[$i]['name_cat'];
            $catalog_id = $values8[$i]['catalog_id'];
            $param_id = $values8[$i]['param_id'];



            if ($pa > 0) {

                $command16 = Yii::$app->db->createCommand('UPDATE `offers_issaplus` SET `param_id` =:param_id , `category_name` =:category_name, `category_id` =:category_id WHERE id=:id'
                )
                    ->bindParam(':category_name', $name_displayed )
                    ->bindParam(':category_id', $catalog_id)
                    ->bindParam(':param_id', $param_id)
                    ->bindParam(':id', $pa);
                $command16->execute();
            } else { }

            $i++;
        }
        return $this->render('magic');

    }



    protected function findModel($id)
    {
        if (($model = OffersIssaplus::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
