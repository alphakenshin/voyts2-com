<?php

namespace backend\controllers;

use backend\models\NpCities;
use LisDev\Delivery\NovaPoshtaApi2;
use Yii;
use backend\models\NpAreas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NpAreasController implements the CRUD actions for NpAreas model.
 */
class NpAreasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NpAreas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NpAreas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionImport()
    {
        $np = new NovaPoshtaApi2('9d68a93f6bcaf2bdd6e6d92c61abd22d');

//        $res = $np->documentsTracking('20450197975131');

        $result = $np
            ->model('Address')
            ->method('getAreas')
            ->params(array(
                'Имя_параметра_1' => 'Значение_параметра_1',
                'Имя_параметра_2' => 'Значение_параметра_2',
            ))
            ->execute();

        $i=-1;

//        $json = json_encode($result['data']);
//        $res = json_decode( $json, true );
        foreach ($result['data'] as $res) {
            $i++;
            $np_areas = new NpAreas();

            if(isset($res['Ref']))   $np_areas->ref = $res['Ref'];
            if(isset($res['AreasCenter'])) $np_areas->areas_center  = $res['AreasCenter'];
            if(isset($res['Description'])) $np_areas->description  = $res['Description'];




//            echo '<pre>' . print_r($res, true) . '</pre>';


            $np_areas->save();

        }

        return $this->render('import');
    }




    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NpAreas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NpAreas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NpAreas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NpAreas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NpAreas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NpAreas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NpAreas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
