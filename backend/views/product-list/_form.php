<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'available')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'currency_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'barcode')->textInput() ?>

    <?= $form->field($model, 'sales_notes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'composition')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_eu_10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'S')->textInput() ?>

    <?= $form->field($model, 'M')->textInput() ?>

    <?= $form->field($model, 'L')->textInput() ?>

    <?= $form->field($model, 'XL')->textInput() ?>

    <?= $form->field($model, 'XXL')->textInput() ?>

    <?= $form->field($model, 'XXXL')->textInput() ?>

    <?= $form->field($model, 'cloth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'season')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'for_full')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'silhouette')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'print')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutout_collar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'additional_details')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeve')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
