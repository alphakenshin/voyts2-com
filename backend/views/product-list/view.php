<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'available',
            'price',
            'category_id',
            'currency_id',
            'source',
            'main_img',
            'gallery_1',
            'gallery_2',
            'gallery_3',
            'gallery_4',
            'gallery_5',
            'brand',
            'model',
            'vendor_code',
            'delivery',
            'description:ntext',
            'country_of_origin',
            'barcode',
            'sales_notes',
            'composition',
            'sizes_eu_1',
            'sizes_eu_2',
            'sizes_eu_3',
            'sizes_eu_4',
            'sizes_eu_5',
            'sizes_eu_6',
            'sizes_eu_7',
            'sizes_eu_8',
            'sizes_eu_9',
            'sizes_eu_10',
            'S',
            'M',
            'L',
            'XL',
            'XXL',
            'XXXL',
            'cloth',
            'color',
            'style',
            'season',
            'product_length',
            'for_full',
            'silhouette',
            'print',
            'cutout_collar',
            'additional_details',
            'sleeve',
        ],
    ]) ?>

</div>
