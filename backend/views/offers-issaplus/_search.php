<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersIssaplusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-issaplus-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sku') ?>

    <?= $form->field($model, 'category_id') ?>

    <?= $form->field($model, 'source') ?>

    <?= $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'collection') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'style') ?>

    <?php // echo $form->field($model, 'size_1') ?>

    <?php // echo $form->field($model, 'size_2') ?>

    <?php // echo $form->field($model, 'size_3') ?>

    <?php // echo $form->field($model, 'size_4') ?>

    <?php // echo $form->field($model, 'size_5') ?>

    <?php // echo $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'price_uah') ?>

    <?php // echo $form->field($model, 'price_rub') ?>

    <?php // echo $form->field($model, 'price_usd') ?>

    <?php // echo $form->field($model, 'opt_price_uah') ?>

    <?php // echo $form->field($model, 'opt_price_rub') ?>

    <?php // echo $form->field($model, 'opt_price_usd') ?>

    <?php // echo $form->field($model, 'main_img') ?>

    <?php // echo $form->field($model, 'gallery_1') ?>

    <?php // echo $form->field($model, 'gallery_2') ?>

    <?php // echo $form->field($model, 'gallery_3') ?>

    <?php // echo $form->field($model, 'gallery_4') ?>

    <?php // echo $form->field($model, 'gallery_5') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
