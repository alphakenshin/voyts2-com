<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersIssaplus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-issaplus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'param_id')->textInput() ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collection_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collection_id')->textInput() ?>

    <?= $form->field($model, 'color_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_id')->textInput() ?>

    <?= $form->field($model, 'style_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style_id')->textInput() ?>

    <?= $form->field($model, 'sizes_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_1_id')->textInput() ?>

    <?= $form->field($model, 'sizes_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_2_id')->textInput() ?>

    <?= $form->field($model, 'sizes_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_3_id')->textInput() ?>

    <?= $form->field($model, 'sizes_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_4_id')->textInput() ?>

    <?= $form->field($model, 'sizes_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_5_id')->textInput() ?>

    <?= $form->field($model, 'price_uah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_rub')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_usd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_uah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_rub')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_usd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'updated_by')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
