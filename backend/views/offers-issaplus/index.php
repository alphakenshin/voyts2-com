<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Issapluses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-issaplus-index">



    <p>Если список товаров пуст - запустите импорт товаров Issaplus <br></p>
    <p></p>
        <?= Html::a('Импорт товаров Issaplus', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'sku',
//            'param_id',
//            'url:url',
            'price',
            //'category_name',
            //'category_id',
            //'source',
            [
                'attribute' => 'available',
                'value' => function($data) {
                    $str11 = '';
                    if($data->available === 'true') {$str11 = '<span class="text-success">Да</span>';}
                    if($data->available === 'false') {$str11 = '<span class="text-success">Нет</span>';}
//                    else {$str11 = '<span class="text-success">Нет</span>';}
                    return  $str11;
                },
                'format' => 'html',
            ],
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            'model',
            'collection_name',
            //'collection_id',
            'color_name',
            //'color_id',
            //'style_name',
            //'style_id',
            'sizes_1',
            //'sizes_1_id',
            //'sizes_2',
            //'sizes_2_id',
            //'sizes_3',
            //'sizes_3_id',
            //'sizes_4',
            //'sizes_4_id',
            //'sizes_5',
            //'sizes_5_id',
            //'price_uah',
            //'price_rub',
            //'price_usd',
            //'opt_price_uah',
            //'opt_price_rub',
            //'opt_price_usd',
            //'created_at',
            //'created_by',
            //'updated_by',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
