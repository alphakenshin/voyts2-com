<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары с ресурса issaplus.com успешно импортированы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-issaplus-index">

    <p>
        Запустите поиск соответствия по словарям для настройки Sphinx индексов <br></p>
    <p></p><br>

        <?= Html::a('Запустить поиск', ['magic'], ['class' => 'btn btn-success']) ?>
    </p>

</div>