<?php
//use PDO;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\OffersIssaplusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Issapluses';
$this->params['breadcrumbs'][] = $this->title;

$ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

$request = Yii::$app->request;


$docs = array();
$start = 0;
$offset = 100;
$current = 1;
if (isset($_GET['start'])) {
    $start = $_GET['start'];
    $current = $start / $offset + 1;
}

$search_query = $query = trim($request->get('query'));

$select = array();
$where = array();
$where_price = array();
$where_sizes = array();
$where_size_1 = array();
$where_category = array();
$where_collection = array();
$where_color = array();
$where_sizes = array();
$where_size_1 = array();
$where_style = array();
$where_price = array();

if (isset($_GET['sizes'])) {
    $w = implode(',', $_GET['sizes']);
    $where['sizes'] = 'sizes in (' . $w . ') ';
}

if (isset($_GET['gallery_1'])) {
    $w = implode(',', $_GET['gallery_1']);
    $where['gallery_1'] = ' gallery_1 in (' . $w . ') ';
}

if (isset($_GET['category_name'])) {
    $w = implode(',', $_GET['category_name']);
    $where['category_name'] = ' category_name in (' . $w . ') ';
}

if (isset($_GET['category_id'])) {
    $w = implode(',', $_GET['category_id']);
    $where['category_id'] = ' category_id in (' . $w . ') ';
}

if (isset($_GET['style_id'])) {
    $w = implode(',', $_GET['style_id']);
    $where['style_id'] = ' style_id in (' . $w . ') ';
}

if (isset($_GET['price'])) {
    $w = array();
    foreach ($_GET['price'] as $c) {
        $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
    }
    $w = implode(' OR ', $w);
    $select['price'] = 'IF(' . $w . ',1,0) as w_p';
    $where['price'] = 'w_p = 1';
}
if (isset($_GET['size_1_id'])) {
    $w = implode(',', $_GET['size_1_id']);
    $where['size_1_id'] = ' size_1_id in (' . $w . ') ';
}
//if (isset($_GET['size_1'])) {
//
//    $search_query .= ' @size_1 ' . implode('|', $_GET['size_1']);
//}
if (count($where) > 0) {
    $where_category = $where_collection = $where_color = $where_sizes = $where_size_1 = $where_style = $where_price = $where;
    if (isset($where_style['style_id'])) {
        unset($where_style['style_id']);
    }
    if (count($where_style) > 0) {
        $where_style = ' AND ' . implode(' AND ', $where_style);
    } else {
        $where_style = '';
    }

    if (isset($where_category['category_id'])) {
        unset($where_category['category_id']);
    }
    if (count($where_category) > 0) {
        $where_category = ' AND ' . implode(' AND ', $where_category);
    } else {
        $where_category = '';
    }

    if (isset($where_price['price'])) {
        unset($where_price['price']);
    }
    if (count($where_price) > 0) {
        $where_price = ' AND ' . implode(' AND ', $where_price);
    } else {
        $where_price = '';
    }

    if (isset($where_collection['category_name'])) {
        unset($where_collection['category_name']);
    }
    if (count($where_collection) > 0) {
        $where_collection = ' AND ' . implode(' AND ', $where_collection);
    } else {
        $where_collection = '';
    }

    if (isset($where_color['gallery_1'])) {
        unset($where_color['gallery_1']);
    }
    if (count($where_color) > 0) {
        $where_color = ' AND ' . implode(' AND ', $where_color);
    } else {
        $where_color = '';
    }



    if (isset($where_size_1['size_1_id'])) {
        unset($where_size_1['size_1_id']);
    }
    if (count($where_size_1) > 0) {
        $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
    } else {
        $where_size_1 = '';
    }

    if (isset($where_sizes['sizes'])) {
        unset($where_sizes['sizes']);
    }
    if (count($where_sizes) > 0) {
        $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
    } else {
        $where_sizes = '';
    }

//    $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
//    $where = ' AND ' . implode(' AND ', $where);
} else {
    $where_category = $where_collection = $where_color  = $where_sizes = $where_size_1 = $where_style = $where_price = $where = '';
}
if (count($select) > 0) {
    $select = ',' . implode(',', $select);
} else {
    $select = '';
}
$indexes = 'sphinx_index_offers_issaplus';

$stmt = $ln_sph->prepare("SELECT * from sphinx_index_offers_issaplus  WHERE MATCH(:match) $where  LIMIT $start,$offset ");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$docs = $stmt->fetchAll();

$meta = $ln_sph->query("SHOW META")->fetchAll();
foreach ($meta as $m) {
    $meta_map[$m['Variable_name']] = $m['Value'];
}
//'<pre></pre>' . print_r($meta_map) . '<pre></pre>';

$total_found = $meta_map['total_found'];
$total = $meta_map['total'];


$ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

//$sql = array();
//$rows = array();
//$sql[] = "SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_category GROUP BY category_id  LIMIT 0,2000";
////        '<pre>' . print_r($sql) . '</pre>';
//
////       '<pre>' . print_r($sql) . '</pre>';
//
//
//$sql = implode('; ', $sql);
////        '<pre>' . print_r($sql) . '</pre>';
//$stmt = $ln_sph->prepare($sql);
//$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
//$stmt->execute();
//$category_id = $stmt->fetchAll();



//echo($category_id[0][0]) . '<br></pre>';
//echo($category_id[0][1]) . '<br></pre>';
//echo($category_id[0][2]) . '<br></pre>';
//echo($category_id[0][3]) . '<br></pre>';
//echo($category_id[0][4]) . '<br></pre>';
//echo($category_id[0][5]) . '<br></pre>';
// expressions are not yet supported in multi-query optimization,so we run them separate
//foreach ($attrs as $attr) {
//    $rows[$attr] = $stmt->fetchAll();
//    '<pre>' . print_r($rows[$attr]) . '</pre>';
//    $stmt->nextRowset();
//}

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_category  GROUP BY category_id ORDER BY category_id ASC  LIMIT 0,1000");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$category_id = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY category_name ORDER BY category_name ASC  LIMIT 0,1000");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$category_name = $stmt->fetchAll();

$stmt = $ln_sph->prepare("select groupby(), group_concat(id) as selected,COUNT(*) as cnt from $indexes where match(:match) $where_sizes group by sizes  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$sizes = $stmt->fetchAll();
'<pre>' . print_r($sizes) . '</pre>';


$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_size_1  GROUP BY size_1_id ORDER BY size_1_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$size_1_id = $stmt->fetchAll();
//'<pre>' . print_r($size_1_id) . '</pre>';
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$style_id = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY gallery_1 ORDER BY gallery_1 ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$gallery_1 = $stmt->fetchAll();

//$stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id ORDER BY color_id ASC  LIMIT 0,10");
//$stmt->bindValue(':match', $query, PDO::PARAM_STR);
//$stmt->execute();
//$color_id = $stmt->fetchAll();
//    '<pre>' . print_r($category_name) . '</pre>';


echo($category_id[1][0]) . '- 0<br></pre>';
echo($category_id[1][1]) . '- 1<br></pre>';
echo($category_id[1][2]) . '- 2<br></pre>';
echo($category_id[1][3]) . '- 3<br></pre>';
echo($category_id[1][4]) . '- 4<br></pre>';
echo($category_id[1][5]) . '- 5<br></pre>';
echo($category_id[1][6]) . '- 6<br></pre>';
echo($category_id[1][7]) . '- 7<br></pre>';
echo($category_id[1][8]) . '- 8<br></pre>';
echo($category_id[1][9]) . '- 9<br></pre>';
echo($category_id[1][10]) . '- 10<br></pre>';
//echo($category_id[1][11]) . ' 11<br></pre>';

$facets = array();
foreach ($category_id as $p) {
    $facets['category_id'][] = array(
        'value' => $p['category_id'],
        'count' => $p['cnt']
//        'name' => $p[1]
    );
}
foreach ($category_name as $p) {
    $facets['category_name'][] = array(
        'value' => $p['category_name'],
        'count' => $p['cnt']
//        'name' => $p[3]
    );
}
foreach ($size_1_id as $p) {
    $facets['size_1_id'][] = array(
        'value' => $p['size_1_id'],
        'count' => $p['cnt'],
        'name' => $p[9]
    );
}
//foreach ($sizes as $p) {
//    $facets['sizes'][] = array(
//        'value' => $p['sizes'],
//        'count' => $p['cnt']
//
//    );
//}
foreach ($gallery_1 as $p) {
    $facets['gallery_1'][] = array(
        'value' => $p['gallery_1'],
        'count' => $p['cnt']
//        'name' => $p[5]
    );
}
foreach ($style_id as $p) {
    $facets['style_id'][] = array(
        'value' => $p['style_id'],
        'count' => $p['cnt'],
        'name' => $p[7]
    );
}
//foreach ($style as $p) {
//    $facets['style'][] = array(
//        'value' => $p['style'],
//        'count' => $p['cnt'],
//    );
//}
foreach ($prices as $p) {
    $facets['price'][] = array(
        'value' => $p['price_seg'],
        'count' => $p['cnt']
    );
}


//foreach ($rows as $k => $v) {
//    foreach ($v as $x) {
//        $facets[$k][] = array(
//            'value' => $x['selected'],
//            'count' => $x['cnt']
//        );
//    }
//}
?>
<form method="GET" action="" id="search_form">
    <br> <br>   <div class="container"><header>
            <h1>Одежда для женщин</h1>
        </header>
        <div class="col-xs-12 flex-active"><div class="row-fluid">
                <div class="span2">
                    <br> <br>
<!--               --><?php //    '<pre>' . print_r($sizes) . '</pre>'; ?>

                    <?php if (isset($facets['category_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Категории</legend>
                                <div class="control-group">
                                    <?php foreach($facets['category_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="category_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['category_id']) && (in_array($item['value'],$_GET['category_id'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_category_id" value="Reset" data-target="category_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['price'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Стоимость</legend>
                                <div class="control-group">
                                    <?php foreach($facets['price'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox" name="price[]"
                                                                        value="<?=$item['value'];?>"
                                                <?=(($request->get('price')!== null) && (in_array($item['value'],$request->get('price'))))?'checked':'';?>>
                                            <?=($item['value']*200).'-'.(($item['value']+1)*200).' грн ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_price" value="Reset" data-target="price" class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['category_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Категория</legend>
                                <div class="control-group">
                                    <?php foreach($facets['category_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="category_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['category_name']) && (in_array($item['value'],$_GET['category_name'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_category_name" value="Reset" data-target="category_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['gallery_1'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Цвет</legend>
                                <div class="control-group">
                                    <?php foreach($facets['gallery_1'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="gallery_1[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['gallery_1']) && (in_array($item['value'],$_GET['gallery_1'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_gallery_1" value="Reset" data-target="gallery_1"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['category_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Категории ID</legend>
                                <div class="control-group">
                                    <?php foreach($facets['category_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="category_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['category_id']) && (in_array($item['value'],$_GET['category_id'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_category_id" value="Reset" data-target="category_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['size_1_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Размер 1</legend>
                                <div class="control-group">
                                    <?php foreach($facets['size_1_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="size_1_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['size_1_id']) && (in_array($item['value'],$_GET['size_1_id'])))?'checked':'';?>>
                                            <?= $item['name'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_size_1_id" value="Reset" data-target="size_1_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

<!--                    --><?php //if (isset($facets['sizes'])): ?>
<!--                        <div class="col-sm-3"><fieldset>-->
<!--                                <legend>Размеры 2</legend>-->
<!--                                <div class="control-group">-->
<!--                                    --><?php //foreach($facets['sizes'] as $item):?>
<!--                                        <label class="checkbox"> <input type="checkbox"-->
<!--                                                                        name="sizes[]" value="--><?//=$item['value'];?><!--"-->
<!--                                                --><?//=(isset($_GET['sizes']) && (in_array($item['value'],$_GET['sizes'])))?'checked':'';?>
<!--                                            --><?//= $item['value'].' ('.$item['count'].')'?>
<!--                                        </label>-->
<!--                                    --><?php //endforeach;?>
<!--                                    <input type="button"-->
<!--                                           name="reset_sizes" value="Reset" data-target="sizes"-->
<!--                                           class="btn">-->
<!--                                </div>-->
<!--                            </fieldset></div>-->
<!--                    --><?php //endif; ?>

                </div>
            </div></div>
        <div class="clearfix"></div>
        <br><br>
        <div class="span9">
            <div class="container">


                <div class="row">



                    <input type="text" class="input-large" name="query" id="suggest"
                           autocomplete="off"
                           value="<?=  ($request->get('query')!== null)?htmlentities($request->get('query')):''?>">
                    <input type="submit" class="btn btn-primary" id="send"
                           name="send" value="Submit">
                    <button type="reset" class="btn " value="Reset">Reset</button>
                </div>

                <br>
                <br>
                <div class="row">
                    <?php if (count($docs) > 0): ?>
                        <p class="lead">
                            Всего найдено:<?=$total_found?>
                        </p>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                        <div class="span9">


                                <?php $i = 0; foreach ($docs as $doc): ?>
                                <li>
                                    <a class="cbp-vm-image" href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $doc['id']])?>">
                                        <div class="simpleCart_shelfItem">
                                            <div class="view view-first">
                                                <div class="inner_content clearfix">
                                                    <div class="product_image">
<!--                                                        <img src="--><?//=  $doc['gallery_1'] ?><!--" alt="--><?//=  $doc['model'] ?><!--">-->
                                                        <div class="mask">
                                                            <div class="info">Стиль: <?= $doc['style_name'] ?></div>
                                                        </div>
                                                        <div class="product_container">
                                                            <div class="cart-left">
                                                                <p class="title"><?= $doc['model'] ?></p>
                                                            </div>
                                                            <div class="pricey"><span class="item_price"><?= $doc['price'] ?> грн. </span></div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </a>
                                    <div class="cbp-vm-details">

                                    </div>
                                    <a class="cbp-vm-icon cbp-vm-add item_add" data-id="<?= $doc['id'] ?>" href="#">Add to cart</a>
                        </div>
                    <?php $i++ ?>
                    <?php if ($i % 4 == 0): ?>
                    <div class="clearfix"></div>

                    <?php endif; ?>
                    <?php endforeach; ?>


                        </div>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                    <?php elseif (isset($_GET['query']) && $_GET['query'] != ''): ?>
                        <p class="lead">Nothing found!</p>
                    <?php endif; ?>
                </div>
            </div>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="/js/bootstrap-3.1.1.min.js"></script>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script>
    function __highlight(s, t) {
        console.log('tester1');
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(document).ready(function() {
        $(':checkbox').change(function() {
            console.log('tester2');
            $("#search_form").trigger('submit');

        });
        $(':reset').click(function(){
            location.search ='';
            console.log('tester3');
        });
        $('input[name^=reset_]').click(function(){
            $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
            $("#search_form").trigger('submit');
            console.log('tester4');

        });
    });
</script>
