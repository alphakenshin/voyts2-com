<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersIssaplus */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Issapluses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="offers-issaplus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sku',
            'param_id',
            'url:url',
            'price',
            'category_name',
            'category_id',
            'source',
            'main_img',
            'gallery_1',
            'gallery_2',
            'gallery_3',
            'gallery_4',
            'gallery_5',
            'model',
            'collection_name',
            'collection_id',
            'color_name',
            'color_id',
            'style_name',
            'style_id',
            'sizes_1',
            'sizes_1_id',
            'sizes_2',
            'sizes_2_id',
            'sizes_3',
            'sizes_3_id',
            'sizes_4',
            'sizes_4_id',
            'sizes_5',
            'sizes_5_id',
            'price_uah',
            'price_rub',
            'price_usd',
            'opt_price_uah',
            'opt_price_rub',
            'opt_price_usd',
            'created_at',
            'created_by',
            'updated_by',
            'updated_at',
        ],
    ]) ?>

</div>
