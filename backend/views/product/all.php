<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар всех производителей сведен вместе.  Обновите связи с фото товара ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <p></p>
    <?= Html::a('Обновить связь с фото ', ['image'], ['class' => 'btn btn-success']) ?>
    </p>

</div>
