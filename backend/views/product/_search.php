<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ProductSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'type') ?>

    <?= $form->field($model, 'available') ?>

    <?= $form->field($model, 'price') ?>

    <?= $form->field($model, 'category_id') ?>

    <?php // echo $form->field($model, 'currency_id') ?>

    <?php // echo $form->field($model, 'source') ?>

    <?php // echo $form->field($model, 'main_img') ?>

    <?php // echo $form->field($model, 'gallery_1') ?>

    <?php // echo $form->field($model, 'gallery_2') ?>

    <?php // echo $form->field($model, 'gallery_3') ?>

    <?php // echo $form->field($model, 'gallery_4') ?>

    <?php // echo $form->field($model, 'gallery_5') ?>

    <?php // echo $form->field($model, 'brand') ?>

    <?php // echo $form->field($model, 'model') ?>

    <?php // echo $form->field($model, 'vendor_code') ?>

    <?php // echo $form->field($model, 'delivery') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'country_of_origin') ?>

    <?php // echo $form->field($model, 'barcode') ?>

    <?php // echo $form->field($model, 'sales_notes') ?>

    <?php // echo $form->field($model, 'composition') ?>

    <?php // echo $form->field($model, 'sizes_eu_1') ?>

    <?php // echo $form->field($model, 'sizes_eu_2') ?>

    <?php // echo $form->field($model, 'sizes_eu_3') ?>

    <?php // echo $form->field($model, 'sizes_eu_4') ?>

    <?php // echo $form->field($model, 'sizes_eu_5') ?>

    <?php // echo $form->field($model, 'sizes_eu_6') ?>

    <?php // echo $form->field($model, 'sizes_eu_7') ?>

    <?php // echo $form->field($model, 'sizes_eu_8') ?>

    <?php // echo $form->field($model, 'sizes_eu_9') ?>

    <?php // echo $form->field($model, 'sizes_eu_10') ?>

    <?php // echo $form->field($model, 'S') ?>

    <?php // echo $form->field($model, 'M') ?>

    <?php // echo $form->field($model, 'L') ?>

    <?php // echo $form->field($model, 'XL') ?>

    <?php // echo $form->field($model, 'XXL') ?>

    <?php // echo $form->field($model, 'XXXL') ?>

    <?php // echo $form->field($model, 'cloth') ?>

    <?php // echo $form->field($model, 'color') ?>

    <?php // echo $form->field($model, 'style') ?>

    <?php // echo $form->field($model, 'season') ?>

    <?php // echo $form->field($model, 'product_length') ?>

    <?php // echo $form->field($model, 'for_full') ?>

    <?php // echo $form->field($model, 'silhouette') ?>

    <?php // echo $form->field($model, 'print') ?>

    <?php // echo $form->field($model, 'cutout_collar') ?>

    <?php // echo $form->field($model, 'additional_details') ?>

    <?php // echo $form->field($model, 'sleeve') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
