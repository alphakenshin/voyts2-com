<?php

use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\ElFinder;
mihaildev\elfinder\Assets::noConflict($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>


    <?= $form->field($model, 'param_id')->textInput() ?>

    <?= $form->field($model, 'available')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'hit')->checkbox([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'new')->checkbox([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'sale')->checkbox([ '0', '1', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store')->textInput() ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?php  echo $form->field($model, 'description')->widget(CKEditor::className(),[
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',['rows' => 6]) ]);
    ?>

    <?= $form->field($model, 'manufacturer_warranty')->textInput() ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'barcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_type_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_type_id')->textInput() ?>

    <?= $form->field($model, 'sales_notes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'composition_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'composition_id')->textInput() ?>

    <?= $form->field($model, 'collection_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collection_id')->textInput() ?>

    <?= $form->field($model, 'silhouette_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'silhouette_id')->textInput() ?>

    <?= $form->field($model, 'cloth_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cloth_id')->textInput() ?>

    <?= $form->field($model, 'color_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_id')->textInput() ?>

    <?= $form->field($model, 'print_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'print_id')->textInput() ?>

    <?= $form->field($model, 'style_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style_id')->textInput() ?>

    <?= $form->field($model, 'season_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'season_id')->textInput() ?>

    <?= $form->field($model, 'for_full')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'brand_id')->textInput() ?>

    <?= $form->field($model, 'product_length_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_length_id')->textInput() ?>

    <?= $form->field($model, 'sleeve_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeve_id')->textInput() ?>

    <?= $form->field($model, 'cutout_collar_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutout_collar_id')->textInput() ?>

    <?= $form->field($model, 'additional_details_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'additional_details_id')->textInput() ?>

    <?= $form->field($model, 'sizes_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_1_id')->textInput() ?>

    <?= $form->field($model, 'sizes_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_2_id')->textInput() ?>

    <?= $form->field($model, 'sizes_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_3_id')->textInput() ?>

    <?= $form->field($model, 'sizes_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_4_id')->textInput() ?>

    <?= $form->field($model, 'sizes_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_5_id')->textInput() ?>

    <?= $form->field($model, 'sizes_6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_6_id')->textInput() ?>

    <?= $form->field($model, 'size')->textInput(['maxlength' => true]) ?>




    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
