<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">



    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'sku',
//            'param_id',
            'available',
//            'url:url',
            'price',
//            'old_price',
//            'currency_id',
            'category_name',
//            'category_id',
            'source',
//            'main_img',
  [
                'attribute' => 'main_img',
                'value'     =>  Html::img("/images/165x110{$model['main_img']}", ['alt' => $model['model'],'height' => 80]),
            'format' => 'html',
],
            [
                'attribute' => 'gallery_1',
                'value'     =>  Html::img("/images/165x110{$model['gallery_1']}", ['alt' => $model['model'],'height' => 80]),
                'format' => 'html',
            ],  [
                'attribute' => 'gallery_2',
                'value'     =>  Html::img("/images/165x110{$model['gallery_2']}", ['alt' => $model['model'],'height' => 80]),
                'format' => 'html',
            ],  [
                'attribute' => 'gallery_3',
                'value'     =>  Html::img("/images/165x110{$model['gallery_3']}", ['alt' => $model['model'],'height' => 80]),
                'format' => 'html',
            ],  [
                'attribute' => 'gallery_4',
                'value'     =>  Html::img("/images/165x110{$model['gallery_4']}", ['alt' => $model['model'],'height' => 80]),
                'format' => 'html',
            ],  [
                'attribute' => 'gallery_5',
                'value'     =>  Html::img("/images/165x110{$model['gallery_5']}", ['alt' => $model['model'],'height' => 80]),
                'format' => 'html',
            ],
//        Html::img("/images/165x110{$data->gallery_1}", ['alt' => $data->model,'height' => 80])
//            'gallery_1',
//            'gallery_2',
//            'gallery_3',
//            'gallery_4',
//            'gallery_5',
//            'delivery',
//            'store',
            'model',
//            'vendor',
//            'vendor_code',
            'description:ntext',
//            'manufacturer_warranty',
//            'country_of_origin',
//            'barcode',
//            'product_type_name',
//            'product_type_id',
//            'sales_notes',
//            'composition_name',
//            'composition_id',
//            'collection_name',
//            'collection_id',
//            'silhouette_name',
//            'silhouette_id',
//            'cloth_name',
//            'cloth_id',
//            'color_name',
//            'color_id',
//            'print_name',
//            'print_id',
//            'style_name',
//            'style_id',
//            'season_name',
//            'season_id',
//            'for_full',
//            'brand_name',
//            'brand_id',
//            'product_length_name',
//            'product_length_id',
//            'sleeve_name',
//            'sleeve_id',
//            'cutout_collar_name',
//            'cutout_collar_id',
//            'additional_details_name',
//            'additional_details_id',
            'size',
            'sizes_1',
            'sizes_1_id',
//            'sizes_2',
//            'sizes_2_id',
//            'sizes_3',
//            'sizes_3_id',
//            'sizes_4',
//            'sizes_4_id',
//            'sizes_5',
//            'sizes_5_id',
//            'sizes_6',
//            'sizes_6_id',
            [
                'attribute' => 'hit',
                'value' => !$model->hit ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>',
                'format' => 'html',
            ],
            //'new',
            [
                'attribute' => 'new',
                'value' =>  !$model->new ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>',
                'format' => 'html',
            ],
            //'sale',
            [
                'attribute' => 'sale',
                'value' => !$model->sale ? '<span class="text-danger">Нет</span>' : '<span class="text-success">Да</span>',
                'format' => 'html',
            ],
        ],
    ]) ?>

</div>
