<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ImportProduct */

$this->title = 'Update Import Product: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Import Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="import-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
