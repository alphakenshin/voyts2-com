<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Import Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="import-product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Import Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product_id',
            'sku',
            'category_id',
            'name',
            //'link_to',
            'collection',
            'color',
            'style',
            'sizes',
            'price_UAH',
            'price_RUB',
            'price_USD',
            //'opt_price_UAH',
            //'opt_price_RUB',
            //'opt_price_USD',
            //'images:ntext',
            //'created_at',
            //'created_by',
            //'updated_by',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
