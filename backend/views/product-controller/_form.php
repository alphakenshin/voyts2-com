<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ImportProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="import-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->textInput() ?>

    <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link_to')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'collection')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_UAH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_RUB')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_USD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_UAH')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_RUB')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'opt_price_USD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'images')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'created_by')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'updated_by')->dropDownList([ '0', '1', '2', '3', '4', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
