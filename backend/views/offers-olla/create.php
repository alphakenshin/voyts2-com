<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersOlla */

$this->title = 'Create Offers Olla';
$this->params['breadcrumbs'][] = ['label' => 'Offers Ollas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-olla-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
