<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Olla';
$this->params['breadcrumbs'][] = $this->title;

$ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

$request = Yii::$app->request;


$docs = array();
$start = 0;
$offset = 20;
$current = 1;
if (isset($_GET['start'])) {
    $start = $_GET['start'];
    $current = $start / $offset + 1;
}

$search_query = $query = trim($request->get('query'));

$select = array();
$where = array();
$where_price = array();
$where_sizes = array();
$where_size_1 = array();
$where_category = array();
$where_collection = array();
$where_color = array();
$where_sizes = array();
$where_size_1 = array();
$where_style = array();
$where_price = array();

if (isset($_GET['cloth_name'])) {
    $w = implode(',', $_GET['cloth_name']);
    $where['cloth_name'] = 'sizes in (' . $w . ') ';
}

if (isset($_GET['additional_details_name'])) {
    $w = implode(',', $_GET['additional_details_name']);
    $where['additional_details_name'] = ' additional_details_name in (' . $w . ') ';
}

if (isset($_GET['print_name'])) {
    $w = implode(',', $_GET['print_name']);
    $where['print_name'] = ' print_name in (' . $w . ') ';
}

if (isset($_GET['category_id'])) {
    $w = implode(',', $_GET['category_id']);
    $where['category_id'] = ' category_id in (' . $w . ') ';
}

if (isset($_GET['brand_name'])) {
    $w = implode(',', $_GET['brand_name']);
    $where['brand_name'] = ' brand_name in (' . $w . ') ';
}

if (isset($_GET['price'])) {
    $w = array();
    foreach ($_GET['price'] as $c) {
        $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
    }
    $w = implode(' OR ', $w);
    $select['price'] = 'IF(' . $w . ',1,0) as w_p';
    $where['price'] = 'w_p = 1';
}
if (isset($_GET['size_1_id'])) {
    $w = implode(',', $_GET['size_1_id']);
    $where['size_1_id'] = ' size_1_id in (' . $w . ') ';
}
//if (isset($_GET['size_1'])) {
//
//    $search_query .= ' @size_1 ' . implode('|', $_GET['size_1']);
//}
if (count($where) > 0) {
    $where_category = $where_collection = $where_color = $where_sizes = $where_size_1 = $where_style = $where_price = $where;
    if (isset($where_style['brand_name'])) {
        unset($where_style['brand_name']);
    }
    if (count($where_style) > 0) {
        $where_style = ' AND ' . implode(' AND ', $where_style);
    } else {
        $where_style = '';
    }

    if (isset($where_category['category_id'])) {
        unset($where_category['category_id']);
    }
    if (count($where_category) > 0) {
        $where_category = ' AND ' . implode(' AND ', $where_category);
    } else {
        $where_category = '';
    }

    if (isset($where_price['price'])) {
        unset($where_price['price']);
    }
    if (count($where_price) > 0) {
        $where_price = ' AND ' . implode(' AND ', $where_price);
    } else {
        $where_price = '';
    }

    if (isset($where_collection['product_type_name'])) {
        unset($where_collection['product_type_name']);
    }
    if (count($where_collection) > 0) {
        $where_collection = ' AND ' . implode(' AND ', $where_collection);
    } else {
        $where_collection = '';
    }

    if (isset($where_color['additional_details_name'])) {
        unset($where_color['additional_details_name']);
    }
    if (count($where_color) > 0) {
        $where_color = ' AND ' . implode(' AND ', $where_color);
    } else {
        $where_color = '';
    }

    if (isset($where_size_1['size_1_id'])) {
        unset($where_size_1['size_1_id']);
    }
    if (count($where_size_1) > 0) {
        $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
    } else {
        $where_size_1 = '';
    }

    if (isset($where_sizes['sizes'])) {
        unset($where_sizes['sizes']);
    }
    if (count($where_sizes) > 0) {
        $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
    } else {
        $where_sizes = '';
    }

//    $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
//    $where = ' AND ' . implode(' AND ', $where);
} else {
    $where_category = $where_collection = $where_color  = $where_sizes = $where_size_1 = $where_style = $where_price = $where = '';
}
if (count($select) > 0) {
    $select = ',' . implode(',', $select);
} else {
    $select = '';
}
$indexes = 'sphinx_index_offers_olla';

$stmt = $ln_sph->prepare("SELECT * from sphinx_index_offers_karree  WHERE MATCH(:match) $where  LIMIT $start,$offset ");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$docs = $stmt->fetchAll();

$meta = $ln_sph->query("SHOW META")->fetchAll();
foreach ($meta as $m) {
    $meta_map[$m['Variable_name']] = $m['Value'];
}
//'<pre></pre>' . print_r($meta_map) . '<pre></pre>';

$total_found = $meta_map['total_found'];
$total = $meta_map['total'];


$ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);


$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_category  GROUP BY category_id ORDER BY category_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$category_id = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY print_name ORDER BY print_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$print_name = $stmt->fetchAll();

$stmt = $ln_sph->prepare("select groupby(), group_concat(id) as selected,COUNT(*) as cnt from $indexes where match(:match) $where_sizes group by sizes  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$sizes = $stmt->fetchAll();
//'<pre>' . print_r($sizes) . '</pre>';


$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_size_1  GROUP BY size_1_id ORDER BY size_1_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$size_1_id = $stmt->fetchAll();
//'<pre>' . print_r($size_1_id) . '</pre>';
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY brand_name ORDER BY brand_name ASC  LIMIT 0,1000");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$brand_name = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY additional_details_name ORDER BY additional_details_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$additional_details_name = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$collection_id = $stmt->fetchAll();
//$stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY brand_name ORDER BY brand_name ASC  LIMIT 0,10");
//$stmt->bindValue(':match', $query, PDO::PARAM_STR);
//$stmt->execute();
//$brand_name = $stmt->fetchAll();
//    '<pre>' . print_r($collection_id) . '</pre>';


//echo($size_1_id[1][0]) . ' 0<br></pre>';
//echo($size_1_id[1][1]) . ' 1<br></pre>';
//echo($size_1_id[1][2]) . ' 2<br></pre>';
//echo($size_1_id[1][3]) . ' 3<br></pre>';
//echo($size_1_id[1][4]) . ' 4<br></pre>';
//echo($size_1_id[1][5]) . ' 5<br></pre>';
//echo($size_1_id[1][6]) . ' 6<br></pre>';
//echo($size_1_id[1][7]) . ' 7<br></pre>';
//echo($size_1_id[1][8]) . ' 8<br></pre>';
//echo($size_1_id[1][9]) . ' 9<br></pre>';
//echo($size_1_id[1][10]) . ' 10<br></pre>';
//echo($size_1_id[1][11]) . ' 11<br></pre>';

$facets = array();
foreach ($category_id as $p) {
    $facets['category_id'][] = array(
        'value' => $p['category_id'],
        'count' => $p['cnt'],
        'name' => $p[1]
    );
}
foreach ($collection_id as $p) {
    $facets['collection_id'][] = array(
        'value' => $p['collection_id'],
        'count' => $p['cnt'],
        'name' => $p[3]
    );
}
foreach ($size_1_id as $p) {
    $facets['size_1_id'][] = array(
        'value' => $p['size_1_id'],
        'count' => $p['cnt'],
        'name' => $p[9]
    );
}
foreach ($print_name as $p) {
    $facets['print_name'][] = array(
        'value' => $p['print_name'],
        'count' => $p['cnt']

    );
}
foreach ($additional_details_name as $p) {
    $facets['additional_details_name'][] = array(
        'value' => $p['additional_details_name'],
        'count' => $p['cnt']
//        'name' => $p[5]
    );
}
foreach ($brand_name as $p) {
    $facets['brand_name'][] = array(
        'value' => $p['brand_name'],
        'count' => $p['cnt']
//        'name' => $p[7]
    );
}
//foreach ($style as $p) {
//    $facets['style'][] = array(
//        'value' => $p['style'],
//        'count' => $p['cnt'],
//    );
//}
foreach ($prices as $p) {
    $facets['price'][] = array(
        'value' => $p['price_seg'],
        'count' => $p['cnt']
    );
}


//foreach ($rows as $k => $v) {
//    foreach ($v as $x) {
//        $facets[$k][] = array(
//            'value' => $x['selected'],
//            'count' => $x['cnt']
//        );
//    }
//}
?>
<form method="GET" action="" id="search_form">
    <br> <br>   <div class="container"><header>
            <h1>Одежда для женщин</h1>
        </header>
        <div class="col-xs-12 flex-active"><div class="row-fluid">
                <div class="span2">
                    <br> <br>
                    <!--               --><?php //    '<pre>' . print_r($sizes) . '</pre>'; ?>

                    <?php if (isset($facets['category_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Тип</legend>
                                <div class="control-group">
                                    <?php foreach($facets['category_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="category_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['category_id']) && (in_array($item['value'],$_GET['category_id'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_category_id" value="Reset" data-target="category_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['price'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Стоимость</legend>
                                <div class="control-group">
                                    <?php foreach($facets['price'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox" name="price[]"
                                                                        value="<?=$item['value'];?>"
                                                <?=(($request->get('price')!== null) && (in_array($item['value'],$request->get('price'))))?'checked':'';?>>
                                            <?=($item['value']*200).'-'.(($item['value']+1)*200).' грн ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_price" value="Reset" data-target="price" class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['collection_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Коллекция</legend>
                                <div class="control-group">
                                    <?php foreach($facets['collection_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="collection_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['collection_id']) && (in_array($item['value'],$_GET['collection_id'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_collection_id" value="Reset" data-target="collection_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['additional_details_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Детали</legend>
                                <div class="control-group">
                                    <?php foreach($facets['additional_details_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="additional_details_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['additional_details_name']) && (in_array($item['value'],$_GET['additional_details_name'])))?'checked':'';?>>
                                            <?= $item['value'] ?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_additional_details_name" value="Reset" data-target="additional_details_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['brand_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Бренд</legend>
                                <div class="control-group">
                                    <?php foreach($facets['brand_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="brand_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['brand_name']) && (in_array($item['value'],$_GET['brand_name'])))?'checked':'';?>>
                                            <?= $item['value'] ?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_brand_name" value="Reset" data-target="brand_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['size_1_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Размер 1</legend>
                                <div class="control-group">
                                    <?php foreach($facets['size_1_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="size_1_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['size_1_id']) && (in_array($item['value'],$_GET['size_1_id'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_size_1_id" value="Reset" data-target="size_1_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['print_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Принт</legend>
                                <div class="control-group">
                                    <?php foreach($facets['print_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="print_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['print_name']) && (in_array($item['value'],$_GET['print_name'])))?'checked':'';?>>
                                            <?= $item['value'] ?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_print_name" value="Reset" data-target="cloth_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                </div>
            </div></div>
        <div class="clearfix"></div>
        <br><br>
        <div class="span9">
            <div class="container">


                <div class="row">



                    <input type="text" class="input-large" name="query" id="suggest"
                           autocomplete="off"
                           value="<?=  ($request->get('query')!== null)?htmlentities($request->get('query')):''?>">
                    <input type="submit" class="btn btn-primary" id="send"
                           name="send" value="Submit">
                    <button type="reset" class="btn " value="Reset">Reset</button>
                </div>

                <br>
                <br>
                <div class="row">
                    <?php if (count($docs) > 0): ?>
                        <p class="lead">
                            Всего найдено:<?=$total_found?>
                        </p>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                        <div class="span9">
                            <table class="table">
                                <tr>
                                    <th>Модель</th>
                                    <th>Тип</th>
                                    <th>Размер</th>
                                    <th>Цвет</th>
                                    <th>Стиль</th>
                                    <th>Коллекция</th>
                                    <!--						      <th>Бренд</th>-->
                                </tr>
                                <?php foreach ($docs as $doc): ?>
                                    <tr>
                                        <td><?= $doc['model']?></td>
                                        <td><?= $doc['category_id'] ?></td>
                                        <!--                                        <td>--><?//= $doc['size_1'] ?><!--</td>-->
                                        <!--                                        <td>--><?//= $doc['sizes'] ?><!--</td>-->

                                        <td><?= $doc['color_name'] ?></td>
                                        <!--                                        <td>--><?//= $doc['color_name'] ?><!--</td>-->
                                        <!--                                        <td>--><?//= $doc['collection_name'] ?><!--</td>-->

                                        <td><?php //echo $brands[$doc['brand_id']-1]  ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                    <?php elseif (isset($_GET['query']) && $_GET['query'] != ''): ?>
                        <p class="lead">Nothing found!</p>
                    <?php endif; ?>
                </div>
            </div>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="/js/bootstrap-3.1.1.min.js"></script>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script>
    function __highlight(s, t) {
        console.log('tester1');
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(document).ready(function() {
        $(':checkbox').change(function() {
            console.log('tester2');
            $("#search_form").trigger('submit');

        });
        $(':reset').click(function(){
            location.search ='';
            console.log('tester3');
        });
        $('input[name^=reset_]').click(function(){
            $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
            $("#search_form").trigger('submit');
            console.log('tester4');

        });
    });
</script>

