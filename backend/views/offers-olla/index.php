<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Ollas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-olla-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Если список товаров пуст - запустите импорт товаров Olla <br></p>
    <p></p>
    <?= Html::a('Импорт товаров Olla', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'sort_id',
//            'id',
//            'available',
            [
                'attribute' => 'available',
                'value' => function($data) {
                    $str11 = '';
                    if($data->available === 'true') {$str11 = '<span class="text-success">Да</span>';}
                    if($data->available === 'false') {$str11 = '<span class="text-success">Нет</span>';}
//                    else {$str11 = '<span class="text-success">Нет</span>';}
                    return  $str11;
                },
                'format' => 'html',
            ],
            'group_id',
//            'url:url',
            'price',
            //'old_price',
            //'currency_id',
            //'category_id',
            'category_name',
            //'source',
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            //'delivery',
            'model',
            //'vendor',
            //'vendor_code',
            'description:html',
            //'country_of_origin',
            //'barcode',
            //'sales_notes',
            //'product_type',
            'sizes',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
