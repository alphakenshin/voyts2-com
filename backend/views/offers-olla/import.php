<?php

use backend\models\OffersTwo;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар с ресурса olla.ua успешно импортирован';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-olla-index">

    Запустите поиск соответствия по словарям для настройки Sphinx индексов <br></p>
    <p></p><br>

    <?= Html::a('Запустить поиск', ['magic'], ['class' => 'btn btn-success']) ?>
    </p>

</div>