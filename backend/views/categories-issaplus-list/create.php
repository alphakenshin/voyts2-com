<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesIssaplus */

$this->title = 'Create Categories Issaplus';
$this->params['breadcrumbs'][] = ['label' => 'Categories Issapluses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-issaplus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
