<?php

use backend\models\UkrSettlements;
use common\models\DeliveryType;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UkrSettlements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ukr-settlements-form">

    <?php $form = ActiveForm::begin(); ?>

<!--    --><?php
//    $region = UkrSettlements::find()->indexBy('region')->asArray()->all();
//    $regionList = ArrayHelper::map($region, 'region_id', 'region');
//
//            $areasN = asort($regionList);
//
//    ?>
<!--    --><?//= $form->field($model, 'region')->dropDownList($regionList, ['region_id' => 'region', 'prompt' => '-- Оберіть спосіб діставки --']); ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'area_id')->textInput() ?>

    <?= $form->field($model, 'region')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'region_id')->textInput() ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
