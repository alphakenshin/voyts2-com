<?php

use backend\models\UkrSettlements;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ukr Settlements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukr-settlements-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ukr Settlements', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


<!--    --><?php //$form = ActiveForm::begin(); ?>
<!--    --><?php
//    $region = UkrSettlements::find()->indexBy('region')->asArray()->all();
//    $regionList = ArrayHelper::map($region, 'id', 'region');
//?>
<!--    <pre> --><?php // print_r($regionList) ?><!--  </pre>-->
<!---->
<!---->
<!--    --><?php //echo $form->field($regions, 'region')->dropDownList($regionList, ['id' => 'region', 'prompt' => '-- Оберіть спосіб діставки --']); ?>
<!---->
<!--    --><?php //ActiveForm::end(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'city',
            'area',
            'area_id',
            'region',
            //'region_id',
            //'longitude',
            //'latitude',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
