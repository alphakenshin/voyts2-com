<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UkrSettlements */

$this->title = 'Create Ukr Settlements';
$this->params['breadcrumbs'][] = ['label' => 'Ukr Settlements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ukr-settlements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
