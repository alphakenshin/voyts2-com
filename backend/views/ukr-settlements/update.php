<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\UkrSettlements */

$this->title = 'Update Ukr Settlements: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ukr Settlements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ukr-settlements-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
