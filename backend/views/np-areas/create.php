<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NpAreas */

$this->title = 'Create Np Areas';
$this->params['breadcrumbs'][] = ['label' => 'Np Areas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-areas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
