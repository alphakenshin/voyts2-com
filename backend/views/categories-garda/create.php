<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesGarda */

$this->title = 'Create Categories Garda';
$this->params['breadcrumbs'][] = ['label' => 'Categories Gardas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-garda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
