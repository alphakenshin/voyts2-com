<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>




<div class="menu-form">

    <?php $form = ActiveForm::begin([
            'action' => ($model->isNewRecord) ? \yii\helpers\Url::toRoute(['/menu/create']) : \yii\helpers\Url::toRoute(['/menu/update', 'id'=>$model->id]) ]); ?>

    <?= $form->field($model, 'tree')->dropDownList([1]) ?>

    <?= $form->field($model, 'sub')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Menu::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
