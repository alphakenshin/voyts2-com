<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesGlem */

$this->title = 'Create Categories Glem';
$this->params['breadcrumbs'][] = ['label' => 'Categories Glems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-glem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
