<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Імпорт відділень НП завершено';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-warehouses-import">
    <p>
        Запустите поиск соответствия по словарям для настройки Sphinx индексов <br></p>
    <p></p><br>

    <?= Html::a('Запустить поиск', ['magic'], ['class' => 'btn btn-success']) ?>
    </p>

</div>