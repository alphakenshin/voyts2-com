<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NpWarehouses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="np-warehouses-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'site_key')->textInput() ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_address_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_of_warehouse')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_area_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_regions_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_type_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post_finance')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bicycle_parking')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_access')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pos_terminal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'international_shipping')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'total_max_weight_allowed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'place_max_weight_allowed')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'reception')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'delivery')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'schedule')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'district_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'warehouse_status')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_of_warehouse')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
