<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NpWarehouses */

$this->title = 'Create Np Warehouses';
$this->params['breadcrumbs'][] = ['label' => 'Np Warehouses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-warehouses-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
