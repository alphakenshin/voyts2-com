<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Якщо список відділень пустий, натисніть кнопку "Завантажити список"';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-warehouses-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Скачать список', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'site_key',
            'description',
            'description_ru',
            'short_address',
            //'short_address_ru',
            //'phone',
            //'type_of_warehouse',
            //'ref',
            //'number',
            //'city_ref',
            //'city_description',
            //'city_description_ru',
            //'settlement_ref',
            //'settlement_description',
            //'settlement_area_description',
            //'settlement_regions_description',
            //'settlement_type_description',
            //'longitude',
            //'latitude',
            //'post_finance',
            //'bicycle_parking',
            //'payment_access',
            //'pos_terminal',
            //'international_shipping',
            //'total_max_weight_allowed',
            //'place_max_weight_allowed',
            //'reception:ntext',
            //'delivery:ntext',
            //'schedule:ntext',
            //'district_code',
            //'warehouse_status',
            //'category_of_warehouse',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
