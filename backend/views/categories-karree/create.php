<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesKarree */

$this->title = 'Create Categories Karree';
$this->params['breadcrumbs'][] = ['label' => 'Categories Karrees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-karree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
