<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\ProductWomensClothes */

$this->title = 'Create Product Womens Clothes';
$this->params['breadcrumbs'][] = ['label' => 'Product Womens Clothes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-womens-clothes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
