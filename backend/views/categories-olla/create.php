<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\CategoriesOlla */

$this->title = 'Create Categories Olla';
$this->params['breadcrumbs'][] = ['label' => 'Categories Ollas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-olla-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
