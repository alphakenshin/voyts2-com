<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersAll */

$this->title = 'Create Offers All';
$this->params['breadcrumbs'][] = ['label' => 'Offers Alls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-all-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
