<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\NpCities */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="np-cities-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ref')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery_1')->textInput() ?>

    <?= $form->field($model, 'delivery_2')->textInput() ?>

    <?= $form->field($model, 'delivery_3')->textInput() ?>

    <?= $form->field($model, 'delivery_4')->textInput() ?>

    <?= $form->field($model, 'delivery_5')->textInput() ?>

    <?= $form->field($model, 'delivery_6')->textInput() ?>

    <?= $form->field($model, 'delivery_7')->textInput() ?>

    <?= $form->field($model, 'area')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'is_branch')->textInput() ?>

    <?= $form->field($model, 'prevent_entry_new_streets_user')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'conglomerates')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_id')->textInput() ?>

    <?= $form->field($model, 'settlement_type_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'settlement_type_description_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'special_cash_check')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
