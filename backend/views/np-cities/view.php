<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\NpCities */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Np Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="np-cities-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'description',
            'description_ru',
//            'ref',
            'delivery_1',
            'delivery_2',
            'delivery_3',
            'delivery_4',
            'delivery_5',
            'delivery_6',
            'delivery_7',
            'area',
            'settlement_type',
            'is_branch',
            'prevent_entry_new_streets_user',
            'conglomerates',
            'city_id',
            'settlement_type_description',
            'settlement_type_description_ru',
            'special_cash_check',
        ],
    ]) ?>

</div>
