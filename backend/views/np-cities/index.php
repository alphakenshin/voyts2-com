<?php

use backend\models\NpAreas;
use backend\models\NpCities;
use backend\models\NpWarehouses;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Np Cities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-cities-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Np Cities', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php


    $regions  = NpAreas::find()->indexBy('city_ref')->asArray()->one();
    $warehouses  = NpWarehouses::find()->indexBy('ref')->asArray()->one();

    ?>
<div> <?php  print_r($regions) ?>  </div>;
    <div> <?php  print_r($warehouses) ?>  </div>;

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'description',
            'description_ru',
            'ref',
//            [
//                'attribute' => 'ref',
//                'value' => function($data) {
//                    return $data->city_ref->description;
//                },
//            ],
            'delivery_1',
            //'delivery_2',
            //'delivery_3',
            //'delivery_4',
            //'delivery_5',
            //'delivery_6',
            //'delivery_7',
 //            [
//                'attribute' => 'area',
//                'value' => function($data) {
//                    return $data->regions->description;
//                },
//            ],
            //'settlement_type',
            //'is_branch',
            //'prevent_entry_new_streets_user',
            //'conglomerates',
            //'city_id',
            //'settlement_type_description',
            //'settlement_type_description_ru',
            //'special_cash_check',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
