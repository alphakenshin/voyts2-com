<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\NpCities */

$this->title = 'Create Np Cities';
$this->params['breadcrumbs'][] = ['label' => 'Np Cities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="np-cities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
