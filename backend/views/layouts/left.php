<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Заказы', 'icon' => 'tags', 'url' => ['/order']],
//                    ['label' => 'Категории', 'icon' => 'tags', 'url' => ['/category']],
                    ['label' => 'Товары', 'icon' => 'tags', 'url' => ['/product']],

                    [
                        'label' => 'Загрузка товаров',
                        'icon' => 'download',
                        'url' => '#',
                        'items' => [
                            [


                                'label' => 'garda.com.ua',
                                'icon' => 'server',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Просмотр', 'icon' => 'file-excel-o', 'url' => '/offers-garda/',],

                                            ['label' => 'Загрузить', 'icon' => 'download', 'url' => '/offers-garda/import',],
                                    ['label' => 'Фоматирование под Sphinx', 'icon' => 'download', 'url' => '/offers-garda/magic',],
                                            ['label' => 'Групировать', 'icon' => 'circle-o', 'url' => '/offers-garda/group',],
                                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => '/offers-garda/import',],
                                        ],
                                    ],

                            ['label' => 'glem.com.ua', 'icon' => 'server', 'url' => '#',
                                'items' => [
                                    ['label' => 'Просмотр', 'icon' => 'file-excel-o', 'url' => '/offers-glem/',],

                                            ['label' => 'Загрузить', 'icon' => 'download', 'url' => '/offers-glem/import',],
                                    ['label' => 'Фоматирование под Sphinx', 'icon' => 'download', 'url' => '/offers-glem/magic',],
//                                            ['label' => 'Групировать', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => '#',],
                                        ],
                                    ],



                            ['label' => 'issaplus.com', 'icon' => 'server', 'url' => '#',
                                'items' => [
                                    ['label' => 'Просмотр', 'icon' => 'file-excel-o', 'url' => '/offers-issaplus/',],

                                            ['label' => 'Загрузить', 'icon' => 'download', 'url' => '/offers-issaplus/import',],
                                            ['label' => 'Фоматирование под Sphinx', 'icon' => 'download', 'url' => '/offers-issaplus/magic',],
//                                            ['label' => 'Групировать', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => '#',],
                                        ],
                                    ],


                            ['label' => 'karree.com.ua', 'icon' => 'server', 'url' => '#',
                                'items' => [
                                    ['label' => 'Просмотр', 'icon' => 'file-excel-o', 'url' => '/offers-karree/',],
                                    ['label' => 'Загрузить', 'icon' => 'download', 'url' => '/offers-karree/import',],
                                    ['label' => 'Извлечение размеров', 'icon' => 'download', 'url' => '/offers-karree/asort',],
                                            ['label' => 'Фоматирование под Sphinx', 'icon' => 'download', 'url' => '/offers-karree/magic',],
                                            ['label' => 'Групировать', 'icon' => 'circle-o', 'url' => '/offers-karree/group',],
                                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => '#',],
                                        ],
                                    ],

                            ['label' => 'olla.ua', 'icon' => 'server', 'url' => '#',
                                'items' => [
                                    ['label' => 'Просмотр', 'icon' => 'file-excel-o', 'url' => '/offers-olla/',],
                                            ['label' => 'Загрузить', 'icon' => 'download', 'url' => '/offers-olla/import',],
                                    ['label' => 'Фоматирование под Sphinx', 'icon' => 'download', 'url' => '/offers-olla/magic',],
                                            ['label' => 'Групировать', 'icon' => 'circle-o', 'url' => '/offers-olla-group/group',],
                                            ['label' => 'Удалить', 'icon' => 'remove', 'url' => '#',],

                                ],
                            ],
                        ],
                    ],

                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Объединение товаров', 'icon' => 'server', 'url' => ['/product/group']],
                    ['label' => 'Загрузка фото товаров', 'icon' => 'download', 'url' => ['/product/upload']],
                    ['label' => 'Объединение товаров и фото', 'icon' => 'server', 'url' => ['/product/image']],



                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
