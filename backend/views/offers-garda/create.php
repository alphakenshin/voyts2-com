<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGarda */

$this->title = 'Create Offers Garda';
$this->params['breadcrumbs'][] = ['label' => 'Offers Gardas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-garda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
