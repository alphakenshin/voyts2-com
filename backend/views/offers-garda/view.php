<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGarda */

$this->title = $model->sort_id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Gardas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="offers-garda-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sort_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sort_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sort_id',
            'id',
            'available',
            'group_id',
            'url:url',
            'price',
            'currency_id',
            'category_name',
            'category_id',
            'source',
            'main_img',
            'gallery_1',
            'gallery_2',
            'gallery_3',
            'gallery_4',
            'gallery_5',
            'model',
            'vendor_code',
            'description:ntext',
            'country_of_origin',
            'product_type_name',
            'product_type_id',
            'sales_notes',
            'composition_name',
            'composition_id',
            'sizes',
            'sizes_id',
            'silhouette_name',
            'silhouette_id',
            'cloth_name',
            'cloth_id',
            'color_name',
            'color_id',
            'print_name',
            'print_id',
            'style_name',
            'style_id',
            'season_name',
            'season_id',
            'brand_name',
            'brand_id',
            'product_length_name',
            'product_length_id',
            'sleeve_name',
            'sleeve_id',
        ],
    ]) ?>

</div>
