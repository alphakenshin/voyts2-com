<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Gardas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-garda-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Если список товаров пуст - запустите импорт товаров Garda <br></p>
    <p></p>
    <?= Html::a('Импорт товаров Garda', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'sort_id',
            'id',
//            'available',
            [
                'attribute' => 'available',
                'value' => function($data) {
        $str11 = '';
                    if($data->available === 'true') {$str11 = '<span class="text-success">Да</span>';}
                    if($data->available === 'false') {$str11 = '<span class="text-success">Нет</span>';}
//                    else {$str11 = '<span class="text-success">Нет</span>';}
                    return  $str11;
                },
                'format' => 'html',
            ],
//            'group_id',
//            'url:url',
            'price',
            //'currency_id',
            'category_name',
            //'category_id',
            //'source',
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            'model',
            //'vendor_code',
            'description:ntext',
            //'country_of_origin',
            //'product_type_name',
            //'product_type_id',
            //'sales_notes',
            'composition_name',
            //'composition_id',
            'sizes',
            //'sizes_id',
            //'silhouette_name',
            //'silhouette_id',
//            'cloth_name',
            //'cloth_id',
            'color_name',
            //'color_id',
            //'print_name',
            //'print_id',
//            'style_name',
            //'style_id',
//            'season_name',
//            'season_id',
//            'brand_name',
            //'brand_id',
            'product_length_name',
            //'product_length_id',
            //'sleeve_name',
            //'sleeve_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
