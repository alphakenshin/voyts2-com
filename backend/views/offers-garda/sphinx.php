<?php
//use PDO;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\OffersIssaplusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Issapluses';
$this->params['breadcrumbs'][] = $this->title;

$ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

$request = Yii::$app->request;


$docs = array();
$start = 0;
$offset = 10;
$current = 1;
if (isset($_GET['start'])) {
    $start = $_GET['start'];
    $current = $start / $offset + 1;
}

$search_query = $query = trim($request->get('query'));

$select = array();
$where = array();
$where_price = array();
$where_sizes = array();
$where_size_1 = array();
$where_category = array();
$where_collection = array();
$where_color = array();
$where_sizes = array();
$where_size_1 = array();
$where_composition = array();
$where_price = array();

if (isset($_GET['sizes'])) {
    $w = implode(',', $_GET['sizes']);
    $where['sizes'] = 'sizes in (' . $w . ') ';
}

if (isset($_GET['color_id'])) {
    $w = implode(',', $_GET['color_id']);
    $where['color_id'] = 'color_id in (' . $w . ') ';
}

if (isset($_GET['product_length_name'])) {
    $w = implode(',', $_GET['product_length_name']);
    $where['product_length_name'] = 'product_length_name in (' . $w . ') ';
}

if (isset($_GET['product_type_name'])) {
    $w = implode(',', $_GET['product_type_name']);
    $where['product_type_name'] = 'product_type_name in (' . $w . ') ';
}

if (isset($_GET['category_id'])) {
    $w = implode(',', $_GET['category_id']);
    $where['category_id'] = 'category_id in (' . $w . ') ';
}

if (isset($_GET['composition_name'])) {
    $w = implode(',', $_GET['composition_name']);
    $where['composition_name'] = 'composition_name in (' . $w . ') ';
}

if (isset($_GET['price'])) {
    $w = array();
    foreach ($_GET['price'] as $c) {
        $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
    }
    $w = implode(' OR ', $w);
    $select['price'] = 'IF(' . $w . ',1,0) as w_p';
    $where['price'] = 'w_p = 1';
}
if (isset($_GET['silhouette_id'])) {
    $w = implode(',', $_GET['silhouette_id']);
    $where['silhouette_id'] = 'silhouette_id in (' . $w . ') ';
}
//if (isset($_GET['size_1'])) {
//
//    $search_query .= ' @size_1 ' . implode('|', $_GET['size_1']);
//}
if (count($where) > 0) {
    $where_category = $where_collection = $where_color = $where_color_name = $where_product_name = $where_sizes = $where_size_1 = $where_composition = $where_price = $where;
    if (isset($where_composition['composition_name'])) {
        unset($where_composition['composition_name']);
    }
    if (count($where_composition) > 0) {
        $where_composition = ' AND ' . implode(' AND ', $where_composition);
    } else {
        $where_composition = '';
    }

    if (isset($where_category['category_id'])) {
        unset($where_category['category_id']);
    }
    if (count($where_category) > 0) {
        $where_category = ' AND ' . implode(' AND ', $where_category);
    } else {
        $where_category = '';
    }

    if (isset($where_price['price'])) {
        unset($where_price['price']);
    }
    if (count($where_price) > 0) {
        $where_price = ' AND ' . implode(' AND ', $where_price);
    } else {
        $where_price = '';
    }

    if (isset($where_product_name['product_length_name'])) {
        unset($where_product_name['product_type_name']);
    }
    if (count($where_product_name) > 0) {
        $where_product_name = ' AND ' . implode(' AND ', $where_product_name);
    } else {
        $where_product_name = '';
    }

    if (isset($where_collection['product_type_id'])) {
        unset($where_collection['product_type_id']);
    }
    if (count($where_collection) > 0) {
        $where_collection = ' AND ' . implode(' AND ', $where_collection);
    } else {
        $where_collection = '';
    }

    if (isset($where_color['color_id'])) {
        unset($where_color['color_id']);
    }
    if (count($where_color) > 0) {
        $where_color = ' AND ' . implode(' AND ', $where_color);
    } else {
        $where_color = '';
    }


    if (isset($where_size_1['silhouette_id'])) {
        unset($where_size_1['silhouette_id']);
    }
    if (count($where_size_1) > 0) {
        $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
    } else {
        $where_size_1 = '';
    }

    if (isset($where_sizes['sizes'])) {
        unset($where_sizes['sizes']);
    }
    if (count($where_sizes) > 0) {
        $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
    } else {
        $where_sizes = '';
    }

//    $where_size_1 = ' AND ' . implode(' AND ', $where_size_1);
//    $where = ' AND ' . implode(' AND ', $where);
} else {
    $where_category = $where_collection = $where_color = $where_color_name = $where_product_name = $where_sizes = $where_size_1 = $where_composition = $where_price = $where = '';
}
if (count($select) > 0) {
    $select = ',' . implode(',', $select);
} else {
    $select = '';
}
$indexes = 'sphinx_index_offers_garda';

$stmt = $ln_sph->prepare("SELECT * from sphinx_index_offers_garda  WHERE MATCH(:match) $where  LIMIT $start,$offset ");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$docs = $stmt->fetchAll();

$meta = $ln_sph->query("SHOW META")->fetchAll();
foreach ($meta as $m) {
    $meta_map[$m['Variable_name']] = $m['Value'];
}
//'<pre></pre>' . print_r($meta_map) . '<pre></pre>';

$total_found = $meta_map['total_found'];
$total = $meta_map['total'];


$ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

//$sql = array();
//$rows = array();
//$sql[] = "SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_category GROUP BY category_id  LIMIT 0,2000";
////        '<pre>' . print_r($sql) . '</pre>';
//
////       '<pre>' . print_r($sql) . '</pre>';
//
//
//$sql = implode('; ', $sql);
////        '<pre>' . print_r($sql) . '</pre>';
//$stmt = $ln_sph->prepare($sql);
//$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
//$stmt->execute();
//$category_id = $stmt->fetchAll();



//echo($category_id[0][0]) . '<br></pre>';
//echo($category_id[0][1]) . '<br></pre>';
//echo($category_id[0][2]) . '<br></pre>';
//echo($category_id[0][3]) . '<br></pre>';
//echo($category_id[0][4]) . '<br></pre>';
//echo($category_id[0][5]) . '<br></pre>';
// expressions are not yet supported in multi-query optimization,so we run them separate
//foreach ($attrs as $attr) {
//    $rows[$attr] = $stmt->fetchAll();
//    '<pre>' . print_r($rows[$attr]) . '</pre>';
//    $stmt->nextRowset();
//}

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_category  GROUP BY category_id ORDER BY category_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$category_id = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_name  GROUP BY sizes ORDER BY sizes ASC  LIMIT 0,1000");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$sizes = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY product_length_name ORDER BY product_length_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$product_length_name = $stmt->fetchAll();

//$stmt = $ln_sph->prepare("select groupby(), group_concat(id) as selected,COUNT(*) as cnt from $indexes where match(:match) $where_sizes group by sizes  LIMIT 0,10");
//$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
//$stmt->execute();
//$sizes = $stmt->fetchAll();
//'<pre>' . print_r($sizes) . '</pre>';


$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_size_1  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$silhouette_id = $stmt->fetchAll();
//'<pre>' . print_r($silhouette_id) . '</pre>';
$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_name ORDER BY style_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$composition_name = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id ORDER BY color_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$color_id = $stmt->fetchAll();

$stmt = $ln_sph->prepare("SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color_name  GROUP BY color_name ORDER BY color_name ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$color_name = $stmt->fetchAll();
//'<pre>' . print_r($color_name) . '</pre>';

//$stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id ORDER BY color_id ASC  LIMIT 0,10");
//$stmt->bindValue(':match', $query, PDO::PARAM_STR);
//$stmt->execute();
//$color_id = $stmt->fetchAll();
//    '<pre>' . print_r($product_type_id) . '</pre>';


//echo($silhouette_id[1][0]) . ' 0<br></pre>';
//echo($silhouette_id[1][1]) . ' 1<br></pre>';
//echo($silhouette_id[1][2]) . ' 2<br></pre>';
//echo($silhouette_id[1][3]) . ' 3<br></pre>';
//echo($silhouette_id[1][4]) . ' 4<br></pre>';
//echo($silhouette_id[1][5]) . ' 5<br></pre>';
//echo($silhouette_id[1][6]) . ' 6<br></pre>';
//echo($silhouette_id[1][7]) . ' 7<br></pre>';
//echo($silhouette_id[1][8]) . ' 8<br></pre>';
//echo($silhouette_id[1][9]) . ' 9<br></pre>';
//echo($silhouette_id[1][10]) . ' 10<br></pre>';
//echo($silhouette_id[1][11]) . ' 11<br></pre>';

$facets = array();
foreach ($category_id as $p) {
    $facets['category_id'][] = array(
        'value' => $p['category_id'],
        'count' => $p['cnt'],
        'name' => $p[1]
    );
}
foreach ($product_length_name as $p) {
    $facets['product_length_name'][] = array(
        'value' => $p['product_length_name'],
        'count' => $p['cnt']
//        'name' => $p[3]
    );
}
foreach ($silhouette_id as $p) {
    $facets['silhouette_id'][] = array(
        'value' => $p['silhouette_id'],
        'count' => $p['cnt'],
        'name' => $p[9]
    );
}
foreach ($sizes as $p) {
    $facets['sizes'][] = array(
        'value' => $p['sizes'],
        'count' => $p['cnt']

    );
}
foreach ($color_id as $p) {
    $facets['color_id'][] = array(
        'value' => $p['color_id'],
        'count' => $p['cnt'],
        'name' => $p[5]
    );
}
foreach ($color_name as $p) {
    $facets['color_name'][] = array(
        'value' => $p['color_name'],
        'count' => $p['cnt']
//        'name' => $p[5]
    );
}
foreach ($composition_name as $p) {
    $facets['composition_name'][] = array(
        'value' => $p['composition_name'],
        'count' => $p['cnt']
//        'name' => $p[7]
    );
}
//foreach ($style as $p) {
//    $facets['style'][] = array(
//        'value' => $p['style'],
//        'count' => $p['cnt'],
//    );
//}
foreach ($prices as $p) {
    $facets['price'][] = array(
        'value' => $p['price_seg'],
        'count' => $p['cnt']
    );
}


//foreach ($rows as $k => $v) {
//    foreach ($v as $x) {
//        $facets[$k][] = array(
//            'value' => $x['selected'],
//            'count' => $x['cnt']
//        );
//    }
//}
?>
<form method="GET" action="" id="search_form">
    <br> <br>   <div class="container"><header>
            <h1>Одежда для женщин</h1>
        </header>
        <div class="col-xs-12 flex-active"><div class="row-fluid">
                <div class="span2">
                    <br> <br>
                    <!--               --><?php //    '<pre>' . print_r($sizes) . '</pre>'; ?>

                    <?php if (isset($facets['category_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Тип</legend>
                                <div class="control-group">
                                    <?php foreach($facets['category_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="category_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['category_id']) && (in_array($item['value'],$_GET['category_id'])))?'checked':'';?>>
                                            <?= $item['name'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_category_id" value="Reset" data-target="category_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['price'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Стоимость</legend>
                                <div class="control-group">
                                    <?php foreach($facets['price'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox" name="price[]"
                                                                        value="<?=$item['value'];?>"
                                                <?=(($request->get('price')!== null) && (in_array($item['value'],$request->get('price'))))?'checked':'';?>>
                                            <?=($item['value']*200).'-'.(($item['value']+1)*200).' грн ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_price" value="Reset" data-target="price" class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['product_length_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>размеры</legend>
                                <div class="control-group">
                                    <?php foreach($facets['product_length_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="product_length_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['product_length_name']) && (in_array($item['value'],$_GET['product_length_name'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_product_length_name" value="Reset" data-target="product_length_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['color_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Цвет</legend>
                                <div class="control-group">
                                    <?php foreach($facets['color_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="color_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['color_name']) && (in_array($item['value'],$_GET['color_name'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_color_name" value="Reset" data-target="color_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['composition_name'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Состав</legend>
                                <div class="control-group">
                                    <?php foreach($facets['composition_name'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="composition_name[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['composition_name']) && (in_array($item['value'],$_GET['composition_name'])))?'checked':'';?>>
                                            <?= $item['value'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_composition_name" value="Reset" data-target="composition_name"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <?php if (isset($facets['silhouette_id'])): ?>
                        <div class="col-sm-3"><fieldset>
                                <legend>Размер 1</legend>
                                <div class="control-group">
                                    <?php foreach($facets['silhouette_id'] as $item):?>
                                        <label class="checkbox"> <input type="checkbox"
                                                                        name="silhouette_id[]" value="<?=$item['value'];?>"
                                                <?=(isset($_GET['silhouette_id']) && (in_array($item['value'],$_GET['silhouette_id'])))?'checked':'';?>>
                                            <?= $item['name'].' ('.$item['count'].')'?>
                                        </label>
                                    <?php endforeach;?>
                                    <input type="button"
                                           name="reset_silhouette_id" value="Reset" data-target="silhouette_id"
                                           class="btn">
                                </div>
                            </fieldset></div>
                    <?php endif; ?>

                    <!--                    --><?php //if (isset($facets['sizes'])): ?>
                    <!--                        <div class="col-sm-3"><fieldset>-->
                    <!--                                <legend>Размеры 2</legend>-->
                    <!--                                <div class="control-group">-->
                    <!--                                    --><?php //foreach($facets['sizes'] as $item):?>
                    <!--                                        <label class="checkbox"> <input type="checkbox"-->
                    <!--                                                                        name="sizes[]" value="--><?//=$item['value'];?><!--"-->
                    <!--                                                --><?//=(isset($_GET['sizes']) && (in_array($item['value'],$_GET['sizes'])))?'checked':'';?>
                    <!--                                            --><?//= $item['value'].' ('.$item['count'].')'?>
                    <!--                                        </label>-->
                    <!--                                    --><?php //endforeach;?>
                    <!--                                    <input type="button"-->
                    <!--                                           name="reset_sizes" value="Reset" data-target="sizes"-->
                    <!--                                           class="btn">-->
                    <!--                                </div>-->
                    <!--                            </fieldset></div>-->
                    <!--                    --><?php //endif; ?>

                </div>
            </div></div>
        <div class="clearfix"></div>
        <br><br>
        <div class="span9">
            <div class="container">


                <div class="row">



                    <input type="text" class="input-large" name="query" id="suggest"
                           autocomplete="off"
                           value="<?=  ($request->get('query')!== null)?htmlentities($request->get('query')):''?>">
                    <input type="submit" class="btn btn-primary" id="send"
                           name="send" value="Submit">
                    <button type="reset" class="btn " value="Reset">Reset</button>
                </div>

                <br>
                <br>
                <div class="row">
                    <?php if (count($docs) > 0): ?>
                        <p class="lead">
                            Всего найдено:<?=$total_found?>
                        </p>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                        <div class="span9">
                            <table class="table">
                                <tr>
                                    <th>Модель</th>
                                    <th>Тип</th>
                                    <th>Размер</th>
                                    <th>Цвет</th>
                                    <th>Стиль</th>
                                    <th>Коллекция</th>
                                    <!--						      <th>Бренд</th>-->
                                </tr>
                                <?php foreach ($docs as $doc): ?>
                                    <tr>
                                        <td><?= $doc['model']?></td>
                                        <td><?= $doc['category_id'] ?></td>
                                        <td><?= $doc['product_length_name'] ?></td>
                                        <!--                                        <td>--><?//= $doc['sizes'] ?><!--</td>-->

                                        <td><?= $doc['color_name'] ?></td>
                                        <td><?= $doc['style_name'] ?></td>
<!--                                        <td>--><?//= $doc['collection_name'] ?><!--</td>-->

                                        <td><?php //echo $brands[$doc['brand_id']-1]  ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
                    <?php elseif (isset($_GET['query']) && $_GET['query'] != ''): ?>
                        <p class="lead">Nothing found!</p>
                    <?php endif; ?>
                </div>
            </div>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="/js/bootstrap-3.1.1.min.js"></script>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script>
    function __highlight(s, t) {
        console.log('tester1');
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(document).ready(function() {
        $(':checkbox').change(function() {
            console.log('tester2');
            $("#search_form").trigger('submit');

        });
        $(':reset').click(function(){
            location.search ='';
            console.log('tester3');
        });
        $('input[name^=reset_]').click(function(){
            $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
            $("#search_form").trigger('submit');
            console.log('tester4');

        });
    });
</script>
