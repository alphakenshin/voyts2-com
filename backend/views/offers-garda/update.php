<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGarda */

$this->title = 'Update Offers Garda: ' . $model->sort_id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Gardas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sort_id, 'url' => ['view', 'id' => $model->sort_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="offers-garda-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
