<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар с ресурса glem.com.ua успешно импортирован';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-glem-index">

    Запустите поиск соответствия по словарям для настройки Sphinx индексов <br></p>
    <p></p><br>

    <?= Html::a('Запустить поиск', ['magic'], ['class' => 'btn btn-success']) ?>
    </p>

</div>