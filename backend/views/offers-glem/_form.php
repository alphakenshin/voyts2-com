<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGlem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-glem-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'param_id')->textInput() ?>

    <?= $form->field($model, 'types')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'available')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'currency_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'store')->textInput() ?>

    <?= $form->field($model, 'delivery')->textInput() ?>

    <?= $form->field($model, 'product_type_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_type_id')->textInput() ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'manufacturer_warranty')->textInput() ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'barcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sales_notes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'silhouette')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cloth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_id')->textInput() ?>

    <?= $form->field($model, 'sort_color_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sort_color_id')->textInput() ?>

    <?= $form->field($model, 'print')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'season')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeve')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'all_quantity')->textInput() ?>

    <?= $form->field($model, 'for_full')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_1_id')->textInput() ?>

    <?= $form->field($model, 'sizes_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_2_id')->textInput() ?>

    <?= $form->field($model, 'sizes_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_3_id')->textInput() ?>

    <?= $form->field($model, 'sizes_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_4_id')->textInput() ?>

    <?= $form->field($model, 'sizes_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_5_id')->textInput() ?>

    <?= $form->field($model, 'sizes_6')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_6_id')->textInput() ?>

    <?= $form->field($model, 'sizes_7')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_7_id')->textInput() ?>

    <?= $form->field($model, 'sizes_8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_8_id')->textInput() ?>

    <?= $form->field($model, 'sizes_9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_9_id')->textInput() ?>

    <?= $form->field($model, 'sizes_10')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_10_id')->textInput() ?>

    <?= $form->field($model, 'sizes_11')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_11_id')->textInput() ?>

    <?= $form->field($model, 'sizes_12')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_12_id')->textInput() ?>

    <?= $form->field($model, 'sizes_13')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_13_id')->textInput() ?>

    <?= $form->field($model, 'sizes_14')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_14_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
