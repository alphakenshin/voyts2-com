<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Glems';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-glem-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>Если список товаров пуст - запустите импорт товаров Glem <br></p>
    <p></p>
    <?= Html::a('Импорт товаров Glem', ['import'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'sort_id',
            'sku',
//            'param_id',
//            'types',
//            'available',
            [
                'attribute' => 'available',
                'value' => function($data) {
                    $str11 = '';
                    if($data->available === 'true') {$str11 = '<span class="text-success">Да</span>';}
                    if($data->available === 'false') {$str11 = '<span class="text-success">Нет</span>';}
//                    else {$str11 = '<span class="text-success">Нет</span>';}
                    return  $str11;
                },
                'format' => 'html',
            ],
            //'url:url',
            //'price',
            //'currency_id',
            'category_name',
            //'category_id',
            //'source',
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            //'store',
            //'delivery',
            //'product_type_name',
            //'product_type_id',
            'model',
            //'vendor',
            //'vendor_code',
            'description:html',
            //'manufacturer_warranty',
            //'country_of_origin',
            //'barcode',
            //'sales_notes',
            //'size_s_quantity',
            //'size_m_quantity',
            //'size_l_quantity',
            //'size_xl_quantity',
            //'size_xxl_quantity',
            //'size_xxxl_quantity',
            //'silhouette',
            //'cloth',
            'color_name',
            //'color_id',
            //'print',
            //'style',
            //'season',
            //'product_length',
            //'sleeve',
            //'all_quantity',
            //'for_full',
            'sizes',
            //'sizes_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
