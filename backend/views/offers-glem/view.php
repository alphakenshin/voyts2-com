<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGlem */

$this->title = $model->sort_id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Glems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="offers-glem-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sort_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sort_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sort_id',
            'sku',
            'param_id',
            'types',
            'available',
            'url:url',
            'price',
            'currency_id',
            'category_name',
            'category_id',
            'source',
            'main_img',
            'gallery_1',
            'gallery_2',
            'gallery_3',
            'gallery_4',
            'gallery_5',
            'store',
            'delivery',
            'product_type_name',
            'product_type_id',
            'model',
            'vendor',
            'vendor_code',
            'description:ntext',
            'manufacturer_warranty',
            'country_of_origin',
            'barcode',
            'sales_notes',
            'silhouette',
            'cloth',
            'color_name',
            'color_id',
            'sort_color_name',
            'sort_color_id',
            'print',
            'style',
            'season',
            'product_length',
            'sleeve',
            'all_quantity',
            'for_full',
            'sizes_1',
            'sizes_1_id',
            'sizes_2',
            'sizes_2_id',
            'sizes_3',
            'sizes_3_id',
            'sizes_4',
            'sizes_4_id',
            'sizes_5',
            'sizes_5_id',
            'sizes_6',
            'sizes_6_id',
            'sizes_7',
            'sizes_7_id',
            'sizes_8',
            'sizes_8_id',
            'sizes_9',
            'sizes_9_id',
            'sizes_10',
            'sizes_10_id',
            'sizes_11',
            'sizes_11_id',
            'sizes_12',
            'sizes_12_id',
            'sizes_13',
            'sizes_13_id',
            'sizes_14',
            'sizes_14_id',
        ],
    ]) ?>

</div>
