<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersGlem */

$this->title = 'Create Offers Glem';
$this->params['breadcrumbs'][] = ['label' => 'Offers Glems', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-glem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
