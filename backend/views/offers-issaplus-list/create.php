<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersIssaplus */

$this->title = 'Create Offers Issaplus';
$this->params['breadcrumbs'][] = ['label' => 'Offers Issapluses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-issaplus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
