<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersKarree */

$this->title = 'Update Offers Karree: ' . $model->sort_id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Karrees', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->sort_id, 'url' => ['view', 'id' => $model->sort_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="offers-karree-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
