<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersKarree */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-karree-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'available')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'currency_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category_id')->textInput() ?>

    <?= $form->field($model, 'source')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_img')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_1')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_4')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gallery_5')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'delivery')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'country_of_origin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'barcode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sales_notes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_type_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_type_id')->textInput() ?>

    <?= $form->field($model, 'silhouette_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'silhouette_id')->textInput() ?>

    <?= $form->field($model, 'cloth_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cloth_id')->textInput() ?>

    <?= $form->field($model, 'color_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'color_id')->textInput() ?>

    <?= $form->field($model, 'print_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'print_id')->textInput() ?>

    <?= $form->field($model, 'style_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'style_id')->textInput() ?>

    <?= $form->field($model, 'season_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'season_id')->textInput() ?>

    <?= $form->field($model, 'for_full')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_length_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'product_length_id')->textInput() ?>

    <?= $form->field($model, 'sleeve_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sleeve_id')->textInput() ?>

    <?= $form->field($model, 'cutout_collar_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cutout_collar_id')->textInput() ?>

    <?= $form->field($model, 'additional_details_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'additional_details_id')->textInput() ?>

    <?= $form->field($model, 'sizes')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sizes_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
