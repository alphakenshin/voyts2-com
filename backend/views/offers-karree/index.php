<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Offers Karrees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-karree-index">

    <p>Если список товаров пуст - запустите импорт товаров Karree <br></p>
    <p></p>
    <?= Html::a('Импорт товаров Karree', ['import'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'sort_id',
            'id',
//            'available',
            [
                'attribute' => 'available',
                'value' => function($data) {
                    $str11 = '';
                    if($data->available === 'true') {$str11 = '<span class="text-success">Да</span>';}
                    if($data->available === 'false') {$str11 = '<span class="text-success">Нет</span>';}
//                    else {$str11 = '<span class="text-success">Нет</span>';}
                    return  $str11;
                },
                'format' => 'html',
            ],
//            'url:url',
            'price',
            //'currency_id',
            //'category_id',
            'category_name',
            //'source',
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            //'delivery',
            'model',
            //'vendor',
            //'vendor_code',
            //'description:ntext',
            //'country_of_origin',
            //'barcode',
            //'sales_notes',
            //'product_type_name',
            //'product_type_id',
            //'silhouette_name',
            //'silhouette_id',
            'cloth_name',
            //'cloth_id',
            'color_name',
            //'color_id',
            //'print_name',
            //'print_id',
            //'style_name',
            //'style_id',
            'season_name',
            //'season_id',
            //'for_full',
            'product_length_name',
            //'product_length_id',
            //'sleeve_name',
            //'sleeve_id',
            //'cutout_collar_name',
            //'cutout_collar_id',
            //'additional_details_name',
            //'additional_details_id',
            'sizes',
            //'sizes_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
