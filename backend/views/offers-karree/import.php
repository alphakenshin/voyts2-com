<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары с ресурса karree.com.ua успешно импортированы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-karree-index">
    <p>
        Запустите процесс получение размеров из заголовка товаров <br></p>
    <p></p><br>

    <?= Html::a('Запустить', ['sizes'], ['class' => 'btn btn-success']) ?>
    </p>

</div>