<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersKarree */

$this->title = 'Create Offers Karree';
$this->params['breadcrumbs'][] = ['label' => 'Offers Karrees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-karree-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
