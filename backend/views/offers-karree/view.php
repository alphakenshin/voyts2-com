<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\OffersKarree */

$this->title = $model->sort_id;
$this->params['breadcrumbs'][] = ['label' => 'Offers Karrees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="offers-karree-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->sort_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->sort_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'sort_id',
            'id',
            'available',
            'url:url',
            'price',
            'currency_id',
            'category_id',
            'source',
            'main_img',
            'gallery_1',
            'gallery_2',
            'gallery_3',
            'gallery_4',
            'gallery_5',
            'delivery',
            'model',
            'vendor',
            'vendor_code',
            'description:ntext',
            'country_of_origin',
            'barcode',
            'sales_notes',
            'product_type_name',
            'product_type_id',
            'silhouette_name',
            'silhouette_id',
            'cloth_name',
            'cloth_id',
            'color_name',
            'color_id',
            'print_name',
            'print_id',
            'style_name',
            'style_id',
            'season_name',
            'season_id',
            'for_full',
            'product_length_name',
            'product_length_id',
            'sleeve_name',
            'sleeve_id',
            'cutout_collar_name',
            'cutout_collar_id',
            'additional_details_name',
            'additional_details_id',
            'sizes',
            'sizes_id',
        ],
    ]) ?>

</div>
