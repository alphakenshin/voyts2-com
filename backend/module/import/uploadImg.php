<?php

// Каким-то образом получим ссылку
$url = 'https://site.ru/picture.jpg';

// Проверим HTTP в адресе ссылки
if (!preg_match("/^https?:/i", $url) && filter_var($url, FILTER_VALIDATE_URL)) {
    die('Укажите корректную ссылку на удалённый файл.');
}

// Запустим cURL с нашей ссылкой
$ch = curl_init($url);

// Укажем настройки для cURL
curl_setopt_array($ch, [

    // Укажем максимальное время работы cURL
    CURLOPT_TIMEOUT => 60,

    // Разрешим следовать перенаправлениям
    CURLOPT_FOLLOWLOCATION => 1,

    // Разрешим результат писать в переменную
    CURLOPT_RETURNTRANSFER => 1,

    // Включим индикатор загрузки данных
    CURLOPT_NOPROGRESS => 0,

    // Укажем размер буфера 1 Кбайт
    CURLOPT_BUFFERSIZE => 1024,

    // Напишем функцию для подсчёта скачанных данных
    // Подробнее: http://stackoverflow.com/a/17642638
    CURLOPT_PROGRESSFUNCTION => function ($ch, $dwnldSize, $dwnld, $upldSize, $upld) {

        // Когда будет скачано больше 5 Мбайт, cURL прервёт работу
        if ($dwnld > 1024 * 1024 * 5) {
            return -1;
        }
    },

    // Включим проверку сертификата (по умолчанию)
    CURLOPT_SSL_VERIFYPEER => 1,

    // Проверим имя сертификата и его совпадение с указанным хостом (по умолчанию)
    CURLOPT_SSL_VERIFYHOST => 2,

    // Укажем сертификат проверки
    // Скачать: https://curl.haxx.se/docs/caextract.html
    CURLOPT_CAINFO => __DIR__ . '/cacert.pem',
]);

$raw   = curl_exec($ch);    // Скачаем данные в переменную
$info  = curl_getinfo($ch); // Получим информацию об операции
$error = curl_errno($ch);   // Запишем код последней ошибки

// Завершим сеанс cURL
curl_close($ch);