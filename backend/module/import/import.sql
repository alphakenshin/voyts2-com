-- old
INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes_eu_1`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`)
SELECT `catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes_eu_1`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_garda`, `match_table` WHERE `match_table`.`garda_id` = `offers_garda`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `types`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `size`, `sizes_eu_1`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`)
SELECT `catalog_id`, `types`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `size`, `sizes_eu_1`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_glem`, `match_table` WHERE `match_table`.`glem_id` = `offers_glem`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `color`)
SELECT `catalog_id`, `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`,`size_1`, `size_2`, `size_3`, `size_4`, `size_5`,`color`
FROM `voyts-com_loc`.`offers_issaplus`, `match_table` WHERE `match_table`.`issaplus_id` = `offers_issaplus`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes_eu_1`)
SELECT `catalog_id`, `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes_eu_1`
FROM `voyts-com_loc`.`offers_karree`, `match_table` WHERE `match_table`.`karree_id` = `offers_karree`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`)
SELECT `catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`
FROM `voyts-com_loc`.`offers_olla`, `match_table` WHERE `match_table`.`olla_id` = `offers_olla`.`category_id`;



--

INSERT INTO `voyts-com_loc`.`offers_all` (
 `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`)
SELECT  `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_garda`;

INSERT INTO `voyts-com_loc`.`offers_all` (
 `types`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `sizes`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`)
SELECT  `types`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `sizes`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_glem`;

INSERT INTO `voyts-com_loc`.`offers_all` (
 `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`, `sizes`, `size_2`, `size_3`, `size_4`, `size_5`, `color`)
SELECT `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`,`sizes`, `size_2`, `size_3`, `size_4`, `size_5`,`color`
FROM `voyts-com_loc`.`offers_issaplus`;

INSERT INTO `voyts-com_loc`.`offers_all` (
 `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes`)
SELECT `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes`
FROM `voyts-com_loc`.`offers_karree`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes`)
SELECT  `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes`
FROM `voyts-com_loc`.`offers_olla`;





--


INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`)
SELECT `catalog_id`, `group_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`, `sales_notes`, `composition`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `brand`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_garda_group`, `match_table` WHERE `match_table`.`garda_id` = `offers_garda_group`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `type`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `size`, `sizes_eu_1`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`)
SELECT `catalog_id`, `type`, `url`, `price`, `currency_id`, `category_id`, `source`, `delivery`, `pref`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `product_type`, `sales_notes`, `size`, `sizes_eu_1`, `barcode`, `color`, `print`, `style`, `season`, `product_length`, `sleeve`
FROM `voyts-com_loc`.`offers_glem`, `match_table` WHERE `match_table`.`glem_id` = `offers_glem`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `color`)
SELECT `catalog_id`, `url`, `price`, `price_uah`,`price_rub`,`price_usd`,`opt_price_uah`,`opt_price_rub`,`opt_price_usd`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `style`,`size_1`, `size_2`, `size_3`, `size_4`, `size_5`,`color`
FROM `voyts-com_loc`.`offers_issaplus`, `match_table` WHERE `match_table`.`issaplus_id` = `offers_issaplus`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`)
SELECT `catalog_id`, `available`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`
FROM `voyts-com_loc`.`offers_karree_group`, `match_table` WHERE `match_table`.`karree_id` = `offers_karree_group`.`category_id`;

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`)
SELECT `catalog_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`
FROM `voyts-com_loc`.`offers_olla_group`, `match_table` WHERE `match_table`.`olla_id` = `offers_olla_group`.`category_id`;




-- Issaplus all

INSERT INTO `attr_category_issaplus` (`id`, `input_name`, `category_issaplus_id`, `name_displayed`)
SELECT  `attr_category`.`id`, `attr_category`.`input_name`, `attr_category`.`category_id`, `categories_issaplus`.`name`
FROM `attr_category`
JOIN `categories_issaplus`
ON `attr_category`.`input_name` = `categories_issaplus`.`id`



--  Женская обувь

INSERT INTO `voyts-com_loc`.`offers_all` (
`catalog_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`)
SELECT `catalog_id`, `url`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`
FROM `voyts-com_loc`.`offers_olla_group`, `categories_olla_womens_shoes` WHERE `categories_olla_womens_shoes`.`olla_id` = `offers_olla_group`.`category_id`;

--
--
--
--  Женская обувь OLLA

INSERT INTO `voyts-com_loc`.`product_olla_womens_shoes` (
`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`
)

SELECT `categories_olla_womens_shoes`.`name`, `offers_olla_group`.`available`, `offers_olla_group`.`price`, `offers_olla_group`.`currency_id`, `offers_olla_group`.`category_id`, `offers_olla_group`.`source`, `offers_olla_group`.`main_img`,
`offers_olla_group`.`gallery_1`, `offers_olla_group`.`gallery_2`, `offers_olla_group`.`gallery_3`, `offers_olla_group`.`gallery_4`, `offers_olla_group`.`gallery_5`, `offers_olla_group`.`delivery`, `offers_olla_group`.`model`,
`offers_olla_group`.`vendor`, `offers_olla_group`.`vendor_code`, `offers_olla_group`.`description`, `offers_olla_group`.`country_of_origin`, `offers_olla_group`.`barcode`, `offers_olla_group`.`sales_notes`, `offers_olla_group`.`product_type`,
`offers_olla_group`.`sizes_eu_1`, `offers_olla_group`.`sizes_eu_2`, `offers_olla_group`.`sizes_eu_3`, `offers_olla_group`.`sizes_eu_4`, `offers_olla_group`.`sizes_eu_5`, `offers_olla_group`.`sizes_eu_6`, `offers_olla_group`.`sizes_eu_7`,
`offers_olla_group`.`sizes_eu_8`, `offers_olla_group`.`sizes_eu_9`, `offers_olla_group`.`sizes_eu_10`

FROM `offers_olla_group`
JOIN `categories_olla_womens_shoes`
ON `offers_olla_group`.`category_id` = `categories_olla_womens_shoes`.`id`

---

--  Мужская обувь OLLA

INSERT INTO `voyts-com_loc`.`product_olla_mens_shoes` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`,
 `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`)

SELECT `categories_olla_mens_shoes`.`name`, `offers_olla_group`.`available`, `offers_olla_group`.`price`, `offers_olla_group`.`currency_id`, `offers_olla_group`.`category_id`, `offers_olla_group`.`source`, `offers_olla_group`.`main_img`,
`offers_olla_group`.`gallery_1`, `offers_olla_group`.`gallery_2`, `offers_olla_group`.`gallery_3`, `offers_olla_group`.`gallery_4`, `offers_olla_group`.`gallery_5`, `offers_olla_group`.`delivery`, `offers_olla_group`.`model`,
`offers_olla_group`.`vendor`, `offers_olla_group`.`vendor_code`, `offers_olla_group`.`description`, `offers_olla_group`.`country_of_origin`, `offers_olla_group`.`barcode`, `offers_olla_group`.`sales_notes`, `offers_olla_group`.`product_type`,
`offers_olla_group`.`sizes_eu_1`, `offers_olla_group`.`sizes_eu_2`, `offers_olla_group`.`sizes_eu_3`, `offers_olla_group`.`sizes_eu_4`, `offers_olla_group`.`sizes_eu_5`, `offers_olla_group`.`sizes_eu_6`, `offers_olla_group`.`sizes_eu_7`,
`offers_olla_group`.`sizes_eu_8`, `offers_olla_group`.`sizes_eu_9`, `offers_olla_group`.`sizes_eu_10`

FROM `offers_olla_group`
JOIN `categories_olla_mens_shoes`
ON `offers_olla_group`.`category_id` = `categories_olla_mens_shoes`.`id`


---

--  Детская обувь OLLA

INSERT INTO `voyts-com_loc`.`product_olla_childs_shoes` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`,
 `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`)

SELECT `categories_olla_childs_shoes`.`name`, `offers_olla_group`.`available`, `offers_olla_group`.`price`, `offers_olla_group`.`currency_id`, `offers_olla_group`.`category_id`, `offers_olla_group`.`source`, `offers_olla_group`.`main_img`,
`offers_olla_group`.`gallery_1`, `offers_olla_group`.`gallery_2`, `offers_olla_group`.`gallery_3`, `offers_olla_group`.`gallery_4`, `offers_olla_group`.`gallery_5`, `offers_olla_group`.`delivery`, `offers_olla_group`.`model`,
`offers_olla_group`.`vendor`, `offers_olla_group`.`vendor_code`, `offers_olla_group`.`description`, `offers_olla_group`.`country_of_origin`, `offers_olla_group`.`barcode`, `offers_olla_group`.`sales_notes`, `offers_olla_group`.`product_type`,
`offers_olla_group`.`sizes_eu_1`, `offers_olla_group`.`sizes_eu_2`, `offers_olla_group`.`sizes_eu_3`, `offers_olla_group`.`sizes_eu_4`, `offers_olla_group`.`sizes_eu_5`, `offers_olla_group`.`sizes_eu_6`, `offers_olla_group`.`sizes_eu_7`,
`offers_olla_group`.`sizes_eu_8`, `offers_olla_group`.`sizes_eu_9`, `offers_olla_group`.`sizes_eu_10`

FROM `offers_olla_group`
JOIN `categories_olla_childs_shoes`
ON `offers_olla_group`.`category_id` = `categories_olla_childs_shoes`.`id`


---
--
--
--
--  Женская одежда OLLA

INSERT INTO `voyts-com_loc`.`product_olla_womens_clothes` (
`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `delivery`, `model`, `vendor`, `vendor_code`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`
)

SELECT `categories_olla_womens_clothes`.`name`, `offers_olla_group`.`available`, `offers_olla_group`.`price`, `offers_olla_group`.`currency_id`, `offers_olla_group`.`category_id`, `offers_olla_group`.`source`, `offers_olla_group`.`main_img`,
`offers_olla_group`.`gallery_1`, `offers_olla_group`.`gallery_2`, `offers_olla_group`.`gallery_3`, `offers_olla_group`.`gallery_4`, `offers_olla_group`.`gallery_5`, `offers_olla_group`.`delivery`, `offers_olla_group`.`model`,
`offers_olla_group`.`vendor`, `offers_olla_group`.`vendor_code`, `offers_olla_group`.`description`, `offers_olla_group`.`country_of_origin`, `offers_olla_group`.`barcode`, `offers_olla_group`.`sales_notes`, `offers_olla_group`.`product_type`,
`offers_olla_group`.`sizes_eu_1`, `offers_olla_group`.`sizes_eu_2`, `offers_olla_group`.`sizes_eu_3`, `offers_olla_group`.`sizes_eu_4`, `offers_olla_group`.`sizes_eu_5`, `offers_olla_group`.`sizes_eu_6`, `offers_olla_group`.`sizes_eu_7`,
`offers_olla_group`.`sizes_eu_8`, `offers_olla_group`.`sizes_eu_9`, `offers_olla_group`.`sizes_eu_10`

FROM `offers_olla_group`
JOIN `categories_olla_womens_clothes`
ON `offers_olla_group`.`category_id` = `categories_olla_womens_clothes`.`id`

---
--  Женская одежда Garda

INSERT INTO `voyts-com_loc`.`product_womens_clothes` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`,
 `model`, `sku`, `description`, `country_of_origin`, `sales_notes`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`,
`sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`,
 `cloth`, `color`, `style`, `season`, `product_length`)


SELECT `categories_garda`.`name`, `offers_garda_group`.`available`, `offers_garda_group`.`price`, `offers_garda_group`.`currency_id`, `offers_garda_group`.`category_id`,
`offers_garda_group`.`source`, `offers_garda_group`.`main_img`,
`offers_garda_group`.`gallery_1`, `offers_garda_group`.`gallery_2`, `offers_garda_group`.`gallery_3`, `offers_garda_group`.`gallery_4`, `offers_garda_group`.`gallery_5`,
 `offers_garda_group`.`model`,`offers_garda_group`.`sku`, `offers_garda_group`.`description`, `offers_garda_group`.`country_of_origin`,
 `offers_garda_group`.`sales_notes`, `offers_garda_group`.`sizes_eu_1`, `offers_garda_group`.`sizes_eu_2`,
 `offers_garda_group`.`sizes_eu_3`, `offers_garda_group`.`sizes_eu_4`, `offers_garda_group`.`sizes_eu_5`, `offers_garda_group`.`sizes_eu_6`, `offers_garda_group`.`sizes_eu_7`,
`offers_garda_group`.`sizes_eu_8`, `offers_garda_group`.`sizes_eu_9`, `offers_garda_group`.`sizes_eu_10`, `offers_garda_group`.`cloth`, `offers_garda_group`.`color`,
 `offers_garda_group`.`style`,  `offers_garda_group`.`season`,  `offers_garda_group`.`product_length`

FROM `offers_garda_group`
JOIN `categories_garda`
ON `offers_garda_group`.`category_id` = `categories_garda`.`id`

---

--  Женская одежда Glem

INSERT INTO `voyts-com_loc`.`product_womens_clothes` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`,`main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`,
`brand`, `model`, `sku`, `delivery`, `description`, `country_of_origin`, `sales_notes`, `S`, `M`, `L`, `XL`, `XXL`, `XXXL`,  `color`, `style`, `season`, `product_length`, `for_full`)


SELECT  `categories_glem`.`name`, `offers_glem`.`available`, `offers_glem`.`price`, `offers_glem`.`currency_id`, `offers_glem`.`category_id`,  `offers_glem`.`source`, `offers_glem`.`main_img`, `offers_glem`.`gallery_1`, `offers_glem`.`gallery_2`,
 `offers_glem`.`gallery_3`, `offers_glem`.`gallery_4`, `offers_glem`.`gallery_5`, `offers_glem`.`vendor`, `offers_glem`.`model`, `offers_glem`.`vendor_code`,  `offers_glem`.`delivery`, `offers_glem`.`description`,
`offers_glem`.`country_of_origin`, `offers_glem`.`sales_notes`, `offers_glem`.`size_s_quantity`, `offers_glem`.`size_m_quantity`, `offers_glem`.`size_l_quantity`, `offers_glem`.`size_xl_quantity`, `offers_glem`.`size_xxl_quantity`,
`offers_glem`.`size_xxxl_quantity`, `offers_glem`.`color`, `offers_glem`.`style`, `offers_glem`.`season`, `offers_glem`.`product_length`, `offers_glem`.`for_full`

FROM `offers_glem`
JOIN `categories_glem`
ON `offers_glem`.`category_id` = `categories_glem`.`id`

-----
------

--  Женская одежда Karree

INSERT INTO `voyts-com_loc`.`product_womens_clothes` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `model`,`sku`, `delivery`, `description`, `country_of_origin`, `sales_notes`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`,
`cloth`, `color`, `style`, `season`, `product_length`, `for_full`, `silhouette`, `print`, `cutout_collar`, `additional_details`, `sleeve`)

SELECT `categories_karree`.`name`, `offers_karree_group`.`available`, `offers_karree_group`.`price`, `offers_karree_group`.`currency_id`, `offers_karree_group`.`category_id`, `offers_karree_group`.`source`, `offers_karree_group`.`main_img`,
`offers_karree_group`.`gallery_1`, `offers_karree_group`.`gallery_2`, `offers_karree_group`.`gallery_3`, `offers_karree_group`.`gallery_4`, `offers_karree_group`.`gallery_5`, `offers_karree_group`.`model`, `offers_karree_group`.`vendor_code`, `offers_karree_group`.`delivery`, `offers_karree_group`.`description`, `offers_karree_group`.`country_of_origin`, `offers_karree_group`.`sales_notes`, `offers_karree_group`.`sizes_eu_1`, `offers_karree_group`.`sizes_eu_2`, `offers_karree_group`.`sizes_eu_3`, `offers_karree_group`.`sizes_eu_4`, `offers_karree_group`.`sizes_eu_5`, `offers_karree_group`.`sizes_eu_6`, `offers_karree_group`.`sizes_eu_7`, `offers_karree_group`.`sizes_eu_8`, `offers_karree_group`.`sizes_eu_9`, `offers_karree_group`.`sizes_eu_10`, `offers_karree_group`.`cloth`, `offers_karree_group`.`color`, `offers_karree_group`.`style`, `offers_karree_group`.`season`, `offers_karree_group`.`product_length`, `offers_karree_group`.`for_full`, `offers_karree_group`.`silhouette`,
`offers_karree_group`.`print`, `offers_karree_group`.`cutout_collar`, `offers_karree_group`.`additional_details`, `offers_karree_group`.`sleeve`

FROM `offers_karree_group`
JOIN `categories_karree`
ON `offers_karree_group`.`category_id` = `categories_karree`.`id`

-----


INSERT INTO `voyts-com_loc`.`product` (`type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`,
 `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `main_img`, `gallery_1`,
 `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`,
 `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`,
  `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`,
  `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`,
   `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`,
   `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`,
   `param_8`, `name_9`, `param_9`, `name_10`)

SELECT `offers_all`.`type`, `offers_all`.`available`, `offers_all`.`catalog_id`, `offers_all`.`url`, `offers_all`.`price`, `offers_all`.`price_uah`, `offers_all`.`price_rub`, `offers_all`.`price_usd`, `offers_all`.`opt_price_uah`,
 `offers_all`.`opt_price_rub`, `offers_all`.`opt_price_usd`, `offers_all`.`currency_id`, `offers_all`.`sale`, `offers_all`.`category_id`, `offers_all`.`composition`, `offers_all`.`source`, `offers_all_img`.`main_img`, `offers_all_img`.`gallery_1`,
 `offers_all_img`.`gallery_2`, `offers_all_img`.`gallery_3`, `offers_all_img`.`gallery_4`, `offers_all_img`.`gallery_5`, `offers_all`.`store`, `offers_all`.`brand`, `offers_all`.`delivery`, `offers_all`.`pref`, `offers_all`.`model`, `offers_all`.`vendor`,
 `offers_all`.`vendor_code`, `offers_all`.`description`, `offers_all`.`manufacturer_warranty`, `offers_all`.`country_of_origin`, `offers_all`.`size`, `offers_all`.`barcode`,
  `offers_all`.`sales_notes`, `offers_all`.`product_type`, `offers_all`.`sizes_eu_1`, `offers_all`.`sizes_eu_2`, `offers_all`.`sizes_eu_3`, `offers_all`.`sizes_eu_4`, `offers_all`.`sizes_eu_5`, `offers_all`.`sizes_eu_6`, `offers_all`.`sizes_eu_7`,
  `offers_all`.`sizes_eu_8`, `offers_all`.`sizes_eu_9`, `offers_all`.`sizes_eu_10`, `offers_all`.`sizes_eu_11`, `offers_all`.`silhouette`, `offers_all`.`cloth`, `offers_all`.`color`, `offers_all`.`print`, `offers_all`.`style`, `offers_all`.`season`,
   `offers_all`.`for_full`, `offers_all`.`product_length`, `offers_all`.`sleeve`, `offers_all`.`cutout_collar`, `offers_all`.`additional_details`, `offers_all`.`all_quantity`, `offers_all`.`size_s_quantity`, `offers_all`.`size_m_quantity`,
   `offers_all`.`size_l_quantity`, `offers_all`.`size_xl_quantity`, `offers_all`.`size_xxl_quantity`, `offers_all`.`size_xxxl_quantity`, `offers_all`.`size_1`, `offers_all`.`size_2`, `offers_all`.`size_3`, `offers_all`.`size_4`, `offers_all`.`size_5`,
   `offers_all`.`param_8`, `offers_all`.`name_9`, `offers_all`.`param_9`, `offers_all`.`name_10`
FROM `offers_all`
LEFT OUTER JOIN `offers_all_img`
ON `offers_all`.`id` = `offers_all_img`.`id`


INSERT INTO `voyts-com_loc`.`product` (`type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`,
 `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `main_img`, `gallery_1`,
 `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`,
 `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`,
  `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`,
  `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`,
   `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`,
   `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`,
   `param_8`, `name_9`, `param_9`, `name_10`)

SELECT `offers_all`.`type`, `offers_all`.`available`, `offers_all`.`catalog_id`, `offers_all`.`url`, `offers_all`.`price`, `offers_all`.`price_uah`, `offers_all`.`price_rub`, `offers_all`.`price_usd`, `offers_all`.`opt_price_uah`,
 `offers_all`.`opt_price_rub`, `offers_all`.`opt_price_usd`, `offers_all`.`currency_id`, `offers_all`.`sale`, `offers_all`.`category_id`, `offers_all`.`composition`, `offers_all`.`source`, `offers_all_img`.`main_img`, `offers_all_img`.`gallery_1`,
 `offers_all_img`.`gallery_2`, `offers_all_img`.`gallery_3`, `offers_all_img`.`gallery_4`, `offers_all_img`.`gallery_5`, `offers_all`.`store`, `offers_all`.`brand`, `offers_all`.`delivery`, `offers_all`.`pref`, `offers_all`.`model`, `offers_all`.`vendor`,
 `offers_all`.`vendor_code`, `offers_all`.`description`, `offers_all`.`manufacturer_warranty`, `offers_all`.`country_of_origin`, `offers_all`.`size`, `offers_all`.`barcode`,
  `offers_all`.`sales_notes`, `offers_all`.`product_type`, `offers_all`.`sizes_eu_1`, `offers_all`.`sizes_eu_2`, `offers_all`.`sizes_eu_3`, `offers_all`.`sizes_eu_4`, `offers_all`.`sizes_eu_5`, `offers_all`.`sizes_eu_6`, `offers_all`.`sizes_eu_7`,
  `offers_all`.`sizes_eu_8`, `offers_all`.`sizes_eu_9`, `offers_all`.`sizes_eu_10`, `offers_all`.`sizes_eu_11`, `offers_all`.`silhouette`, `offers_all`.`cloth`, `offers_all`.`color`, `offers_all`.`print`, `offers_all`.`style`, `offers_all`.`season`,
   `offers_all`.`for_full`, `offers_all`.`product_length`, `offers_all`.`sleeve`, `offers_all`.`cutout_collar`, `offers_all`.`additional_details`, `offers_all`.`all_quantity`, `offers_all`.`size_s_quantity`, `offers_all`.`size_m_quantity`,
   `offers_all`.`size_l_quantity`, `offers_all`.`size_xl_quantity`, `offers_all`.`size_xxl_quantity`, `offers_all`.`size_xxxl_quantity`, `offers_all`.`size_1`, `offers_all`.`size_2`, `offers_all`.`size_3`, `offers_all`.`size_4`, `offers_all`.`size_5`,
   `offers_all`.`param_8`, `offers_all`.`name_9`, `offers_all`.`param_9`, `offers_all`.`name_10`
FROM `offers_all`
LEFT OUTER JOIN `offers_all_img`
ON `offers_all`.`id` = `offers_all_img`.`id`

--
-- INSERT INTO `voyts-com_loc`.`offers_all` (`id`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`)
-- SELECT `id`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`
-- FROM `voyts-com_loc`.`offers_all_img` WHERE `offers_all`.`id` = `offers_all_img`.`id`;


-- INSERT INTO `voyts-com_loc`.`product` (`id`, `type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`, `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`, `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `param_8`, `name_9`, `param_9`, `name_10`)
-- SELECT `id`, `type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`, `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`,`sizes_eu_2`,`sizes_eu_3`,`sizes_eu_4`,`sizes_eu_5`,`sizes_eu_6`,`sizes_eu_7`,`sizes_eu_8`,`sizes_eu_9`,`sizes_eu_10`,`sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`, `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `param_8`, `name_9`, `param_9`, `name_10`
-- FROM `voyts-com_loc`.`offers_all` WHERE `product`.`id` = `offers_all`.`id`;


-- INSERT INTO `voyts-com_loc`.`product` (
-- `id`, `type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`, `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`, `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `param_8`, `name_9`, `param_9`, `name_10`)
-- SELECT
-- `id`, `type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`, `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`, `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`, `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`, `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`, `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`, `param_8`, `name_9`, `param_9`, `name_10`
-- FROM `voyts-com_loc`.`offers_all` WHERE `id` = `offers_all`.`id`;
--
--
-- INSERT INTO `voyts-com_loc`.`product` (`id`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`)
-- SELECT `id`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5` FROM `voyts-com_loc`.`offers_all_img`
-- WHERE `offers_all_img`.`id` = `id`;


INSERT INTO `voyts-com_loc`.`product` (`type`, `available`, `catalog_id`, `url`, `price`, `price_uah`, `price_rub`, `price_usd`, `opt_price_uah`,
 `opt_price_rub`, `opt_price_usd`, `currency_id`, `sale`, `category_id`, `composition`, `source`, `main_img`, `gallery_1`,
 `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `store`, `brand`, `delivery`, `pref`, `model`, `vendor`,
 `vendor_code`, `description`, `manufacturer_warranty`, `country_of_origin`, `size`, `barcode`,
  `sales_notes`, `product_type`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`,
  `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `sizes_eu_11`, `silhouette`, `cloth`, `color`, `print`, `style`, `season`,
   `for_full`, `product_length`, `sleeve`, `cutout_collar`, `additional_details`, `all_quantity`, `size_s_quantity`, `size_m_quantity`,
   `size_l_quantity`, `size_xl_quantity`, `size_xxl_quantity`, `size_xxxl_quantity`, `size_1`, `size_2`, `size_3`, `size_4`, `size_5`,
   `param_8`, `name_9`, `param_9`, `name_10`)

SELECT `offers_all`.`type`, `offers_all`.`available`, `offers_all`.`catalog_id`, `offers_all`.`url`, `offers_all`.`price`, `offers_all`.`price_uah`, `offers_all`.`price_rub`, `offers_all`.`price_usd`, `offers_all`.`opt_price_uah`,
 `offers_all`.`opt_price_rub`, `offers_all`.`opt_price_usd`, `offers_all`.`currency_id`, `offers_all`.`sale`, `offers_all`.`category_id`, `offers_all`.`composition`, `offers_all`.`source`, `offers_all_img`.`main_img`, `offers_all_img`.`gallery_1`,
 `offers_all_img`.`gallery_2`, `offers_all_img`.`gallery_3`, `offers_all_img`.`gallery_4`, `offers_all_img`.`gallery_5`, `offers_all`.`store`, `offers_all`.`brand`, `offers_all`.`delivery`, `offers_all`.`pref`, `offers_all`.`model`, `offers_all`.`vendor`,
 `offers_all`.`vendor_code`, `offers_all`.`description`, `offers_all`.`manufacturer_warranty`, `offers_all`.`country_of_origin`, `offers_all`.`size`, `offers_all`.`barcode`,
  `offers_all`.`sales_notes`, `offers_all`.`product_type`, `offers_all`.`sizes_eu_1`, `offers_all`.`sizes_eu_2`, `offers_all`.`sizes_eu_3`, `offers_all`.`sizes_eu_4`, `offers_all`.`sizes_eu_5`, `offers_all`.`sizes_eu_6`, `offers_all`.`sizes_eu_7`,
  `offers_all`.`sizes_eu_8`, `offers_all`.`sizes_eu_9`, `offers_all`.`sizes_eu_10`, `offers_all`.`sizes_eu_11`, `offers_all`.`silhouette`, `offers_all`.`cloth`, `offers_all`.`color`, `offers_all`.`print`, `offers_all`.`style`, `offers_all`.`season`,
   `offers_all`.`for_full`, `offers_all`.`product_length`, `offers_all`.`sleeve`, `offers_all`.`cutout_collar`, `offers_all`.`additional_details`, `offers_all`.`all_quantity`, `offers_all`.`size_s_quantity`, `offers_all`.`size_m_quantity`,
   `offers_all`.`size_l_quantity`, `offers_all`.`size_xl_quantity`, `offers_all`.`size_xxl_quantity`, `offers_all`.`size_xxxl_quantity`, `offers_all`.`size_1`, `offers_all`.`size_2`, `offers_all`.`size_3`, `offers_all`.`size_4`, `offers_all`.`size_5`,
   `offers_all`.`param_8`, `offers_all`.`name_9`, `offers_all`.`param_9`, `offers_all`.`name_10`
FROM `offers_all`
LEFT OUTER JOIN `offers_all_img`
ON `offers_all`.`id` = `offers_all_img`.`id`



INSERT INTO `voyts-com_loc`.`product_w_cl` (`type`, `available`, `price`, `currency_id`, `category_id`, `source`, `main_img`, `gallery_1`, `gallery_2`, `gallery_3`, `gallery_4`, `gallery_5`, `brand`, `model`, `vendor_code`, `delivery`, `description`, `country_of_origin`, `barcode`, `sales_notes`, `composition`, `sizes_eu_1`, `sizes_eu_2`, `sizes_eu_3`, `sizes_eu_4`, `sizes_eu_5`, `sizes_eu_6`, `sizes_eu_7`, `sizes_eu_8`, `sizes_eu_9`, `sizes_eu_10`, `S`, `M`, `L`, `XL`, `XXL`, `XXXL`, `cloth`, `color`, `style`, `season`, `product_length`, `for_full`, `silhouette`, `print`, `cutout_collar`, `additional_details`, `sleeve`)
SELECT  `product_womens_clothes`.`type`, `product_womens_clothes`.`available`, `product_womens_clothes`.`price`, `product_womens_clothes`.`currency_id`, `product_womens_clothes`.`category_id`, `product_womens_clothes`.`source`, `product_womens_clothes_img`.`main_img`, `product_womens_clothes_img`.`gallery_1`, `product_womens_clothes_img`.`gallery_2`, `product_womens_clothes_img`.`gallery_3`, `product_womens_clothes_img`.`gallery_4`, `product_womens_clothes_img`.`gallery_5`, `product_womens_clothes`.`brand`, `product_womens_clothes`.`model`, `product_womens_clothes`.`vendor_code`, `product_womens_clothes`.`delivery`, `product_womens_clothes`.`description`, `product_womens_clothes`.`country_of_origin`, `product_womens_clothes`.`barcode`, `product_womens_clothes`.`sales_notes`, `product_womens_clothes`.`composition`, `product_womens_clothes`.`sizes_eu_1`, `product_womens_clothes`.`sizes_eu_2`, `product_womens_clothes`.`sizes_eu_3`, `product_womens_clothes`.`sizes_eu_4`, `product_womens_clothes`.`sizes_eu_5`, `product_womens_clothes`.`sizes_eu_6`, `product_womens_clothes`.`sizes_eu_7`, `product_womens_clothes`.`sizes_eu_8`, `product_womens_clothes`.`sizes_eu_9`, `product_womens_clothes`.`sizes_eu_10`, `product_womens_clothes`.`S`, `product_womens_clothes`.`M`, `product_womens_clothes`.`L`, `product_womens_clothes`.`XL`, `product_womens_clothes`.`XXL`, `product_womens_clothes`.`XXXL`, `product_womens_clothes`.`cloth`, `product_womens_clothes`.`color`, `product_womens_clothes`.`style`, `product_womens_clothes`.`season`, `product_womens_clothes`.`product_length`, `product_womens_clothes`.`for_full`, `product_womens_clothes`.`silhouette`, `product_womens_clothes`.`print`, `product_womens_clothes`.`cutout_collar`, `product_womens_clothes`.`additional_details`, `product_womens_clothes`.`sleeve`
FROM `product_womens_clothes`
LEFT OUTER JOIN `product_womens_clothes_img`
ON `product_womens_clothes`.`vendor_code` = `product_womens_clothes_img`.`vendor_code`
