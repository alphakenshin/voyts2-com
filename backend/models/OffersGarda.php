<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_garda".
 *
 * @property int $sort_id
 * @property int $id
 * @property int|null $param_id
 * @property string $available
 * @property int|null $group_id
 * @property string|null $url
 * @property int|null $price
 * @property string|null $currency_id
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $model
 * @property string|null $vendor_code
 * @property string|null $description
 * @property string|null $country_of_origin
 * @property string|null $product_type_name
 * @property int|null $product_type_id
 * @property string|null $sales_notes
 * @property string|null $composition_name
 * @property int|null $composition_id
 * @property string|null $sizes
 * @property int|null $sizes_id
 * @property string|null $silhouette_name
 * @property int|null $silhouette_id
 * @property string|null $cloth_name
 * @property int|null $cloth_id
 * @property string|null $color_name
 * @property int|null $color_id
 * @property string|null $sort_color_name
 * @property int|null $sort_color_id
 * @property string|null $print_name
 * @property int|null $print_id
 * @property string|null $style_name
 * @property int|null $style_id
 * @property string|null $season_name
 * @property int|null $season_id
 * @property string|null $brand_name
 * @property int|null $brand_id
 * @property string|null $product_length_name
 * @property int|null $product_length_id
 * @property string|null $sleeve_name
 * @property int|null $sleeve_id
 */
class OffersGarda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_garda';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['id', 'available', 'source'], 'required'],
//            [['id', 'param_id', 'group_id', 'price', 'category_id', 'product_type_id', 'composition_id', 'sizes_id', 'silhouette_id', 'cloth_id', 'color_id', 'sort_color_id', 'print_id', 'style_id', 'season_id', 'brand_id', 'product_length_id', 'sleeve_id'], 'integer'],
//            [['description'], 'string'],
//            [['available', 'source', 'sizes'], 'string', 'max' => 32],
//            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'vendor_code', 'country_of_origin', 'product_type_name', 'sales_notes', 'composition_name', 'silhouette_name', 'cloth_name', 'color_name', 'print_name', 'style_name', 'season_name', 'brand_name', 'product_length_name', 'sleeve_name'], 'string', 'max' => 255],
//            [['category_name', 'sort_color_name'], 'string', 'max' => 128],
//            [['id'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sort_id' => 'Sort ID',
            'id' => 'ID',
            'param_id' => 'Param ID',
            'available' => 'Available',
            'group_id' => 'Group ID',
            'url' => 'Url',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'model' => 'Model',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'country_of_origin' => 'Country Of Origin',
            'product_type_name' => 'Product Type Name',
            'product_type_id' => 'Product Type ID',
            'sales_notes' => 'Sales Notes',
            'composition_name' => 'Composition Name',
            'composition_id' => 'Composition ID',
            'sizes' => 'Sizes',
            'sizes_id' => 'Sizes ID',
            'silhouette_name' => 'Silhouette Name',
            'silhouette_id' => 'Silhouette ID',
            'cloth_name' => 'Cloth Name',
            'cloth_id' => 'Cloth ID',
            'color_name' => 'Color Name',
            'color_id' => 'Color ID',
            'sort_color_name' => 'Sort Color Name',
            'sort_color_id' => 'Sort Color ID',
            'print_name' => 'Print Name',
            'print_id' => 'Print ID',
            'style_name' => 'Style Name',
            'style_id' => 'Style ID',
            'season_name' => 'Season Name',
            'season_id' => 'Season ID',
            'brand_name' => 'Brand Name',
            'brand_id' => 'Brand ID',
            'product_length_name' => 'Product Length Name',
            'product_length_id' => 'Product Length ID',
            'sleeve_name' => 'Sleeve Name',
            'sleeve_id' => 'Sleeve ID',
        ];
    }
}
