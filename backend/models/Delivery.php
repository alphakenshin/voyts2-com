<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property int $delivery_id
 * @property int $city_id
 * @property int $department_id
 * @property string $description
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['delivery_id', 'city_id', 'department_id', 'description'], 'required'],
            [['delivery_id', 'city_id', 'department_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'delivery_id' => 'Delivery ID',
            'city_id' => 'City ID',
            'department_id' => 'Department ID',
            'description' => 'Description',
        ];
    }
}
