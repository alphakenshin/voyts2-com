<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_issaplus".
 *
 * @property int $id
 * @property string $sku
 * @property int|null $param_id
 * @property string $available
 * @property string $url
 * @property int|null $price
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string $model
 * @property string $collection_name
 * @property int|null $collection_id
 * @property string|null $color_name
 * @property int|null $color_id
 * @property string|null $sort_color_name
 * @property int|null $sort_color_id
 * @property string $style_name
 * @property int|null $style_id
 * @property string|null $sizes_1
 * @property int|null $sizes_1_id
 * @property string|null $sizes_2
 * @property int|null $sizes_2_id
 * @property string|null $sizes_3
 * @property int|null $sizes_3_id
 * @property string|null $sizes_4
 * @property int|null $sizes_4_id
 * @property string|null $sizes_5
 * @property int|null $sizes_5_id
 * @property float $price_uah
 * @property float $price_rub
 * @property float $price_usd
 * @property float $opt_price_uah
 * @property float $opt_price_rub
 * @property float $opt_price_usd
 */
class OffersIssaplus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_issaplus';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['sku', 'url', 'source', 'model', 'collection_name', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd'], 'required'],
//            [['param_id', 'price', 'category_id', 'collection_id', 'color_id', 'sort_color_id', 'style_id', 'sizes_1_id', 'sizes_2_id', 'sizes_3_id', 'sizes_4_id', 'sizes_5_id'], 'integer'],
//            [['price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd'], 'number'],
//            [['sku', 'category_name', 'collection_name', 'sort_color_name'], 'string', 'max' => 128],
//            [['available'], 'string', 'max' => 21],
//            [['url', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'color_name'], 'string', 'max' => 255],
//            [['source', 'sizes_1', 'sizes_2', 'sizes_3', 'sizes_4', 'sizes_5'], 'string', 'max' => 32],
//            [['style_name'], 'string', 'max' => 64],
//            [['sku'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'param_id' => 'Param ID',
            'available' => 'Available',
            'url' => 'Url',
            'price' => 'Price',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'model' => 'Model',
            'collection_name' => 'Collection Name',
            'collection_id' => 'Collection ID',
            'color_name' => 'Color Name',
            'color_id' => 'Color ID',
            'sort_color_name' => 'Sort Color Name',
            'sort_color_id' => 'Sort Color ID',
            'style_name' => 'Style Name',
            'style_id' => 'Style ID',
            'sizes_1' => 'Sizes 1',
            'sizes_1_id' => 'Sizes 1 ID',
            'sizes_2' => 'Sizes 2',
            'sizes_2_id' => 'Sizes 2 ID',
            'sizes_3' => 'Sizes 3',
            'sizes_3_id' => 'Sizes 3 ID',
            'sizes_4' => 'Sizes 4',
            'sizes_4_id' => 'Sizes 4 ID',
            'sizes_5' => 'Sizes 5',
            'sizes_5_id' => 'Sizes 5 ID',
            'price_uah' => 'Price Uah',
            'price_rub' => 'Price Rub',
            'price_usd' => 'Price Usd',
            'opt_price_uah' => 'Opt Price Uah',
            'opt_price_rub' => 'Opt Price Rub',
            'opt_price_usd' => 'Opt Price Usd',
        ];
    }
}
