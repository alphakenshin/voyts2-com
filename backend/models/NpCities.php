<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "np_cities".
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $description_ru
 * @property string|null $ref
 * @property int|null $delivery_1
 * @property int|null $delivery_2
 * @property int|null $delivery_3
 * @property int|null $delivery_4
 * @property int|null $delivery_5
 * @property int|null $delivery_6
 * @property int|null $delivery_7
 * @property string|null $area
 * @property string|null $settlement_type
 * @property int|null $is_branch
 * @property string|null $prevent_entry_new_streets_user
 * @property string|null $conglomerates
 * @property int|null $city_id
 * @property string|null $settlement_type_description
 * @property string|null $settlement_type_description_ru
 * @property int|null $special_cash_check
 */
class NpCities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_cities';
    }

    public function getNpCitiesWarehouses() {
        return $this->hasMany(NpWarehouses::className(),['city_ref' => 'ref']);
    }

//    public function rules()
//    {
//        return [
//            [['delivery_1', 'delivery_2', 'delivery_3', 'delivery_4', 'delivery_5', 'delivery_6', 'delivery_7', 'is_branch', 'city_id', 'special_cash_check'], 'integer'],
//            [['description', 'description_ru', 'ref', 'area', 'settlement_type', 'prevent_entry_new_streets_user', 'conglomerates', 'settlement_type_description', 'settlement_type_description_ru'], 'string', 'max' => 255],
//        ];
//    }

    public function getNpAreas() {
        return $this->hasOne(NpAreas::className(),['ref' => 'area']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'description_ru' => 'Description Ru',
            'ref' => 'Ref',
            'delivery_1' => 'Delivery 1',
            'delivery_2' => 'Delivery 2',
            'delivery_3' => 'Delivery 3',
            'delivery_4' => 'Delivery 4',
            'delivery_5' => 'Delivery 5',
            'delivery_6' => 'Delivery 6',
            'delivery_7' => 'Delivery 7',
            'area' => 'Area',
            'settlement_type' => 'Settlement Type',
            'is_branch' => 'Is Branch',
            'prevent_entry_new_streets_user' => 'Prevent Entry New Streets User',
            'conglomerates' => 'Conglomerates',
            'city_id' => 'City ID',
            'settlement_type_description' => 'Settlement Type Description',
            'settlement_type_description_ru' => 'Settlement Type Description Ru',
            'special_cash_check' => 'Special Cash Check',
        ];
    }
}
