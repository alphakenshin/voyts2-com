<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_olla_group".
 *
 * @property int $id
 * @property string|null $sku
 * @property int|null $param_id
 * @property string|null $available
 * @property string|null $url
 * @property int|null $price
 * @property int|null $old_price
 * @property string|null $currency_id
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $delivery
 * @property string|null $model
 * @property string|null $vendor_code
 * @property string|null $description
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $brand_name
 * @property int|null $brand_id
 * @property string|null $sizes_1
 * @property int|null $sizes_1_id
 * @property string|null $sizes_2
 * @property int|null $sizes_2_id
 * @property string|null $sizes_3
 * @property int|null $sizes_3_id
 * @property string|null $sizes_4
 * @property int|null $sizes_4_id
 * @property string|null $sizes_5
 * @property int|null $sizes_5_id
 * @property string|null $sizes_6
 * @property int|null $sizes_6_id
 * @property string|null $product_type
 */
class OffersOllaGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_olla_group';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['param_id', 'price', 'old_price', 'category_id', 'brand_id', 'sizes_1_id', 'sizes_2_id', 'sizes_3_id', 'sizes_4_id', 'sizes_5_id', 'sizes_6_id'], 'integer'],
//            [['source'], 'required'],
//            [['description'], 'string'],
//            [['sku', 'category_name'], 'string', 'max' => 128],
//            [['available', 'source', 'sizes_1', 'sizes_2', 'sizes_3', 'sizes_4', 'sizes_5', 'sizes_6'], 'string', 'max' => 32],
//            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'delivery', 'model', 'vendor_code', 'country_of_origin', 'barcode', 'sales_notes', 'brand_name', 'product_type'], 'string', 'max' => 255],
//            [['sku'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'param_id' => 'Param ID',
            'available' => 'Available',
            'url' => 'Url',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'currency_id' => 'Currency ID',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'delivery' => 'Delivery',
            'model' => 'Model',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'brand_name' => 'Brand Name',
            'brand_id' => 'Brand ID',
            'sizes_1' => 'Sizes 1',
            'sizes_1_id' => 'Sizes 1 ID',
            'sizes_2' => 'Sizes 2',
            'sizes_2_id' => 'Sizes 2 ID',
            'sizes_3' => 'Sizes 3',
            'sizes_3_id' => 'Sizes 3 ID',
            'sizes_4' => 'Sizes 4',
            'sizes_4_id' => 'Sizes 4 ID',
            'sizes_5' => 'Sizes 5',
            'sizes_5_id' => 'Sizes 5 ID',
            'sizes_6' => 'Sizes 6',
            'sizes_6_id' => 'Sizes 6 ID',
            'product_type' => 'Product Type',
        ];
    }
}
