<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_karree".
 *
 * @property int $sort_id
 * @property int $id
 * @property int|null $param_id
 * @property string|null $available
 * @property string|null $url
 * @property int|null $price
 * @property string|null $currency_id
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $delivery
 * @property string|null $model
 * @property string|null $vendor
 * @property string|null $vendor_code
 * @property string|null $description
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $product_type_name
 * @property int|null $product_type_id
 * @property string|null $silhouette_name
 * @property int|null $silhouette_id
 * @property string|null $cloth_name
 * @property int|null $cloth_id
 * @property string|null $color_name
 * @property int|null $color_id
 * @property string|null $sort_color_name
 * @property int|null $sort_color_id
 * @property string|null $print_name
 * @property int|null $print_id
 * @property string|null $style_name
 * @property int|null $style_id
 * @property string|null $season_name
 * @property int|null $season_id
 * @property string|null $for_full
 * @property string|null $product_length_name
 * @property int|null $product_length_id
 * @property string|null $sleeve_name
 * @property int|null $sleeve_id
 * @property string|null $cutout_collar_name
 * @property int|null $cutout_collar_id
 * @property string|null $additional_details_name
 * @property int|null $additional_details_id
 * @property string|null $sizes
 * @property int|null $sizes_id
 */
class OffersKarree extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_karree';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['id', 'source'], 'required'],
//            [['id', 'param_id', 'price', 'category_id', 'product_type_id', 'silhouette_id', 'cloth_id', 'color_id', 'sort_color_id', 'print_id', 'style_id', 'season_id', 'product_length_id', 'sleeve_id', 'cutout_collar_id', 'additional_details_id', 'sizes_id'], 'integer'],
//            [['description'], 'string'],
//            [['available', 'source'], 'string', 'max' => 32],
//            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'delivery', 'model', 'vendor', 'vendor_code', 'country_of_origin', 'barcode', 'sales_notes', 'product_type_name', 'silhouette_name', 'cloth_name', 'color_name', 'print_name', 'style_name', 'season_name', 'for_full', 'product_length_name', 'sleeve_name', 'cutout_collar_name', 'additional_details_name', 'sizes'], 'string', 'max' => 255],
//            [['category_name', 'sort_color_name'], 'string', 'max' => 128],
//            [['id'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sort_id' => 'Sort ID',
            'id' => 'ID',
            'param_id' => 'Param ID',
            'available' => 'Available',
            'url' => 'Url',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'delivery' => 'Delivery',
            'model' => 'Model',
            'vendor' => 'Vendor',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'product_type_name' => 'Product Type Name',
            'product_type_id' => 'Product Type ID',
            'silhouette_name' => 'Silhouette Name',
            'silhouette_id' => 'Silhouette ID',
            'cloth_name' => 'Cloth Name',
            'cloth_id' => 'Cloth ID',
            'color_name' => 'Color Name',
            'color_id' => 'Color ID',
            'sort_color_name' => 'Sort Color Name',
            'sort_color_id' => 'Sort Color ID',
            'print_name' => 'Print Name',
            'print_id' => 'Print ID',
            'style_name' => 'Style Name',
            'style_id' => 'Style ID',
            'season_name' => 'Season Name',
            'season_id' => 'Season ID',
            'for_full' => 'For Full',
            'product_length_name' => 'Product Length Name',
            'product_length_id' => 'Product Length ID',
            'sleeve_name' => 'Sleeve Name',
            'sleeve_id' => 'Sleeve ID',
            'cutout_collar_name' => 'Cutout Collar Name',
            'cutout_collar_id' => 'Cutout Collar ID',
            'additional_details_name' => 'Additional Details Name',
            'additional_details_id' => 'Additional Details ID',
            'sizes' => 'Sizes',
            'sizes_id' => 'Sizes ID',
        ];
    }
}
