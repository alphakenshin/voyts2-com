<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_contacts".
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $firstname
 * @property string $lastname
 * @property int $country_id
 * @property int $region_id
 * @property int $city_id
 * @property string $phone
 * @property string $email
 * @property string $additional_info
 */
class OrderContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'firstname', 'lastname', 'country_id', 'region_id', 'city_id', 'phone', 'email', 'additional_info'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['country_id', 'region_id', 'city_id'], 'integer'],
            [['additional_info'], 'string'],
            [['firstname', 'lastname', 'phone', 'email'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
            'phone' => 'Phone',
            'email' => 'Email',
            'additional_info' => 'Additional Info',
        ];
    }
}
