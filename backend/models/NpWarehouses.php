<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "np_warehouses".
 *
 * @property int $id
 * @property int $site_key
 * @property string $description
 * @property string $description_ru
 * @property string $short_address
 * @property string $short_address_ru
 * @property string $phone
 * @property string $type_of_warehouse
 * @property string $ref
 * @property string $number
 * @property string $city_ref
 * @property string $city_description
 * @property string $city_description_ru
 * @property string $settlement_ref
 * @property string $settlement_description
 * @property string $settlement_area_description
 * @property string $settlement_regions_description
 * @property string $settlement_type_description
 * @property string $longitude
 * @property string $latitude
 * @property string $post_finance
 * @property string $bicycle_parking
 * @property string $payment_access
 * @property string $pos_terminal
 * @property string $international_shipping
 * @property string $total_max_weight_allowed
 * @property string $place_max_weight_allowed
 * @property string $reception
 * @property string $delivery
 * @property string $schedule
 * @property string $district_code
 * @property string $warehouse_status
 * @property string $category_of_warehouse
 */
class NpWarehouses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_warehouses';
    }

    public function getNpWarehousesCities() {
        return $this->hasOne(NpCities::className(),['ref' => 'city_ref']);
    }
//    public function rules()
//    {
//        return [
//            [['id', 'site_key', 'description', 'description_ru', 'short_address', 'short_address_ru', 'phone', 'type_of_warehouse', 'ref', 'number', 'city_ref', 'city_description', 'city_description_ru', 'settlement_ref', 'settlement_description', 'settlement_area_description', 'settlement_regions_description', 'settlement_type_description', 'longitude', 'latitude', 'post_finance', 'bicycle_parking', 'payment_access', 'pos_terminal', 'international_shipping', 'total_max_weight_allowed', 'place_max_weight_allowed', 'reception', 'delivery', 'schedule', 'district_code', 'warehouse_status', 'category_of_warehouse'], 'required'],
//            [['id', 'site_key'], 'integer'],
//            [['reception', 'delivery', 'schedule'], 'string'],
//            [['description', 'description_ru', 'short_address', 'short_address_ru', 'phone', 'type_of_warehouse', 'ref', 'number', 'city_ref', 'city_description', 'city_description_ru', 'settlement_ref', 'settlement_description', 'settlement_area_description', 'settlement_regions_description', 'settlement_type_description', 'longitude', 'latitude', 'post_finance', 'bicycle_parking', 'payment_access', 'pos_terminal', 'international_shipping', 'total_max_weight_allowed', 'place_max_weight_allowed', 'district_code', 'warehouse_status', 'category_of_warehouse'], 'string', 'max' => 255],
//            [['id'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_key' => 'Site Key',
            'description' => 'Description',
            'description_ru' => 'Description Ru',
            'short_address' => 'Short Address',
            'short_address_ru' => 'Short Address Ru',
            'phone' => 'Phone',
            'type_of_warehouse' => 'Type Of Warehouse',
            'ref' => 'Ref',
            'number' => 'Number',
            'city_ref' => 'City Ref',
            'city_description' => 'City Description',
            'city_description_ru' => 'City Description Ru',
            'settlement_ref' => 'Settlement Ref',
            'settlement_description' => 'Settlement Description',
            'settlement_area_description' => 'Settlement Area Description',
            'settlement_regions_description' => 'Settlement Regions Description',
            'settlement_type_description' => 'Settlement Type Description',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'post_finance' => 'Post Finance',
            'bicycle_parking' => 'Bicycle Parking',
            'payment_access' => 'Payment Access',
            'pos_terminal' => 'Pos Terminal',
            'international_shipping' => 'International Shipping',
            'total_max_weight_allowed' => 'Total Max Weight Allowed',
            'place_max_weight_allowed' => 'Place Max Weight Allowed',
            'reception' => 'Reception',
            'delivery' => 'Delivery',
            'schedule' => 'Schedule',
            'district_code' => 'District Code',
            'warehouse_status' => 'Warehouse Status',
            'category_of_warehouse' => 'Category Of Warehouse',
        ];
    }
}
