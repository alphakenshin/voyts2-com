<?php



public function getOrderItems() {
    return $this->hasMany(OrderItems::className(),['order_id' => 'id']);
}
/**
 * {@inheritdoc}
 */
public function rules()
{
    return [
        [['created_at', 'updated_at', 'qty', 'sum', 'name', 'email', 'phone', 'address'], 'required'],
        [['created_at', 'updated_at'], 'safe'],
        [['qty'], 'integer'],
        [['sum'], 'number'],
        [['status'], 'string'],
        [['name', 'email', 'phone', 'address'], 'string', 'max' => 255],
    ];
}