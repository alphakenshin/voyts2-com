<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_all_group".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $available
 * @property int $catalog_id
 * @property int $group_id
 * @property string|null $url
 * @property int|null $price
 * @property float|null $price_uah
 * @property float|null $price_rub
 * @property float|null $price_usd
 * @property float|null $opt_price_uah
 * @property float|null $opt_price_rub
 * @property float|null $opt_price_usd
 * @property string|null $currency_id
 * @property string|null $sale
 * @property int|null $category_id
 * @property string|null $composition
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property int|null $store
 * @property string|null $brand
 * @property string|null $delivery
 * @property string|null $pref
 * @property string|null $model
 * @property string|null $vendor
 * @property string|null $vendor_code
 * @property string|null $description
 * @property int|null $manufacturer_warranty
 * @property string|null $country_of_origin
 * @property string|null $size
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $product_type
 * @property string|null $sizes_eu_1
 * @property string|null $sizes_eu_2
 * @property string|null $sizes_eu_3
 * @property string|null $sizes_eu_4
 * @property string|null $sizes_eu_5
 * @property string|null $sizes_eu_6
 * @property string|null $sizes_eu_7
 * @property string|null $sizes_eu_8
 * @property string|null $sizes_eu_9
 * @property string|null $sizes_eu_10
 * @property string|null $sizes_eu_11
 * @property string|null $silhouette
 * @property string|null $cloth
 * @property string|null $color
 * @property string|null $print
 * @property string|null $style
 * @property string|null $season
 * @property string|null $for_full
 * @property string|null $product_length
 * @property string|null $sleeve
 * @property string|null $cutout_collar
 * @property string|null $additional_details
 * @property int|null $all_quantity
 * @property int|null $size_s_quantity
 * @property int|null $size_m_quantity
 * @property int|null $size_l_quantity
 * @property int|null $size_xl_quantity
 * @property int|null $size_xxl_quantity
 * @property int|null $size_xxxl_quantity
 * @property string|null $size_1
 * @property string|null $size_2
 * @property string|null $size_3
 * @property string|null $size_4
 * @property string|null $size_5
 * @property string|null $param_8
 * @property string|null $name_9
 * @property string|null $param_9
 * @property string|null $name_10
 */
class OffersAllGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_all_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'catalog_id', 'source'], 'required'],
            [['id', 'catalog_id', 'price', 'category_id', 'store', 'manufacturer_warranty', 'all_quantity', 'size_s_quantity', 'size_m_quantity', 'size_l_quantity', 'size_xl_quantity', 'size_xxl_quantity', 'size_xxxl_quantity'], 'integer'],
            [['price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd'], 'number'],
            [['description'], 'string'],
            [['type', 'composition', 'pref'], 'string', 'max' => 64],
            [['available', 'source', 'group_id', 'brand', 'sizes_eu_1', 'sizes_eu_2', 'sizes_eu_3', 'sizes_eu_4', 'sizes_eu_5', 'sizes_eu_6', 'sizes_eu_7', 'sizes_eu_8', 'sizes_eu_9', 'sizes_eu_10', 'sizes_eu_11'], 'string', 'max' => 32],
            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'vendor', 'vendor_code', 'country_of_origin', 'size', 'barcode', 'sales_notes', 'product_type', 'silhouette', 'cloth', 'color', 'print', 'style', 'season', 'for_full', 'product_length', 'sleeve', 'additional_details', 'size_1', 'size_2', 'size_3', 'size_4', 'size_5', 'param_8', 'name_9', 'param_9', 'name_10'], 'string', 'max' => 255],
            [['sale', 'delivery'], 'string', 'max' => 16],
            [['cutout_collar'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'available' => 'Available',
            'catalog_id' => 'Catalog ID',
            'group_id' => 'ID Группы',
            'url' => 'Url',
            'price' => 'Price',
            'price_uah' => 'Price Uah',
            'price_rub' => 'Price Rub',
            'price_usd' => 'Price Usd',
            'opt_price_uah' => 'Opt Price Uah',
            'opt_price_rub' => 'Opt Price Rub',
            'opt_price_usd' => 'Opt Price Usd',
            'currency_id' => 'Currency ID',
            'sale' => 'Sale',
            'category_id' => 'Category ID',
            'composition' => 'Composition',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'store' => 'Store',
            'brand' => 'Brand',
            'delivery' => 'Delivery',
            'pref' => 'Pref',
            'model' => 'Model',
            'vendor' => 'Vendor',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'manufacturer_warranty' => 'Manufacturer Warranty',
            'country_of_origin' => 'Country Of Origin',
            'size' => 'Size',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'product_type' => 'Product Type',
            'sizes_eu_1' => 'Sizes Eu 1',
            'sizes_eu_2' => 'Sizes Eu 2',
            'sizes_eu_3' => 'Sizes Eu 3',
            'sizes_eu_4' => 'Sizes Eu 4',
            'sizes_eu_5' => 'Sizes Eu 5',
            'sizes_eu_6' => 'Sizes Eu 6',
            'sizes_eu_7' => 'Sizes Eu 7',
            'sizes_eu_8' => 'Sizes Eu 8',
            'sizes_eu_9' => 'Sizes Eu 9',
            'sizes_eu_10' => 'Sizes Eu 10',
            'sizes_eu_11' => 'Sizes Eu 11',
            'silhouette' => 'Silhouette',
            'cloth' => 'Cloth',
            'color' => 'Color',
            'print' => 'Print',
            'style' => 'Style',
            'season' => 'Season',
            'for_full' => 'For Full',
            'product_length' => 'Product Length',
            'sleeve' => 'Sleeve',
            'cutout_collar' => 'Cutout Collar',
            'additional_details' => 'Additional Details',
            'all_quantity' => 'All Quantity',
            'size_s_quantity' => 'Size S Quantity',
            'size_m_quantity' => 'Size M Quantity',
            'size_l_quantity' => 'Size L Quantity',
            'size_xl_quantity' => 'Size Xl Quantity',
            'size_xxl_quantity' => 'Size Xxl Quantity',
            'size_xxxl_quantity' => 'Size Xxxl Quantity',
            'size_1' => 'Size 1',
            'size_2' => 'Size 2',
            'size_3' => 'Size 3',
            'size_4' => 'Size 4',
            'size_5' => 'Size 5',
            'param_8' => 'Param 8',
            'name_9' => 'Name 9',
            'param_9' => 'Param 9',
            'name_10' => 'Name 10',
        ];
    }
}
