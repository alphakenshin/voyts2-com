<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_womens_clothes_img".
 *
 * @property int $id
 * @property string|null $vendor_code
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 */
class ProductWomensClothesImg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_womens_clothes_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vendor_code'], 'string', 'max' => 64],
            [['main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vendor_code' => 'Vendor Code',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
        ];
    }
}
