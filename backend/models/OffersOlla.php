<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_olla".
 *
 * @property int $sort_id
 * @property int $id
 * @property int|null $param_id
 * @property string|null $available
 * @property int|null $group_id
 * @property string|null $url
 * @property int|null $price
 * @property int|null $old_price
 * @property string|null $currency_id
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $delivery
 * @property string|null $model
 * @property string|null $brand_name
 * @property int|null $brand_id
 * @property string|null $vendor_code
 * @property string|null $description
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $product_type
 * @property string|null $sizes
 * @property int|null $sizes_id
 */
class OffersOlla extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_olla';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['id', 'source'], 'required'],
//            [['id', 'param_id', 'group_id', 'price', 'old_price', 'category_id', 'brand_id', 'sizes_id'], 'integer'],
//            [['description'], 'string'],
//            [['available', 'source'], 'string', 'max' => 32],
//            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'delivery', 'model', 'brand_name', 'vendor_code', 'country_of_origin', 'barcode', 'sales_notes', 'product_type'], 'string', 'max' => 255],
//            [['category_name'], 'string', 'max' => 128],
//            [['sizes'], 'string', 'max' => 64],
//            [['id'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sort_id' => 'Sort ID',
            'id' => 'ID',
            'param_id' => 'Param ID',
            'available' => 'Available',
            'group_id' => 'Group ID',
            'url' => 'Url',
            'price' => 'Price',
            'old_price' => 'Old Price',
            'currency_id' => 'Currency ID',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'delivery' => 'Delivery',
            'model' => 'Model',
            'brand_name' => 'Brand Name',
            'brand_id' => 'Brand ID',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'product_type' => 'Product Type',
            'sizes' => 'Sizes',
            'sizes_id' => 'Sizes ID',
        ];
    }
}
