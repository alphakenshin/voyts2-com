<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ukr_settlements".
 *
 * @property int $id
 * @property string|null $city
 * @property string|null $area
 * @property int $area_id
 * @property string|null $region
 * @property int $region_id
 * @property string|null $longitude
 * @property string|null $latitude
 */
class UkrSettlements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ukr_settlements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['area_id', 'region_id'], 'required'],
            [['area_id', 'region_id'], 'integer'],
            [['city', 'region'], 'string', 'max' => 128],
            [['area'], 'string', 'max' => 64],
            [['longitude', 'latitude'], 'string', 'max' => 255],
        ];
    }

    public $regions;
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'area' => 'Area',
            'area_id' => 'Area ID',
            'region' => 'Region',
            'region_id' => 'Region ID',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
        ];
    }
}
