<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "import_product".
 *
 * @property int $id
 * @property int $product_id
 * @property string $sku
 * @property int $category_id
 * @property string $name
 * @property string $link_to
 * @property string $collection
 * @property string $color
 * @property string $style
 * @property string $sizes
 * @property string $price_UAH
 * @property string $price_RUB
 * @property string $price_USD
 * @property string $opt_price_UAH
 * @property string $opt_price_RUB
 * @property string $opt_price_USD
 * @property string $images
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $updated_at
 */
class ImportProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'import_product';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['product_id', 'sku', 'category_id', 'name', 'link_to', 'collection', 'color', 'style', 'sizes', 'price_UAH', 'price_RUB', 'price_USD', 'opt_price_UAH', 'opt_price_RUB', 'opt_price_USD', 'images', 'created_by', 'updated_by'], 'required'],
//            [['product_id', 'category_id'], 'integer'],
//            [['price_UAH', 'price_RUB', 'price_USD', 'opt_price_UAH', 'opt_price_RUB', 'opt_price_USD'], 'number'],
//            [['images', 'created_by', 'updated_by'], 'string'],
//            [['created_at', 'updated_at'], 'safe'],
//            [['sku', 'name'], 'string', 'max' => 128],
//            [['link_to', 'color', 'sizes'], 'string', 'max' => 255],
//            [['collection', 'style'], 'string', 'max' => 64],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'sku' => 'Sku',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'link_to' => 'Link To',
            'collection' => 'Collection',
            'color' => 'Color',
            'style' => 'Style',
            'sizes' => 'Sizes',
            'price_UAH' => 'Price Uah',
            'price_RUB' => 'Price Rub',
            'price_USD' => 'Price Usd',
            'opt_price_UAH' => 'Opt Price Uah',
            'opt_price_RUB' => 'Opt Price Rub',
            'opt_price_USD' => 'Opt Price Usd',
            'images' => 'Images',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
