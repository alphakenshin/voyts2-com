<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "offers_glem".
 *
 * @property int $sort_id
 * @property string $sku
 * @property int|null $param_id
 * @property string|null $types
 * @property string|null $available
 * @property string|null $url
 * @property int|null $price
 * @property string|null $currency_id
 * @property string|null $category_name
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property int|null $store
 * @property int $delivery
 * @property string|null $product_type_name
 * @property int|null $product_type_id
 * @property string|null $model
 * @property string|null $vendor
 * @property string|null $vendor_code
 * @property string|null $description
 * @property int|null $manufacturer_warranty
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $silhouette
 * @property string|null $cloth
 * @property string|null $color_name
 * @property int|null $color_id
 * @property string|null $sort_color_name
 * @property int|null $sort_color_id
 * @property string|null $print
 * @property string|null $style
 * @property string|null $season
 * @property string|null $product_length
 * @property string|null $sleeve
 * @property int|null $all_quantity
 * @property string|null $for_full
 * @property string|null $sizes_1
 * @property int|null $sizes_1_id
 * @property string|null $sizes_2
 * @property int|null $sizes_2_id
 * @property string|null $sizes_3
 * @property int|null $sizes_3_id
 * @property string|null $sizes_4
 * @property int|null $sizes_4_id
 * @property string|null $sizes_5
 * @property int|null $sizes_5_id
 * @property string|null $sizes_6
 * @property int|null $sizes_6_id
 * @property string|null $sizes_7
 * @property int|null $sizes_7_id
 * @property string|null $sizes_8
 * @property int|null $sizes_8_id
 * @property string|null $sizes_9
 * @property int|null $sizes_9_id
 * @property string|null $sizes_10
 * @property int|null $sizes_10_id
 * @property string|null $sizes_11
 * @property int|null $sizes_11_id
 * @property string|null $sizes_12
 * @property int|null $sizes_12_id
 * @property string|null $sizes_13
 * @property int|null $sizes_13_id
 * @property string|null $sizes_14
 * @property int|null $sizes_14_id
 */
class OffersGlem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_glem';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['sku', 'source'], 'required'],
//            [['param_id', 'price', 'category_id', 'store', 'delivery', 'product_type_id', 'manufacturer_warranty', 'color_id', 'sort_color_id', 'all_quantity', 'sizes_1_id', 'sizes_2_id', 'sizes_3_id', 'sizes_4_id', 'sizes_5_id', 'sizes_6_id', 'sizes_7_id', 'sizes_8_id', 'sizes_9_id', 'sizes_10_id', 'sizes_11_id', 'sizes_12_id', 'sizes_13_id', 'sizes_14_id'], 'integer'],
//            [['description'], 'string'],
//            [['sku', 'category_name', 'sort_color_name', 'sizes_2', 'sizes_3', 'sizes_4', 'sizes_5', 'sizes_6', 'sizes_7', 'sizes_8', 'sizes_9', 'sizes_10', 'sizes_11', 'sizes_12', 'sizes_13', 'sizes_14'], 'string', 'max' => 128],
//            [['types', 'product_type_name'], 'string', 'max' => 64],
//            [['available', 'source'], 'string', 'max' => 32],
//            [['url', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'vendor', 'vendor_code', 'country_of_origin', 'barcode', 'sales_notes', 'silhouette', 'cloth', 'color_name', 'print', 'style', 'season', 'product_length', 'sleeve', 'for_full', 'sizes_1'], 'string', 'max' => 255],
//            [['sku'], 'unique'],
//            [['vendor_code'], 'unique'],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sort_id' => 'Sort ID',
            'sku' => 'Sku',
            'param_id' => 'Param ID',
            'types' => 'Types',
            'available' => 'Available',
            'url' => 'Url',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'category_name' => 'Category Name',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'store' => 'Store',
            'delivery' => 'Delivery',
            'product_type_name' => 'Product Type Name',
            'product_type_id' => 'Product Type ID',
            'model' => 'Model',
            'vendor' => 'Vendor',
            'vendor_code' => 'Vendor Code',
            'description' => 'Description',
            'manufacturer_warranty' => 'Manufacturer Warranty',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'silhouette' => 'Silhouette',
            'cloth' => 'Cloth',
            'color_name' => 'Color Name',
            'color_id' => 'Color ID',
            'sort_color_name' => 'Sort Color Name',
            'sort_color_id' => 'Sort Color ID',
            'print' => 'Print',
            'style' => 'Style',
            'season' => 'Season',
            'product_length' => 'Product Length',
            'sleeve' => 'Sleeve',
            'all_quantity' => 'All Quantity',
            'for_full' => 'For Full',
            'sizes_1' => 'Sizes 1',
            'sizes_1_id' => 'Sizes 1 ID',
            'sizes_2' => 'Sizes 2',
            'sizes_2_id' => 'Sizes 2 ID',
            'sizes_3' => 'Sizes 3',
            'sizes_3_id' => 'Sizes 3 ID',
            'sizes_4' => 'Sizes 4',
            'sizes_4_id' => 'Sizes 4 ID',
            'sizes_5' => 'Sizes 5',
            'sizes_5_id' => 'Sizes 5 ID',
            'sizes_6' => 'Sizes 6',
            'sizes_6_id' => 'Sizes 6 ID',
            'sizes_7' => 'Sizes 7',
            'sizes_7_id' => 'Sizes 7 ID',
            'sizes_8' => 'Sizes 8',
            'sizes_8_id' => 'Sizes 8 ID',
            'sizes_9' => 'Sizes 9',
            'sizes_9_id' => 'Sizes 9 ID',
            'sizes_10' => 'Sizes 10',
            'sizes_10_id' => 'Sizes 10 ID',
            'sizes_11' => 'Sizes 11',
            'sizes_11_id' => 'Sizes 11 ID',
            'sizes_12' => 'Sizes 12',
            'sizes_12_id' => 'Sizes 12 ID',
            'sizes_13' => 'Sizes 13',
            'sizes_13_id' => 'Sizes 13 ID',
            'sizes_14' => 'Sizes 14',
            'sizes_14_id' => 'Sizes 14 ID',
        ];
    }
}
