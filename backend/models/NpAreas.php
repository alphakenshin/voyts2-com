<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "np_areas".
 *
 * @property int $id
 * @property string $ref
 * @property string $areas_center
 * @property string $description
 */
class NpAreas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_areas';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['ref', 'areas_center', 'description'], 'required'],
//            [['ref', 'areas_center', 'description'], 'string', 'max' => 255],
//            [['ref'], 'unique'],
//        ];
//    }

    public function getNpCities() {
        return $this->hasMany(NpCities::className(),['area' => 'ref']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ref' => 'Ref',
            'areas_center' => 'Areas Center',
            'description' => 'Description',
        ];
    }
}
