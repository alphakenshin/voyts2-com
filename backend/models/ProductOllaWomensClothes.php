<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_olla_womens_clothes".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $available
 * @property int|null $price
 * @property string|null $currency_id
 * @property string|null $sale
 * @property int|null $category_id
 * @property string|null $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property int|null $store
 * @property string|null $brand
 * @property string|null $delivery
 * @property string|null $model
 * @property string|null $vendor
 * @property string|null $sku
 * @property string|null $description
 * @property int|null $manufacturer_warranty
 * @property string|null $country_of_origin
 * @property string|null $barcode
 * @property string|null $sales_notes
 * @property string|null $product_type
 * @property string|null $sizes_eu_1
 * @property string|null $sizes_eu_2
 * @property string|null $sizes_eu_3
 * @property string|null $sizes_eu_4
 * @property string|null $sizes_eu_5
 * @property string|null $sizes_eu_6
 * @property string|null $sizes_eu_7
 * @property string|null $sizes_eu_8
 * @property string|null $sizes_eu_9
 * @property string|null $sizes_eu_10
 * @property string|null $color
 * @property string|null $style
 * @property string|null $season
 */
class ProductOllaWomensClothes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_olla_womens_clothes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'category_id', 'store', 'manufacturer_warranty'], 'integer'],
            [['description'], 'string'],
            [['type'], 'string', 'max' => 64],
            [['available', 'source', 'brand', 'sizes_eu_1', 'sizes_eu_2', 'sizes_eu_3', 'sizes_eu_4', 'sizes_eu_5', 'sizes_eu_6', 'sizes_eu_7', 'sizes_eu_8', 'sizes_eu_9', 'sizes_eu_10'], 'string', 'max' => 32],
            [['currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'model', 'vendor', 'sku', 'country_of_origin', 'barcode', 'sales_notes', 'product_type', 'color', 'style', 'season'], 'string', 'max' => 255],
            [['sale', 'delivery'], 'string', 'max' => 16],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'available' => 'Available',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'sale' => 'Sale',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'store' => 'Store',
            'brand' => 'Brand',
            'delivery' => 'Delivery',
            'model' => 'Model',
            'vendor' => 'Vendor',
            'sku' => 'Sku',
            'description' => 'Description',
            'manufacturer_warranty' => 'Manufacturer Warranty',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'product_type' => 'Product Type',
            'sizes_eu_1' => 'Sizes Eu 1',
            'sizes_eu_2' => 'Sizes Eu 2',
            'sizes_eu_3' => 'Sizes Eu 3',
            'sizes_eu_4' => 'Sizes Eu 4',
            'sizes_eu_5' => 'Sizes Eu 5',
            'sizes_eu_6' => 'Sizes Eu 6',
            'sizes_eu_7' => 'Sizes Eu 7',
            'sizes_eu_8' => 'Sizes Eu 8',
            'sizes_eu_9' => 'Sizes Eu 9',
            'sizes_eu_10' => 'Sizes Eu 10',
            'color' => 'Color',
            'style' => 'Style',
            'season' => 'Season',
        ];
    }
}
