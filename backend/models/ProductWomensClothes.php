<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_womens_clothes".
 *
 * @property int $id
 * @property string|null $type
 * @property int|null $available
 * @property int|null $price
 * @property string|null $currency_id
 * @property int|null $category_id
 * @property string $source
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $brand
 * @property string|null $model
 * @property string|null $sku
 * @property int|null $delivery
 * @property string|null $description
 * @property string|null $country_of_origin
 * @property int|null $barcode
 * @property string|null $sales_notes
 * @property string|null $composition
 * @property string|null $sizes_eu_1
 * @property string|null $sizes_eu_2
 * @property string|null $sizes_eu_3
 * @property string|null $sizes_eu_4
 * @property string|null $sizes_eu_5
 * @property string|null $sizes_eu_6
 * @property string|null $sizes_eu_7
 * @property string|null $sizes_eu_8
 * @property string|null $sizes_eu_9
 * @property string|null $sizes_eu_10
 * @property int|null $S
 * @property int|null $M
 * @property int|null $L
 * @property int|null $XL
 * @property int|null $XXL
 * @property int|null $XXXL
 * @property string|null $cloth
 * @property string|null $color
 * @property string|null $style
 * @property string|null $season
 * @property string|null $product_length
 * @property string|null $for_full
 */
class ProductWomensClothes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_womens_clothes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['available', 'price', 'category_id', 'delivery', 'barcode', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'], 'integer'],
            [['source'], 'required'],
            [['description'], 'string'],
            [['type', 'currency_id', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'brand', 'model', 'sku', 'country_of_origin', 'sales_notes', 'composition', 'cloth', 'color', 'style', 'season', 'product_length'], 'string', 'max' => 255],
            [['source', 'sizes_eu_1', 'sizes_eu_2', 'sizes_eu_3', 'sizes_eu_4', 'sizes_eu_5', 'sizes_eu_6', 'sizes_eu_7', 'sizes_eu_8', 'sizes_eu_9', 'sizes_eu_10', 'for_full'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'available' => 'Available',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'brand' => 'Brand',
            'model' => 'Model',
            'sku' => 'Sku',
            'delivery' => 'Delivery',
            'description' => 'Description',
            'country_of_origin' => 'Country Of Origin',
            'barcode' => 'Barcode',
            'sales_notes' => 'Sales Notes',
            'composition' => 'Composition',
            'sizes_eu_1' => 'Sizes Eu 1',
            'sizes_eu_2' => 'Sizes Eu 2',
            'sizes_eu_3' => 'Sizes Eu 3',
            'sizes_eu_4' => 'Sizes Eu 4',
            'sizes_eu_5' => 'Sizes Eu 5',
            'sizes_eu_6' => 'Sizes Eu 6',
            'sizes_eu_7' => 'Sizes Eu 7',
            'sizes_eu_8' => 'Sizes Eu 8',
            'sizes_eu_9' => 'Sizes Eu 9',
            'sizes_eu_10' => 'Sizes Eu 10',
            'S' => 'S',
            'M' => 'M',
            'L' => 'L',
            'XL' => 'Xl',
            'XXL' => 'Xxl',
            'XXXL' => 'Xxxl',
            'cloth' => 'Cloth',
            'color' => 'Color',
            'style' => 'Style',
            'season' => 'Season',
            'product_length' => 'Product Length',
            'for_full' => 'For Full',
        ];
    }
}
