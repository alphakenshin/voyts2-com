<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "np_warehouses".
 *

 * @property int|null $site_key
 * @property string|null $description

 * @property int|null $city_id
 * @property string|null $city_description

 * @property string|null $settlement_area_description
 * @property int|null $area_id
 *

 */
class NpWarehouses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_warehouses';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['site_key', 'city_id', 'area_id', 'delivery_id'], 'integer'],
            [['description', 'city_description', 'city_description_ru', 'settlement_area_description'], 'string', 'max' => 255],

        ];
    }

    public function getOrder() {
        return $this->hasOne(Order::className(),
            ['area_id' => 'area_id'],
            ['city_id' => 'city_id'],
            ['site_key' => 'site_key']
        );
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'site_key' => Yii::t('app', 'Відділення'),
            'description' => Yii::t('app', 'Description'),
            'description_ru' => Yii::t('app', 'Description Ru'),
            'short_address' => Yii::t('app', 'Short Address'),
            'short_address_ru' => Yii::t('app', 'Short Address Ru'),
            'phone' => Yii::t('app', 'Phone'),
            'type_of_warehouse' => Yii::t('app', 'Type Of Warehouse'),
            'ref' => Yii::t('app', 'Ref'),
            'number' => Yii::t('app', 'Number'),
            'city_ref' => Yii::t('app', 'City Ref'),
            'city_id' => Yii::t('app', 'City ID'),
            'city_description' => Yii::t('app', 'City Description'),
            'city_description_ru' => Yii::t('app', 'City Description Ru'),
            'settlement_ref' => Yii::t('app', 'Settlement Ref'),
            'settlement_description' => Yii::t('app', 'Settlement Description'),
            'settlement_area_ref' => Yii::t('app', 'Settlement Area Ref'),
            'settlement_area_description' => Yii::t('app', 'Settlement Area Description'),
            'area_id' => Yii::t('app', 'Area ID'),
            'settlement_regions_description' => Yii::t('app', 'Settlement Regions Description'),
            'regions_id' => Yii::t('app', 'Regions ID'),
            'settlement_type_description' => Yii::t('app', 'Settlement Type Description'),
            'longitude' => Yii::t('app', 'Longitude'),
            'latitude' => Yii::t('app', 'Latitude'),
            'post_finance' => Yii::t('app', 'Post Finance'),
            'bicycle_parking' => Yii::t('app', 'Bicycle Parking'),
            'payment_access' => Yii::t('app', 'Payment Access'),
            'pos_terminal' => Yii::t('app', 'Pos Terminal'),
            'international_shipping' => Yii::t('app', 'International Shipping'),
            'total_max_weight_allowed' => Yii::t('app', 'Total Max Weight Allowed'),
            'place_max_weight_allowed' => Yii::t('app', 'Place Max Weight Allowed'),
            'reception' => Yii::t('app', 'Reception'),
            'delivery' => Yii::t('app', 'Delivery'),
            'schedule' => Yii::t('app', 'Schedule'),
            'district_code' => Yii::t('app', 'District Code'),
            'warehouse_status' => Yii::t('app', 'Warehouse Status'),
            'category_of_warehouse' => Yii::t('app', 'Category Of Warehouse'),
        ];
    }
}
