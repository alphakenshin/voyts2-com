<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "offers_issaplus".
 *
 * @property int $id
 * @property string $sku
 * @property int $category_id
 * @property string $source
 * @property string $model
 * @property string $url
 * @property string $collection
 * @property string $color
 * @property string $style
 * @property string|null $size_1
 * @property string|null $size_2
 * @property string|null $size_3
 * @property string|null $size_4
 * @property string|null $size_5
 * @property int|null $price
 * @property float $price_uah
 * @property float $price_rub
 * @property float $price_usd
 * @property float $opt_price_uah
 * @property float $opt_price_rub
 * @property float $opt_price_usd
 * @property string|null $main_img
 * @property string|null $gallery_1
 * @property string|null $gallery_2
 * @property string|null $gallery_3
 * @property string|null $gallery_4
 * @property string|null $gallery_5
 * @property string|null $created_at
 * @property string $created_by
 * @property string $updated_by
 * @property string|null $updated_at
 */
class OffersIssaplusSearch extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'offers_issaplus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'sku', 'category_id', 'source', 'model', 'url', 'collection', 'color', 'style', 'price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd', 'created_by', 'updated_by'], 'required'],
            [['id', 'category_id', 'price'], 'integer'],
            [['price_uah', 'price_rub', 'price_usd', 'opt_price_uah', 'opt_price_rub', 'opt_price_usd'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string'],
            [['sku'], 'string', 'max' => 128],
            [['source', 'size_1', 'size_2', 'size_3', 'size_4', 'size_5'], 'string', 'max' => 32],
            [['model', 'url', 'color', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5'], 'string', 'max' => 255],
            [['collection', 'style'], 'string', 'max' => 64],
            [['sku'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'category_id' => 'Category ID',
            'source' => 'Source',
            'model' => 'Model',
            'url' => 'Url',
            'collection' => 'Collection',
            'color' => 'Color',
            'style' => 'Style',
            'size_1' => 'Size 1',
            'size_2' => 'Size 2',
            'size_3' => 'Size 3',
            'size_4' => 'Size 4',
            'size_5' => 'Size 5',
            'price' => 'Price',
            'price_uah' => 'Price Uah',
            'price_rub' => 'Price Rub',
            'price_usd' => 'Price Usd',
            'opt_price_uah' => 'Opt Price Uah',
            'opt_price_rub' => 'Opt Price Rub',
            'opt_price_usd' => 'Opt Price Usd',
            'main_img' => 'Main Img',
            'gallery_1' => 'Gallery 1',
            'gallery_2' => 'Gallery 2',
            'gallery_3' => 'Gallery 3',
            'gallery_4' => 'Gallery 4',
            'gallery_5' => 'Gallery 5',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
