<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "delivery".
 *
 * @property int $id
 * @property int $delivery_id
 * @property int $city_id
 * @property int $department_id
 * @property string $description
 */
class Delivery extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery';
    }

//    public function getDeliveryType() {
//
//        return $this->hasMany(DeliveryType::className(), ['id' => 'delivery_id']);
//    }

//    public function getOrder() {
//        return $this->hasOne(OrderContacts::className(),
//            ['delivery_id' => 'delivery_id']
//
//        );
//    }

    public function rules()
    {
        return [
            [['delivery_id', 'city_id', 'department_id', 'description'], 'required'],
            [['delivery_id', 'city_id', 'department_id'], 'integer'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'delivery_id' => Yii::t('app', 'Delivery ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'department_id' => Yii::t('app', 'Department ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
