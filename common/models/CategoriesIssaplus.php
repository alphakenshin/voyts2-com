<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "categories_issaplus".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $name
 * @property string $source
 * @property int|null $sort
 * @property int|null $status
 * @property string|null $content
 * @property string|null $keywords
 * @property string|null $description
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 */
class CategoriesIssaplus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories_issaplus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'sort', 'status', 'created_by', 'updated_by'], 'integer'],
            [['source'], 'required'],
            [['content', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 64],
            [['source'], 'string', 'max' => 32],
            [['keywords'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'source' => 'Source',
            'sort' => 'Sort',
            'status' => 'Status',
            'content' => 'Content',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
