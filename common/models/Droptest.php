<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "droptest".
 *
 * @property int $id
 * @property string $name
 * @property int $cat
 * @property int $subcat
 */
class Droptest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'droptest';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'cat', 'subcat'], 'required'],
            [['cat', 'subcat'], 'integer'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function getCats()
    {
        return $this->hasOne(Dropdown::className(), ['id' => 'cat']);
    }

    public function getSubcats()
    {
        return $this->hasOne(Dropdown::className(), ['id' => 'subcat']);
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'cat' => 'Cat',
            'subcat' => 'Subcat',
        ];
    }
}
