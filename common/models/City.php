<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "city".
 *
 * @property int $id
 * @property int $city_id
 * @property string $city_name
 * @property int $region_id
 * @property string $region_name
 */
class City extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'city_name', 'region_id', 'region_name'], 'required'],
            [['city_id', 'region_id'], 'integer'],
            [['city_name', 'region_name'], 'string', 'max' => 64],
        ];
    }
//    public function getOrderContacts() {
//
//        return $this->hasOne(OrderContacts::className(),
//            ['city_id' => 'city_id'],
//            ['region_id' => 'region_id'],
//            ['country_id' => 'country_id']
//        );
//    }

//    public function getOrderCities() {
//        return $this->hasOne(OrderContacts::className(), ['id' => 'order_id']);
//    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'city_name' => Yii::t('app', 'City Name'),
            'region_id' => Yii::t('app', 'Region ID'),
            'region_name' => Yii::t('app', 'Region Name'),
        ];
    }
}
