<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "delivery_type".
 *
 * @property int $id
 * @property string $delivery_type
 * @property string $description
 */
class DeliveryType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_type';
    }

    public function getOrder() {
        return $this->hasOne(Order::className(),
            ['delivery_id' => 'delivery_id']

        );
    }

    public function rules()
    {
        return [
            [['delivery_type', 'description'], 'required'],
            [['delivery_type'], 'string', 'max' => 128],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'delivery_type' => Yii::t('app', 'Delivery Type'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
