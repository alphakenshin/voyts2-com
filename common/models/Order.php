<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $created_at
 * @property string $updated_at
 * @property int $qty
 * @property string|null $size
 * @property float $sum
 * @property string $status
 * @property string $firstname
 * @property string $lastname
 * @property int $delivery_id
 * @property int $area_id
 * @property int $region_id
 * @property int $city_id
 * @property int $site_key
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $additional_info
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public function getOrderItems()
    {
        return $this->hasMany(OrderItems::className(),
            ['order_id' => 'id']
//            ['size' => 'size']
//            ,
//            ['region_id' => 'region_id'],
//            ['city_id' => 'city_id']
        );
    }

    public function getAreas() {
        return $this->hasOne(NpAreas::className(),
            ['area_id' => 'area_ids']

        );
    }

    public function getDeliveryType() {
        return $this->hasOne(DeliveryType::className(),
            ['delivery_id' => 'delivery_id']

        );
    }

    public function getNpWarehouses() {
        return $this->hasOne(NpWarehouses::className(),
            ['area_id' => 'area_id'],
            ['city_id' => 'city_id'],
            ['site_key' => 'site_key']
        );
    }

    public function UkrSettlements() {
        return $this->hasOne(UkrSettlements::className(),
            ['area_id' => 'area_id'],
            ['region_id' => 'region_id'],
            ['city_id' => 'city_id']

        );
    }

    public function rules()
    {
        return [
            [['firstname', 'lastname', 'area_id', 'city_id', 'address'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['qty', 'delivery_id', 'area_id', 'region_id', 'city_id', 'site_key'], 'integer'],
            [['sum'], 'number'],
            [['status'], 'boolean'],
            [['address', 'additional_info'], 'string'],
            [['size', 'firstname', 'lastname', 'phone', 'email'], 'string', 'max' => 64],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'qty' => 'Qty',
            'size' => 'Size',
            'sum' => 'Sum',
            'status' => 'Status',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'delivery_id' => 'Діствка',
            'area_id' => 'Область',
            'region_id' => 'Район',
            'city_id' => 'Населений пункт',
            'site_key' => 'Відділення',
            'address' => 'Адреса',
            'phone' => 'Phone',
            'email' => 'Email',
            'additional_info' => 'Additional Info',
        ];
    }
}
