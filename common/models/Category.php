<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $name
 * @property int|null $garda_id
 * @property int|null $glem_id
 * @property int|null $issaplus_id
 * @property int|null $karree_id
 * @property int|null $olla_id
 * @property int|null $sort
 * @property int|null $status
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'garda_id', 'glem_id', 'issaplus_id', 'karree_id', 'olla_id', 'sort', 'status'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['garda_id'], 'unique'],
            [['glem_id'], 'unique'],
            [['karree_id'], 'unique'],
            [['issaplus_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'garda_id' => 'Garda ID',
            'glem_id' => 'Glem ID',
            'issaplus_id' => 'Issaplus ID',
            'karree_id' => 'Karree ID',
            'olla_id' => 'Olla ID',
            'sort' => 'Sort',
            'status' => 'Status',
        ];
    }

    public function getCategory() {

        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
