<?php

namespace common\models;

use creocoder\nestedsets\NestedSetsBehavior;
use creocoder\nestedsets\NestedSetsQueryBehavior;
use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 * @property string $url
 * @property string|null $text
 */
class MenuQuery extends \yii\db\ActiveQuery
{
   public function behaviors()
   {
       return [
           NestedSetsQueryBehavior::className(),
       ];
   }
}
