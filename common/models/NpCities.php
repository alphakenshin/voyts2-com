<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "np_cities".
 *
 * @property int $id
 * @property string|null $description
 * @property string|null $description_ru
 * @property string|null $ref
 * @property int|null $delivery_1
 * @property int|null $delivery_2
 * @property int|null $delivery_3
 * @property int|null $delivery_4
 * @property int|null $delivery_5
 * @property int|null $delivery_6
 * @property int|null $delivery_7
 * @property string|null $area
 * @property string|null $settlement_type
 * @property int|null $is_branch
 * @property string|null $prevent_entry_new_streets_user
 * @property string|null $conglomerates
 * @property int|null $city_id
 * @property string|null $settlement_type_description
 * @property string|null $settlement_type_description_ru
 * @property int|null $special_cash_check
 */
class NpCities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_cities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'delivery_1', 'delivery_2', 'delivery_3', 'delivery_4', 'delivery_5', 'delivery_6', 'delivery_7', 'is_branch', 'city_id', 'special_cash_check'], 'integer'],
            [['description', 'description_ru', 'ref', 'area', 'settlement_type', 'prevent_entry_new_streets_user', 'conglomerates', 'settlement_type_description', 'settlement_type_description_ru'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'description_ru' => Yii::t('app', 'Description Ru'),
            'ref' => Yii::t('app', 'Ref'),
            'delivery_1' => Yii::t('app', 'Delivery 1'),
            'delivery_2' => Yii::t('app', 'Delivery 2'),
            'delivery_3' => Yii::t('app', 'Delivery 3'),
            'delivery_4' => Yii::t('app', 'Delivery 4'),
            'delivery_5' => Yii::t('app', 'Delivery 5'),
            'delivery_6' => Yii::t('app', 'Delivery 6'),
            'delivery_7' => Yii::t('app', 'Delivery 7'),
            'area' => Yii::t('app', 'Area'),
            'settlement_type' => Yii::t('app', 'Settlement Type'),
            'is_branch' => Yii::t('app', 'Is Branch'),
            'prevent_entry_new_streets_user' => Yii::t('app', 'Prevent Entry New Streets User'),
            'conglomerates' => Yii::t('app', 'Conglomerates'),
            'city_id' => Yii::t('app', 'City ID'),
            'settlement_type_description' => Yii::t('app', 'Settlement Type Description'),
            'settlement_type_description_ru' => Yii::t('app', 'Settlement Type Description Ru'),
            'special_cash_check' => Yii::t('app', 'Special Cash Check'),
        ];
    }
}
