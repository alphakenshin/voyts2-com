<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "np_areas".
 *
 * @property int $areas_id
 * @property string $description
 */
class NpAreas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'np_areas';
    }


    public function getAreasOrder() {
        return $this->hasOne(Order::className(),
            ['areas_id' => 'area_id']

        );
    }

    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'areas_id' => Yii::t('app', 'Areas ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
