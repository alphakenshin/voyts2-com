<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 11/24/19
 * Time: 4:14 AM
 */

namespace common\widgets;


use frontend\models\Category;
use yii\base\Widget;
use Yii;

class MenuWidget extends Widget
{
    public $tpl;
    public $model;
    public $data;
    public $tree;
    public $menuHtml;


    public function init() {
        parent::init();
        if( $this->tpl === null )
        {
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run() {

        // get cache
        if ($this->tpl == 'menu.php') {
            $menu = Yii::$app->cache->get('menu');
            if ($menu) return $menu;
        }


        $this->data = Category::find()->indexBy('id')->asArray()->all();
//         echo '<pre>';
//        print_r($this->data);
//        echo '</pre>';die;
        $this->tree = $this->getTree();
        $this->menuHtml = $this->getMenuHtml($this->tree);
        // set cache
        if ($this->tpl == 'menu.php') {
            Yii::$app->cache->set('menu', $this->menuHtml, 60);
        }
        return $this->menuHtml;
    }

    protected function getTree() {
        $tree = [];
        foreach ($this->data as $id=>&$node) {
            error_reporting(E_ALL & ~E_NOTICE);
            if (!isset($node['parent_id']))
                $tree[$id] = &$node;
            else

                $this->data[$node['parent_id']]['childs'][$node['catalog_id']] = &$node;
//            debug($this->data);
        }

        return $tree;
    }

    protected function getMenuHtml($tree, $tab = '')
    {   $str = '';
//        $str = '<li class="">
//    <a href="/" class="">Home</a></li>';
        foreach ($tree as $category) {
            $str .= $this->catToTemplate($category, $tab);

        }
//        $str = $str . '<li class=""><a href="/" class="">Contacts</a></li>';
        return $str;
    }

    protected function catToTemplate($category, $tab) {
        ob_start();
        include __DIR__ . '/menu_tpl/' . $this->tpl;
        return ob_get_clean();
    }
}