<li><?php if( !isset($category['childs']) ): ?>
<a href="<?= \yii\helpers\Url::to(['category/view', 'id' =>$category['id']]) ?>"><?= $category['name'] ?></a></li>
    <?php elseif ( isset($category['childs']) ): ?>
<li class="dropdown"><a href="<?= \yii\helpers\Url::to(['category/view', 'id' =>$category['id']]) ?>" class="dropdown-toggle" data-toggle="dropdown"> <?= $category['name'] ?><b class="caret"></b>
            </a>

    <?php endif; ?>

    <?php if ( isset($category['childs']) ): ?>
    <ul class="dropdown-menu multi-column columns-2">
        <div class="row">
            <div class="col-sm-12">
                <ul class="multi-column-dropdown">
                    <h6>NEW IN</h6>
                    <li><a href="<?= \yii\helpers\Url::to(['category/view', 'id' =>$category['id']]) ?>"></a></li>
            <?= $this->getMenuHtml($category['childs']) ?>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </ul>
    <?php endif; ?>
</li>