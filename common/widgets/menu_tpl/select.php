<option
    value="<?= $category['category_id']?>"
    <?php if($category['category_id'] == $this->model->parent_id) echo ' selected'?>
    <?php if($category['category_id'] == $this->model->category_id) echo ' disabled'?>
><?= $tab . $category['name']?></option>
<?php if ( isset($category['childs']) ): ?>
    <ul>
        <?= $this->getMenuHtml($category['childs'], $tab . '-') ?>
    </ul>
<?php endif; ?>