<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/bootstrap.css',
        'css/component.css',
        'css/flexslider.css',
        'css/site.css',
        'css/style.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/bootstrap-3.1.1.min.js',
//        'js/simpleCart.min.js',
//        'js/responsiveslides.min.js',
//        'js/flexisel.js',
//        'js/flexslider.js',



    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
//        'js/jquery.min.js',

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
