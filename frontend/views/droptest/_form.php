<?php

use common\models\Dropdown;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\depdrop\DepDrop;
//use app\modules\proj\models\Drop;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\proj\models\Droptest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="droptest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textarea(['rows' => 6]) ?>
    <?php
    $catList = Dropdown::find()->where('id = drop_id')->all();
    $catList = ArrayHelper::map($catList, 'id', 'name');
    ?>
    <?= $form->field($model, 'cat')->dropDownList($catList, ['id' => 'cat-id', 'prompt' => '-- Select Category --']); ?>

    <?php
//    if (!$model->isNewRecord && isset($model->cat))
//    {
//        $subcatList = Dropdown::find()->where('drop_id = :id', [':id' => $model->cat])->all();
//        $subcatList_ = ArrayHelper::map($subcatList, 'id', 'name');
//
//        if (isset($model->subcat))
//        {
//            $subcatList = [$model->subcat => $model->subcats->name];
//            unset($subcatList_[$model->subcat]);
//            $subcatList = $subcatList + ['' => 'Select...'];
//        }
//        else
//        {
//            $subcatList = ['' => 'Select...'];
//        }
//
//        $subcatList = $subcatList + $subcatList_;
//    }
//    else
        $subcatList = [];

    echo $form->field($model, 'subcat')->widget(DepDrop::classname(), [
        'options' => ['id' => 'subcat-id'],
        'data' => $subcatList,
        'pluginOptions' => [
            'depends' => ['cat-id'],
            'placeholder' => 'Select...',
            'url' => Url::to(['subcat'])
        ]
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>