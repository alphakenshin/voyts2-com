<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Order Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-contacts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Order Contacts', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'updated_at',
            'qty',
            'size',
            //'sum',
            //'status',
            //'firstname',
            //'lastname',
            //'delivery_id',
            //'area_id',
            //'site_key',
            //'city_id',
            //'address:ntext',
            //'phone',
            //'email:email',
            //'additional_info:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
