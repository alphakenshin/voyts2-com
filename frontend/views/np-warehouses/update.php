<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\NpWarehouses */

$this->title = 'Update Np Warehouses: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Np Warehouses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="np-warehouses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
