<?php

use common\models\Dropdown;
use common\models\NpWarehouses;
use kartik\depdrop\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\NpWarehouses */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="np-warehouses-form">

    <?php $form = ActiveForm::begin(); ?>
<?php
    $warehousesNp = NpWarehouses::find()->indexBy('area_id')->asArray()->all();
    $catList = ArrayHelper::map($warehousesNp, 'area_id', 'settlement_area_description');

    /*** START ****/
    /*** собственная функция, вместо нее использовал $catList = ArrayHelper::map(...), результат их выполнения одинаков ****/

    $areas = array();
    foreach ($warehousesNp as $p) {
    $key = $p['area_id'];
    $areas[$key] = $p['settlement_area_description'];
    }
    /*** END ****/
    $areasN = asort($catList);

    ?>
    <?= $form->field($model, 'area_id')->dropDownList($catList, ['id' => 'cat-id', 'prompt' => '-- Select Category --']); ?>

    <?php

    $subcatList = [];

    echo $form->field($model, 'city_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'subcat-id'],
        'data' => $subcatList,
        'pluginOptions' => [
            'depends' => ['cat-id'],
            'placeholder' => 'Select...',
            'url' => Url::to(['city'])
        ]
    ]);

    $warehouseList = [];

    echo $form->field($model, 'site_key')->widget(DepDrop::classname(), [
        'options' => ['id' => 'warehouse-id'],
        'data' => $warehouseList,
        'pluginOptions'=>[
            'depends'=>['cat-id', 'subcat-id'],
            'placeholder'=>'Select...',
            'url' => Url::to(['warehouse'])
        ]
    ]);
    ?>





    <?= $form->field($model, 'city_description_ru')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'delivery_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
