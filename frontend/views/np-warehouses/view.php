<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\NpWarehouses */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Np Warehouses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="np-warehouses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'site_key',
            'description',
            'description_ru',
            'short_address',
            'short_address_ru',
            'phone',
            'type_of_warehouse',
            'ref',
            'number',
            'city_ref',
            'city_id',
            'city_description',
            'city_description_ru',
            'settlement_ref',
            'settlement_description',
            'settlement_area_ref',
            'settlement_area_description',
            'area_id',
            'settlement_regions_description',
            'regions_id',
            'settlement_type_description',
            'longitude',
            'latitude',
            'post_finance',
            'bicycle_parking',
            'payment_access',
            'pos_terminal',
            'international_shipping',
            'total_max_weight_allowed',
            'place_max_weight_allowed',
            'reception:ntext',
            'delivery:ntext',
            'schedule:ntext',
            'district_code',
            'warehouse_status',
            'category_of_warehouse',
            'delivery_id',
        ],
    ]) ?>

</div>
