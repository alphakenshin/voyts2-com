<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container">
    <div class="products-page">
                <?php  echo '<pre>' . print_r($product, true) . '</pre>';
 ?>
        <!--        --><?php //debug($product->category) ?>
        <div class="products">
            <div class="product-listy">
                <h2>our Products</h2>
                <ul class="product-list">
                    <li><a href="">New Products</a></li>
                    <li><a href="">Old Products</a></li>
                    <li><a href="">T-shirts</a></li>
                    <li><a href="">pants</a></li>
                    <li><a href="">Dress</a></li>
                    <li><a href="">Shorts</a></li>
                    <li><a href="#">Shirts</a></li>
                    <li><a href="register.html">Register</a></li>
                </ul>
            </div>
            <div class="latest-bis">
                <img src="/web/images/products/l4.jpg" class="img-responsive" alt="">
                <div class="offer">
                    <p>40%</p>
                    <small>Top Offer</small>
                </div>
            </div>
            <div class="tags">
                <h4 class="tag_head">Tags Widget</h4>
                <ul class="tags_links">
                    <li><a href="#">Kitesurf</a></li>
                    <li><a href="#">Super</a></li>
                    <li><a href="#">Duper</a></li>
                    <li><a href="#">Theme</a></li>
                    <li><a href="#">Men</a></li>
                    <li><a href="#">Women</a></li>
                    <li><a href="#">Equipment</a></li>
                    <li><a href="#">Best</a></li>
                    <li><a href="#">Accessories</a></li>
                    <li><a href="#">Men</a></li>
                    <li><a href="#">Apparel</a></li>
                    <li><a href="#">Super</a></li>
                    <li><a href="#">Duper</a></li>
                    <li><a href="#">Theme</a></li>
                    <li><a href="#">Responsive</a></li>
                    <li><a href="#">Women</a></li>
                    <li><a href="#">Equipment</a></li>
                </ul>

            </div>

        </div>
        <?php
//        $mainImg = $product->getImage();
//        $gallery = $product->getImages();
//         echo '<pre>' . print_r($product, true) . '</pre>';
?>
        <div class="new-product page">
            <div class="col-md-5 zoom-grid">
                <div class="flexslider">
                    <ul class="slides">


                        <?php if(!empty($product->gallery_1)): ?>
                            <li data-thumb="/images/900x600<?= $product->gallery_1 ?>">
                                <div class="thumb-image"><img class="img-responsive" src="/images/900x600/<?= $product->gallery_1 ?>" alt="<?= $product->model ?>" data-imagezoom="true"></div>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($product->gallery_2)): ?>
                            <li data-thumb="/images/900x600<?= $product->gallery_2 ?>">
                                <div class="thumb-image"><img class="img-responsive" src="/images/900x600/<?= $product->gallery_2 ?>" alt="<?= $product->model ?>" data-imagezoom="true"></div>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($product->gallery_3)): ?>
                            <li data-thumb="/images/900x600<?= $product->gallery_3 ?>">
                                <div class="thumb-image"><img class="img-responsive" src="/images/900x600/<?= $product->gallery_3 ?>" alt="<?= $product->model ?>" data-imagezoom="true"></div>
                            </li>
                        <?php endif; ?>



                    </ul>
                </div>
            </div>
            <div class="col-md-7 dress-info">
                <div class="dress-name">
                    <h3><?= $product->model ?></h3>
                    <span><?= $product->price ?> грн.</span>
                    <div class="clearfix"></div>
                    <?= $product->description ?>
                </div>
                <?php if(!empty($product->manufacturer_warranty)): ?>
                    <div class="span span1">
                        <p class="left">FABRIC ORIGIN: </p>
                        <p class="right"><?= $product->manufacturer_warranty ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->country_of_origin)): ?>
                    <div class="span span2">
                        <p class="left">Произведено в: </p>
                        <p class="right"><?= $product->country_of_origin ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->brand)): ?>
                <div class="span span3">
                    <p class="left">Бренд: </p>
                    <p class="right"><?= $product->	brand ?></p>
                    <div class="clearfix"></div>
                </div>
                <?php endif; ?>
                <?php if(!empty($product->color)): ?>
                    <div class="span span3">
                        <p class="left">Цвет: </p>
                        <p class="right"><?= $product->color ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->silhouette)): ?>
                    <div class="span span3">
                        <p class="left">Силуэт: </p>
                        <p class="right"><?= $product->silhouette ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->	cloth)): ?>
                    <div class="span span3">
                        <p class="left">Ткань: </p>
                        <p class="right"><?= $product->	cloth ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->print)): ?>
                    <div class="span span3">
                        <p class="left">Принт: </p>
                        <p class="right"><?= $product->print ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->style)): ?>
                    <div class="span span3">
                        <p class="left">Стиль: </p>
                        <p class="right"><?= $product->style ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->season)): ?>
                    <div class="span span3">
                        <p class="left">Сезон: </p>
                        <p class="right"><?= $product->season ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->	product_length)): ?>
                    <div class="span span3">
                        <p class="left">Длина изделия: </p>
                        <p class="right"><?= $product->	product_length ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->sleeve)): ?>
                    <div class="span span3">
                        <p class="left">Рукав: </p>
                        <p class="right"><?= $product->sleeve ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->	cutout_collar)): ?>
                    <div class="span span3">
                        <p class="left">Воротник: </p>
                        <p class="right"><?= $product->	cutout_collar ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->additional_details)): ?>
                    <div class="span span3">
                        <p class="left">Дополнительные детали: </p>
                        <p class="right"><?= $product->additional_details ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->	sale)): ?>
                    <div class="span span3">
                        <p class="left">Скидка: </p>
                        <p class="right"><?= $product->	sale ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->size)): ?>
                    <div class="span span4">
                        <p class="left">SIZE</p>
                        <p class="right"><span class="selection-box"><select class="domains valid" name="domains">
										   <option><?= $product->size ?></option>
<!--										   <option>L</option>-->
<!--										   <option>XL</option>-->
<!--										   <option>FS</option>-->
<!--										   <option>S</option>-->
									   </select></span></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->sizes_eu_1)): ?>
                    <div class="span span4">
                        <p class="left">SIZE</p>
                        <p class="right"><span class="selection-box"><select class="domains valid" name="domains">
                                    <?php if(!empty($product->sizes_eu_1)): ?>		   <option><?= $product->sizes_eu_1 ?></option><?php endif; ?>                                    <!--										   <option>XL</option>-->
                                    <?php if(!empty($product->sizes_eu_2)): ?>		   <option><?= $product->sizes_eu_2 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_3)): ?>		   <option><?= $product->sizes_eu_3 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_4)): ?>		   <option><?= $product->sizes_eu_4 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_5)): ?>		   <option><?= $product->sizes_eu_5 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_6)): ?>		   <option><?= $product->sizes_eu_6 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_7)): ?>		   <option><?= $product->sizes_eu_7 ?></option><?php endif; ?>                                    <!--										   <option>XL</option>-->
                                    <?php if(!empty($product->sizes_eu_8)): ?>		   <option><?= $product->sizes_eu_8 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_9)): ?>		   <option><?= $product->sizes_eu_9 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_10)): ?>		   <option><?= $product->sizes_eu_10 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_11)): ?>		   <option><?= $product->sizes_eu_11 ?></option><?php endif; ?>
                                    <?php if(!empty($product->sizes_eu_12)): ?>		   <option><?= $product->sizes_eu_12 ?></option><?php endif; ?>
									   </select></span></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(($product->source) == 'glem' ): ?>
                    <div class="span span5">
                        <p class="left"></p>
                        <p class="right">NEW</p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($product->sale) =='1' ): ?>
                    <div class="span span6">
                        <p class="left">SALE</p>
                        <p class="right">-10%</p>
                        <div class="clearfix"></div>
                    </div>

                <?php endif; ?>
                <div class="purchase">
                    <!--                    <label>Количество:</label>-->
                    <input type="text" value="1" id="qty" />
                    <a href="<?= Url::to(['cart/add', 'id' => $product->id]) ?>" data-id="<?= $product->id ?>" class="item_add">Купить сейчас</a>
                    <div class="social-icons">
                        <ul>
                            <li><a class="facebook1" href="#"></a></li>
                            <li><a class="twitter1" href="#"></a></li>
                            <li><a class="googleplus1" href="#"></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <script src="/js/imagezoom.js"></script>
                <!-- FlexSlider -->
                <script defer="" src="/js/jquery.flexslider.js"></script>
                <script>
                    // Can also be used with $(document).ready()
                    $(window).load(function() {
                        $('.flexslider').flexslider({
                            animation: "slide",
                            controlNav: "thumbnails"
                        });
                    });
                </script>
            </div>
            <div class="clearfix"></div>
            <div class="reviews-tabs">
                <!-- Main component for a primary marketing message or call to action -->
                <ul class="nav nav-tabs responsive hidden-xs hidden-sm" id="myTab">
                    <li class="test-class active"><a class="deco-none misc-class" href="#how-to"> More Information</a></li>
                    <li class="test-class"><a href="#features">Specifications</a></li>
                    <li class="test-class"><a class="deco-none" href="#source">Reviews (7)</a></li>
                </ul>

                <div class="tab-content responsive hidden-xs hidden-sm">
                    <div class="tab-pane active" id="how-to">
                        <p class="tab-text"><?= $product->description ?></p>
                    </div>
                    <div class="tab-pane" id="features">
                        <p class="tab-text"><?= $product->description ?></p>

                    </div>
                    <div class="tab-pane" id="source">
                        <div class="response">
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MARCH 21, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MARCH 26, 2054</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MAY 25, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>FEB 13, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>JAN 28, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>APR 18, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>DEC 25, 2014</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                </div><div class="panel-group responsive visible-xs visible-sm" id="collapse-myTab"><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle deco-none misc-class" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-how-to"> More Information</a></h4></div><div id="collapse-how-to" class="panel-collapse collapse in" style="height: auto;"></div></div><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-features">Specifications</a></h4></div><div id="collapse-features" class="panel-collapse collapse"></div></div><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle deco-none" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-source">Reviews (7)</a></h4></div><div id="collapse-source" class="panel-collapse collapse"></div></div></div>
            </div>
            <div class="span span7">
                <p class="left">Категория:</p>
                <p class="right"><a  href="<?= \yii\helpers\Url::to(['category/view', 'id'=> $product->category->id])?>"><?= $product->category->name ?></a></p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
</div>
<div class="other-products products-grid">
    <div class="container">
        <?php if(!empty($products)): ?>
        <header>
            <h3 class="like text-center">Популярные товары</h3>
        </header>
        <?php foreach ($products as $product): ?>
            <?php $productImg = $product->getImage(); ?>
            <div class="col-md-4 product simpleCart_shelfItem text-center">
                <a href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $product->id])?>"><?= Html::img("/web/{$productImg->getPath()}", ['alt' => $product->model]) ?>
                </a>
                <div class="mask">
                    <a href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $product->id])?>">Quick View</a>
                </div>
                <a class="product_name" href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $product->id])?>"><?= $product->model ?></a>
                <p><i></i> <span class="item_price"><?= $product->price_uah ?> грн. </span></p>
            </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
    </div>
    <?php endif; ?>
</div>
<script src="/js/responsive-tabs.js"></script>
<script type="text/javascript">
    $( '#myTab a' ).click( function ( e ) {
        e.preventDefault();
        $( this ).tab( 'show' );
    } );

    $( '#moreTabs a' ).click( function ( e ) {
        e.preventDefault();
        $( this ).tab( 'show' );
    } );

    ( function( $ ) {
        // Test for making sure event are maintained
        $( '.js-alert-test' ).click( function () {
            alert( 'Button Clicked: Event was maintained' );
        } );
        fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
    } )( jQuery );

</script>