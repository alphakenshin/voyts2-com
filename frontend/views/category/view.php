<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
?>
<!-- content-section-starts -->
<div class="container">
    <div class="products-page">
        <div class="new-product">
            <div class="new-product-top">
                <ul class="product-top-list">
                    <li><a href="index.html">Home</a>&nbsp;<span>&gt;</span></li>
                    <li><span class="act"><?= $category->name ?></span>&nbsp;</li>
                </ul>
                <p class="back"><a href="index.html">Back to Previous</a></p>
                <div class="clearfix"></div>
            </div>
            <div class="mens-toolbar">

                        <br><br>
                        <div class="col-xs-12 flex-active">
                            <div class="product-search"><div class="form-group">
                        <?php echo 'Категории: <br>';
//                        foreach ($facets['type'] as $frame) {
//                            $type = $frame['value'];
//                            $count = $frame['count'];
//
//                            echo $type . ' (' . $count . ')<br>';
//                        }
//                        foreach ($facets['brand'] as $frame) {
//                            $type = $frame['value'];
//                            $count = $frame['count'];
//                            echo $type . ' (' . $count . ')<br>';
//                        }
//                        foreach ($facets['country_of_origin'] as $frame) {
//                            $type = $frame['value'];
//                            $count = $frame['count'];
//                            echo $type . ' (' . $count . ')<br>';
//                        }
//                        foreach ($facets['sales_notes'] as $frame) {
//                            $type = $frame['value'];
//                            $count = $frame['count'];
//                            echo $type . ' (' . $count . ')<br>';
//                        } ?>
                                </div>
                    </div>
                </div>
                <?php if(!empty($products)): ?>
                <?php echo LinkPager::widget(['pagination' => $pages,]) ?>
                <div class="clearfix"></div>
            </div>
            <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                <div class="cbp-vm-options">
                    <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid" title="grid">Grid View</a>
                    <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list" title="list">List View</a>
                </div>
                <div class="pages">
<!--                    <div class="limiter visible-desktop">-->
<!--                        <label>Show</label>-->
<!--                        <select>-->
<!--                            <option value="" selected="selected">-->
<!--                                9                </option>-->
<!--                            <option value="">-->
<!--                                15                </option>-->
<!--                            <option value="">-->
<!--                                30                </option>-->
<!--                        </select> per page-->
<!--                    </div>-->
                </div>
                <div class="clearfix"></div>

                <ul>
                    <?php $i = 0; foreach ($products as $product): ?>
<!--                    --><?php //$mainImg = $product->getImage(); ?>

<?php //          echo '<pre>' . print_r($product, true) . '</pre>';
?>

                    <li>
                        <a class="cbp-vm-image" href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $product->id])?>">
                            <div class="simpleCart_shelfItem">
                                <div class="view view-first">
                                    <div class="inner_content clearfix">
                                        <div class="product_image">
                                            <img src="/images/womens-clothes/450x300/<?=  $product->main_img ?>" alt="<?=  $product->model ?>">
                                            <div class="mask">
                                                <div class="info">Quick View</div>
                                            </div>
                                            <div class="product_container">
                                                <div class="cart-left">
                                                    <p class="title"><?= $product->model ?></p>
                                                </div>
                                                <div class="pricey"><span class="item_price"><?= $product->price ?> грн. </span></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </a>
                        <div class="cbp-vm-details">
                            <?= $product->description ?>
                        </div>
                        <a class="cbp-vm-icon cbp-vm-add item_add" data-id="<?= $product->id ?>" href="#">Add to cart</a>
            </div>
            <?php $i++ ?>
            <?php if ($i % 4 == 0): ?>
                <div class="clearfix"></div>

            <?php endif; ?>
            <?php endforeach; ?>
            </li>
            <?php else: ?>
                <h2>Здесь товаров пока нет... </h2>
            <?php endif; ?>


            </ul>
        </div>
        <script src="/js/cbpViewModeSwitch.js" type="text/javascript"></script>

    </div>
