<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Modal;
use frontend\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?><!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!--    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />-->
    <!--     jQuery (necessary for Bootstrap's JavaScript plugins)-->
    <!--    <script src="js/jquery.min.js"></script>-->
    <!-- Custom Theme files -->
    <!--    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />-->
    <!-- Custom Theme files -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>


    <!--webfont-->
    <!-- for bootstrap working -->
    <!-- //for bootstrap working -->
    <!--    <script type="text/javascript" src="js/bootstrap-3.1.1.min.js"></script>-->
    <!-- cart -->
    <!--    <script src="js/simpleCart.min.js"> </script>-->


    <!--    --><?php //$this->registerJsFile('/js/simpleCart.min.js', ['position' => \yii\web\View::POS_BEGIN]) ?>
    <?php $this->registerJsFile('/js/responsiveslides.min.js', ['position' => \yii\web\View::POS_BEGIN]) ?>
    <!--    --><?php //$this->registerJsFile('/js/cbpViewModeSwitch.js', ['position' => \yii\web\View::POS_END]) ?>
    <?php $this->registerJsFile('/js/classie.js', ['position' => \yii\web\View::POS_END]) ?>
    <?php $this->registerJsFile('/js/jquery.flexisel.js', ['position' => \yii\web\View::POS_END]) ?>
    <?php $this->registerJsFile('/js/main.js', ['position' => \yii\web\View::POS_END]) ?>

    <!-- cart -->
    <!--    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />-->
    <?php $this->head() ?>
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1);
        }
    </script>
</head>
<body>
<?php $this->beginBody() ?>
<!-- header-section-starts -->
<div class="header">
    <div class="header-top-strip">
        <div class="container">
            <div class="header-top-left">
                <ul>
                    <li><a href="
<!--                    --><?php //echo \app\widgets\Wlang::widget(); ?>
"><span class="glyphicon"></span></a></li>

                    <li><a href="<?= \yii\helpers\Url::to('/admin')?>"><span class="glyphicon glyphicon-user"> </span>Login</a></li>
                    <?php if(!Yii::$app->user->isGuest):?>
                        <li><a href="<?= \yii\helpers\Url::to(['/site/logout'])?>"><span class="glyphicon glyphicon-lock"> </span>
                                <?= Yii::$app->user->identity['username']?>( Выход )</a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="header-right">
                <div class="cart box_1">
                    <a href="#" onclick="return getCart()">
                        <h3> <span class="simpleCart_total"> $0.00 </span> (<span id="simpleCart_quantity" class="simpleCart_quantity"> 0 </span>)<img src="/images/bag.png" alt=""></h3>
                    </a>
                    <p><a href="javascript:;" class="simpleCart_empty">Empty cart</a></p>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- header-section-ends -->
<div class="banner-top">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="logo">
                    <h1><a href="<?= \yii\helpers\Url::home(); ?>"><span>E</span> -Shop</a></h1>
                </div>
            </div>
            <!--/.navbar-header-->

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/">Home</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Женщинам <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-1">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="multi-column-dropdown">
                                        <h6><a href="/women-clothes">ОДЕЖДА</a></h6>
                                        <h6><a href="/women-shoes">ОБУВЬ</a></h6>
                                        <h6><a href="/women-accessories">АКСЕССУАРЫ</a></h6>

                                    </ul>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Мужчинам <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-3">
                            <div class="row">
                                <div class="col-sm-4">
                                    <ul class="multi-column-dropdown">
                                        <h6><a href="/men-clothes">ОДЕЖДА</a></h6>
                                        <h6><a href="/men-shoes">ОБУВЬ</a></h6>
                                        <h6><a href="/men-accessories">АКСЕССУАРЫ</a></h6>


                                        <!--                                        <li><a href="/">New In Bags</a></li>-->
                                        <!--                                        <li><a href="/">New In Shoes</a></li>-->
                                        <!--                                        <li><a href="/">New In Watches</a></li>-->
                                        <!--                                        <li><a href="/">New In Beauty</a></li>-->
                                    </ul>
                                </div>

                            </div>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Детям <b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-1">
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="multi-column-dropdown">
                                        <h6><a href="/children-clothes">ОДЕЖДА</a></h6>
                                        <h6><a href="/children-shoes">ОБУВЬ</a></h6>
                                        <h6><a href="/children-accessories">АКСЕССУАРЫ</a></h6>

                                    </ul>
                                </div>


                                <div class="clearfix"></div>
                            </div>
                        </ul>
                    </li>

                    <li><a href="typography.html">TYPO</a></li>
                    <li><a href="contact.html">CONTACT</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->

        <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
</div>


<?= $content ?>
<!-- content-section-ends-here -->
<div class="news-letter">
    <div class="container">
        <div class="join">
            <h6>JOIN OUR MAILING LIST</h6>
            <div class="sub-left-right">
                <form>
                    <input type="text" value="Enter Your Email Here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter Your Email Here';}" />
                    <input type="submit" value="SUBSCRIBE" />
                </form>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="footer_top">
            <div class="span_of_4">
                <div class="col-md-3 span1_of_4">
                    <h4>Shop</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>help</h4>
                    <ul class="f_nav">
                        <li><a href="#">frequently asked  questions</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>account</h4>
                    <ul class="f_nav">
                        <li><a href="account.html">login</a></li>
                        <li><a href="register.html">create an account</a></li>
                        <li><a href="#">create wishlist</a></li>
                        <li><a href="checkout.html">my shopping bag</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">create wishlist</a></li>
                    </ul>
                </div>
                <div class="col-md-3 span1_of_4">
                    <h4>popular</h4>
                    <ul class="f_nav">
                        <li><a href="#">new arrivals</a></li>
                        <li><a href="#">men</a></li>
                        <li><a href="#">women</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">kids</a></li>
                        <li><a href="#">brands</a></li>
                        <li><a href="#">trends</a></li>
                        <li><a href="#">sale</a></li>
                        <li><a href="#">style videos</a></li>
                        <li><a href="#">login</a></li>
                        <li><a href="#">brands</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="cards text-center">
            <img src="/images/cards.jpg" alt="" />
        </div>
        <div class="copyright text-center">
            <p>© 2015 Eshop. All Rights Reserved | Design by   <a href="http://w3layouts.com">  W3layouts</a></p>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
<?php
Modal::begin([
    'header' => '<h2>Корзина</h2>',
    'id' => 'cart',
    'size' => 'modal-lg',
    'footer' => '<button type="button" class="btn btn-secondary" data-dismiss="modal">Продолжить покупки</button>
        <a href="' . \yii\helpers\Url::to(['cart/view']) . '"class="btn btn-success">Оформить заказ</a>
     <button type="button" class="btn btn-danger" onclick="clearCart()">Очистить корзину</button>'

]);

Modal::end();
?></body>
</html>

<?php $this->endPage() ?>