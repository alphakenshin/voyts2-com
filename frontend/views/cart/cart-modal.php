<?php use yii\helpers\Html;
use yii\helpers\Url;

if(!empty($session['cart'])): ?>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Фото</th>
                <th>Наименование</th>
                <th>Размер</th>
                <th>Кол-во</th>
                <th>Цена</th>
                <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
            </tr>
            </thead>
            <tbody>
<!--            <pre>--><?php //print_r($session['cart'])?><!--</pre>-->

            <?php foreach ($session['cart'] as $id => $item): ?>
                <tr>
                    <td><?= Html::img("/images/165x110{$item['img']}", ['alt' => $item['name'],'height' => 80])?></td>
                    <td><?= $item['name']?></td>

                    <?php if(!empty($item['size'])): ?>
                    <td><?= $item['size']?></td>
                    <?php else: ?>
                        <td></td>
                    <?php endif; ?>
                    <td><?= $item['qty']?></td>
                    <td><?= $item['price']?></td>
                    <td><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4">Итого: </td>
                <td><?= $session['cart.qty']?> </td>

            </tr>
            <tr>
                <td colspan="4">На сумму: </td>
                <td><?= $session['cart.sum']?> </td>

            </tr>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <h3>Корзина пуста</h3>
<?php endif; ?>
<script>     $('.del-item').on('click', function() {
        var id = $(this).data('id');
        console.log(id);
        $.ajax({
            url: '/cart/del-item',
            data: {id: id},
            type: 'GET',
            success: function (res) {
                if (!res) alert('Ошибка!');
                console.log(res);
                showCart(res);
            },
            error: function () {
                alert('Error!');
            }
        });
    });</script>