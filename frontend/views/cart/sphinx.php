<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>
<form method="GET" action="" id="search_form">
        <br> <br>
        <div class="container"><header>
                <h1><?= Html::encode($this->title) ?></h1>
            </header>
            <?php $this->params['breadcrumbs'][] = $this->title; ?>

            <div class="col-xs-12 flex-active">
                <div class="row-fluid">
                    <div class="span2">
                        <br> <br>
                        <?php if (isset($facets['area_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Область</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['area_id'] as $item):?>
                                            <label class="radio"> <input type="radio"
                                                                            name="area_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['area_id']) && (in_array($item['value'],$_GET['area_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { } else { echo $item['name']  .' ('.$item['count'].')'; } ?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_area_id" value="Reset" data-target="area_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['regions_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Район</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['regions_id'] as $item):?>
                                            <label class="radio"> <input type="radio"
                                                                            name="regions_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['regions_id']) && (in_array($item['value'],$_GET['regions_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другая'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_regions_id" value="Reset" data-target="regions_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['city_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Міста</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['city_id'] as $item):?>
                                            <label class="radio"> <input type="radio"
                                                                            name="city_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['city_id']) && (in_array($item['value'],$_GET['city_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_city_id" value="Reset" data-target="city_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <div class="row">
                            <?php if (count($docs) > 0): ?>
                            <p class="lead">
                                Всего найдено:<?=$total_found?>
                            </p>

                            <div class="span9">


                            </div>
                            <?php $i = 0; foreach ($docs as $doc): ?>

                                    <div class="simpleCart_shelfItem">
                                        <div class="view view-first">
                                            <div class="inner_content clearfix">


                                                    <div class="product_container">

<!--                                                        <div class="pricey"><span class="item_price">--><?//= $doc['id'] ?><!-- </span></div>-->
<!--                                                        <div class="pricey"><span class="item_price">--><?//= $doc['ref'] ?><!-- </span></div>-->
                                                        <div class="pricey"><span class="item_price"><?= $doc['description'] ?> </span></div>
<!--                                                        <div class="pricey"><span class="item_price">--><?//= $doc['phone'] ?><!-- </span></div>-->
<!--                                                        <div class="pricey"><span class="item_price">--><?//= $doc['short_address'] ?><!-- </span></div>-->
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                          </div>
                                    </div>


                                <?php endforeach; ?>
                        </div>

                        <?php elseif (isset($_GET['query']) && $_GET['query'] != ''): ?>
                        <h2>Відділень Нової Пошти немає... </h2>
                        <?php endif; ?>
                    </div>
                </div>
</form>
</div>


<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
<!--    <script src="/js/bootstrap-3.1.1.min.js"></script>-->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script>
    function __highlight(s, t) {
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(document).ready(function() {
        $(':radio,:checkbox').change(function() {

            $("#search_form").trigger('submit');

        });
        $(':reset').click(function(){

            location.search ='';
        });
        $('input[name^=reset_]').click(function(){
            $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
            $("#search_form").trigger('submit');

        });

    });
</script>



</div>