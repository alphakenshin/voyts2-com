<?php

use backend\models\NpAreas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>


<?php

$form = ActiveForm::begin();

$areasNp = NpAreas::find()->indexBy('ref')->asArray()->all();

echo $form->field($model, 'area')->dropDownList($areasNp, ['prompt' => 'Выберите область', 'ref' => 'region-selector']);
echo $form->field($model, 'city')->dropDownList([], ['prompt' => 'Выберите город', 'ref' => 'city-selector']);
echo Html::submitButton('Сохранить', ['class' => 'btn btn-success']);
ActiveForm::end();
?>
<script type="text/javascript">
    $('#region-selector').on('change', function () {
        $.ajax('/cart-controller/get-cities/' + this.value, {
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    $('#city-selector').empty();
                    $.each(data.cities, function (key, val) {
                        $('#city-selector').append('<option value="' + val.ref + '">' + val.description + '</option>');
                    });
                }
            }
        });
    });
</script>