<?php


use common\models\City;
use common\models\DeliveryType;
use kartik\depdrop\DepDrop;
use LisDev\Delivery\NovaPoshtaApi2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\Dropdown;
use common\models\NpWarehouses;

?>
<div class="container">
    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif; ?>
    <?php if(!empty($session['cart'])): ?>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Фото</th>
                    <th>Наименование</th>
                    <th>Размер</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Сумма</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($session['cart'] as $id => $item): ?>
                    <tr>
                        <td><?= Html::img("/images/165x110{$item['img']}", ['alt' => $item['name'],'height' => 80])?></td>
                        <td><a href="<?= Url::to(['product/view', 'id' => $id]) ?>"><?= $item['name']?></td>
                        <td><?= $item['size']?></td>
                        <td><?= $item['qty']?></td>
                        <td><?= $item['price']?></td>
                        <td><?= $item['price']*$item['qty'] ?></td>
                        <td><span data-id="<?= $id ?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span></td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5">Итого: </td>
                    <td><?= $session['cart.qty']?> </td>

                </tr>
                <tr>
                    <td colspan="5">На сумму: </td>
                    <td><?= $session['cart.sum']?> </td>

                </tr>
                </tbody>
            </table>
        </div>
        <hr/>
        <?php $form = ActiveForm::begin() ?>
        <?= $form->field($order, 'firstname') ?>
        <?= $form->field($order, 'lastname') ?>

       <?php

//    $city_ref = '000655d4-4079-11de-b509-001d92f78698';

//        $areasNp = NpAreas::find()->indexBy('ref')->asArray()->all();
//        $yyy = NpCities::find()->indexBy('area')->asArray()->one();
//        $warehousesNp = NpWarehouses::find()->where(['city_ref' => $city_ref])->asArray()->all();
//        $citiesNp = NpCities::find()->where(['area' => '71508140-9b87-11de-822f-000c2965ae0e'])->asArray()->all();


//        $regions  = City::find()->indexBy('region_id')->asArray()->all();
//        $cities = City::find()->indexBy('city_id')->asArray()->all();
//        $country = City::find()->indexBy('country_id')->asArray()->all();
        ?>
<!--        <pre> --><?php //print_r($yyy) ?><!--</pre>-->
<!--        <pre> --><?php //print_r($xxx) ?><!--</pre>-->
<!--        <pre> --><?php //print_r($zzz) ?><!--</pre>-->

        <?php
        $delivery = DeliveryType::find()->indexBy('delivery_id')->asArray()->all();
        $deliveryList = ArrayHelper::map($delivery, 'delivery_id', 'delivery_type');

//        $areasN = asort($catList);

        ?>
        <?= $form->field($order, 'delivery_id')->dropDownList($deliveryList, ['id' => 'del-id', 'prompt' => '-- Оберіть спосіб діставки --']); ?>
<?php



        $areaList = [];
        echo $form->field($order, 'area_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'area-id'],
            'data' => $areaList,
            'pluginOptions' => [
                'depends' => ['del-id'],
                'placeholder' => 'Область...',
                'url' => Url::to(['area'])
            ]
        ]);


        $subcatList = [];

    echo $form->field($order, 'city_id')->widget(DepDrop::classname(), [
        'options' => ['id' => 'subcat-id'],
        'data' => $subcatList,
        'pluginOptions' => [
            'depends' => ['del-id', 'area-id'],
            'placeholder' => 'Місто...',
            'url' => Url::to(['city'])
        ]
    ]);

    $warehouseList = [];

    echo $form->field($order, 'site_key')->widget(DepDrop::classname(), [
        'options' => ['id' => 'warehouse-id'],
        'data' => $warehouseList,
        'pluginOptions'=>[
            'depends'=>['del-id', 'area-id', 'subcat-id'],
            'placeholder'=>'Відділення...',
            'url' => Url::to(['warehouse'])
        ]
    ]);

                        /***  Укр пошта ***/


        $area2List = [];
        echo $form->field($order, 'area_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'area-two-id'],
            'data' => $area2List,
            'pluginOptions' => [
                'depends' => ['del-id'],
                'placeholder' => 'Область...',
                'url' => Url::to(['area-two'])
            ]
        ]);

        $region2List = [];
        echo $form->field($order, 'region_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'region-two-id'],
            'data' => $region2List,
            'pluginOptions' => [
                'depends' => ['del-id', 'area-two-id'],
                'placeholder' => 'Район...',
                'url' => Url::to(['region-two'])
            ]
        ]);


        $city2List = [];

        echo $form->field($order, 'city_id')->widget(DepDrop::classname(), [
            'options' => ['id' => 'city-two-id'],
            'data' => $city2List,
            'pluginOptions' => [
                'depends' => ['del-id', 'area-two-id', 'region-two-id'],
                'placeholder' => 'Населений пункт...',
                'url' => Url::to(['city-two'])
            ]
        ]);


    /***  ***/








?>








<!--        --><?php //if (isset($country)): ?>
<!--            <div class="form-group field-ordercontacts-country_id has-success">-->
<!--                <label class="control-label" for="ordercontacts-country_id">Страна  </label>-->
<!--                <select id="ordercontacts-country_id" class="form-control" name="OrderContacts[country_id]" aria-invalid="false">-->
<!--                    --><?php //foreach ($country as $item):?>
<!--                    <option value="--><?//= $item['country_id']?><!--">--><?//= $item['country_name']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?php //if (isset($regions)): ?>
<!--            <div class="form-group field-ordercontacts-region_id required has-success">-->
<!--                <label class="control-label" for="ordercontacts-region_id">Область </label>-->
<!--                <select id="ordercontacts-region_id" class="form-control" name="OrderContacts[region_id]" aria-invalid="false">-->
<!--                    --><?php //foreach ($regions as $item):?>
<!--                        <option value="--><?//= $item['region_id']?><!--">--><?//= $item['region_name']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?php //if (isset($cities)): ?>
<!--    <div class="form-group field-ordercontacts-city_id required has-success">-->
<!--        <label class="control-label" for="ordercontacts-city_id">Город     </label>-->
<!--        <select  id="ordercontacts-city_id" class="form-control" name="OrderContacts[city_id]" aria-invalid="false">-->
<!--                --><?php //foreach ($cities as $item):?>
<!--                        <option value="--><?//= $item['city_id']?><!--">--><?//= $item['city_name']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?php //if (isset($areasNp)): ?>
<!--            <div class="form-group field-np-areas-ref required has-success">-->
<!--                <label class="control-label" for="np_areas-ref">Область </label>-->
<!--                <select id="np_areas-ref" class="form-control" name="NpAreas[ref]" aria-invalid="false">-->
<!--                    --><?php //foreach ($areasNp as $item):?>
<!--                        <option value="--><?//= $item['ref']?><!--">--><?//= $item['description']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?php //if (isset($citiesNp)): ?>
<!--            <div class="form-group field-np-areas-ref required has-success">-->
<!--                <label class="control-label" for="np_areas-ref">Місто </label>-->
<!--                <select id="np_areas-ref" class="form-control" name="NpAreas[ref]" aria-invalid="false">-->
<!--                    --><?php //foreach ($citiesNp as $item):?>
<!--                        <option value="--><?//= $item['ref']?><!--">--><?//= $item['description']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>
<!---->
<!--        --><?php //if (isset($warehousesNp)): ?>
<!--            <div class="form-group field-np-areas-ref required has-success">-->
<!--                <label class="control-label" for="np_areas-ref">Відділення </label>-->
<!--                <select id="np_areas-ref" class="form-control" name="NpAreas[ref]" aria-invalid="false">-->
<!--                    --><?php //foreach ($warehousesNp as $item):?>
<!--                        <option value="--><?//= $item['ref']?><!--">--><?//= $item['description']?><!--</option>-->
<!--                    --><?php //endforeach;?>
<!--                </select>-->
<!--            </div><br>-->
<!--        --><?php //endif; ?>




        <?= $form->field($order, 'email') ?>
        <?= $form->field($order, 'phone') ?>
        <?= $form->field($order, 'address')->textInput(['maxlength' => true]) ?>
        <?= $form->field($order, 'additional_info')->textarea(['rows' => 6]) ?>

        <?= Html::submitButton('Заказать', ['class' => 'btn btn-success'] ) ?>
        <?php $form = ActiveForm::end() ?>

    <?php else: ?>
        <h3>Корзина пуста</h3>
    <?php endif; ?>


    <script>     $('.del-item').on('click', function() {
            var id = $(this).data('id');
            console.log(id);
            $.ajax({
                url: '/cart/del-item',
                data: {id: id},
                type: 'GET',
                success: function (res) {
                    if (!res) alert('Ошибка!');
                    console.log(res);
                    showCart(res);
                },
                error: function () {
                    alert('Error!');
                }
            });
        });</script>
</div>
