<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>



	<form method="GET" action="" id="search_form">
        <br> <br>   <div class="container"><header>
                    <h1>Одежда для женщин</h1>
            </header>
        <div class="col-xs-12 flex-active"><div class="row-fluid">
			<div class="span2">
				<br> <br>

				<div class="col-sm-3"><fieldset>
					<legend>Тип</legend>
					<div class="control-group">
									<?php foreach($facets['types'] as $item):?>
									<label class="checkbox"> <input type="checkbox"
							name="types[]" value="<?=$item['value'];?>"
							<?=(($request->get('types')!== null) && (in_array($item['value'],$request->get('types'))))?'checked':'';?>>
                                        <?=$item['value'].' ('.$item['count'].')'?>
									</label>
									<?php endforeach;?>
                                    <input type="button"
							name="reset_types" value="Reset" data-target="types"
							class="btn">
					</div>
				 </fieldset></div>

				<div class="col-sm-3"><fieldset>
					<legend>Стоимость</legend>
					<div class="control-group">
									<?php foreach($facets['price'] as $item):?>
									<label class="checkbox"> <input type="checkbox" name="price[]"
							value="<?=$item['value'];?>"
							<?=(($request->get('price')!== null) && (in_array($item['value'],$request->get('price'))))?'checked':'';?>>
										<?=($item['value']*200).'-'.(($item['value']+1)*200).' ('.$item['count'].')'?>
									</label>
									<?php endforeach;?>
                                    <input type="button"
							name="reset_price" value="Reset" data-target="price" class="btn">
                    </div>
                    </fieldset></div>

					<div class="col-sm-3"><fieldset>
						<legend>Размеры</legend>
						<div class="control-group">
										<?php foreach($facets['sizes'] as $item):?>
										<label class="checkbox"> <input type="checkbox"
								name="sizes[]" value="<?=$item['value'];?>"
								<?=(($request->get('sizes')!== null) && (in_array($item['value'],$request->get('sizes'))))?'checked':'';?>>
											<?=$item['value'].' ('.$item['count'].')'?>
										</label>
										<?php endforeach;?>
                                        <input type="button"
								name="reset_sizes" value="Reset" data-target="sizes"
								class="btn">
						</div>
					 </fieldset></div>
                </div>
			</div></div>
                <div class="clearfix"></div>
<br><br>
			<div class="span9">
				<div class="container">


					<div class="row">



								<input type="text" class="input-large" name="query" id="suggest"
									autocomplete="off"
									value="<?=  ($request->get('query')!== null)?htmlentities($request->get('query')):''?>">
								<input type="submit" class="btn btn-primary" id="send"
									name="send" value="Submit">
								<button type="reset" class="btn " value="Reset">Reset</button>
							</div>

                    <br>
                    <br>
					<div class="row">
						<?php if (count($docs) > 0): ?>
							<p class="lead">
							Всего найдено:<?=$total_found?>
						</p>
<!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
						<div class="span9">
						  <table class="table">
						  <tr>
						      <th>Модель</th>
						      <th>Тип</th>
						      <th>Стоимость</th>
						      <th>Цвет</th>
<!--						      <th>Бренд</th>-->
						  </tr>
						  <?php foreach ($docs as $doc): ?>
						  <tr>
						      <td><?= $doc['model']?></td>
						      <td><?= $doc['types'] ?></td>
						      <td><?= $doc['price'] ?></td>
						      <td><?= $doc['sizes'] ?></td>
						      <td><?php //echo $brands[$doc['brand_id']-1]  ?></td>
						  </tr>
						  <?php endforeach; ?>
						  </table>
						</div>
<!--						<div class="span9">--><?php //include 'template/paginator.php';?><!--</div>-->
						<?php elseif (($request->get('query') !== null) && ($request->get('query')) != ''): ?>
						<p class="lead">Nothing found!</p>
						<?php endif; ?>
					</div>
                </div>
	</form>
                </div></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>


<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

<script>
    function __highlight(s, t) {
        var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
        return s.replace(matcher, "<strong>$1</strong>");
    }
    $(document).ready(function() {
        $(':checkbox').change(function() {

            $("#search_form").trigger('submit');

        });
        $(':reset').click(function(){
            location.search ='';
        });
        $('input[name^=reset_]').click(function(){
            $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
            $("#search_form").trigger('submit');

        });
    });
</script>