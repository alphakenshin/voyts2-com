<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\OffersAll */

$this->title = $model->model;
$this->params['breadcrumbs'][] = ['label' => 'Offers Alls', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

    <div class="container">
    <div class="products-page">
        <div class="new-product">
            <div class="col-md-5 zoom-grid">
                <div class="flexslider">
                    <ul class="slides">


                        <?php if(!empty($model->gallery_1)): ?>
                            <li data-thumb="<?= Url::to("/images/165x110{$model->gallery_1}")?>">
                                <div class="thumb-image"> <?= Html::img("/images/900x600{$model->gallery_1}", ['data-imagezoom' => "true", 'alt' => $model->model, 'class' => 'img-responsive']) ?></div>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($model->gallery_2)): ?>
                            <li data-thumb="<?= Url::to("/images/165x110{$model->gallery_2}")?>">
                                <div class="thumb-image"> <?= Html::img("/images/900x600{$model->gallery_2}", ['data-imagezoom' => "true", 'alt' => $model->model, 'class' => 'img-responsive']) ?></div>
                            </li>
                        <?php endif; ?>
                        <?php if(!empty($model->gallery_3)): ?>
                            <li data-thumb="<?= Url::to("/images/165x110{$model->gallery_3}")?>">
                                <div class="thumb-image"> <?= Html::img("/images/900x600{$model->gallery_1}",  ['data-imagezoom' => "true", 'alt' => $model->model, 'class' => 'img-responsive']) ?></div>
                            </li>
                        <?php endif; ?>



                    </ul>
                </div>
            </div>
            <div class="col-md-7 dress-info">
                <div class="dress-name">
                    <h3><?= $model->model ?></h3>
                    <span>$<?= $model->price ?></span>
                    <div class="clearfix"></div>
                    <?= $model->description ?>
                </div>
                <?php if(!empty($model->category_name)): ?>
                    <div class="span span1">
                        <p class="left">Тип</p>
                        <p class="right"><?= $model->category_name ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->country_of_origin)): ?>
                    <div class="span span1">
                        <p class="left">FABRIC ORIGIN</p>
                        <p class="right"><?= $model->country_of_origin ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->manufacturer_warranty)): ?>
                    <div class="span span2">
                        <p class="left">MADE IN</p>
                        <p class="right"><?= $model->manufacturer_warranty ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->brand_name)): ?>
                    <div class="span span3">
                        <p class="left">Бренд</p>
                        <p class="right"><?= $model->brand_name ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>

                <?php if(!empty($model->style_name)): ?>
                    <div class="span span3">
                        <p class="left">Стиль</p>
                        <p class="right"><?= $model->style_name ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->collection_name)): ?>
                    <div class="span span3">
                        <p class="left">Колекция</p>
                        <p class="right"><?= $model->collection_name ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->color_name)): ?>
                    <div class="span span3">
                        <p class="left">Цвет</p>
                        <p class="right"><?= $model->color_name ?></p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->sizes_1)): ?>


                    <div class="span span4">
                        <p class="left">Размер</p>
                        <p class="right"><span class="selection-box">
                                <select class="domains valid" id="orderitems-size" name="domains">
										   <option><?= $model->sizes_1 ?></option>

                                <?php if(!empty($model->sizes_2)): ?>
                                    <option><?= $model->sizes_2 ?></option>
                                <?php endif; ?>
                                    <?php if(!empty($model->sizes_3)): ?>
                                        <option><?= $model->sizes_3 ?></option>
                                    <?php endif; ?>
                                    <?php if(!empty($model->sizes_4)): ?>
                                        <option><?= $model->sizes_4 ?></option>
                                    <?php endif; ?>
                                    <?php if(!empty($model->sizes_5)): ?>
                                        <option><?= $model->sizes_5 ?></option>
                                    <?php endif; ?>
									   </select>
                            </span>
                        </p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(($model->product_type_name) == '1' ): ?>
                    <div class="span span5">
                        <p class="left"></p>
                        <p class="right">NEW</p>
                        <div class="clearfix"></div>
                    </div>
                <?php endif; ?>
                <?php if(!empty($model->sale) =='1' ): ?>
                    <div class="span span6">
                        <p class="left">SALE</p>
                        <p class="right">-10%</p>
                        <div class="clearfix"></div>
                    </div>

                <?php endif; ?>
                <div class="purchase">
                    <!--                    <label>Количество:</label>-->
                    <input type="text" value="1" id="qty" />
                    <a href="<?= Url::to(['cart/add', 'id' => $model->id]) ?>" data-id="<?= $model->id ?>" class="item_add">Купить сейчас</a>
                    <div class="social-icons">
                        <ul>
                            <li><a class="facebook1" href="#"></a></li>
                            <li><a class="twitter1" href="#"></a></li>
                            <li><a class="googleplus1" href="#"></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <script src="/js/imagezoom.js"></script>
                <!-- FlexSlider -->
                <script defer="" src="/js/jquery.flexslider.js"></script>
                <script>
                    // Can also be used with $(document).ready()
                    $(window).load(function() {
                        $('.flexslider').flexslider({
                            animation: "slide",
                            controlNav: "thumbnails"
                        });
                    });
                </script>
            </div>
            <div class="clearfix"></div>
            <div class="reviews-tabs">
                <!-- Main component for a primary marketing message or call to action -->
                <ul class="nav nav-tabs responsive hidden-xs hidden-sm" id="myTab">
                    <li class="test-class active"><a class="deco-none misc-class" href="#how-to"> Опис товару</a></li>
                    <li class="test-class"><a href="#features">Specifications</a></li>
                    <li class="test-class"><a class="deco-none" href="#source">Reviews (7)</a></li>
                </ul>

                <div class="tab-content responsive hidden-xs hidden-sm">
                    <div class="tab-pane active" id="how-to">
                        <p class="tab-text"><?= $model->description ?></p>
                    </div>
                    <div class="tab-pane" id="features">
                        <p class="tab-text"><?= $model->description ?></p>

                    </div>
                    <div class="tab-pane" id="source">
                        <div class="response">
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MARCH 21, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MARCH 26, 2054</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>MAY 25, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>FEB 13, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>JAN 28, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>APR 18, 2015</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="media response-info">
                                <div class="media-left response-text-left">
                                    <a href="#">
                                        <img class="media-object" src="/images/icon1.png" alt="">
                                    </a>
                                    <h5><a href="#">Username</a></h5>
                                </div>
                                <div class="media-body response-text-right">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit,There are many variations of passages of Lorem Ipsum available,
                                        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                    <ul>
                                        <li>DEC 25, 2014</li>
                                        <li><a href="single.html">Reply</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                </div><div class="panel-group responsive visible-xs visible-sm" id="collapse-myTab"><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle deco-none misc-class" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-how-to"> More Information</a></h4></div><div id="collapse-how-to" class="panel-collapse collapse in" style="height: auto;"></div></div><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-features">Specifications</a></h4></div><div id="collapse-features" class="panel-collapse collapse"></div></div><div class="panel panel-default test-class"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle deco-none" data-toggle="collapse" data-parent="#collapse-myTab" href="#collapse-source">Reviews (7)</a></h4></div><div id="collapse-source" class="panel-collapse collapse"></div></div></div>
            </div>
            <div class="span span7">
                <p class="left">Категория:</p>
                <p class="right"><a  href="<?= \yii\helpers\Url::to(['category/view', 'id'=> $model->category_name])?>"><?= $model->category_name ?></a></p>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
    </div>

    <div class="other-products products-grid">
        <div class="container">
            <?php if(!empty($hits)): ?>
            <header>
                <h3 class="like text-center">Популярные товары</h3>
            </header>
            <?php foreach ($hits as $hit): ?>

                <div class="col-md-4 product simpleCart_shelfItem text-center">
                    <a href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $hit->id])?>"><?= Html::img("/images/450x300{$hit->main_img}", ['alt' => $hit->model]) ?>
                    </a>
                    <div class="mask">
                        <a href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $hit->id])?>">Quick View</a>
                    </div>
                    <a class="product_model" href="<?= \yii\helpers\Url::to(['product/view', 'id'=> $hit->id])?>"><?= $hit->model ?></a>
                    <p><a class="item_add" href="#"><i></i> <span class="item_price">$<?= $hit->price ?></span></a></p>
                </div>
            <?php endforeach; ?>
            <div class="clearfix"></div> <?php endif; ?>
        </div>

    </div>
        <script src="/js/responsive-tabs.js"></script>
        <script type="text/javascript">
            $( '#myTab a' ).click( function ( e ) {
                e.preventDefault();
                $( this ).tab( 'show' );
            } );

            $( '#moreTabs a' ).click( function ( e ) {
                e.preventDefault();
                $( this ).tab( 'show' );
            } );

            ( function( $ ) {
                // Test for making sure event are maintained
                $( '.js-alert-test' ).click( function () {
                    alert( 'Button Clicked: Event was maintained' );
                } );
                fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
            } )( jQuery );

        </script>