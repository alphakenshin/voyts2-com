<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аксессуары для мужчин';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-maccessories-index">

    <form method="GET" action="" id="search_form">
        <br> <br>
        <div class="container"><header>
                <h1><?= Html::encode($this->title) ?></h1>
            </header>
            <?php $this->params['breadcrumbs'][] = $this->title; ?>

            <div class="col-xs-12 flex-active">
                <div class="row-fluid">
                    <div class="span2">
                        <br> <br>


                        <?php if (isset($facets['category_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Категории</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['category_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="category_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['category_id']) && (in_array($item['value'],$_GET['category_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другая'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_category_id" value="Reset" data-target="category_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['price'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Стоимость</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['price'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox" name="price[]"
                                                                            value="<?=$item['value'];?>"
                                                    <?=(($request->get('price')!== null) && (in_array($item['value'],$request->get('price'))))?'checked':'';?>>
                                                <?=($item['value']*200).'-'.(($item['value']+1)*200).' грн ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_price" value="Reset" data-target="price" class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sort_color_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Базовые цвета</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sort_color_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sort_color_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sort_color_id']) && (in_array($item['value'],$_GET['sort_color_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sort_color_id" value="Reset" data-target="sort_color_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['color_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Цвет</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['color_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="color_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['color_id']) && (in_array($item['value'],$_GET['color_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_color_id" value="Reset" data-target="color_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['collection_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Коллекция</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['collection_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="collection_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['collection_id']) && (in_array($item['value'],$_GET['collection_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Нет' .' ('.$item['count'].')';} else { echo $item['name']  .' ('.$item['count'].')'; } ?>

                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_collection_id" value="Reset" data-target="collection_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['product_length_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Длина</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['product_length_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="product_length_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['product_length_id']) && (in_array($item['value'],$_GET['product_length_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другая'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_product_length_id" value="Reset" data-target="product_length_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['brand_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Бренд</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['brand_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="brand_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['brand_id']) && (in_array($item['value'],$_GET['brand_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другой'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_brand_id" value="Reset" data-target="brand_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['style_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Стиль</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['style_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="style_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['style_id']) && (in_array($item['value'],$_GET['style_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { } else { echo $item['name']  .' ('.$item['count'].')'; } ?>
                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_style_id" value="Reset" data-target="style_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sizes_1_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Размер 1</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sizes_1_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sizes_1_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sizes_1_id']) && (in_array($item['value'],$_GET['sizes_1_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sizes_1_id" value="Reset" data-target="sizes_1_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sizes_2_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Размер 2</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sizes_2_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sizes_2_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sizes_2_id']) && (in_array($item['value'],$_GET['sizes_2_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sizes_2_id" value="Reset" data-target="sizes_2_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sizes_3_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Размер 3</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sizes_3_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sizes_3_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sizes_3_id']) && (in_array($item['value'],$_GET['sizes_3_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sizes_3_id" value="Reset" data-target="sizes_3_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sizes_4_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Размер 4</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sizes_4_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sizes_4_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sizes_4_id']) && (in_array($item['value'],$_GET['sizes_4_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sizes_4_id" value="Reset" data-target="sizes_4_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                        <?php if (isset($facets['sizes_5_id'])): ?>
                            <div class="col-sm-3 filters"><fieldset>
                                    <legend>Размер 5</legend>
                                    <div class="control-group">
                                        <?php foreach($facets['sizes_5_id'] as $item):?>
                                            <label class="checkbox"> <input type="checkbox"
                                                                            name="sizes_5_id[]" value="<?=$item['value'];?>"
                                                    <?=(isset($_GET['sizes_5_id']) && (in_array($item['value'],$_GET['sizes_5_id'])))?'checked':'';?>>
                                                <?php if($item['name']=== '') { echo 'Другие'; } else { echo $item['name']; } ?>
                                                <?php echo' ('.$item['count'].')'?>                                            </label>
                                        <?php endforeach;?>
                                        <input type="button"
                                               name="reset_sizes_5_id" value="Reset" data-target="sizes_5_id"
                                               class="btn">
                                    </div>
                                </fieldset></div>
                        <?php endif; ?>

                    </div>
                </div></div>
            <div class="clearfix"></div>
            <br><br>

            <div class="container">


                <div class="row">



                    <input type="text" class="input-large" name="query" id="suggest"
                           autocomplete="off"
                           value="<?=  ($request->get('query')!== null)?htmlentities($request->get('query')):''?>">
                    <input type="submit" class="btn btn-primary" id="send"
                           name="send" value="Submit">
                    <button type="reset" class="btn " value="Reset">Reset</button>
                </div>
                <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">
                    <div class="cbp-vm-options">
                        <a href="#" class="cbp-vm-icon cbp-vm-grid cbp-vm-selected" data-view="cbp-vm-view-grid" title="grid">Grid View</a>
                        <a href="#" class="cbp-vm-icon cbp-vm-list" data-view="cbp-vm-view-list" title="list">List View</a>
                    </div>

                    <div class="clearfix"></div>

                    <ul>
                        <br>
                        <br>
                        <div class="row">
                            <?php if (count($docs) > 0): ?>
                            <p class="lead">
                                Всего найдено:<?=$total_found?>
                            </p>

                            <div class="span9"><?php if ($total_found >= 1000)  $pageCount = ceil(1000/$offset);
                                else  $pageCount = ceil($total_found/$offset);
                                $_GET['query'] = $query;
                                ?>
                                <?php if ($pageCount): ?>
                                    <?php
                                    $start = 0;
                                    if($current >=2) {
                                        $previous = $current -1;
                                    }
                                    if($current+1<=$pageCount) {
                                        $next = $current+1;
                                    }
                                    $range = 5;
                                    $first = 0;
                                    $last = $pageCount;
                                    ?>
                                    <div class="paginationControl">
                                        <!-- First page link -->
                                        <?php if (isset($previous)): ?>
                                            <a href="/men-accessories?<?php $_GET['start'] = $first*$offset; echo http_build_query($_GET);?>">
                                                <i class="icon-step-backward"></i></a> |
                                        <?php else: ?>
                                            <i class="icon-step-backward disabled"></i></a> |
                                        <?php endif; ?>
                                        <!-- Previous page link -->
                                        <?php if (isset($previous)): ?>
                                            <a href="/men-accessories?<?php $_GET['start'] = ($previous-1)*$offset; echo http_build_query($_GET);?>">
                                                <i class="icon-arrow-left"></i></a> |
                                        <?php else: ?>
                                            <span class="disabled"><i class="icon-arrow-left"></i></span> |
                                        <?php endif; ?>

                                        <!-- Numbered page links -->
                                        <?php for($page = ($current-$range);$page < ($current+$range+1);$page++): ?>
                                            <?php if ($page > 0 && $page <=$pageCount): ?>
                                                <?php if ($page != $current): ?>
                                                    <a href="/men-accessories?<?php $_GET['start'] = ($page-1)*$offset; echo http_build_query($_GET);?>">
                                                        <?php echo $page; ?>
                                                    </a> |
                                                <?php else: ?>
                                                    <?php echo $page; ?> |
                                                <?php endif; ?>
                                            <?php endif;?>
                                        <?php endfor; ?>

                                        <!-- Next page link -->
                                        <?php if (isset($next)): ?>
                                            <a href="/men-accessories?<?php $_GET['start'] = ($next-1)*$offset; echo http_build_query($_GET);?>">
                                                <i class="icon-arrow-right"></i></a>|
                                        <?php else: ?>
                                            <span class="disabled"><i class="icon-arrow-right"></i></span> |
                                        <?php endif; ?>

                                        <!-- Last page link -->
                                        <?php if (isset($next)): ?>
                                            <a href="/men-accessories?<?php $_GET['start'] = ($last-1)*$offset; echo http_build_query($_GET);?>">
                                                <i class="icon-step-forward"></i>
                                            </a>
                                        <?php else: ?>
                                            <i class="icon-step-forward disabled"></i>
                                        <?php endif; ?>
                                    </div>
                                <?php endif; ?>

                            </div>
                            <?php $i = 0; foreach ($docs as $doc): ?>
                            <li>
                                <a class="cbp-vm-image" href="<?= Url::to(['product/view', 'id'=> $doc['id']])?>">
                                    <div class="simpleCart_shelfItem">
                                        <div class="view view-first">
                                            <div class="inner_content clearfix">
                                                <div class="product_image">

                                                    <?= Html::img("/images/450x300{$doc['gallery_1']}", ['alt' => $doc['model']]) ?>
                                                    <div class="mask">
                                                        <div class="info">Стиль: <?= $doc['product_type_name'] ?></div>
                                                    </div>
                                                    <div class="product_container">
                                                        <div class="cart-left">
                                                            <p class="title"><?= $doc['model'] ?></p>
                                                        </div>
                                                        <div class="pricey"><span class="item_price"><?= $doc['price'] ?> грн. </span></div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </a>
                                <div class="cbp-vm-details">

                                </div>
                                <a class="cbp-vm-icon cbp-vm-add item_add" data-id="<?= $doc['id'] ?>" href="#">Add to cart</a>
                        </div>
                        <?php $i++ ?>
                        <?php if ($i % 4 == 0): ?>
                            <div class="clearfix"></div>

                        <?php endif; ?>
                        <?php endforeach; ?>



                </div>
                <div class="span9"><?php if ($total_found >= 1000)  $pageCount = ceil(1000/$offset);
                    else  $pageCount = ceil($total_found/$offset);
                    $_GET['query'] = $query;
                    ?>
                    <?php if ($pageCount): ?>
                        <?php
                        $start = 0;
                        if($current >=2) {
                            $previous = $current -1;
                        }
                        if($current+1<=$pageCount) {
                            $next = $current+1;
                        }
                        $range = 5;
                        $first = 0;
                        $last = $pageCount;
                        ?>
                        <div class="paginationControl">
                            <!-- First page link -->
                            <?php if (isset($previous)): ?>
                                <a href="/men-accessories?<?php $_GET['start'] = $first*$offset; echo http_build_query($_GET);?>">
                                    <i class="icon-step-backward"></i></a> |
                            <?php else: ?>
                                <i class="icon-step-backward disabled"></i></a> |
                            <?php endif; ?>
                            <!-- Previous page link -->
                            <?php if (isset($previous)): ?>
                                <a href="/men-accessories?<?php $_GET['start'] = ($previous-1)*$offset; echo http_build_query($_GET);?>">
                                    <i class="icon-arrow-left"></i></a> |
                            <?php else: ?>
                                <span class="disabled"><i class="icon-arrow-left"></i></span> |
                            <?php endif; ?>

                            <!-- Numbered page links -->
                            <?php for($page = ($current-$range);$page < ($current+$range+1);$page++): ?>
                                <?php if ($page > 0 && $page <=$pageCount): ?>
                                    <?php if ($page != $current): ?>
                                        <a href="/men-accessories?<?php $_GET['start'] = ($page-1)*$offset; echo http_build_query($_GET);?>">
                                            <?php echo $page; ?>
                                        </a> |
                                    <?php else: ?>
                                        <?php echo $page; ?> |
                                    <?php endif; ?>
                                <?php endif;?>
                            <?php endfor; ?>

                            <!-- Next page link -->
                            <?php if (isset($next)): ?>
                                <a href="/men-accessories?<?php $_GET['start'] = ($next-1)*$offset; echo http_build_query($_GET);?>">
                                    <i class="icon-arrow-right"></i></a>|
                            <?php else: ?>
                                <span class="disabled"><i class="icon-arrow-right"></i></span> |
                            <?php endif; ?>

                            <!-- Last page link -->
                            <?php if (isset($next)): ?>
                                <a href="/men-accessories?<?php $_GET['start'] = ($last-1)*$offset; echo http_build_query($_GET);?>">
                                    <i class="icon-step-forward"></i>
                                </a>
                            <?php else: ?>
                                <i class="icon-step-forward disabled"></i>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?></div>

                <?php elseif (isset($_GET['query']) && $_GET['query'] != ''): ?>
                    <h2>Таких товаров нет в наличии... </h2>
                <?php endif; ?>
            </div>
        </div>
    </form>

</div>


<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
    <!--<script src="/js/bootstrap-3.1.1.min.js"></script>-->
    <script src="/js/cbpViewModeSwitch.js" type="text/javascript"></script>


    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>

    <script>
        function __highlight(s, t) {
            console.log('tester1');
            var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(t)+")", "ig" );
            return s.replace(matcher, "<strong>$1</strong>");
        }
        $(document).ready(function() {
            $(':checkbox').change(function() {
                console.log('tester2');
                $("#search_form").trigger('submit');

            });
            $(':reset').click(function(){
                location.search ='';
                console.log('tester3');
            });
            $('input[name^=reset_]').click(function(){
                $('input[name^='+$(this).attr('data-target')+']').removeAttr('checked');
                $("#search_form").trigger('submit');
                console.log('tester4');

            });
        });
    </script>



</div>
