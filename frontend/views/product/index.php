<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Offers All', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'sku',
//            'size',
            'param_id',
            'available',
            'url:url',
            //'price',
            //'old_price',
            //'currency_id',
            //'category_name',
            //'category_id',
            //'source',
            //'main_img',
            //'gallery_1',
            //'gallery_2',
            //'gallery_3',
            //'gallery_4',
            //'gallery_5',
            //'delivery',
            //'store',
            //'model',
            //'vendor',
            //'vendor_code',
            //'description:ntext',
            //'manufacturer_warranty',
            //'country_of_origin',
            //'barcode',
            //'product_type_name',
            //'product_type_id',
            //'sales_notes',
            //'composition_name',
            //'composition_id',
            //'collection_name',
            //'collection_id',
            //'silhouette_name',
            //'silhouette_id',
            //'cloth_name',
            //'cloth_id',
            //'color_name',
            //'color_id',
            //'print_name',
            //'print_id',
            //'style_name',
            //'style_id',
            //'season_name',
            //'season_id',
            //'for_full',
            //'brand_name',
            //'brand_id',
            //'product_length_name',
            //'product_length_id',
            //'sleeve_name',
            //'sleeve_id',
            //'cutout_collar_name',
            //'cutout_collar_id',
            //'additional_details_name',
            //'additional_details_id',
            //'sizes_1',
            //'sizes_1_id',
            //'sizes_2',
            //'sizes_2_id',
            //'sizes_3',
            //'sizes_3_id',
            //'sizes_4',
            //'sizes_4_id',
            //'sizes_5',
            //'sizes_5_id',
            //'sizes_6',
            //'sizes_6_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
