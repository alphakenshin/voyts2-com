<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'yii2images' => [
            'class' => 'rico\yii2images\Module',
            //be sure, that permissions ok
            //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
            'imagesStorePath' => 'images/store', //path to origin images
            'imagesCachePath' => 'images/cache', //path to resized copies
            'graphicsLibrary' => 'GD', //but really its better to use 'Imagick'
            'placeHolderPath' => '@webroot/images/placeHolder.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
            'imageCompressionQuality' => 100, // Optional. Default value is 85.
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCsrfValidation' => false
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
//                '<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
//                'category/<id:\d+>/page/<page:\d>' => 'category/view',
//                'category/<id:\d+>' => 'category/view',
//                'women/clothes/<id:\d+>' => 'offers-w-clothes/view',
//                'women/shoes/<id:\d+>' => 'offers-w-shoes/view',
//                'women/accessories/<id:\d+>' => 'offers-w-accessories/view',
//                'men/clothes/<id:\d+>' => 'offers-m-clothes/view',
//                'men/shoes/<id:\d+>' => 'offers-m-shoes/view',
//                'men/accessories/<id:\d+>' => 'offers-m-accessories/view',
//                'child/clothes/<id:\d+>' => 'offers-ch-clothes/view',
//                'child/shoes/<id:\d+>' => 'offers-ch-shoes/view',
//                'child/accessories/<id:\d+>' => 'offers-ch-accessories/view',
//                'women/clothes/' => 'offers-w-clothes/sphinx',
//                'women/shoes/' => 'offers-w-shoes/sphinx',
//                'women/accessories/' => 'offers-w-accessories/sphinx',
//                'men/clothes/' => 'offers-m-clothes/sphinx',
//                'men/shoes/' => 'offers-m-shoes/sphinx',
//                'men/accessories/' => 'offers-m-accessories/sphinx',
//                'child/clothes/' => 'offers-ch-clothes/sphinx',
//                'child/shoes/' => 'offers-ch-shoes/sphinx',
//                'child/accessories/' => 'offers-ch-accessories/sphinx',
                'product/view/<id:\d+>' => 'product/view',
                '/women-clothes' => 'product/women-clothes',
                '/women-shoes' => 'product/women-shoes',
                '/women-accessories' => 'product/women-accessories',
                '/men-clothes' => 'product/men-clothes',
                '/men-shoes' => 'product/men-shoes',
                '/men-accessories' => 'product/men-accessories',
                '/children-clothes' => 'product/children-clothes',
                '/children-shoes' => 'product/children-shoes',
                '/children-accessories' => 'product/children-accessories'


            ],
        ],

    ],
    'controllerMap' => [
        'elfinder' => [
            'class' => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root' => [
//                'baseUrl'=>'/web',
                'path' => 'upload/global',
                'name' => 'Global'
            ],

        ]
    ],
    'params' => $params,
];
