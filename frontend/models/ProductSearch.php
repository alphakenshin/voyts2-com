<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Product;

/**
 * ProductSearch represents the model behind the search form of `frontend\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */

    public $min_price;
    public $max_price;

    public function rules()
    {
        return [
            [['id', 'min_price', 'max_price', 'category_id', 'barcode', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL'], 'integer'],
            [['types', 'available', 'currency_id', 'source', 'main_img', 'gallery_1', 'gallery_2', 'gallery_3', 'gallery_4', 'gallery_5', 'brand', 'model', 'vendor_code', 'delivery', 'description', 'country_of_origin', 'sales_notes', 'composition', 'sizes_eu_1', 'sizes_eu_2', 'sizes_eu_3', 'sizes_eu_4', 'sizes_eu_5', 'sizes_eu_6', 'sizes_eu_7', 'sizes_eu_8', 'sizes_eu_9', 'sizes_eu_10', 'cloth', 'color', 'style', 'season', 'product_length', 'for_full', 'silhouette', 'print', 'cutout_collar', 'additional_details', 'sleeve'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['size'] = ['asc'];
//        [
//            'asc' => ['first_name' => SORT_ASC, 'last_name' => SORT_ASC],
//            'desc' => ['first_name' => SORT_DESC, 'last_name' => SORT_DESC],
//            'default' => SORT_DESC,
//            'label' => 'Name',
//        ];


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'category_id' => $this->category_id,
            'barcode' => $this->barcode,
            'S' => $this->S,
            'M' => $this->M,
            'L' => $this->L,
            'XL' => $this->XL,
            'XXL' => $this->XXL,
            'XXXL' => $this->XXXL,
        ]);

        $query->andFilterWhere([
                            'types' => $this->types,
                            'sizes_eu_1' => $this->sizes_eu_1,
                            'color' => $this->color,
                            'style' => $this->style,
            ])
            ->andFilterWhere(['like', 'available', $this->available])
            ->andFilterWhere(['like', 'currency_id', $this->currency_id])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'main_img', $this->main_img])
            ->andFilterWhere(['like', 'gallery_1', $this->gallery_1])
            ->andFilterWhere(['like', 'gallery_2', $this->gallery_2])
            ->andFilterWhere(['like', 'gallery_3', $this->gallery_3])
            ->andFilterWhere(['like', 'gallery_4', $this->gallery_4])
            ->andFilterWhere(['like', 'gallery_5', $this->gallery_5])
            ->andFilterWhere(['like', 'brand', $this->brand])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'delivery', $this->delivery])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'country_of_origin', $this->country_of_origin])
            ->andFilterWhere(['like', 'sales_notes', $this->sales_notes])
            ->andFilterWhere(['like', 'composition', $this->composition])
            ->andFilterWhere(['like', 'sizes_eu_1', $this->sizes_eu_1])
            ->andFilterWhere(['like', 'sizes_eu_2', $this->sizes_eu_2])
            ->andFilterWhere(['like', 'sizes_eu_3', $this->sizes_eu_3])
            ->andFilterWhere(['like', 'sizes_eu_4', $this->sizes_eu_4])
            ->andFilterWhere(['like', 'sizes_eu_5', $this->sizes_eu_5])
            ->andFilterWhere(['like', 'sizes_eu_6', $this->sizes_eu_6])
            ->andFilterWhere(['like', 'sizes_eu_7', $this->sizes_eu_7])
            ->andFilterWhere(['like', 'sizes_eu_8', $this->sizes_eu_8])
            ->andFilterWhere(['like', 'sizes_eu_9', $this->sizes_eu_9])
            ->andFilterWhere(['like', 'sizes_eu_10', $this->sizes_eu_10])
            ->andFilterWhere(['like', 'cloth', $this->cloth])
            ->andFilterWhere(['like', 'season', $this->season])
            ->andFilterWhere(['like', 'product_length', $this->product_length])
            ->andFilterWhere(['like', 'for_full', $this->for_full])
            ->andFilterWhere(['like', 'silhouette', $this->silhouette])
            ->andFilterWhere(['like', 'print', $this->print])
            ->andFilterWhere(['like', 'cutout_collar', $this->cutout_collar])
            ->andFilterWhere(['like', 'additional_details', $this->additional_details])
            ->andFilterWhere(['like', 'sleeve', $this->sleeve])
        ->andFilterWhere(['and',
                    ['>=','price', $this->min_price],
                    ['<=','price', $this->max_price],
    ]);

        return $dataProvider;
    }
}
