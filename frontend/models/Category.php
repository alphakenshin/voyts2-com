<?php

namespace frontend\models;


use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $name
 * @property int|null $garda_id
 * @property int|null $glem_id
 * @property int|null $issaplus_id
 * @property int|null $karree_id
 * @property int|null $olla_id
 * @property string $source
 * @property int|null $sort
 * @property int|null $status
 * @property string|null $content
 * @property string|null $keywords
 * @property string|null $description
 * @property int|null $created_by
 * @property string|null $created_at
 * @property int|null $updated_by
 * @property string|null $updated_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];
    }

    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
//    public function rules()
//    {
//        return [
//            [['parent_id', 'garda_id', 'glem_id', 'issaplus_id', 'karree_id', 'olla_id', 'sort', 'status', 'created_by', 'updated_by'], 'integer'],
//            [['source'], 'required'],
//            [['content', 'description'], 'string'],
//            [['created_at', 'updated_at'], 'safe'],
//            [['name'], 'string', 'max' => 64],
//            [['source'], 'string', 'max' => 32],
//            [['keywords'], 'string', 'max' => 255],
//        ];
//    }

    /**
     * {@inheritdoc}
     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'parent_id' => 'Parent ID',
//            'name' => 'Name',
//            'garda_id' => 'Garda ID',
//            'glem_id' => 'Glem ID',
//            'issaplus_id' => 'Issaplus ID',
//            'karree_id' => 'Karree ID',
//            'olla_id' => 'Olla ID',
//            'source' => 'Source',
//            'sort' => 'Sort',
//            'status' => 'Status',
//            'content' => 'Content',
//            'keywords' => 'Keywords',
//            'description' => 'Description',
//            'created_by' => 'Created By',
//            'created_at' => 'Created At',
//            'updated_by' => 'Updated By',
//            'updated_at' => 'Updated At',
//        ];
//    }

//    public function getProducts() {
//
//        return $this->hasMany(Product::className(), ['catalog_id' => 'id']);
//    }



}
