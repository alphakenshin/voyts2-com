<?php

namespace frontend\controllers;

use common\models\ProductSearch;
use Yii;
use common\models\Product;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use PDO;

/**
 * OffersAllController implements the CRUD actions for OffersAll model.
 */
class ProductController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OffersAll models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => $product = Product::find()->where(['available'=> 'true'])->limit(12)->all()
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionWomenClothes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_w_clothes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('women-clothes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionWomenShoes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_w_shoes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('women-shoes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionWomenAccessories()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;

            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_w_accessories';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();

        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('women-accessories', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }

    public function actionMenClothes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_m_clothes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('men-clothes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionMenShoes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_m_shoes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();



        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('men-shoes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionMenAccessories()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_m_accessories';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('men-accessories', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }

    public function actionChildrenClothes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_ch_clothes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('children-clothes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionChildrenShoes()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_ch_shoes';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('children-shoes', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }
    public function actionChildrenAccessories()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 20;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cat = array();
        $where_brand = array();
        $where_price = array();
        $where_color = array();
        $where_sort_color = array();
        $where_product_type = array();
        $where_sizes = array();
        $where_collection = array();
        $where_silhouette = array();
        $where_composition = array();
        $where_additional_details = array();
        $where_cutout_collar = array();
        $where_sleeve = array();
        $where_product_length = array();
        $where_season = array();
        $where_style = array();
        $where_print = array();
        $where_cloth = array();

        if (isset($_GET['category_id'])) {
            $w = implode(',', $_GET['category_id']);
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

        if (isset($_GET['brand_id'])) {
            $w = implode(',', $_GET['brand_id']);
            $where['brand_id'] = ' brand_id in (' . $w . ') ';
        }

        if (isset($_GET['price'])) {
            $w = array();
            foreach ($_GET['price'] as $c) {
                $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
            }
            $w = implode(' OR ', $w);
            $select['price'] = 'IF(' . $w . ',1,0) as w_p';
            $where['price'] = 'w_p = 1';
        }

        if (isset($_GET['color_id'])) {
            $w = implode(',', $_GET['color_id']);
            $where['color_id'] = ' color_id in (' . $w . ') ';
        }

        if (isset($_GET['sort_color_id'])) {
            $w = implode(',', $_GET['sort_color_id']);
            $where['sort_color_id'] = ' sort_color_id in (' . $w . ') ';
        }

        if (isset($_GET['product_type_id'])) {
            $w = implode(',', $_GET['product_type_id']);
            $where['product_type_id'] = ' product_type_id in (' . $w . ') ';
        }

        if (isset($_GET['sizes_1'])) {
            $w = implode(',', $_GET['sizes_1']);
            $where['sizes_1'] = ' sizes_1 in (' . $w . ') ';
        }

        if (isset($_GET['cloth_id'])) {
            $w = implode(',', $_GET['cloth_id']);
            $where['cloth_id'] = ' cloth_id in (' . $w . ') ';
        }

        if (isset($_GET['collection_id'])) {
            $w = implode(',', $_GET['collection_id']);
            $where['collection_id'] = ' collection_id in (' . $w . ') ';
        }

        if (isset($_GET['composition_id'])) {
            $w = implode(',', $_GET['composition_id']);
            $where['composition_id'] = ' composition_id in (' . $w . ') ';
        }

        if (isset($_GET['additional_details_id'])) {
            $w = implode(',', $_GET['additional_details_id']);
            $where['additional_details_id'] = ' additional_details_id in (' . $w . ') ';
        }

        if (isset($_GET['cutout_collar_id'])) {
            $w = implode(',', $_GET['cutout_collar_id']);
            $where['cutout_collar_id'] = ' cutout_collar_id in (' . $w . ') ';
        }

        if (isset($_GET['sleeve_id'])) {
            $w = implode(',', $_GET['sleeve_id']);
            $where['sleeve_id'] = ' sleeve_id in (' . $w . ') ';
        }

        if (isset($_GET['product_length_id'])) {
            $w = implode(',', $_GET['product_length_id']);
            $where['product_length_id'] = ' product_length_id in (' . $w . ') ';
        }

        if (isset($_GET['season_id'])) {
            $w = implode(',', $_GET['season_id']);
            $where['season_id'] = ' season_id in (' . $w . ') ';
        }

        if (isset($_GET['style_id'])) {
            $w = implode(',', $_GET['style_id']);
            $where['style_id'] = ' style_id in (' . $w . ') ';
        }

        if (isset($_GET['print_id'])) {
            $w = implode(',', $_GET['print_id']);
            $where['print_id'] = ' print_id in (' . $w . ') ';
        }

        if (isset($_GET['silhouette_id'])) {
            $w = implode(',', $_GET['silhouette_id']);
            $where['silhouette_id'] = ' silhouette_id in (' . $w . ') ';
        }




        if (count($where) > 0) {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where;
            if (isset($where_cat['category_id'])) {
                unset($where_cat['category_id']);
            }
            if (count($where_cat) > 0) {
                $where_cat = ' AND ' . implode(' AND ', $where_cat);
            } else {
                $where_cat = '';
            }

            if (isset($where_brand['brand_id'])) {
                unset($where_brand['brand_id']);
            }
            if (count($where_brand) > 0) {
                $where_brand = ' AND ' . implode(' AND ', $where_brand);
            } else {
                $where_brand = '';
            }

            if (isset($where_price['price'])) {
                unset($where_price['price']);
            }
            if (count($where_price) > 0) {
                $where_price = ' AND ' . implode(' AND ', $where_price);
            } else {
                $where_price = '';
            }

            if (isset($where_color['color_id'])) {
                unset($where_color['color_id']);
            }
            if (count($where_color) > 0) {
                $where_color = ' AND ' . implode(' AND ', $where_color);
            } else {
                $where_color = '';
            }

            if (isset($where_sort_color['sort_color_id'])) {
                unset($where_sort_color['sort_color_id']);
            }
            if (count($where_sort_color) > 0) {
                $where_sort_color = ' AND ' . implode(' AND ', $where_sort_color);
            } else {
                $where_sort_color = '';
            }

            if (isset($where_product_type['product_type_id'])) {
                unset($where_product_type['product_type_id']);
            }
            if (count($where_product_type) > 0) {
                $where_product_type = ' AND ' . implode(' AND ', $where_product_type);
            } else {
                $where_product_type = '';
            }

            if (isset($where_sizes['sizes_1_id'])) {
                unset($where_sizes['sizes_1_id']);
            }
            if (count($where_sizes) > 0) {
                $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
            } else {
                $where_sizes = '';
            }

            if (isset($where_cloth['cloth_id'])) {
                unset($where_cloth['cloth_id']);
            }
            if (count($where_cloth) > 0) {
                $where_cloth = ' AND ' . implode(' AND ', $where_cloth);
            } else {
                $where_cloth = '';
            }

            if (isset($where_collection['collection_id'])) {
                unset($where_collection['collection_id']);
            }
            if (count($where_collection) > 0) {
                $where_collection = ' AND ' . implode(' AND ', $where_collection);
            } else {
                $where_collection = '';
            }

            if (isset($where_composition['composition_id'])) {
                unset($where_composition['composition_id']);
            }
            if (count($where_composition) > 0) {
                $where_composition = ' AND ' . implode(' AND ', $where_composition);
            } else {
                $where_composition = '';
            }

            if (isset($where_additional_details['additional_details_id'])) {
                unset($where_additional_details['additional_details_id']);
            }
            if (count($where_additional_details) > 0) {
                $where_additional_details = ' AND ' . implode(' AND ', $where_additional_details);
            } else {
                $where_additional_details = '';
            }

            if (isset($where_cutout_collar['cutout_collar_id'])) {
                unset($where_cutout_collar['cutout_collar_id']);
            }
            if (count($where_cutout_collar) > 0) {
                $where_cutout_collar = ' AND ' . implode(' AND ', $where_cutout_collar);
            } else {
                $where_cutout_collar = '';
            }

            if (isset($where_sleeve['sleeve_id'])) {
                unset($where_sleeve['sleeve_id']);
            }
            if (count($where_sleeve) > 0) {
                $where_sleeve = ' AND ' . implode(' AND ', $where_sleeve);
            } else {
                $where_sleeve = '';
            }

            if (isset($where_product_length['product_length_id'])) {
                unset($where_product_length['product_length_id']);
            }
            if (count($where_product_length) > 0) {
                $where_product_length = ' AND ' . implode(' AND ', $where_product_length);
            } else {
                $where_product_length = '';
            }

            if (isset($where_season['season_id'])) {
                unset($where_season['season_id']);
            }
            if (count($where_season) > 0) {
                $where_season = ' AND ' . implode(' AND ', $where_season);
            } else {
                $where_season = '';
            }

            if (isset($where_style['style_id'])) {
                unset($where_style['style_id']);
            }
            if (count($where_style) > 0) {
                $where_style = ' AND ' . implode(' AND ', $where_style);
            } else {
                $where_style = '';
            }

            if (isset($where_print['print_id'])) {
                unset($where_print['print_id']);
            }
            if (count($where_print) > 0) {
                $where_print = ' AND ' . implode(' AND ', $where_print);
            } else {
                $where_print = '';
            }

            if (isset($where_silhouette['silhouette_id'])) {
                unset($where_silhouette['silhouette_id']);
            }
            if (count($where_silhouette) > 0) {
                $where_silhouette = ' AND ' . implode(' AND ', $where_silhouette);
            } else {
                $where_silhouette = '';
            }

            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cat = $where_brand = $where_price = $where_color = $where_sort_color = $where_product_type = $where_sizes = $where_cloth =
            $where_collection = $where_composition = $where_additional_details = $where_cutout_collar = $where_sleeve =
            $where_product_length = $where_season = $where_style = $where_print = $where_silhouette = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_offers_ch_accessories';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM 
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color_id  ORDER BY color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sort_color  GROUP BY sort_color_id  ORDER BY sort_color_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sort_color_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand  GROUP BY brand_id  ORDER BY brand_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $brand_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cat  GROUP BY category_id  ORDER BY category_id  ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $category_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_type  GROUP BY product_type_id ORDER BY product_type_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_type_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes_1_id ORDER BY sizes_1_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes_1_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cloth  GROUP BY cloth_id ORDER BY cloth_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cloth_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_collection  GROUP BY collection_id ORDER BY collection_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $collection_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_composition  GROUP BY composition_id ORDER BY composition_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $composition_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_additional_details  GROUP BY additional_details_id ORDER BY additional_details_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $additional_details_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cutout_collar  GROUP BY cutout_collar_id ORDER BY cutout_collar_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $cutout_collar_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sleeve  GROUP BY sleeve_id ORDER BY sleeve_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sleeve_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_product_length  GROUP BY product_length_id ORDER BY product_length_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $product_length_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_season  GROUP BY season_id ORDER BY season_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $season_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_style  GROUP BY style_id ORDER BY style_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $style_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_print  GROUP BY print_id ORDER BY print_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $print_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_silhouette  GROUP BY silhouette_id ORDER BY silhouette_id ASC  LIMIT 0,10");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $silhouette_id = $stmt->fetchAll();

        $facets = array();
        foreach ($category_id as $p) {
            $facets['category_id'][] = array(
                'value' => $p['category_id'],
                'count' => $p['cnt'],
                'name' => $p[2]
            );
        }
        foreach ($brand_id as $p) {
            $facets['brand_id'][] = array(
                'value' => $p['brand_id'],
                'count' => $p['cnt'],
                'name' => $p[31],
            );
        }
        foreach ($color_id as $p) {
            $facets['color_id'][] = array(
                'value' => $p['color_id'],
                'count' => $p['cnt'],
                'name' => $p[20]
            );
        }
        foreach ($sort_color_id as $p) {
            $facets['sort_color_id'][] = array(
                'value' => $p['sort_color_id'],
                'count' => $p['cnt'],
                'name' => $p[22]
            );
        }
        foreach ($prices as $p) {
            $facets['price'][] = array(
                'value' => $p['price_seg'],
                'count' => $p['cnt']
            );
        }
        foreach ($collection_id as $p) {
            $facets['collection_id'][] = array(
                'value' => $p['collection_id'],
                'count' => $p['cnt'],
                'name' => $p[14]
            );
        }
        foreach ($silhouette_id as $p) {
            $facets['silhouette_id'][] = array(
                'value' => $p['silhouette_id'],
                'count' => $p['cnt'],
                'name' => $p[16]
            );
        }

        foreach ($cloth_id as $p) {
            $facets['cloth_id'][] = array(
                'value' => $p['cloth_id'],
                'count' => $p['cnt'],
                'name' => $p[18]
            );
        }
        foreach ($product_type_id as $p) {
            $facets['product_type_id'][] = array(
                'value' => $p['product_type_id'],
                'count' => $p['cnt'],
                'name' => $p[9]
            );
        }
        foreach ($composition_id as $p) {
            $facets['composition_id'][] = array(
                'value' => $p['composition_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }
        foreach ($additional_details_id as $p) {
            $facets['additional_details_id'][] = array(
                'value' => $p['additional_details_id'],
                'count' => $p['cnt'],
                'name' => $p[39]
            );
        }
        foreach ($cutout_collar_id as $p) {
            $facets['cutout_collar_id'][] = array(
                'value' => $p['cutout_collar_id'],
                'count' => $p['cnt'],
                'name' => $p[37]
            );
        }
        foreach ($sleeve_id as $p) {
            $facets['sleeve_id'][] = array(
                'value' => $p['sleeve_id'],
                'count' => $p['cnt'],
                'name' => $p[35]
            );
        }
        foreach ($product_length_id as $p) {
            $facets['product_length_id'][] = array(
                'value' => $p['product_length_id'],
                'count' => $p['cnt'],
                'name' => $p[33]
            );
        }
        foreach ($season_id as $p) {
            $facets['season_id'][] = array(
                'value' => $p['season_id'],
                'count' => $p['cnt'],
                'name' => $p[28]
            );
        }
        foreach ($style_id as $p) {
            $facets['style_id'][] = array(
                'value' => $p['style_id'],
                'count' => $p['cnt'],
                'name' => $p[26]
            );
        }
        foreach ($print_id as $p) {
            $facets['print_id'][] = array(
                'value' => $p['print_id'],
                'count' => $p['cnt'],
                'name' => $p[24]
            );
        }

        return $this->render('children-accessories', compact( 'facets', 'category_id', 'color_id', 'sort_color_id', 'brand_id', 'sizes_1_id', 'collection_id',
            'silhouette_id', 'cloth_id', 'product_type_id', 'composition_id', 'additional_details_id', 'cutout_collar_id', 'sleeve_id', 'product_length_id',
            'season_id', 'style_id', 'print_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));
    }

    /**
     * Displays a single OffersAll model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $hits = Product::find()->where(['available'=> 'true'])->limit(12)->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'hits' => $hits,
        ]);
    }

    /**
     * Creates a new OffersAll model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OffersAll model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OffersAll model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OffersAll model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OffersAll the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
