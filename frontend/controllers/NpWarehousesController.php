<?php

namespace frontend\controllers;

use common\models\UkrSettlements;
use Yii;
use common\models\NpWarehouses;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Json;

/**
 * NpWarehousesController implements the CRUD actions for NpWarehouses model.
 */
class NpWarehousesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NpWarehouses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NpWarehouses::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NpWarehouses model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NpWarehouses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NpWarehouses();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NpWarehouses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NpWarehouses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NpWarehouses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NpWarehouses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NpWarehouses::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionCity()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null)
            {
                $cat_id = $parents[0];
                $catList = NpWarehouses::find()->where('area_id = :id', [':id' => $cat_id])->limit(50)->all();
                $out = ArrayHelper::map($catList, 'city_id', 'city_description');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['city_id' => $key, 'city_description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['city_id'],
                        'name' => $p['city_description'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionWarehouse() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            $subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                $subcatList = NpWarehouses::find()->where('city_id = :id', [':id' => $subcat_id])->limit(50)->all();
                $out = ArrayHelper::map($subcatList, 'id', 'description');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['city_id' => $key, 'city_description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['city_id'],
                        'name' => $p['city_description'],

                    );
                }

                $areasN = asort($facets);


                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

//    public function actionRegion()
//    {
//        $out = [];
//        if (isset($_POST['depdrop_parents']))
//        {
//            $parents = $_POST['depdrop_parents'];
//            if ($parents != null)
//            {
//                $cat_id = $parents[0];
//                $catList = UkrSettlements::find()->where('area_id = :id', [':id' => $cat_id])->limit(50)->all();
//                $out = ArrayHelper::map($catList, 'region_id', 'region');
//                $result = [];
//                $tmp_arr = [];
//                foreach($out as $key => $value)
//                {
//                    $tmp_arr = ['region_id' => $key, 'region' => $value];
//                    $result[] = $tmp_arr;
//                }
//                $areasN = asort($result);
//                $facets = array();
//                foreach ($result as $p) {
//                    $facets[] = array(
//                        'id' => $p['region_id'],
//                        'name' => $p['region'],
//
//                    );
//                }
//
//                $areasN = asort($facets);
//                echo Json::encode(['output' => $facets, 'selected' => '']);
//                return;
//            }
//        }
//        echo Json::encode(['output' => '', 'selected' => '']);
//    }
//
//
//    public function actionCities() {
//        $out = [];
//        if (isset($_POST['depdrop_parents'])) {
//            $ids = $_POST['depdrop_parents'];
//            $cat_id = empty($ids[0]) ? null : $ids[0];
//            $subcat_id = empty($ids[1]) ? null : $ids[1];
//            if ($cat_id != null) {
//                $subcatList = UkrSettlements::find()->where('region_id = :id', [':id' => $subcat_id])->limit(50)->all();
//                $out = ArrayHelper::map($subcatList, 'id', 'description');
//                $result = [];
//                $tmp_arr = [];
//                foreach($out as $key => $value)
//                {
//                    $tmp_arr = ['city_id' => $key, 'city_description' => $value];
//                    $result[] = $tmp_arr;
//                }
//                $areasN = asort($result);
//                $facets = array();
//                foreach ($result as $p) {
//                    $facets[] = array(
//                        'id' => $p['city_id'],
//                        'name' => $p['city_description'],
//
//                    );
//                }
//
//                $areasN = asort($facets);
//
//
//                echo Json::encode(['output' => $facets, 'selected' => '']);
//                return;
//            }
//        }
//        echo Json::encode(['output' => '', 'selected' => '']);
//    }
}
