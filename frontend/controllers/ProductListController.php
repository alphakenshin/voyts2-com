<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Product;
use frontend\models\ProductSearch;
use yii\data\Pagination;
use yii\sphinx\ActiveDataProvider;
use yii\sphinx\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use PDO;
/**
 * ProductListController implements the CRUD actions for Product model.
 */
class ProductListController extends FrontendController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

//        UPDATE `product` SET `country_of_origin` = '' WHERE `country_of_origin` IS NULL

        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//            'items' => $items,
//            'facets' => $facets,
//            'pages' => $pages,
//            'facets' => $facets,
//            'searchResult' => $searchResult,

        ]);
    }
    public function actionSphinx()
    {
//        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );
        /***
        $query = new Query();
        $query->from('test1')
        ->showMeta(true)
        ->facets([
        'type',
        'brand',
        'country_of_origin',
        'sales_notes',
        'composition',
        'sizes_eu_1',
        'cloth',
        'color',
        'style',
        'season',
        'product_length',
        'for_full',
        'silhouette',
        'print',
        'cutout_collar',
        'additional_details',
        'sleeve',
        ])
        ->search(); // retrieve all rows and facets
        $provider = new ActiveDataProvider([
        'query' => $query,
        'pagination' => [
        'pageSize' => 100,
        ]
        ]);
        $models = $provider->getModels();
        $facets = $provider->getFacets();
        $typeFacet = $provider->getFacet('type');

        foreach ($typeFacet as $frame) {
        $type = $frame['value'];
        $count = $frame['count'];

        echo $type . ' (' . $count . ')<br>';
        }***/
//        $query = Product::find();
//        $countQuery = clone $query;
//        $pages = new Pagination(['totalCount' =>$countQuery->count(), 'pageSize' => 100,
//            'forcePageParam' => false, 'pageSizeParam' => false]);

//        $query_search = new Query();
//        $searchResult = $query_search->from('sphinx_index_products')->match('красное')->all();
//        $query = new Query();
//        $results = $query->from('sphinx_index_products')
//            ->match('')
//            ->facets([
//                'type',
//                'brand',
//                'price',
////                'price' => [
////                    'select' => 'INTERVAL(200,400,600,800) AS price', // using function
////                    'order' => ['FACET()' => SORT_ASC],
////                ],
//                'country_of_origin',
//                'sales_notes',
//                'composition',
//                'sizes_eu_1',
//                'cloth',
//                'color',
//                'style',
//                'season',
//                'product_length',
//                'for_full',
//                'silhouette',
//                'print',
//                'cutout_collar',
//                'additional_details',
//                'sleeve',
//            ])
//            ->search(); // retrieve all rows and facets

//        $items = $results['hits'];
//        $facets = $results['facets'];
//        $ln_sph =  new PDO( 'mysql:host=127.0.0.1;port=9306' );
//

$ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

$request = Yii::$app->request;
$brands = array(
    'One',
    'Two',
    'Three',
    'Four',
    'Five',
    'Six',
    'Seven',
    'Eight',
    'Nine',
    'Ten'
);
$docs = array();
$start = 0;
$offset = 50;
$current = 1;
$request = Yii::$app->request;
if (($request->get('start'))!== null) {
    $start = $request->get('start');
    $current = $start / $offset + 1;
}

$search_query = $query = trim($request->get('query'));
$attrs = array(
    'types',
    'brand',
    'price',
    'category_id',
    'color'
);

$select = array();
$where = array();

$where_cat = array();
$where_typ = array();
$where_brand = array();
$where_price = array();
$where_color = array();

if (($request->get('types'))!== null) {
    $w = implode(',', $request->get('types'));
    $where['types'] = ' types in (' . $w . ') ';
}
        if (($request->get('sizes'))!== null) {
            $w = implode(',', $request->get('sizes'));
            $where['sizes'] = ' sizes in (' . $w . ') ';
        }

        if (($request->get('category_id'))!== null) {
            $w = implode(',', $request->get('category_id'));
            $where['category_id'] = ' category_id in (' . $w . ') ';
        }

if (($request->get('brand'))!== null) {
    $w = implode(',', $request->get('brand'));
    $where['brand'] = ' brand in (' . $w . ') ';
}

if (($request->get('price'))!== null) {
    $w = array();
    foreach ($request->get('price') as $c) {
        $w[] = ' (price >= ' . ($c * 200) . ' AND price <= ' . (($c + 1) * 200 - 1) . ') ';
    }
    $w = implode(' OR ', $w);
    $select['price'] = 'IF(' . $w . ',1,0) as w_p';

    $where['price'] = 'w_p = 1';
}

if (($request->get('color'))!== null) {

    $search_query .= ' @color ' . implode('|', $request->get('color'));
}

if (count($where) > 0) {
    $where_cat = $where_sizes = $where_typ = $where_brand = $where_price = $where_color = $where;
    if (isset($where_sizes['sizes'])) {
        unset($where_sizes['sizes']);
    }
    if (count($where_sizes) > 0) {
        $where_sizes = ' AND ' . implode(' AND ', $where_sizes);
    } else {
        $where_sizes = '';
    }

        if (isset($where_typ['types'])) {
            unset($where_typ['types']);
        }
        if (count($where_typ) > 0) {
            $where_typ = ' AND ' . implode(' AND ', $where_typ);
        } else {
            $where_typ = '';
        }

    if (isset($where_cat['category_id'])) {
        unset($where_cat['category_id']);
    }
    if (count($where_cat) > 0) {
        $where_cat = ' AND ' . implode(' AND ', $where_cat);
    } else {
        $where_cat = '';
    }

    if (isset($where_brand['brand'])) {
        unset($where_brand['brand']);
    }
    if (count($where_brand) > 0) {
        $where_brand = ' AND ' . implode(' AND ', $where_brand);
    } else {
        $where_brand = '';
    }

    if (isset($where_price['price'])) {
        unset($where_price['price']);
    }
    if (count($where_price) > 0) {
        $where_price = ' AND ' . implode(' AND ', $where_price);
    } else {
        $where_price = '';
    }

    $where_color = ' AND ' . implode(' AND ', $where_color);
    $where = ' AND ' . implode(' AND ', $where);
} else {
        $where_cat = $where_sizes = $where_typ = $where_brand = $where_price = $where_color = $where = '';
}
if (count($select) > 0) {
    $select = ',' . implode(',', $select);
} else {
    $select = '';
}
$indexes = 'sphinx_index_products';

$stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$docs = $stmt->fetchAll();
//        '<pre></pre>' . print_r($docs) . '<pre></pre>';
$meta = $ln_sph->query("SHOW META")->fetchAll();
foreach ($meta as $m) {
    $meta_map[$m['Variable_name']] = $m['Value'];
}
$total_found = $meta_map['total_found'];
$total = $meta_map['total'];


$ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

$sql = array();
$rows = array();
//$sql[] = "SELECT *$select,GROUPBY() as selected,COUNT(DISTINCT sizes) as cnt FROM $indexes WHERE MATCH(:match) $where_typ GROUP BY types   LIMIT 0,10";
////        '<pre>' . print_r($sql) . '</pre>';
//
//$sql[] = "SELECT *$select,GROUPBY() as selected,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_brand GROUP BY brand ORDER BY brand ASC LIMIT 0,10";
////        '<pre>' . print_r($sql) . '</pre>';

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_typ  GROUP BY types ORDER BY types ASC  LIMIT 0,1000");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $types = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_sizes  GROUP BY sizes ORDER BY sizes ASC  LIMIT 0,1000");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $sizes = $stmt->fetchAll();
// expressions are not yet supported in multi-query optimization,so we run them separate

$stmt = $ln_sph->prepare("SELECT *,GROUPBY() as selected,COUNT(*) as cnt,INTERVAL(price,200,400,600, 800, 1000, 1400, 2000) as price_seg FROM
$indexes WHERE MATCH(:match)   $where_price  GROUP BY price_seg ORDER BY price_seg ASC  LIMIT 0,10");
$stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
$stmt->execute();
$prices = $stmt->fetchAll();

// string attrs are not yet supported in multi-query optimization, so we run them separate
$stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_color  GROUP BY color ORDER BY color ASC  LIMIT 0,10");
$stmt->bindValue(':match', $query, PDO::PARAM_STR);
$stmt->execute();
$color = $stmt->fetchAll();
//        '<pre>' . print_r($color) . '</pre>';

$facets = array();
foreach ($color as $p) {
    $facets['color'][] = array(
        'value' => $p['color'],
        'count' => $p['cnt']
    );
}
    foreach ($types as $p) {
        $facets['types'][] = array(
            'value' => $p['types'],
            'count' => $p['cnt']
        );
    }
        foreach ($sizes as $p) {
            $facets['sizes'][] = array(
                'value' => $p['sizes'],
                'count' => $p['cnt']
            );
        }

foreach ($prices as $p) {
    $facets['price'][] = array(
        'value' => $p['price_seg'],
        'count' => $p['cnt']
    );
}
foreach ($rows as $k => $v) {
    foreach ($v as $x) {
        $facets[$k][] = array(
            'value' => $x['selected'],
            'count' => $x['cnt']
        );
    }
}



        return $this->render('sphinx',compact( 'prices', 'types', 'facets', 'request', 'docs', 'total_found', 'color', 'sizes'));


//            'pages' => $pages,
//            'facets' => $facets,
//            'searchResult' => $searchResult,


    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
