<?php

namespace frontend\controllers;

use common\models\NpAreas;
use common\models\DeliveryType;
use common\models\NpWarehouses;

use common\models\Order;
use common\models\OrderItems;
use common\models\Product;
use common\models\Cart;

use common\models\UkrSettlements;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use PDO;

use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



class CartController extends FrontendController
{
    public function actionAdd() {
        $id = Yii::$app->request->get('id');
        $size = Yii::$app->request->get('size');
        $qty = (int)Yii::$app->request->get('qty');
        $qty =!$qty ? 1 : $qty;
        $product = Product::findOne($id);
        if (empty($product)) return false;
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->addToCart($product, $size, $qty);
//        debug($session['cart']);
//        debug($session['cart.qty']);
//        debug($session['cart.sum']);
        if(!Yii::$app->request->isAjax) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        return $this->render('cart-modal', compact("session"));
    }

    public function actionClear() {
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');
        $this->layout = false;
        return $this->render('cart-modal', compact("session"));
    }

    public function actionDelItem() {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $this->layout = false;
        return $this->render('cart-modal', compact("session"));
    }

    public function actionView() {
//        '<pre>' . print_r(Yii::$app->params['adminEmail']) .  '</pre>';
        $session = Yii::$app->session;
        $session->open();
        $this->setMeta('Козина');

        $order = new Order();

        if ($order->load(Yii::$app->request->post()))  {

            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            if ($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш заказ принят. Наш Менджер вскоре с Вами свяжется');
                Yii::$app->mailer->compose('order', ['session' =>$session])
                    ->setFrom(['test@gmail.com'=>'voyts.com'])
                    ->setTo($order->email)
                    ->setSubject('Заказ')
////                ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
                    ->send();
                Yii::$app->mailer->compose('order', ['session' =>$session])
                    ->setFrom(['test@gmail.com'=>'voyts.com'])
                    ->setTo(Yii::$app->params['adminEmail'])
                    ->setSubject('Заказ')
////                ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
                    ->send();
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка оформления заказа.');
            }


        }
        return $this->render('view', compact('session', 'order'

        ));

    }

    public function actionShow() {
        $session = Yii::$app->session;
        $session->open();
        $this->layout = false;
        return $this->render('cart-modal', compact("session"));
    }

    protected function saveOrderItems($items, $order_id) {


        foreach ($items as $id =>$item) {
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->size = $item['size'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty']*$item['price'];
            $order_items->save();
        }
    }



    public function actionDelivery()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null)
            {
                $del_id = $parents[0];
                $deliveryList = DeliveryType::find()->where('id = :id', [':id' => $del_id])->limit(50)->all();
                $out = ArrayHelper::map($deliveryList, 'id', 'delivery_type');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['id' => $key, 'delivery_type' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['id'],
                        'name' => $p['delivery_type'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }




    public function actionArea()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {
            $parents = $_POST['depdrop_parents'];
            if ($parents[0] == 1)
            {
                $del_id = $parents[0];

//                '<pre>' . print_r($del_id)  . '</pre>';

                $delList = NpWarehouses::find()->where('delivery_id = :id', [':id' => $del_id])->limit(50)->all();
                $out = ArrayHelper::map($delList, 'area_id', 'settlement_area_description');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['area_id' => $key, 'settlement_area_description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['area_id'],
                        'name' => $p['settlement_area_description'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionCity()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {


//                $del_id = $parents[0];

                $ids = $_POST['depdrop_parents'];
                $del_id = empty($ids[0]) ? null : $ids[0];
                $area_id = empty($ids[1]) ? null : $ids[1];
                if ($del_id == 1) {
                    $areaList = NpWarehouses::find()->where('area_id = :id', [':id' => $area_id])->limit(50)->all();
                $out = ArrayHelper::map($areaList, 'city_id', 'city_description');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['city_id' => $key, 'city_description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['city_id'],
                        'name' => $p['city_description'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionWarehouse() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $del_id = empty($ids[0]) ? null : $ids[0];
            $area_id = empty($ids[1]) ? null : $ids[1];
            $subcat_id = empty($ids[2]) ? null : $ids[2];
            if ($del_id == 1) {
                $subcatList = NpWarehouses::find()->where('city_id = :id', [':id' => $subcat_id])->limit(50)->all();
                $out = ArrayHelper::map($subcatList, 'id', 'description');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['city_id' => $key, 'city_description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['city_id'],
                        'name' => $p['city_description'],

                    );
                }

                $areasN = asort($facets);


                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }




    public function actionAreaTwo()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {
            $parents = $_POST['depdrop_parents'];
            if ($parents[0]  == 3 || $parents[0] == 4)
            {
                $del_id = $parents[0];

//                '<pre>' . print_r($del_id)  . '</pre>';

                $del2List = NpAreas::find()->all();
                $out = ArrayHelper::map($del2List, 'areas_id', 'description');
//                                '<pre>' . print_r($del2List)  . '</pre>';

                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['areas_id' => $key, 'description' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['areas_id'],
                        'name' => $p['description'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionRegionTwo()
    {
        $out = [];
        if (isset($_POST['depdrop_parents']))
        {


//                $del_id = $parents[0];

            $ids = $_POST['depdrop_parents'];
            $del_id = empty($ids[0]) ? null : $ids[0];
            $area_two_id = empty($ids[1]) ? null : $ids[1];
            if ($del_id == 3 || $del_id == 4)
            {
                $area2List = UkrSettlements::find()->where('area_id = :id', [':id' => $area_two_id])->all();
                $out = ArrayHelper::map($area2List, 'region_id', 'region');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['region_id' => $key, 'region' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['region_id'],
                        'name' => $p['region'],

                    );
                }

                $areasN = asort($facets);
                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionCityTwo() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $del_id = empty($ids[0]) ? null : $ids[0];
            $area_two_id = empty($ids[1]) ? null : $ids[1];
            $region_two_id = empty($ids[2]) ? null : $ids[2];
            if ($del_id == 3 || $del_id == 4) {
                $city2List = UkrSettlements::find()->where('region_id = :id', [':id' => $region_two_id])->all();
                $out = ArrayHelper::map($city2List, 'id', 'city');
                $result = [];
                $tmp_arr = [];
                foreach($out as $key => $value)
                {
                    $tmp_arr = ['id' => $key, 'city' => $value];
                    $result[] = $tmp_arr;
                }
                $areasN = asort($result);
                $facets = array();
                foreach ($result as $p) {
                    $facets[] = array(
                        'id' => $p['id'],
                        'name' => $p['city'],

                    );
                }

                $areasN = asort($facets);


                echo Json::encode(['output' => $facets, 'selected' => '']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionSphinx()
    {
        $ln_sph = new PDO( 'mysql:host=127.0.0.1;port=9306' );

        $request = Yii::$app->request;
        $docs = array();
        $start = 0;
        $offset = 200;
        $current = 1;
        if (isset($_GET['start'])) {
            $start = $_GET['start'];
            $current = $start / $offset + 1;
        }

        $search_query = $query = trim($request->get('query'));

        $select = array();
        $where = array();
        $where_cities = array();
        $where_areas = array();
        $where_regions = array();


        if (isset($_GET['area_id'])) {
            $w = implode(',', $_GET['area_id']);
            $where['area_id'] = ' area_id in (' . $w . ') ';
        }

        if (isset($_GET['regions_id'])) {
            $w = implode(',', $_GET['regions_id']);
            $where['regions_id'] = ' regions_id in (' . $w . ') ';
        }

        if (isset($_GET['city_id'])) {
            $w = implode(',', $_GET['city_id']);
            $where['city_id'] = ' city_id in (' . $w . ') ';
        }

        if (count($where) > 0) {
            $where_cities = $where_areas = $where_regions = $where;

            if (isset($where_areas['area_id'])) {
                unset($where_areas['area_id']);
            }
            if (count($where_areas) > 0) {
                $where_areas = ' AND ' . implode(' AND ', $where_areas);
            } else {
                $where_areas = '';
            }

            if (isset($where_regions['regions_id'])) {
                unset($where_regions['regions_id']);
            }
            if (count($where_regions) > 0) {
                $where_regions = ' AND ' . implode(' AND ', $where_regions);
            } else {
                $where_regions = '';
            }

            if (isset($where_cities['city_id'])) {
                unset($where_cities['city_id']);
            }
            if (count($where_cities) > 0) {
                $where_cities = ' AND ' . implode(' AND ', $where_cities);
            } else {
                $where_cities = '';
            }


            $where = ' AND ' . implode(' AND ', $where);

        } else {
            $where_cities = $where_areas = $where_regions = $where = '';
        }
        if (count($select) > 0) {
            $select = ',' . implode(',', $select);
        } else {
            $select = '';
        }
        $indexes = 'sphinx_index_np_warehouses';

        $stmt = $ln_sph->prepare("SELECT *$select FROM $indexes WHERE MATCH(:match) $where  LIMIT $start,$offset ");
        $stmt->bindValue(':match', $search_query, PDO::PARAM_STR);
        $stmt->execute();
        $docs = $stmt->fetchAll();
        $meta = $ln_sph->query("SHOW META")->fetchAll();
        foreach ($meta as $m) {
            $meta_map[$m['Variable_name']] = $m['Value'];
        }
        $total_found = $meta_map['total_found'];
        $total = $meta_map['total'];

        $ln_sph->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_regions  GROUP BY regions_id  ORDER BY regions_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $regions_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_areas  GROUP BY area_id  ORDER BY area_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $area_id = $stmt->fetchAll();

        $stmt = $ln_sph->prepare("SELECT *$select,COUNT(*) as cnt FROM $indexes WHERE MATCH(:match) $where_cities  GROUP BY city_id  ORDER BY city_id  ASC  LIMIT 0,50");
        $stmt->bindValue(':match', $query, PDO::PARAM_STR);
        $stmt->execute();
        $city_id = $stmt->fetchAll();

//'<pre>' . print_r($city_id) . '</pre>';
        echo($regions_id[0][0]) . ' --  0 <br></pre>';
        echo($regions_id[0][1]) . ' -- 1 <br></pre>';
        echo($regions_id[0][2]) . ' -- 2 <br></pre>';
        echo($regions_id[0][3]) . ' -- 3 <br></pre>';
        echo($regions_id[0][4]) . ' -- 4 <br></pre>';
        echo($regions_id[0][5]) . ' -- 5 <br></pre>';
        echo($regions_id[0][6]) . ' -- 6 <br></pre>';
        echo($regions_id[0][7]) . ' -- 7 <br></pre>';
        echo($regions_id[0][8]) . ' -- 8 <br></pre>';
        echo($regions_id[0][9]) . ' -- 9 <br></pre>';
        echo($regions_id[0][10]) . ' -- 10 <br></pre>';
        echo($regions_id[0][11]) . ' -- 11 <br></pre>';
        echo($regions_id[0][12]) . ' -- 12 <br></pre>';
        echo($regions_id[0][13]) . ' -- 13 <br></pre>';
        echo($regions_id[0][14]) . ' -- 14 <br></pre>';
//        echo($regions_id[0][15]) . ' -- 15 <br></pre>';
//        echo($regions_id[0][16]) . ' -- 16 <br></pre>';
//        echo($regions_id[0][17]) . ' -- 17 <br></pre>';
//        echo($regions_id[0][18]) . ' -- 18 <br></pre>';
//        echo($regions_id[0][19]) . ' -- 19 <br></pre>';
//        echo($regions_id[0][20]) . ' -- 20 <br></pre>';
//        echo($regions_id[0][21]) . ' -- 21 <br></pre>';
//        echo($regions_id[0][22]) . ' -- 22 <br></pre>';
//        echo($regions_id[0][23]) . ' -- 23 <br></pre>';
//        echo($regions_id[0][24]) . ' -- 24 <br></pre>';
//        echo($regions_id[0][25]) . ' -- 25 <br></pre>';
//        echo($regions_id[0][26]) . ' -- 26 <br></pre>';
//        echo($regions_id[0][27]) . ' -- 27 <br></pre>';
//        echo($regions_id[0][28]) . ' -- 28 <br></pre>';
//        echo($regions_id[0][29]) . ' -- 29 <br></pre>';
//        echo($regions_id[0][30]) . ' -- 30 <br></pre>';
//        echo($regions_id[0][31]) . ' -- 31 <br></pre>';
//        echo($regions_id[0][32]) . ' -- 32 <br></pre>';
//        echo($regions_id[0][33]) . ' -- 33 <br></pre>';
//        echo($regions_id[0][34]) . ' -- 34 <br></pre>';
//        echo($regions_id[0][35]) . ' -- 35 <br></pre>';
//        echo($regions_id[0][36]) . ' -- 36 <br></pre>';


        $facets = array();
        foreach ($city_id as $p) {
            $facets['city_id'][] = array(
                'value' => $p['city_id'],
                'count' => $p['cnt'],
                'name' => $p[8]
            );
        }
        foreach ($area_id as $p) {
            $facets['area_id'][] = array(
                'value' => $p['area_id'],
                'count' => $p['cnt'],
                'name' => $p[10],
            );
        }
        foreach ($regions_id as $p) {
            $facets['regions_id'][] = array(
                'value' => $p['regions_id'],
                'count' => $p['cnt'],
                'name' => $p[12]
            );
        }

        return $this->render('sphinx', compact( 'facets', 'city_id', 'area_id', 'regions_id', 'request', 'docs', 'total_found', 'offset', 'current', 'query', 'start'));

    }




}