<?php

namespace frontend\controllers;

use common\models\Product;
use Yii;
use frontend\models\Category;
use yii\data\ActiveDataProvider;
use yii\sphinx\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends FrontendController
{
    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex() {
        $hits = Product::find()->limit(100)->all();
//        debug($hits);
//        $this->setMeta('E_SHOP');

        return $this->render('index', compact('hits'));

    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $id = Yii::$app->request->get('id');
        $category = Category::findOne($id);
        if (empty($category)) throw new \yii\web\HttpException(404, 'Такой категории - нет.');
        $query = Product::find()->where(['category_id' => $id]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' =>$countQuery->count(), 'pageSize' => 100,
            'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        $query = new Query();
        $results = $query->from('test1')
            ->facets([
                'type',
                'source',
                'cloth',
                'style',
                'season',
            ])
//            ->showMeta(true)
            ->search(); // retrieve all rows and facets

        $items = $results['hits'];
        $facets = $results['facets'];
//        '<pre>' . print_r($items) . '</pre>';
//       '<pre>' . print_r($facets) . '</pre>';
//        foreach ($results['facets']['source'] as $frame) {
//            $type = $frame['value'];
//            $count = $frame['count'];
//            echo $type . ' (' . $count . ')<br>';
//        }



//        $hitsProducts = Product::find()->where(['category_id' => $id])->where(['hit' => '1'])->limit(1)->all();
//        $this->setMeta('E_SHOP | ' . $category->name, $category->keywords, $category->description);
//                debug($hitsProduct);
        return $this->render('view',compact( 'pages', 'products','category', 'items', 'facets'));
    }



}
