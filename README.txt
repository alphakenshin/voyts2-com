30/11/2019


заменив simplexml_import_dom  {$node = simplexml_import_dom($doc->importNode($reader->expand(), true));}

на  simplexml_load_string

{$xml = simplexml_load_string($reader->readOuterXml());}

получив в результате  simplexml объект вместо массива что дало возможность не использовать

$simpleXml->asXML()

{foreach($data as $key => $simpleXml) {
    if(null !== ($reader->getAttribute('id')))  $result['id'] = $reader->getAttribute('id');
    if(null !== ($reader->getAttribute('available'))) $result['available'] = $reader->getAttribute('available');
    if(null !== ($reader->getAttribute('group_id'))) $result['group_id'] = $reader→getAttribute('group_id');}
    $result[$key] = strip_tags($simpleXml→asXML());}

При использовании которого и появлялись ошибки, а

заменить его на $json = json_encode( $xml );   $xml_array = json_decode( $json, true );

{xml = $xml->attributes();
$json = json_encode( $xml );
$xml_array = json_decode( $json, true );
$result  =  $xml_array['@attributes'];}


4/12/2019

Реализация ETL принципов на сайте:

Определения: https://habr.com/ru/company/newprolab/blog/358530/ и https://habr.com/ru/post/248231/


Extract. Это шаг, на котором датчики принимают на вход данные из различных источников (логов пользователей, копии реляционной БД, внешнего набора данных и т.д.), а затем передают их дальше для последующих преобразований.
Здесь это выгрузка из xml файлов в 5 таблиц для товаров и пять для каталогов.

offers_garda    categories_garda
offers_glem     categories_glem
offers_issaplus categories_issaplus
offers_karree   categories_karree
offers_olla     categories_olla

И общую import_category куда выгружаются все словари

Transform. Это «сердце» любого ETL, этап, когда мы применяем бизнес-логику и делаем фильтрацию, группировку и агрегирование, чтобы преобразовать сырые данные в готовый к анализу датасет. Эта процедура требует понимания бизнес задач и наличия базовых знаний в области.

Создана "таблица соответствий" match_table:
CREATE TABLE `match_table` (
`id` int(11) UNSIGNED NOT NULL,
`parent_id` int(11) UNSIGNED DEFAULT NULL,
`name` varchar(64) DEFAULT NULL,
`garda_id` int(11) DEFAULT NULL,
`glem_id` int(11) DEFAULT NULL,
`issaplus_id` int(11) DEFAULT NULL,
`karree_id` int(11) DEFAULT NULL,
`olla_id` int(11) DEFAULT NULL,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

В ней прописаны соответствия разделов id каталога на сайте id разделам в выгружаемых файлах. Таким образом все товары можно пирвести к имеющемуся на сайте словарю.

Создана таблица `offers_all` в которой собраны все атрибуты и свойства всех товаров в нее будут загружены все товары с привязкой к категориям сайта. В дальнейшем эта таблица будет разбита минимум надвое по типам товаров "обувь", "одежда"
Этап Extract завершен не полностью, но достаточно чтобы модно было выгрузить "товары" в одну таблицу привязать к одному словарю и вывести на сайте в форме : каталог, разделы каталога, тизеры товаров в разделах,и страница товара.

Структура таблицы:
CREATE TABLE `offers_all` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(32) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `avalable` varchar(32) DEFAULT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `sku` varchar(128) DEFAULT NULL,
  `price_uah` decimal(6,2) DEFAULT NULL,
  `price_rub` decimal(6,2) DEFAULT NULL,
  `price_usd` decimal(5,2) DEFAULT NULL,
  `opt_price_uah` decimal(6,2) DEFAULT NULL,
  `opt_price_rub` decimal(6,2) DEFAULT NULL,
  `opt_price_usd` decimal(5,2) DEFAULT NULL,
  `link_to` varchar(255) DEFAULT NULL,
  `collection` varchar(64) DEFAULT NULL,
  `delivery` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `vendor_code` varchar(255) DEFAULT NULL,
  `country_of_origin` varchar(255) DEFAULT NULL,
  `manufacturer_warranty` tinyint(4) DEFAULT NULL,
  `barcode` varchar(64) DEFAULT NULL,
  `sales_notes` varchar(64) DEFAULT NULL,
  `main_img` varchar(255) DEFAULT NULL,
  `gallery_1` varchar(255) DEFAULT NULL,
  `gallery_2` varchar(255) DEFAULT NULL,
  `gallery_3` varchar(255) DEFAULT NULL,
  `gallery_4` varchar(255) DEFAULT NULL,
  `gallery_5` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `product_type` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `composition` varchar(255) DEFAULT NULL,
  `silhouette` varchar(255) DEFAULT NULL,
  `cloth` varchar(255) DEFAULT NULL,
  `print` varchar(255) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `for_full` varchar(255) DEFAULT NULL,
  `product_length` varchar(255) DEFAULT NULL,
  `sleeve` varchar(255) DEFAULT NULL,
  `sizes_eu` varchar(255) DEFAULT NULL,
  `style` varchar(64) NOT NULL,
  `all_quantity` smallint(8) DEFAULT NULL,
  `size_s_quantity` tinyint(4) DEFAULT NULL,
  `size_m_quantity` tinyint(4) DEFAULT NULL,
  `size_l_quantity` tinyint(4) DEFAULT NULL,
  `size_xl_quantity` tinyint(4) DEFAULT NULL,
  `size_xxl_quantity` tinyint(4) DEFAULT NULL,
  `size_xxxl_quantity` tinyint(4) DEFAULT NULL,
  `size_1` varchar(32) DEFAULT NULL,
  `size_2` varchar(32) DEFAULT NULL,
  `size_3` varchar(32) DEFAULT NULL,
  `size_4` varchar(32) DEFAULT NULL,
  `size_5` varchar(32) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` enum('0','1','2','3','4') NOT NULL,
  `updated_by` enum('0','1','2','3','4') NOT NULL,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
И посмотреть это все вживую.

Load. Наконец, мы загружаем обработанные данные и отправляем их в место конечного использования. Полученный набор данных может быть использован конечными пользователями, а может являться входным потоком к еще одному ETL.

